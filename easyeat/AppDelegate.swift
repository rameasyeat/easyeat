//
//  AppDelegate.swift
//  easyeat
//
//  Created by Ramniwas on 16/03/20.
//  Copyright © 2020 EasyEat. All rights reserved.
//

import UIKit
import GoogleSignIn
import Firebase
import GoogleMaps
import GooglePlaces
import FirebaseAuth
import FirebaseCore

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    
    var window: UIWindow?
    var coordinator: MainCoordinator?
    
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        
//        var filePath:String!
//        #if DEBUG
//            print("[FIREBASE] Development mode.")
//            filePath = Bundle.main.path(forResource: "GoogleService-Info", ofType: "plist", inDirectory: "Debug")
//        #else
//            print("[FIREBASE] Production mode.")
//            filePath = Bundle.main.path(forResource: "GoogleService-Info", ofType: "plist", inDirectory: "Release")
//        #endif
//
//        let options = FirebaseOptions.init(contentsOfFile: filePath)!
//        FirebaseApp.configure(options: options)
        

       FirebaseApp.configure()
        autoLogin()
        
        GIDSignIn.sharedInstance().clientID = "262431422959-q52pg4phnoefvfjlitjfkg003iu8aujo.apps.googleusercontent.com"
        
        GMSServices.provideAPIKey(Configuration().environment.googleApisKey);
        GMSPlacesClient.provideAPIKey(Configuration().environment.googleApisKey)
        
        if #available(iOS 13.0, *) {
            window?.overrideUserInterfaceStyle = .light
        }
        tabbarSetting()
        
       // checkUpdateVersion()
        
        return true
    }
    
    fileprivate func tabbarSetting(){
        UITabBar.appearance().unselectedItemTintColor = hexStringToUIColor("#BABABA")
        UITabBar.appearance().tintColor = hexStringToUIColor(korange)
        
        // add Shadow
        
        UITabBar.appearance().layer.shadowOffset = CGSize(width: 0, height: -3)
        UITabBar.appearance().layer.shadowRadius = 3
        UITabBar.appearance().layer.shadowColor = UIColor.black.cgColor
        UITabBar.appearance().layer.shadowOpacity = 1
        UITabBar.appearance().layer.applySketchShadow(color: .white, alpha: 1, x: 0, y: -3, blur: 10)
        
        //        UITabBar.appearance().clipsToBounds = true
        UITabBar.appearance().layer.borderWidth = 0
        UITabBar.appearance().barTintColor = .white
    }
    
    // Mark : get app version
    
    func autoLogin(){
        
        if CurrentUserInfo.randomToken == nil || CurrentUserInfo.randomToken == ""{// cart token
            CurrentUserInfo.randomToken = "".randomToken()
        }
        
        if CurrentUserInfo.isSkip != nil  && CurrentUserInfo.isSkip != ""{
            let navController = UINavigationController()
            navController.navigationBar.isHidden = true
            coordinator = MainCoordinator(navigationController: navController)
            coordinator?.goToTabbarController()
            
        }else {
            let navController = UINavigationController()
            navController.navigationBar.isHidden = true
            coordinator = MainCoordinator(navigationController: navController)
            coordinator?.goToTourView()
        }
        
        window = UIWindow(frame: UIScreen.main.bounds)
        window?.rootViewController = coordinator?.navigationController
        window?.makeKeyAndVisible()
        
        if #available(iOS 13.0, *) {
            window?.overrideUserInterfaceStyle = .light
        }
    }
    
    fileprivate func checkUpdateVersion(){
        DispatchQueue.global().async {
            do {
                let update = try AppUtility.isUpdateAvailable()
                DispatchQueue.main.async {
                    if update == true{
                        
                        AlertWithAction(title: "Update Available", message: "A new version of EasyEat is available. Please update to new version now.", ["UPDATE"], vc: (self.window?.rootViewController)!) { (index) -> (Void) in
                          if let url = URL(string: "https://apps.apple.com/us/app/easy-eat/id1511512728?ls=1") {
                                UIApplication.shared.open(url)
                            }
                        }
                    }
                }
            } catch {
            }
        }
    }
    
    
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
      //  checkUpdateVersion()

    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        
    }
    
    @available(iOS 9.0, *)
    func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any]) -> Bool {
        let googleDidHandle = GIDSignIn.sharedInstance().handle(url)
        
        return googleDidHandle
    }
    
    func application(_ application: UIApplication,open url: URL, sourceApplication: String?, annotation: Any) -> Bool {
        let googleDidHandle = GIDSignIn.sharedInstance().handle(url)
        
        return googleDidHandle
    }
    
    func application(_ application: UIApplication,
                     didReceiveRemoteNotification notification: [AnyHashable : Any],
                     fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        if Auth.auth().canHandleNotification(notification) {
            completionHandler(.noData)
            return
        }
    }
    
    
    
}


public extension UIWindow {
    var visibleViewController: UIViewController? {
        return UIWindow.getVisibleViewControllerFrom(self.rootViewController)
    }
    
    static func getVisibleViewControllerFrom(_ vc: UIViewController?) -> UIViewController? {
        if let nc = vc as? UINavigationController {
            return UIWindow.getVisibleViewControllerFrom(nc.visibleViewController)
        } else if let tc = vc as? UITabBarController {
            return UIWindow.getVisibleViewControllerFrom(tc.selectedViewController)
        } else {
            if let pvc = vc?.presentedViewController {
                return UIWindow.getVisibleViewControllerFrom(pvc)
            } else {
                return vc
            }
        }
    }
}

func getTopViewController() -> UIViewController? {
    let appDelegate = UIApplication.shared.delegate
    if let window = appDelegate!.window {
        return window?.visibleViewController
    }
    return nil
}

// extension AppDelegate {
//
//    func forceUpdate() {
//
//}
