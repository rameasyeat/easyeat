//
//  Storyboarded.swift
//  easyeat
//
//  Created by Ramniwas on 16/03/20.
//  Copyright © 2020 EasyEat. All rights reserved.
//

import Foundation
import UIKit
protocol Storyboarded {
     static func instantiate() -> Self
}

extension Storyboarded where Self : UIViewController{
    
    static func instantiate() -> Self{
        let fullName = NSStringFromClass(self)
        let screenName = fullName.components(separatedBy: ".")[1]
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        return storyboard.instantiateViewController(withIdentifier: screenName) as! Self
    }
}
