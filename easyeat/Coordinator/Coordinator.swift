//
//  Coordinator.swift
//  easyeat
//
//  Created by Ramniwas on 16/03/20.
//  Copyright © 2020 EasyEat. All rights reserved.
//

import UIKit

protocol Coordinator {
    var childCoordinators: [Coordinator] { get set }
    var navigationController: UINavigationController { get set }

    func start()
}
