//
//  MainCoordinator.swift
//  easyeat
//
//  Created by Ramniwas on 16/03/20.
//  Copyright © 2020 EasyEat. All rights reserved.
//

import Foundation
import UIKit
import CoreLocation

class MainCoordinator : Coordinator{
    func start() {
        
    }
    
    var childCoordinators = [Coordinator]()
    var navigationController: UINavigationController
    
    init(navigationController: UINavigationController) {
        self.navigationController = navigationController
    }
    
    func goTostart() {
        let vc = SigninViewController.instantiate()
        vc.coordinator = self
        navigationController.pushViewController(vc, animated: false)
    }
    func goToCountryCodeView() {
        let vc = CountryCodeViewController.instantiate()
        vc.coordinator = self
        navigationController.pushViewController(vc, animated: false)
    }
    func goToTabbarController() {
        let vc = TabbarController.instantiate()
        vc.coordinator = self
        navigationController.pushViewController(vc, animated: false)
    }
    
    func goToScanQRCode(){
        let vc = QRCodeViewController.instantiate()
        vc.coordinator = self
        navigationController.pushViewController(vc, animated: false)
    }
    
    func goToSignupView() {
        let vc = SignupViewController.instantiate()
        vc.coordinator = self
        navigationController.pushViewController(vc, animated: false)
    }
    func goToOTPView(_ dict : SigninInfoModel) {
        let vc = OTPViewController.instantiate()
        vc.coordinator = self
        vc.viewModel.dictInfo = dict
        navigationController.pushViewController(vc, animated: false)
    }
    
    func goToForgotPassword() {
        let vc = ForgotPasswordViewController.instantiate()
        vc.coordinator = self
        navigationController.pushViewController(vc, animated: false)
    }
    
    func goToPofileDetails(_ dict : UserData) {
        let vc = BasicProfileSegmentController.instantiate()
        vc.coordinator = self
        vc.userDictData = dict
        navigationController.pushViewController(vc, animated: false)
    }
    
    func goToUserMobileView() {
        let vc = UserMobileViewController.instantiate()
        vc.coordinator = self
        navigationController.pushViewController(vc, animated: false)
    }
    func goToUserNameView(_ dict : [String : Any]) {
        let vc = UserNameViewController.instantiate()
        vc.coordinator = self
        vc.viewModel.param = dict
        navigationController.pushViewController(vc, animated: false)
    }
    
    func goToTourView() {
        let vc = TourViewController.instantiate()
        vc.coordinator = self
        navigationController.pushViewController(vc, animated: false)
    }
    
    func goToMenuView(){
        let vc = MenuViewController.instantiate()
        vc.coordinator = self
        navigationController.pushViewController(vc, animated: false)
    }
    func goToCartView(_ restaurantDict : RestaurantData){
        let vc = CartViewController.instantiate()
        vc.viewModel.restaurantDict = restaurantDict

        vc.coordinator = self
        navigationController.pushViewController(vc, animated: false)
    }
    
    func goToOrderView(_ dict : RestaurantData){
        let vc = OrderViewViewController.instantiate()
        vc.coordinator = self
        vc.viewModel.restaurantDict = dict
        navigationController.pushViewController(vc, animated: false)
    }
    
    func demo(){
        let vc = DemoViewController.instantiate()
        vc.coordinator = self
        navigationController.pushViewController(vc, animated: false)
    }
    
    func goToProfileView(){
        let vc = ProfileViewController.instantiate()
        vc.coordinator = self
        navigationController.pushViewController(vc, animated: false)
    }
    
    func goToMoreView(){
        let vc = MoreViewController.instantiate()
        vc.coordinator = self
        navigationController.pushViewController(vc, animated: false)
        
    }
    
    func goToOrderHistoryView(){
        let vc = OrderHistoryViewController.instantiate()
        vc.coordinator = self
        navigationController.pushViewController(vc, animated: false)
    }
    
    func goToOrderHistoryDetailView(_ orderId : String){
        let vc = OrderHistoryDetailViewController.instantiate()
        vc.coordinator = self
        vc.viewModel.orderId = orderId
        navigationController.pushViewController(vc, animated: false)
    }
    
    func goTOWebView(_ type : WebViewType){
        let vc = WKWebViewController.instantiate()
        vc.webViewType = type
        navigationController.pushViewController(vc, animated: false)
    }
    
    func goToAddNewAddressView(_ delegate : NewAddressDelegate, _ dict : RestaurantData ){
        let vc = NewAddressViewController.instantiate()
        vc.viewModel.restaurantDict = dict
        vc.coordinator = self
        vc.viewModel.newAddressDelegate = delegate
        navigationController.pushViewController(vc, animated: false)
        
    }
    
    func goToPaymentMode(_ orderDict : OrderData, _ dict : RestaurantData ){
        let vc = PaymentModeViewController.instantiate()
        vc.viewModel.restaurantDict = dict
        vc.coordinator = self
        vc.viewModel.orderData = orderDict
        navigationController.pushViewController(vc, animated: false)
    }
    
    func goToPaymentView(_ orderID : String,_ channelName : String,_ restaurantName : String){
        let vc = PaymentViewController.instantiate()
        vc.viewModel.orderID = orderID
        vc.viewModel.restaurantName = restaurantName
        vc.viewModel.channelName = channelName
        vc.coordinator = self
        navigationController.pushViewController(vc, animated: false)
    }
    func goToPaymentStatus(_ status : PaymentStatus){
           let vc = PaymentStatusViewController.instantiate()
           vc.viewModel.paymentStatus = status
          vc.coordinator = self
           navigationController.pushViewController(vc, animated: false)
       }
    
    func goToRatingView(_ dict : OrderData){
        let vc = RatingViewController.instantiate()
        vc.viewModel.orderData = dict
        vc.coordinator = self
        navigationController.pushViewController(vc, animated: false)
    }
    
    func goToMapView(_ userLocation :CLLocationCoordinate2D,_ restLocation : Location){
        let vc = MapViewController.instantiate()
        vc.viewModel.restLocation = restLocation
        vc.viewModel.userLocation = userLocation
        vc.coordinator = self
       navigationController.pushViewController(vc, animated: false)
    }
    
    func goToLoyalityBenefits(){
        let vc = LoyaltyBenefitsViewController.instantiate()
        vc.coordinator = self
        navigationController.pushViewController(vc, animated: false)
    }
    
    func goToRewardsScreen(){
           let vc = MyRewardsViewController.instantiate()
           vc.coordinator = self
           navigationController.pushViewController(vc, animated: false)
       }
    
    func goToMenuSegment(){
           let vc = MenuSegmentController.instantiate()
           vc.coordinator = self
           navigationController.pushViewController(vc, animated: false)
       }
    
    
}
