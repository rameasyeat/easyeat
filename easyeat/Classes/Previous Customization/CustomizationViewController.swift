//
//  CustomizationViewController.swift
//  easyeat
//
//  Created by Ramniwas Patidar on 14/07/20.
//  Copyright © 2020 EasyEat. All rights reserved.
//

import UIKit

class CustomizationViewController: BaseViewController,Storyboarded {
    
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var itemTypeLabel: CustomLabel!
    @IBOutlet weak var viewBottomHeight: NSLayoutConstraint!

    
  var viewModel : CustomizationModelView = {
      let model = CustomizationModelView()
      return model
  }()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewBottomHeight.constant += CGFloat(0.getSafeAreaHeight())/2
        CustomizationCell.registerWithTable(tblView)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
    }
    
    /// MARK : Button Action
    
    @IBAction func chooseButtonAction(_ sender: Any) {
        
        let controller = VariationViewController.instantiate()
        controller.viewModel.variationDelegate = viewModel.variationDelegate
        controller.viewModel.groupArray = viewModel.groupArray
        controller.viewModel.fromViewType = .menu
        controller.viewModel.selectedGroupIndex = 0
        controller.viewModel.itemDetails = viewModel.itemDetails
        controller.viewModel.restaurantDict = viewModel.restaurantDict
        
        addRootView(controller)
    }
    
    @IBAction func repeatButtonAction(_ sender: Any) {
        
        var dict = [String :Any]()
        dict[krestaurant_id] = CurrentUserInfo.restaurantId
        dict[kcart_token] = CurrentUserInfo.randomToken
        dict[kitem_id] = viewModel.addedItem?.item_id
        dict[kquantity] = 1
        dict[ktable_id] = CurrentUserInfo.tblID
        dict[ktoken] =  CurrentUserInfo.authToken ?? kdefaultToken
        dict[kvariation_ids] = viewModel.addedItem?.variation_ids
        dict[kaddon_ids] = viewModel.addedItem?.addon_ids
        dict[kgvariations] = viewModel.itemDetails?.variation
        
        addItemInCart(APIsEndPoints.kItemInCart.rawValue,dict, handler: {[weak self](result,statusCode)in
            self?.viewModel.variationDelegate?.updateVarientItem(result)
            removeFromSuperView(self!)
        })
    }
    
    override func crossButtonAction(_ sender: Any) {
        removeFromSuperView(self)
    }
}

//// UITableViewDataSource
extension CustomizationViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell  = tableView.dequeueReusableCell(withIdentifier: CustomizationCell.reuseIdentifier, for: indexPath) as! CustomizationCell
        cell.selectionStyle = .none
        cell.commonInit(viewModel.addedItem)
        
        return cell
    }
}

extension CustomizationViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}
