//
//  AddAllergiesViewController.swift
//  easyeat
//
//  Created by Ramniwas on 25/03/20.
//  Copyright © 2020 EasyEat. All rights reserved.
//

import UIKit

protocol AddSubCatAllergiesDelegate {
    func updateAddAllergies(_ isAdded : Bool)
}

protocol SubCatBackDelegate {
    func subcatBackAction()
}
class AddSubCatAllergyController: BaseViewController,Storyboarded {
    
    @IBOutlet weak var saveButton: CustomButton!
    
    @IBOutlet weak var tblView: UITableView!
    var coordinator: MainCoordinator?
    
    var viewModel : AddAllergieViewModel = {
        let viewModel = AddAllergieViewModel()
        return viewModel }()
    
    var isContainSubCat : Bool = false{
        didSet{
            saveButton.setTitle("Save", for: .normal)
            saveButton.alpha = 1.0
            saveButton.isUserInteractionEnabled = true
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        UISetup()
    }
    @IBAction func saveButtonAction(_ sender: Any) {
        addAllergies()
    }
    
    @IBAction func backButtonAction(_ sender: Any) {
        viewModel.subCatBackDelegate?.subcatBackAction()
        removeFromSuperView(self)
    }
    
    
    func removefromView(){
        for v in self.navigationController!.viewControllers{
                  if v is AddSubCatAllergyController {
                      removeFromSuperView(v)
                  }
              }
        viewModel.addSubCatAllergiesDelegate?.updateAddAllergies(false)

      
    }
    
    override func crossButtonAction(_ sender: Any) {
        viewModel.addSubCatAllergiesDelegate?.updateAddAllergies(false)
        removefromView()
    }
    
    private func UISetup(){
        setNavWithOutView()
        AllergiesCell.registerWithTable(tblView)
        getAllergiesCatList()
    }
    
    
    fileprivate func removeView(){
        viewModel.addSubCatAllergiesDelegate?.updateAddAllergies(false)
        removeFromSuperView(self)
        
    }
    
    func addAllergies(){
        
        let selectedCat = viewModel.subCatDict?.subcat?.filter{$0.isSelected == true}
        viewModel.subCatDict?.subcat = selectedCat
        var dict : [String : Any] = [String : Any]()
        dict[ktoken] = CurrentUserInfo.authToken
        dict[kallergy] = viewModel.endcodeModel(viewModel.subCatDict!)
        
        viewModel.addUserAllergies(APIsEndPoints.kAddAllergies.rawValue,dict, handler: {[weak self](statusCode)in
            self?.removefromView()
        })
    }
    
    func getAllergiesCatList(){
        var dict : [String : Any] = [String : Any]()
        dict[ktoken] = CurrentUserInfo.authToken
        viewModel.getAllegiesCatagory(APIsEndPoints.kgetAllergiesFilter.rawValue,dict, handler: {[weak self](statusCode)in
            self?.tblView.isHidden = false
            self?.tblView.reloadData()
        })
    }
    
}

//// UITableViewDataSource
extension AddSubCatAllergyController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.subCatDict?.subcat?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell  = tableView.dequeueReusableCell(withIdentifier: AllergiesCell.reuseIdentifier, for: indexPath) as! AllergiesCell
        cell.selectionStyle = .none
        
        guard let dict = viewModel.subCatDict?.subcat?[indexPath.row] else {return cell}
        cell.subCatCommiInit(dict)
        
        return cell
    }
    
    override func viewDidLayoutSubviews(){
        print(tblView.contentSize.height)
        print(ScreenSize.screenHeight)
        
    }
}

extension AddSubCatAllergyController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return CGFloat(viewModel.cellHeight)
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
        
        guard let dict = viewModel.dictAllergies?.allergies?[indexPath.row] else {return}
        
        if dict.subcat?.count ?? 0 > 0 {
            isContainSubCat = true
        }else{
            isContainSubCat = false
        }
        
        if dict.isSelected == true{
            viewModel.subCatDict?.subcat?[indexPath.row].isSelected = false
        }
        else{
            viewModel.subCatDict?.subcat?[indexPath.row].isSelected = true
        }
        tableView.reloadData()
    }
}

