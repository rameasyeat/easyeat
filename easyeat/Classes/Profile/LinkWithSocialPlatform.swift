//
//  LinkWithSocialPlatform.swift
//  easyeat
//
//  Created by Ramniwas on 27/04/20.
//  Copyright © 2020 EasyEat. All rights reserved.
//

import Foundation
//import FacebookLogin
//import FacebookCore
import GoogleSignIn

class SocialMediaLink {

func linkWithFB(_ controller :UIViewController, handller :@escaping ([String : Any])->Void){
   /* let loginManager = LoginManager()
    loginManager.logOut()
    
    loginManager.logIn(
        permissions: [.publicProfile, .email],
        viewController: controller
    ) { result in
        
        switch result {
        case .failed(let error):
            print(error)
        case .cancelled:
            print("User cancelled login.")
        case .success( _, _, let accessToken):
            
            let facebookRequest = GraphRequest(graphPath: "/me", parameters: [:], httpMethod: .get)
            facebookRequest.start(completionHandler: {(connection,result,error) in
                
                let info = result as! [String : Any]
                if info["name"] as? String != nil {
                }
                handller(info)
            })
            
            print("accessToken: " + String(describing: accessToken))
            break
        }
    }*/
}

func linkWithGoogle(_ controller :UIViewController, handller :@escaping ([String : Any])->Void){
    GIDSignIn.sharedInstance().signIn()

}
    
}

extension SigninViewController: GIDSignInDelegate {
    // Present a view that prompts the user to sign in with Google
    func sign(_ signIn: GIDSignIn!,
              present viewController: UIViewController!)
    {
        self.present(viewController, animated: true, completion: nil)
    }
    
    // Dismiss the "Sign in with Google" view
    func sign(_ signIn: GIDSignIn!,
              dismiss viewController: UIViewController!)
    {
        self.dismiss(animated: true, completion: nil)
    }
    
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!)
    {
        
        if (error == nil)
        {
//            let userId = user.userID
//            let fullName = user.profile.name
//            let email = user.profile.email
        }
        else
        {
            print("\(error.localizedDescription)")
        }
        
    }
    
    private func signIn(signIn: GIDSignIn!, didDisconnectWithUser user:GIDGoogleUser!,
                        withError error: Error!)
    {
        // PeError!y operations when the user disconnects from app here.
        // ...
    }
    
    func signIn(signIn: GIDSignIn!, presentViewController viewController: UIViewController!) {
        self.sign(signIn, present: viewController)
    }
    
    func signIn(signIn: GIDSignIn!, dismissViewController viewController: UIViewController!) {
        self.sign(signIn, dismiss: viewController)
    }
}
