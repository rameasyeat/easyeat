//
//  BasicProfileViewController.swift
//  easyeat
//
//  Created by Ramniwas on 24/03/20.
//  Copyright © 2020 EasyEat. All rights reserved.
//

import UIKit
import MXScroll
import GoogleSignIn


class BasicProfileViewController: UIViewController,Storyboarded {
    
    var rightButton : CustomButton?
    fileprivate let cellHeight = 90.0
    
    @IBOutlet weak var googleButton: CustomButton!
    @IBOutlet weak var fbButton: CustomButton!
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var saveButton: CustomButton!
    @IBOutlet weak var viewBottomHeight: NSLayoutConstraint!
    
    
    var viewModel : BasicProfileViewModel = {
        let viewModel = BasicProfileViewModel()
        return viewModel }()
    
    var isEditProfile : Bool = false {
        didSet {
            if isEditProfile == true{
                rightButton?.setImage(UIImage(named:"cross"), for: .normal)
                saveButton.alpha = 1
                saveButton.isUserInteractionEnabled = true
                viewBottomHeight.constant = 30
            }
            else {
                saveButton.alpha = 0.4
                saveButton.isUserInteractionEnabled = false
                rightButton?.setImage(UIImage(named:"edit"), for:
                    .normal)
                viewBottomHeight.constant = 30

//                viewBottomHeight.constant = 210
                
            }
            tblView.reloadData()
        }
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(self.getEditButtonAction), name: Notification.Name(kEditProfileIdentifier), object: nil)
        UISetup()
    }
    override func viewDidAppear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(kEditProfileIdentifier)

        GIDSignIn.sharedInstance().presentingViewController = self
        GIDSignIn.sharedInstance().delegate = self
    }
    
 
    
    private func UISetup(){
        
        viewModel.infoArray = (self.viewModel.prepareInfo(dictInfo: viewModel.dictInfo))
        BasicProfileCell.registerWithTable(tblView)
        CountryCodeCell.registerWithTable(tblView)
        isEditProfile = false
        
    }
    
    // MARK Button Action
    @objc func getEditButtonAction(notification: Notification) {
        isEditProfile = !isEditProfile
    }
    @IBAction func saveButtonAction(_ sender: Any) {
        updateProfile()
    }
    
     // MARK : button action
      @IBAction func fbButtonAction(_ sender: Any) {
          let social = SocialMediaLink()
          social.linkWithFB(self) { (dict) in
              //apis call for link profile
              print(dict)
          }
      }
      @IBAction func googleButtonAction(_ sender: Any) {
          GIDSignIn.sharedInstance().signIn()

      }
    
    // APis call
    fileprivate func getUserBasicDetails(){
        
        var dict : [String : Any] = [String : Any]()
        dict[ktoken] =  CurrentUserInfo.authToken
        viewModel.getBasicProfileDetails(APIsEndPoints.kbasicProfile.rawValue,dict, handler: {[weak self](statusCode)in
            self?.isEditProfile = false
            self?.saveUserInfo()
            self?.tblView.reloadData()
        })
    }
    
    fileprivate func saveUserInfo(){ // save basic user info
        CurrentUserInfo.userdId = viewModel.userDictData?.user_id
        CurrentUserInfo.userName = viewModel.userDictData?.name
        CurrentUserInfo.dailCode = viewModel.userDictData?.dial_code
    }
    
    // APis call
    fileprivate func updateProfile(){
        
        viewModel.validateFields(dataStore: viewModel.infoArray, validHandler: { (dict, msg, iSucess) in
            
            if iSucess {
                self.viewModel.updateProfileDetails(APIsEndPoints.kUpdateBasicProfile.rawValue,dict, handler: {[weak self](statusCode)in
                    self?.getUserBasicDetails()
                })
            }
            else {
                Alert(title: "", message: msg, vc: self)
            }
            
        })
                
    }
    
    
}

//// UITableViewDataSource
extension BasicProfileViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.infoArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        if indexPath.row == viewModel.mobileCellType {
            let cell  = tableView.dequeueReusableCell(withIdentifier: CountryCodeCell.reuseIdentifier, for: indexPath) as! CountryCodeCell
            cell.selectionStyle = .none
            cell.mobileTextFiled.delegate = self
            cell.mobileTextFiled.isUserInteractionEnabled = false
            
            cell.commonBasicProfileInit(viewModel.infoArray[indexPath.row])
            return cell
        }
        
        let cell  = tableView.dequeueReusableCell(withIdentifier: BasicProfileCell.reuseIdentifier, for: indexPath) as! BasicProfileCell
        cell.selectionStyle = .none
        cell.txtField.delegate = self
        cell.commiInit(viewModel.infoArray[indexPath.row],isEditProfile)
        
        return cell
    }
}

extension BasicProfileViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return CGFloat(cellHeight)
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
        
        if  isEditProfile == false{
            return
        }
        
        if indexPath.row == BasicFiledType.dob.rawValue{
            viewModel.addDatePicker(indexPath.row) {[weak self] (str) in
                self?.tblView.reloadData()
            }
        }
        else if indexPath.row == BasicFiledType.gender.rawValue{
            viewModel.addGenderPicker(indexPath.row) { [weak self](str) in
                self?.tblView.reloadData()
            }
        }
    }
}

extension BasicProfileViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        self.view.endEditing(true)
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
    {
        let point = tblView.convert(textField.bounds.origin, from: textField)
        let index = tblView.indexPathForRow(at: point)
        
        let str = (textField.text as NSString?)?.replacingCharacters(in: range, with: string)
        viewModel.infoArray[(index?.row)!].value = str ?? ""
        return true
    }
}

extension BasicProfileViewController: GIDSignInDelegate {
    // Present a view that prompts the user to sign in with Google
    func sign(_ signIn: GIDSignIn!,
              present viewController: UIViewController!){
        self.present(viewController, animated: true, completion: nil)
    }
    
    func sign(_ signIn: GIDSignIn!,
              dismiss viewController: UIViewController!){
        self.dismiss(animated: true, completion: nil)
    }
    
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!){
        if (error == nil){
//            let userId = user.userID
//            let fullName = user.profile.name
//            let email = user.profile.email
        }
    }
    func signIn(signIn: GIDSignIn!, presentViewController viewController: UIViewController!) {
        self.sign(signIn, present: viewController)
    }
    
    func signIn(signIn: GIDSignIn!, dismissViewController viewController: UIViewController!) {
        self.sign(signIn, dismiss: viewController)
    }
}

extension BasicProfileViewController:MXViewControllerViewSource{
       func viewForMixToObserveContentOffsetChange() -> UIView {
           return self.tblView
       }
}
