//
//  ProfileHeaderViewController.swift
//  easyeat
//
//  Created by Ramniwas on 24/03/20.
//  Copyright © 2020 EasyEat. All rights reserved.
//

import UIKit

class ProfileHeaderViewController: UIViewController,Storyboarded {
    var userDictData : UserData?
    @IBOutlet weak var profilePercentage : CustomLabel?
    @IBOutlet weak var profileiView: VIewShadow!
    @IBOutlet weak var profileImageView: CustomImageView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let completePoint = self.userDictData?.completed_field ?? 0
        let totalPoints = self.userDictData?.total_field ?? 0
        let profileComplete = (completePoint * 100) / totalPoints
        profileProgress(profileImageView, profileiView,CGFloat(profileComplete))
        profilePercentage?.text = "\(profileComplete)%"
        
    }
    
}
