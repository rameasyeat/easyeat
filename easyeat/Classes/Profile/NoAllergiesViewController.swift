//
//  NoAllergiesViewController.swift
//  easyeat
//
//  Created by Ramniwas on 25/03/20.
//  Copyright © 2020 EasyEat. All rights reserved.
//

import UIKit

protocol AllergiesDelegate {
    func addAllergies()
    func noAllergies()
    func resetAllergy()
}
class NoAllergiesViewController: UIViewController,Storyboarded {
    
    @IBOutlet weak var descriptionLabel: CustomLabel!
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var removeAllergiesButton: CustomButton!
    
    var viewModel : NoAllergiesViewModel = {
          let viewModel = NoAllergiesViewModel()
          return viewModel }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        UISetup()
    }
    
    private func UISetup(){
        
        if viewModel.allgergieType == AllergiesType.no {
            bottomView.isHidden = true
            descriptionLabel.text = NSLocalizedString("I've got no allergies", comment: "")
            
            if viewModel.userDictData?.gender == "Male"{
                imgView.image = #imageLiteral(resourceName: "maleAllergies")
            }else {
                imgView.image = #imageLiteral(resourceName: "femaleAllergies")
            }
            
            if viewModel.isEditProfile == true && viewModel.userAllergiesDict?.allergic_items?.is_allergy == 0{
                removeAllergiesButton.isHidden = false
            }
            else{
                removeAllergiesButton.isHidden = true
            }
        }
        else  if viewModel.allgergieType == AllergiesType.add {
            
        }
    }
    
    @IBAction func removeAllergiesButtonAction(_ sender: Any) {
        updateAllergies()
        
    }
    @IBAction func addAllergiesAction(_ sender: Any) {
        viewModel.delegate.addAllergies()
        removeFromSuperView(self)

    }
    @IBAction func noAllergiesAction(_ sender: Any) {
        viewModel.delegate.noAllergies()
        removeFromSuperView(self)

    }
    
    // Apis call
    
    func updateAllergies(){
           var dict : [String : Any] = [String : Any]()
           dict[ktoken] = CurrentUserInfo.authToken
           dict[ktype] = "reset_allergy"
           viewModel.resetAllergies(APIsEndPoints.kUpdateUserAllergies.rawValue,dict, handler: {[weak self](statusCode)in
            self?.viewModel.delegate.resetAllergy()
            removeFromSuperView(self!)


           })
       }
    
    
}
