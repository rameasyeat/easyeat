//
//  ProfileViewController.swift
//  easyeat
//
//  Created by Ramniwas on 21/04/20.
//  Copyright © 2020 EasyEat. All rights reserved.
//

import UIKit
import GoogleSignIn


class ProfileViewController: BaseViewController,Storyboarded {
    
    
    var coordinator: MainCoordinator?
    
    @IBOutlet weak var profileBgView: VIewShadow!
    @IBOutlet weak var earningBgView: VIewShadow!
    @IBOutlet weak var myRewardBgView: VIewShadow!
    
    @IBOutlet weak var userNameLabel: CustomLabel!
    @IBOutlet weak var profileCompleteLabel: CustomLabel!
    @IBOutlet weak var earningLabel: CustomLabel!
    @IBOutlet weak var savingLabel: CustomLabel!
    @IBOutlet weak var profileiView: VIewShadow!
    @IBOutlet weak var profileImageView: CustomImageView!

    
    @IBOutlet weak var googleButton: CustomButton!
    @IBOutlet weak var fbButtonAction: CustomButton!
    
    var progressRing: CircularProgressBar!

    
    
    var viewModel : ProfileViewModel = {
        let model = ProfileViewModel()
        return model
    }()
    
    @IBOutlet weak var tblView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setUI()
    }
        override func viewDidAppear(_ animated: Bool) {
            GIDSignIn.sharedInstance().presentingViewController = self
           GIDSignIn.sharedInstance().delegate = self
        }
    
   
    // UI setup
    fileprivate func setUI(){
        
        setNavWithOutView()
        titleLbl?.text = "Profile"
        viewModel.getCalories()
        getUserBasicDetails()
        getUserNutrition()
        getUserEarning()
        ProfileCell.registerWithTable(tblView)
        
        // border
        profileBgView.layer.applySketchShadow(color: hexStringToUIColor("#0000002E"), alpha: 0.4, x: 0, y: 0, blur: 6, spread: 0)
        earningBgView.layer.applySketchShadow(color: hexStringToUIColor("#0000002E"), alpha: 0.4, x: 0, y: 0, blur: 6, spread: 0)
        myRewardBgView.layer.applySketchShadow(color: hexStringToUIColor("#0000002E"), alpha: 0.4, x: 0, y: 0, blur: 6, spread: 0)

        
        // check is from tabbar
        if viewModel.fromView == FromViewType.tabbar{
                backButton.isHidden = true
            }
    }
    
    fileprivate func setBasicProfileData(){
        
        if viewModel.arrayUserEarning?.count ?? 0 > 0 {
            
            let saving = String(format: "%.2f", viewModel.arrayUserEarning?[0].total_saving ?? 0)
            let earning = String(format: "%.2f", viewModel.arrayUserEarning?[0].total_earning ?? 0)

            earningLabel.text = earning
            savingLabel.text = saving
        }
    }
    
    
    // MARK : button action
    @IBAction func fbButtonAction(_ sender: Any) {
        let social = SocialMediaLink()
        social.linkWithFB(self) { (dict) in
            //apis call for link profile
            print(dict)
        }
    }
    @IBAction func googleButtonAction(_ sender: Any) {
        GIDSignIn.sharedInstance().signIn()
    }
    
    @IBAction func earningButtonAction(_ sender: Any) {
        coordinator?.goToLoyalityBenefits()
    }
    @IBAction func profileButtonAction(_ sender: Any) {
        coordinator?.goToPofileDetails(viewModel.userDictData!)
    }
    @IBAction func rewardButtonAction(_ sender: Any) {
        coordinator?.goToRewardsScreen()
    }
    
   
    
    override func rightButtonAction() {
        coordinator?.goToMoreView()

    }
    // APis call
    fileprivate func getUserBasicDetails(){
        
        var dict : [String : Any] = [String : Any]()
        dict[ktoken] = CurrentUserInfo.authToken
        viewModel.getBasicProfileDetails(APIsEndPoints.kbasicProfile.rawValue,dict, handler: {[weak self](statusCode)in
            self?.updateUI()
            print(statusCode)
        })
    }
    
    
    fileprivate func updateUI(){
        self.userNameLabel.text = self.viewModel.userDictData?.name?.capitalized
        
        let completePoint = self.viewModel.userDictData?.completed_field ?? 0
        let totalPoints = self.viewModel.userDictData?.total_field ?? 0
        
        let profileComplete = (completePoint * 100) / totalPoints
        self.profileCompleteLabel.text = "Complete your profile(\(profileComplete)%)"
        profileProgress(profileImageView, profileiView,CGFloat(profileComplete))

    }
    
    fileprivate func getUserNutrition(){
        
        var dict : [String : Any] = [String : Any]()
        dict[ktoken] = CurrentUserInfo.authToken
        dict[kgmtoffset] = -330
        dict[kdayoffset] = 0
        dict[kno_of_days] = 7
        
        viewModel.getNutritionData(APIsEndPoints.kuserNutrition.rawValue,dict, handler: {[weak  self](statusCode)in
            print(statusCode)
            self?.tblView.reloadData()
        })
    }
    
    fileprivate func getUserEarning(){
        
        var dict : [String : Any] = [String : Any]()
        dict[ktoken] = CurrentUserInfo.authToken
        viewModel.getUserEarining(APIsEndPoints.kUserEarning.rawValue,dict, handler: {[weak self](statusCode)in
            self?.setBasicProfileData()
            print(statusCode)
        })
    }
    
}

// UITableViewDataSource
extension ProfileViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1

//        return  (viewModel.userAvgData?.count ?? 0 > 0) ? 1 : 0
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell  = tableView.dequeueReusableCell(withIdentifier: ProfileCell.reuseIdentifier, for: indexPath) as! ProfileCell
        cell.selectionStyle = .none
        
        cell.viewModel = viewModel
        cell.commonInit()
        
        return cell
    }
}

extension ProfileViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return CGFloat(viewModel.cellHeight)
    }
}


extension ProfileViewController: GIDSignInDelegate {
    // Present a view that prompts the user to sign in with Google
    func sign(_ signIn: GIDSignIn!,
              present viewController: UIViewController!){
        self.present(viewController, animated: true, completion: nil)
    }
    
    func sign(_ signIn: GIDSignIn!,
              dismiss viewController: UIViewController!){
        self.dismiss(animated: true, completion: nil)
    }
    
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!){
        if (error == nil){
//            let userId = user.userID
//            let fullName = user.profile.name
//            let email = user.profile.email
        }
    }
    func signIn(signIn: GIDSignIn!, presentViewController viewController: UIViewController!) {
        self.sign(signIn, present: viewController)
    }
    
    func signIn(signIn: GIDSignIn!, dismissViewController viewController: UIViewController!) {
        self.sign(signIn, dismiss: viewController)
    }
}
