//
//  AddAllergiesViewController.swift
//  easyeat
//
//  Created by Ramniwas on 25/03/20.
//  Copyright © 2020 EasyEat. All rights reserved.
//

import UIKit

protocol AddAllergiesDelegate {
    func updateAddAllergies(_ isAdded : Bool)
}
class AddAllergiesViewController: BaseViewController,Storyboarded,SubCatBackDelegate {
    
    @IBOutlet weak var saveButton: CustomButton!
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var pageControl: UIPageControl!
    
    
    var viewModel : AddAllergieViewModel = {
        let viewModel = AddAllergieViewModel()
        return viewModel }()
    
    var isContainSubCat : Bool = false{
        didSet{
            if isContainSubCat == true{
                saveButton.setTitle("Next", for: .normal)
                
            }else{
                saveButton.setTitle("Save", for: .normal)
            }
            saveButton.alpha = 1.0
            saveButton.isUserInteractionEnabled = true
            
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        UISetup()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.view.isHidden = false
        
    }
    
    @IBAction func saveButtonAction(_ sender: Any) {
        
        let selectedCat = viewModel.dictAllergies?.allergies?.filter{$0.isSelected == true}
        
        if selectedCat?[0].subcat?.count ?? 0 > 0 {
            
            self.view.isHidden = true
            let controller = AddSubCatAllergyController.instantiate()
            controller.viewModel.addSubCatAllergiesDelegate = viewModel.addSubCatAllergiesDelegate
            controller.viewModel.subCatBackDelegate = self
            controller.viewModel.subCatDict = selectedCat?[0]
            addRootView(controller)
        }
        else {
            addAllergies()
        }
    }
    
    override func crossButtonAction(_ sender: Any) {
        removeView(false)
    }
    
    func subcatBackAction() {
        self.view.isHidden = false
    }
    
    private func UISetup(){
        setNavWithOutView()
        AllergiesCell.registerWithTable(tblView)
        getAllergiesCatList()
    }
    
    
    fileprivate func removeView(_ isAdded : Bool){
        removeFromSuperView(self)
        viewModel.addAllergiesDelegate?.updateAddAllergies(isAdded)
        
    }
    
    
    
    func addAllergies(){
        
        let selectedCat = viewModel.dictAllergies?.allergies?.filter{$0.isSelected == true}
        
        
        var dict : [String : Any] = [String : Any]()
        dict[ktoken] = CurrentUserInfo.authToken
        dict[kallergy] = viewModel.endcodeModel((selectedCat?[0])!)
        
        viewModel.addUserAllergies(APIsEndPoints.kAddAllergies.rawValue,dict, handler: {[weak self](statusCode)in
            self?.removeView(true)
        })
    }
    
    func getAllergiesCatList(){
        var dict : [String : Any] = [String : Any]()
        dict[ktoken] = CurrentUserInfo.authToken
        viewModel.getAllegiesCatagory(APIsEndPoints.kgetAllergiesFilter.rawValue,dict, handler: {[weak self](statusCode)in
            self?.tblView.isHidden = false
            self?.tblView.reloadData()
        })
    }
    
}

//// UITableViewDataSource
extension AddAllergiesViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.dictAllergies?.allergies?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell  = tableView.dequeueReusableCell(withIdentifier: AllergiesCell.reuseIdentifier, for: indexPath) as! AllergiesCell
        cell.selectionStyle = .none
        
        guard let dict = viewModel.dictAllergies?.allergies?[indexPath.row] else {return cell}
        
        cell.commiInit(dict)
        
        return cell
    }
    
    override func viewDidLayoutSubviews(){
        print(tblView.contentSize.height)
        print(ScreenSize.screenHeight)
        
    }
}

extension AddAllergiesViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return CGFloat(viewModel.cellHeight)
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
        
        guard let dict = viewModel.dictAllergies?.allergies?[indexPath.row] else {return}
        
        if dict.subcat?.count ?? 0 > 0 {
            isContainSubCat = true
            pageControl.numberOfPages = 2 //dict.subcat?.count ?? 0
        }else{
            isContainSubCat = false
            pageControl.numberOfPages = 0
        }
        
        if dict.isSelected == true{
            viewModel.dictAllergies?.allergies?[indexPath.row].isSelected = false
        }
        else{
            
            for (index, _) in viewModel.dictAllergies!.allergies!.enumerated(){
                viewModel.dictAllergies?.allergies?[index].isSelected = false
            }
            viewModel.dictAllergies?.allergies?[indexPath.row].isSelected = true
        }
        tableView.reloadData()
    }
}

