//
//  ProfileCompleteAnimation.swift
//  easyeat
//
//  Created by Ramniwas on 22/04/20.
//  Copyright © 2020 EasyEat. All rights reserved.
//

import Foundation
import UIKit

func profileProgress(_ profileImageView : CustomImageView,_ profileiView : VIewShadow, _ progress: CGFloat) {
      let xPosition = profileImageView.center.x
      let yPosition = profileImageView.center.y
      let position = CGPoint(x: xPosition, y: yPosition)
      
     let progressRing = CircularProgressBar(radius: profileImageView.frame.size.width/2, position: position, innerTrackColor: .defaultInnerColor, outerTrackColor: .defaultOuterColor, lineWidth: 5)
      progressRing.progress = progress
      profileiView.layer.addSublayer(progressRing)
  }

extension UIColor {
    static func rgb(_ red: CGFloat, _ green: CGFloat, _ blue: CGFloat) -> UIColor {
        return UIColor.init(red: red / 255, green: green / 255, blue: blue / 255, alpha: 1)
    }

    static let defaultOuterColor = hexStringToUIColor("E6EDF4")
    static let defaultInnerColor: UIColor = hexStringToUIColor(kred)
    static let defaultPulseFillColor = hexStringToUIColor("E6EDF4")

}
