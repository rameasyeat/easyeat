//
//  ProfileViewController.swift
//  easyeat
//
//  Created by Ramniwas on 24/03/20.
//  Copyright © 2020 EasyEat. All rights reserved.
//

import UIKit
import MXScroll


class BasicProfileSegmentController: BaseViewController,Storyboarded {
    var mx: MXViewController<MSSegmentControl>!
    var coordinator: MainCoordinator?


    @IBOutlet var containerView: UIView?
    var userDictData : UserData?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        

        setNavWithOutView()
         titleLbl?.text = "Profile"
        loadVC()
    }
    
    
    override func rightButtonAction() {
        NotificationCenter.default.post(name: Notification.Name(kEditProfileIdentifier), object: rightButton, userInfo: nil)
    }

    func getTitle()->[String]{
        let tiles = [NSLocalizedString("Basic", comment: ""),NSLocalizedString("Allergies", comment: "")]
        return tiles
    }
    
    func loadVC() {
        
        let headerView = ProfileHeaderViewController.instantiate()
        headerView.userDictData = userDictData

        let allergiesView = AllergiesViewController.instantiate()
        allergiesView.viewModel.userDictData = userDictData
        allergiesView.rightButton = rightButton

        
        let basicProfile = BasicProfileViewController.instantiate()
        basicProfile.viewModel.userDictData = userDictData
        basicProfile.rightButton = rightButton
        
        let segment = MSSegmentControl(sectionTitles: getTitle())
        setupSegment(segmentView: segment)
        
        mx = MXViewController<MSSegmentControl>.init(headerViewController: headerView, segmentControllers: [basicProfile, allergiesView], segmentView: segment)
        mx.headerViewOffsetHeight = 40
        
        mx.shouldScrollToBottomAtFirstTime = false
        
        addChild(mx)
        containerView?.addSubview(mx.view)
        guard let bounds = containerView?.bounds else {return}
        mx.view.frame = bounds
        mx.didMove(toParent: self)
    }
    
    @IBAction func toBottom(_: Any) {
        if mx != nil {
            mx.scrollToBottom()
        }
    }
    
    func setupSegment(segmentView: MSSegmentControl) {
        segmentView.borderType = .none
        segmentView.backgroundColor =  #colorLiteral(red: 0.9647058824, green: 0.631372549, blue: 0.1607843137, alpha: 0.1131987236)
        segmentView.selectionIndicatorColor = #colorLiteral(red: 0.9647058824, green: 0.631372549, blue: 0.1607843137, alpha: 1)
        segmentView.selectionIndicatorHeight = 2
        
        segmentView.selectedTitleTextAttributes = [NSAttributedString.Key.foregroundColor: #colorLiteral(red: 0.9647058824, green: 0.631372549, blue: 0.1607843137, alpha: 1)]
        segmentView.titleTextAttributes = [NSAttributedString.Key.foregroundColor: #colorLiteral(red: 0.537254902, green: 0.5725490196, blue: 0.6392156863, alpha: 1)]
        
        segmentView.segmentWidthStyle = .fixed
        segmentView.indicatorWidthPercent = 1
        segmentView.selectionStyle = .textWidth
    }
}



