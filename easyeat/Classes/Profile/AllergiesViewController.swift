//
//  AllergiesViewController.swift
//  easyeat
//
//  Created by Ramniwas on 24/03/20.
//  Copyright © 2020 EasyEat. All rights reserved.
//

import UIKit
import MXScroll

class AllergiesViewController: UIViewController,Storyboarded,AllergiesDelegate,AddAllergiesDelegate,AddSubCatAllergiesDelegate {
   
   
    var nameTextFiled: CustomTextField!
    var rightButton : CustomButton?
    
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var addButton: CustomButton!
    
    
    lazy var refreshControl: UIRefreshControl = {
          let refreshControl = UIRefreshControl()
          refreshControl.addTarget(self, action:
              #selector(handleRefresh(_:)),
                                   for: UIControl.Event.valueChanged)
          refreshControl.tintColor = UIColor.gray
          
          return refreshControl
      }()
    
    var viewModel : AllergiesViewModel = {
        let viewModel = AllergiesViewModel()
        return viewModel }()
    
    var isEditProfile : Bool = false {
        didSet {
            if isEditProfile == true{
                rightButton?.setImage(UIImage(named:"cross"), for: .normal)
                addButton.isHidden = false
            }
            else {
                rightButton?.setImage(UIImage(named:"edit"), for: .normal)
                addButton.isHidden = true
            }
            tblView.reloadData()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(self.getEditButtonAction), name: Notification.Name(kEditProfileIdentifier), object: nil)
        UISetup()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(kEditProfileIdentifier)
    }
    
    private func UISetup(){
        tblView.addSubview(refreshControl)
        UserAllergiesCell.registerWithTable(tblView)
        getUserAllergies()
    }
    
    
    // Delegates call
    func addAllergies() {
        
        let controller = AddAllergiesViewController.instantiate()
        controller.viewModel.addAllergiesDelegate = self
        controller.viewModel.addSubCatAllergiesDelegate = self
        
        easyeat.addCategory(controller, AllergiesType.add)
    }
    func noAllergies() {
        // call no allergies apis
        updateAllergies()
    }
    func resetAllergy() {
        
        self.isEditProfile = false
        getUserAllergies()
    }
    
    func updateAddAllergies(_ isAdded: Bool) {
        self.isEditProfile = false
        getUserAllergies()
    }
    
    @objc func handleRefresh(_ refreshControl: UIRefreshControl) {
        refreshControl.endRefreshing()
        self.isEditProfile = false
        getUserAllergies()
    }
    
    
    private func showNoAllergiesView(_ allergiesType : AllergiesType){
        easyeat.noAllergies(self,allergiesType, viewModel.userDictData!,isEditProfile,viewModel.userAllergiesDict!)
        
    }
    
    private func updateUI(){
        
        if viewModel.userAllergiesDict?.allergic_items?.categories?.count ?? 0 > 0{
            removePreviousView(self)
            tblView.isHidden = false
            self.view.bringSubviewToFront(tblView)
            self.tblView.reloadData()
        }
        else {
            if viewModel.userAllergiesDict?.allergic_items?.is_allergy == 0{
                showNoAllergiesView(AllergiesType.no)
            }
            else{
                showNoAllergiesView(AllergiesType.add)
            }
        }
    }
    
    
    // MARK Button Action
    @objc func getEditButtonAction(notification: Notification) {
        isEditProfile = !isEditProfile
        updateUI()
    }
    @IBAction func addButtonAction(_ sender: Any) {
        addAllergies()
    }
    
    @objc func deleteButtonAction(_ sender :UIButton){
        self.isEditProfile = false

        let dict = viewModel.userAllergiesDict?.allergic_items?.categories?[sender.tag]
        deleteUserAllergies(dict?.cat_id ?? "")
    }
    
    // apis call
    
    func getUserAllergies(){
        var dict : [String : Any] = [String : Any]()
        dict[ktoken] = CurrentUserInfo.authToken
        viewModel.getUserAllergies(APIsEndPoints.kgetUserAllergies.rawValue,dict, handler: {[weak self](statusCode)in
            self?.updateUI()
        })
    }
    
    func deleteUserAllergies(_ catid : String){
        var dict : [String : Any] = [String : Any]()
        dict[ktoken] = CurrentUserInfo.authToken
        dict[kcatid] = catid
        viewModel.deleteUserAllergies(APIsEndPoints.kDeleteAllergy.rawValue,dict, handler: {[weak self](statusCode)in
            self?.getUserAllergies()
        })
    }
    
    
    func updateAllergies(){
        var dict : [String : Any] = [String : Any]()
        dict[ktoken] = CurrentUserInfo.authToken
        dict[ktype] = "no_allergy"
        viewModel.updateAllergies(APIsEndPoints.kUpdateUserAllergies.rawValue,dict, handler: {[weak self](statusCode)in
            self?.getUserAllergies()
            
        })
    }
}

//// UITableViewDataSource
extension AllergiesViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.userAllergiesDict?.allergic_items?.categories?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell  = tableView.dequeueReusableCell(withIdentifier: UserAllergiesCell.reuseIdentifier, for: indexPath) as! UserAllergiesCell
        cell.selectionStyle = .none
        cell.commiInit((viewModel.userAllergiesDict?.allergic_items?.categories?[indexPath.row])!, index: indexPath.row)
        
        // delete button Action
        cell.deleteButton.tag = indexPath.row
        cell.deleteButton.addTarget(self, action: #selector(deleteButtonAction(_:)), for: .touchUpInside)
        if isEditProfile == true{
            cell.deleteButtonWidth.constant = CGFloat(viewModel.deleteButtonWidth)
        }else {
            cell.deleteButtonWidth.constant = 0
        }
        
        return cell
    }
}

extension AllergiesViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
        //        return CGFloat(viewModel.cellHeight)
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
    }
}

