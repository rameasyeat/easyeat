//
//  PaymentStatusViewController.swift
//  easyeat
//
//  Created by Ramniwas on 06/05/20.
//  Copyright © 2020 EasyEat. All rights reserved.
//

import UIKit
import Lottie
class PaymentStatusViewController: UIViewController,Storyboarded{
    var coordinator: MainCoordinator?
    @IBOutlet weak var paymentButton: CustomButton!
    
    @IBOutlet weak var animationView: LottieView!
    @IBOutlet weak var statusLabel: CustomLabel!
    @IBOutlet weak var descriptionLabel: CustomLabel!
    @IBOutlet weak var paymentLabel: CustomLabel!
    var viewModel : PaymentStatusModel = {
    let viewModel = PaymentStatusModel()
    return viewModel }()
    
    override func viewDidLoad() {
    }
    
    @IBAction func paymentButtonAction(_ sender: Any) {
    }
    



}
