//
//  PaymentModeViewController.swift
//  easyeat
//
//  Created by Ramniwas on 05/05/20.
//  Copyright © 2020 EasyEat. All rights reserved.
//

import UIKit


class PaymentModeViewController:BaseViewController,Storyboarded,NetbankingModeDelegate {
    var coordinator: MainCoordinator!
    
    var viewModel : PaymentModeViewModel = {
        let model = PaymentModeViewModel()
        return model
    }()
    
    
    @IBOutlet weak var tblView: UITableView!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    
    // SsetupUI
    fileprivate func setupUI(){
        
        setNavigation()
        viewModel.prepareInfo()
        PaymentModeCell.registerWithTable(tblView)
        gePaymentModeType()
        titleLbl?.text = "Payment"
    }
    
    // MARK : Netbanking
    fileprivate func addPaymentOption(_ array : [PaymentMode]){
        let controller = NetbankingViewController.instantiate()
        controller.viewModel.netbankingModeDelegate = self
        controller.viewModel.netBankingArray = array
        addRootView(controller)
    }
    
    func selectedPaymentModeType(_ channelName : String) {
        guard let orderID =  viewModel.orderData?.order_id else {return}
        self.coordinator?.goToPaymentView(orderID, channelName, viewModel.restaurantDict?.name ?? "")
    }
    
    
    // MARK : call APIs
    
     
    func gePaymentModeType(){
        var dict : [String : Any] = [String : Any]()
        dict[korder_id] = viewModel.orderData?.order_id
        viewModel.getPaymentOptions(APIsEndPoints.kgetPaymentMethod.rawValue,dict, handler: {[weak self](statusCode) in
            self?.tblView.reloadData()
        })
    }
    
    
}
// UITableViewDataSource
extension PaymentModeViewController: UITableViewDataSource,UITableViewDelegate{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        viewModel.paymentModeArray.count
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if viewModel.paymentModeArray[section].type == kNetBanking{
            return 1
        }
        return viewModel.paymentModeArray[section].data?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = viewModel.setHeaderView(section)
        return view
        
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        guard let arrayData = viewModel.paymentModeArray[section].data else {return 0}
        if arrayData.count == 0{
            return 0
        }
        return CGFloat(viewModel.headerHeight)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell  = tableView.dequeueReusableCell(withIdentifier: PaymentModeCell.reuseIdentifier, for: indexPath) as! PaymentModeCell
        cell.selectionStyle = .none
        
        if viewModel.paymentModeArray[indexPath.section].type == kNetBanking{
            cell.netBankingInit(viewModel.staticPaymentDict!)
        }
        else {
        cell.commiInit((viewModel.paymentModeArray[indexPath.section].data?[indexPath.row])!)
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
       
        return 70
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if viewModel.paymentModeArray[indexPath.section].type == kNetBanking{
            addPaymentOption(viewModel.paymentModeArray[indexPath.section].data!)
        }
        else if let type = viewModel.paymentModeArray[indexPath.section].type, type == kCash{
                selectedPaymentModeType(type)
        }
        else{
            guard let channelName = viewModel.paymentModeArray[indexPath.section].data?[indexPath.row].data?.channel_id else {
                return
            }
           selectedPaymentModeType(channelName)
        }
    }
    
}
