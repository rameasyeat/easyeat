//
//  PaymentViewController.swift
//  easyeat
//
//  Created by Ramniwas on 06/05/20.
//  Copyright © 2020 EasyEat. All rights reserved.
//

import UIKit
import Lottie
class PaymentViewController: BaseViewController,Storyboarded,MOLPayLibDelegate {
    
    @IBOutlet weak var paymentButton: CustomButton!
    @IBOutlet weak var animationView: LottieView!
    @IBOutlet weak var statusLabel: CustomLabel!
    @IBOutlet weak var descriptionLabel: CustomLabel!
    @IBOutlet weak var paymentLabel: CustomLabel!
    @IBOutlet weak var bgView: UIView!
    
    var coordinator: MainCoordinator?
    var viewModel : PaymentViewModel = {
        let viewModel = PaymentViewModel()
        return viewModel }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        bgView.isHidden = false
        paymentStatus(.processing)
        
        if viewModel.channelName == kCash{
            payInCash()
        }
        else{
            getPaymentData()
        }
    }
    
    fileprivate func addNavigation(){
        setNavWithOutView()
        rightButton.isHidden = true
        titleLbl?.isHidden = true
    }
    
    fileprivate func setupPaymentUI(){
        bgView.isHidden = true

        let mp = MOLPayLib(delegate:self, andPaymentDetails: viewModel.getPaymentInfo())!
        mp.modalPresentationStyle = .fullScreen
        self.present(mp, animated: false) {}
    }
    
    func paymentStatus(_ status : PaymentStatus){
        viewModel.showSuccessAnimation(animationView, status)
        bgView.isHidden = false
        viewModel.paymentStatus  = status
        if status == .processing{
            processingStatus()
        }
        else if status == .failed{
            addNavigation()
            failedStatus()
        }
        else if status == .success{
            sucessStatus()
        }
    }
    
    override func backButtonAction(){
        backToOrderView()
    }
    
    func backToOrderView(){
        for view in self.navigationController!.viewControllers{
            if view is OrderViewViewController{
                self.navigationController?.popToViewController(view, animated: false)
            }
        }
    }
    
    @IBAction func paymentButtonAction(_ sender: Any) {
        if viewModel.paymentStatus == .processing{
        }
        else if viewModel.paymentStatus == .failed{
            setupPaymentUI()
        }
        else if viewModel.paymentStatus == .success{
            backToOrderView()
        }
        
    }
    
    // Set UI
    fileprivate func failedStatus(){
        statusLabel.text = viewModel.failedStatus
        descriptionLabel.text = viewModel.failedDescription
        paymentLabel.text = viewModel.paymentText
        
        paymentButton.isHidden = false
        paymentButton.setTitle("Retry", for: .normal)
        paymentButton.backgroundColor = hexStringToUIColor(kred)
    }
    
    fileprivate func sucessStatus(){
        statusLabel.text = viewModel.sucessStatus
        descriptionLabel.text = viewModel.sucessDescription
        paymentLabel.text = viewModel.paymentText

        paymentButton.isHidden = true
        paymentButton.setTitle("Done", for: .normal)
        paymentButton.backgroundColor = hexStringToUIColor(kgreen)
    }
    
    fileprivate func processingStatus(){
        statusLabel.text = viewModel.processingStatus
        descriptionLabel.text = viewModel.processingDescription
        paymentButton.isHidden = true
    }
    
     func getPaymentData(){
         var dict : [String : Any] = [String : Any]()
        dict[korder_id] = viewModel.orderID
        dict[kpayment_channel] =  viewModel.channelName
        viewModel.initiateRazerpayPayment(APIsEndPoints.kinitiateRazerpayPayment.rawValue,dict, handler: {[weak self](statusCode) in
            self?.setupPaymentUI()
         })
     }
    
    func payInCash(){
            var dict : [String : Any] = [String : Any]()
        dict[ktoken] = CurrentUserInfo.authToken
            viewModel.collectCash(APIsEndPoints.kMessagingput.rawValue,dict, handler: {[weak self](statusCode,msg) in
                self?.view.makeToast(msg, duration: 2.0, completion: { (hide) in
                    self?.backToOrderView()
                })

            })
        }
    
    // MARK : Delegates call
    func transactionResult(_ result: [AnyHashable : Any]!) {
        if result != nil{
            print(result?["Error"] as Any)
            
            if let status_code : String = result?["status_code"] as? String{
                
                if status_code == "00"{ // Success
                    paymentStatus(.success)
                    DispatchQueue.main.asyncAfter(deadline: .now() + 2.0, execute: {
                        self.backToOrderView()
                    })
                }
                else if status_code == "11"{ // Failed
                    paymentStatus(.failed)
                }
                else if status_code == "22"{ // Pending
                    paymentStatus(.processing)
                }
                self.dismiss(animated: true, completion: nil)
                
            }
            else{
                self.dismiss(animated: true, completion: nil)
                backButtonAction()

                Alert(title: "Error", message: result?["Error"] as? String ?? "", vc: RootViewController.controller!)
            }
        }
        else {
            self.dismiss(animated: true, completion: nil)
            backButtonAction()

            Alert(title: "Error", message: "Something wrong with your payment flow process", vc: RootViewController.controller!)
        }
    }
}
