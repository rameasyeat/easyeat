//
//  NetbankingViewController.swift
//  easyeat
//
//  Created by Ramniwas on 06/05/20.
//  Copyright © 2020 EasyEat. All rights reserved.
//

import UIKit



protocol NetbankingModeDelegate {
    func selectedPaymentModeType(_ channelName : String)
}
class NetbankingViewController: BaseViewController,Storyboarded {
    
    @IBOutlet weak var saveButton: CustomButton!
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var bgViewHeight: NSLayoutConstraint!
    var coordinator: MainCoordinator?
    
    var viewModel : NetBankingViewModel = {
        let viewModel = NetBankingViewModel()
        return viewModel }()
    
    var isPaymentModeSelected : Bool = false{
        didSet{
            saveButton.alpha = 1.0
            saveButton.isUserInteractionEnabled = true
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        UISetup()
    }
    
    private func UISetup(){
        setNavigation()
        AllergiesCell.registerWithTable(tblView)
    }
    
    
    // MARK : Button Action
    @IBAction func saveButtonAction(_ sender: Any) {
        
        let selecteModeArray = viewModel.netBankingArray?.filter{$0.isSelected == true}
        if selecteModeArray?.count ?? 0 > 0 {
            viewModel.netbankingModeDelegate?.selectedPaymentModeType((selecteModeArray?[0].data?.channel_id)!)
            removeFromSuperView(self)
        }
    }
    
    override func crossButtonAction(_ sender: Any) {
        removeFromSuperView(self)
    }
    
    fileprivate func removeView(){
        removeFromSuperView(self)
    }
    
    func clearSelectedPaymentMode(){
        let array = viewModel.netBankingArray
        
        for (index,_) in (array?.enumerated())!{
            viewModel.netBankingArray?[index].isSelected = false
        }
    }
    
}

//// UITableViewDataSource
extension NetbankingViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.netBankingArray?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell  = tableView.dequeueReusableCell(withIdentifier: AllergiesCell.reuseIdentifier, for: indexPath) as! AllergiesCell
        cell.selectionStyle = .none
        guard let dict = viewModel.netBankingArray?[indexPath.row] else {return cell}
        cell.paymentMode(dict)
        
        return cell
    }
    
}

extension NetbankingViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return CGFloat(viewModel.cellHeight)
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
        
        clearSelectedPaymentMode()
        viewModel.netBankingArray?[indexPath.row].isSelected = true
        isPaymentModeSelected = true
        
        tableView.reloadData()
    }
}
