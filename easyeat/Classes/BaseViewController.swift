//
//  BaseViewController.swift
//  easyeat
//
//  Created by Ramniwas on 23/03/20.
//  Copyright © 2020 EasyEat. All rights reserved.
//

import UIKit

class BaseViewController: UIViewController {
    
    var backButton : CustomButton!
    var notificationButton : CustomButton!
    var rightButton : CustomButton!

    
    
    var navView : UIView?
    var pageCount : Int = 1
    var totaPageCount : Int  = 1
    var titleLbl  : CustomLabel?
    
    var buttonHeight : CGFloat = 35
    var button_y_Axis: CGFloat = 38
    
    var headeImageview : CustomImageView?
    
    
    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action:
            #selector(BaseViewController.handleRefresh(_:)),
                                 for: UIControl.Event.valueChanged)
        refreshControl.tintColor = UIColor.gray
        
        return refreshControl
    }()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    func setNavigation(){
        
        navView = UIView(frame: CGRect(x: 0, y: 0, width: ScreenSize.screenWidth, height: 76))
        navView?.backgroundColor = hexStringToUIColor(kwhite)
        
        headeImageview = CustomImageView(frame: navView!.frame)
        headeImageview?.image = UIImage(named: "header")
        navView?.addSubview(headeImageview!)
        
        
        // back Button
        backButton = CustomButton(frame: CGRect(x: 16, y: 44 , width: 27, height: 27))
        backButton!.addTarget(self, action:#selector(backButtonAction), for: .touchUpInside)
        backButton.setTitleColor(hexStringToUIColor(korange), for: .normal)
        backButton.setImage(UIImage(named: "back"), for: .normal)
        backButton.backgroundColor = .white
        
        
        titleLbl = CustomLabel(frame: CGRect(x: backButton.frame.origin.x + backButton.frame.size.width + 10, y: button_y_Axis , width: self.view.frame.size.width - (backButton.frame.origin.x + backButton.frame.size.width + 10)*2, height: 27))
        titleLbl?.font = getBoldFont(16)
        titleLbl?.textAlignment = .center
        titleLbl?.backgroundColor = .clear
        titleLbl?.textColor = .white
        
        
        // notification
        notificationButton = CustomButton(frame: CGRect(x: self.view.frame.size.width - 51, y: 44 , width: 27, height: 27))
        notificationButton!.addTarget(self, action:#selector(notificationButtonAction), for: .touchUpInside)
        notificationButton.isHidden = true
        notificationButton.setTitleColor(hexStringToUIColor(korange), for: .normal)
        notificationButton.setImage(UIImage(named: "XMLID"), for: .normal)
        notificationButton.backgroundColor = .white
        
        
        navView?.addSubview(backButton)
        navView?.addSubview(titleLbl!)
        navView?.addSubview(notificationButton)
        self.view.addSubview(navView!)
        
    }
    
    
    func setNavWithOutView(){
        
        navView = VIewShadow(frame: CGRect(x: 0, y: 0, width: ScreenSize.screenWidth, height: 99))
        navView?.backgroundColor = hexStringToUIColor(kwhite)

        navView?.layer.shadowColor = hexStringToUIColor("#DCDDDE").cgColor
        navView?.layer.shadowOffset = CGSize(width: 0, height: 2)
        navView?.layer.opacity = 1
        navView?.layer.shadowRadius = 4
        navView?.layer.masksToBounds = false
        navView?.layer.borderWidth = 1
        navView?.layer.borderColor = hexStringToUIColor("#DCDDDE").cgColor
        
        // back Button
        backButton = CustomButton(frame: CGRect(x: 16, y: button_y_Axis + 14 , width: buttonHeight, height: buttonHeight))
        backButton!.addTarget(self, action:#selector(backButtonAction), for: .touchUpInside)
        backButton.setTitleColor(hexStringToUIColor(korange), for: .normal)
        backButton.setImage(UIImage(named: "back"), for: .normal)
        backButton.backgroundColor = .white
        backButton.cornerRadius = 5
        backButton.layer.applySketchShadow(color: hexStringToUIColor("#00001A"), alpha: 0.1, x: 0, y: 0, blur: 15, spread: 0)
        backButton.layer.masksToBounds = false
        
        
        titleLbl = CustomLabel(frame: CGRect(x: backButton.frame.origin.x + backButton.frame.size.width + 10, y: 58 , width: self.view.frame.size.width - (backButton.frame.origin.x + backButton.frame.size.width + 10)*2, height: buttonHeight))
        titleLbl?.font = getBoldFont(16)
        titleLbl?.textAlignment = .center
        titleLbl?.backgroundColor = .clear
        titleLbl?.textColor = .black

        
        // notification
        rightButton = CustomButton(frame: CGRect(x: self.view.frame.size.width - 51, y: button_y_Axis + 14 , width: buttonHeight, height: buttonHeight))
        rightButton!.addTarget(self, action:#selector(rightButtonAction), for: .touchUpInside)
        rightButton.isHidden = false
        rightButton.setTitleColor(hexStringToUIColor(korange), for: .normal)
        rightButton.setImage(UIImage(named: "RightMenu"), for: .normal)
        rightButton.backgroundColor = .white
        
        rightButton.cornerRadius = 5
        rightButton.layer.applySketchShadow(color: hexStringToUIColor("#00001A"), alpha: 0.1, x: 0, y: 0, blur: 15, spread: 0)
        rightButton.layer.masksToBounds = false
        
        
        navView?.addSubview(backButton)
        navView?.addSubview(titleLbl!)

        navView?.addSubview(rightButton)
        
        self.view.addSubview(navView!)
        
    }
    
    @objc func handleRefresh(_ refreshControl: UIRefreshControl) {
    }
    
    @objc func backButtonAction() {
        self.navigationController?.popViewController(animated: true)
    }
    @objc func notificationButtonAction() {
        self.navigationController?.popViewController(animated: true)
      }
    
    @objc func rightButtonAction() {
        
    }
    
    
    @IBAction func crossButtonAction(_ sender: Any) {
        removeFromSuperView(self)
       }
}
