//
//  ChangeAddressViewController.swift
//  easyeat
//
//  Created by Ramniwas on 30/04/20.
//  Copyright © 2020 EasyEat. All rights reserved.
//

import UIKit
import GooglePlaces

protocol ChangeAddressDelegate {
    func getChangeAddress(_ cord : CLLocationCoordinate2D)
}

class ChangeAddressViewController: UIViewController,Storyboarded {
    var coordinator: MainCoordinator?
    @IBOutlet weak var searchView: VIewShadow!
    @IBOutlet weak var bgView: VIewShadow!
    @IBOutlet weak var searchBar: UISearchBar!
    
    @IBOutlet weak var tblView: UITableView!
    var placesClient: GMSPlacesClient!
    
    
    var  viewModel : ChangeAddressViewModel = {
        let model = ChangeAddressViewModel()
        return model
    }()


    override func viewDidLoad() {
        super.viewDidLoad()
        
        placesClient = GMSPlacesClient()
        ChangeAddressCell.registerWithTable(tblView)
        searchBar.becomeFirstResponder()
    }
    
    
    func searchAddress(_ search : String){
        if search.count < 2{return}
        view.endEditing(true)
        placesClient.autocompleteQuery(search, bounds: .none, filter: .none) { [weak self] (results, error) in
            print(results as Any)
            
            self?.viewModel.placesArray = [PlacesAddress]()
            for result in results! {
                var dict = PlacesAddress()
                dict.address = result.attributedFullText.string
                dict.placeID = result.placeID
                dict.locality = result.attributedPrimaryText.string
                print("Result  with placeID \(result.placeID)")
                print(result.attributedPrimaryText )
                print(result.attributedSecondaryText ?? "")
                self?.viewModel.placesArray?.append(dict)
            }
            self?.tblView.reloadData()
        }
     }
    
    func getAddressFromPlaceID(_ placeId :String){

        // Specify the place data types to return.
        let fields: GMSPlaceField = GMSPlaceField(rawValue: UInt(GMSPlaceField.name.rawValue) |
            UInt(GMSPlaceField.placeID.rawValue) | UInt(GMSPlaceField.coordinate.rawValue))!

        placesClient?.fetchPlace(fromPlaceID: placeId, placeFields: fields, sessionToken: nil, callback: {
          (place: GMSPlace?, error: Error?) in
          if let error = error {
            print("An error occurred: \(error.localizedDescription)")
            return
          }
          if let place = place {
            self.viewModel.changeAddressDelegate?.getChangeAddress(place.coordinate)
            removeFromSuperView(self)
          }
        })
    }
    
    @IBAction func currentLocationButtonAction(_ sender: Any) {
        view.endEditing(true)
        Hude.animation.show()
        UserLocation.sharedManager.startLocationManager { (locality, address, coordinate,isLocationAllow) in
            Hude.animation.hide()
            self.viewModel.changeAddressDelegate?.getChangeAddress(coordinate)
            removeFromSuperView(self)
        }
    }
    @IBAction func crossButtonAction(_ sender: Any) {
        removeFromSuperView(self)
    }
}

// UITableViewDataSource
extension ChangeAddressViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.placesArray?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.001
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0.001
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell  = tableView.dequeueReusableCell(withIdentifier: ChangeAddressCell.reuseIdentifier, for: indexPath) as! ChangeAddressCell
        cell.selectionStyle = .none
        cell.commonInt((viewModel.placesArray?[indexPath.row])!)
        return cell
    }
}

extension ChangeAddressViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
        getAddressFromPlaceID(viewModel.placesArray?[indexPath.row].placeID ?? "")
    }
}
extension ChangeAddressViewController : UISearchBarDelegate{
    
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(ChangeAddressViewController.reload), object: nil)
        self.perform(#selector(ChangeAddressViewController.reload), with: nil, afterDelay: 0.7)
    }

    @objc func reload() {
        guard let searchText = searchBar.text else { return }
        searchAddress(searchText)
    }
}
