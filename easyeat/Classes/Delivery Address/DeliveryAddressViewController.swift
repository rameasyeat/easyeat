//
//  DeliveryAddressViewController.swift
//  easyeat
//
//  Created by Ramniwas on 29/04/20.
//  Copyright © 2020 EasyEat. All rights reserved.
//

import UIKit

protocol DeliveryAddressDelegate {
    func setAddress(_ dict: DeliveryAddress)
    func addNewAddress()
}
class DeliveryAddressViewController: BaseViewController,Storyboarded {
  
    
    var coordinator: MainCoordinator?
    
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var viewBG: VIewShadow!
    
    @IBOutlet weak var tblHeightConstraint: NSLayoutConstraint!
    var viewModel : AddressViewModel = {
        let model = AddressViewModel()
        return model
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    
    fileprivate func setupUI(){
        getAddressList()
        DeliveryAddressCell.registerWithTable(tblView)
    }
    
    @IBAction func addressButtonAction(_ sender: Any) {
        viewModel.deliveryAddressDelegate?.addNewAddress()
        removeFromSuperView(self)
    }
    
    fileprivate func reloadTbl(){
        viewBG.isHidden = false
        
        if self.viewModel.addressArray?.count ?? 0 > 0{
        let index = (self.viewModel.addressArray?.firstIndex{$0.addressid == self.viewModel.selectedAddressId})
        self.viewModel.addressArray?[index ?? 0].isSelected = true
        tblView.reloadData()
        }
    }

    
    // Call Apis
    fileprivate func getAddressList(){
           var dict : [String : Any] = [String : Any]()
           dict[ktoken] =  CurrentUserInfo.authToken
           viewModel.getAddress(APIsEndPoints.kuserProfileNewAddress.rawValue,dict,handler: {[weak self](statusCode)in
            self?.reloadTbl()
           })
       }
    
//    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
//        removeFromSuperView(self)
//    }
    
}
// UITableViewDataSource
extension DeliveryAddressViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       
        return viewModel.addressArray?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell  = tableView.dequeueReusableCell(withIdentifier: DeliveryAddressCell.reuseIdentifier, for: indexPath) as! DeliveryAddressCell
        cell.selectionStyle = .none
        cell.commonInit((viewModel.addressArray?[indexPath.row])!)
        
        return cell
    }
    
}

extension DeliveryAddressViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
        
        viewModel.deliveryAddressDelegate?.setAddress((viewModel.addressArray?[indexPath.row])!)
        removeFromSuperView(self)

    }
}
