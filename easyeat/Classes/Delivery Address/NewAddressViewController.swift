//
//  NewAddressViewController.swift
//  easyeat
//
//  Created by Ramniwas on 29/04/20.
//  Copyright © 2020 EasyEat. All rights reserved.
//

import UIKit
import MapKit
import GoogleMaps

protocol NewAddressDelegate {
    func setAddress(_ dict: DeliveryAddress)
}

enum AddresTestFiledtype : Int{
    case houserNumber = 0
    case remark
}
class NewAddressViewController: BaseViewController,Storyboarded,GMSMapViewDelegate,ChangeAddressDelegate {
    var coordinator: MainCoordinator?
    
    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var pinView: UIImageView!
    @IBOutlet weak var addressLabel: CustomLabel!
    @IBOutlet weak var localityLable: CustomLabel!
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var saveButton: CustomButton!
    
    var houserNumberTextFiled: CustomTextField!
    var remarkTextFiled: CustomTextField!
    
    var isAddressAdd : Bool = false{
        didSet{
            if isAddressAdd == true{
                saveButton.alpha = 1.0
                saveButton.isUserInteractionEnabled = true
            }else{
                saveButton.alpha = 0.4
                saveButton.isUserInteractionEnabled = false
            }
        }
    }
    
    var viewModel : NewAddressViewModel = {
        let viewModel = NewAddressViewModel()
        return viewModel }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // MARK : Initial setup
        setNavWithOutView()
        navView?.layer.borderWidth = 0

        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()

        viewModel.setRestaurentLocation()
        UISetup()
        SetUpMap()
        viewModel.geoCoder = CLGeocoder()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        let scrollHeight = (0.getSafeAreaHeight() == 0) ? self.tblView.contentSize.height - (UIScreen.main.bounds.height - 78) : 0
        self.tblView.setContentOffset(CGPoint(x: 0, y: scrollHeight), animated: false)

    }
   
    
    
    func SetUpMap(){
        let camera = GMSCameraPosition.camera(withLatitude:viewModel.lat ?? 0, longitude: viewModel.lng ?? 0, zoom: 17)
        viewModel.googleMapView = GMSMapView.map(withFrame: CGRect(x: 0, y: 0,width: ScreenSize.screenWidth, height: self.mapView.frame.height), camera: camera)
        viewModel.googleMapView.delegate = self
        self.mapView.addSubview(viewModel.googleMapView)
        mapView.bringSubviewToFront(pinView)
    }
    
    private func UISetup(){
        navView?.backgroundColor = .clear
        rightButton.isHidden = true
        titleLbl?.isHidden = true
        tblView.keyboardDismissMode = .onDrag
        
        viewModel.infoArray = (self.viewModel.prepareInfo())
        SigninCell.registerWithTable(tblView)
    }
    
    
    @IBAction func changeButtonAction(_ sender: Any) {
        callChangeAddressView()
    }
    @IBAction func saveButtonAction(_ sender: Any) {
        saveAddress()
    }
    fileprivate func callChangeAddressView(){
        let controller = ChangeAddressViewController.instantiate()
        controller.viewModel.changeAddressDelegate = self
        addRootView(controller)
    }
    
    func getChangeAddress(_ cord : CLLocationCoordinate2D){
        viewModel.lat = cord.latitude
        viewModel.lng = cord.longitude
        if (viewModel.googleMapView != nil) {
            viewModel.googleMapView.removeFromSuperview()
        }
        SetUpMap()
    }
    
    private func enableSaveButton(){
        let str = viewModel.infoArray[0].value
           if str == "" {
               isAddressAdd = false
           }
           else {
               isAddressAdd = true
           }
       }
    
    private func clearData(){
        
        isAddressAdd = false
        
        for (index, _) in viewModel.infoArray.enumerated(){
            viewModel.infoArray[index].value = ""
        }
        tblView.reloadData()
        houserNumberTextFiled.becomeFirstResponder()

    }
    
    // MARK : call APIa
    fileprivate func saveAddress(){
        var dict : [String : Any] = [String : Any]()
        dict[ktoken] =  CurrentUserInfo.authToken
        dict[klat] =  viewModel.lat
        dict[klng] =  viewModel.lng
        dict[kflatDetails] =  viewModel.infoArray[0].value
        dict[klandmark] =  viewModel.infoArray[1].value
        dict[kaddress] =  addressLabel.text
        
        viewModel.saveNewAddress(APIsEndPoints.kcreateAddress.rawValue,dict, handler: {[weak self](statusCode)in
       
//            guard let dict =  self?.viewModel.setdeliveryAddress(self?.addressLabel.text ?? "") else {return}
//            self?.viewModel.newAddressDelegate?.setAddress(dict)
            self?.navigationController?.popViewController(animated: true)
        })
        
    }
    
    
    // Google   Delegate
    func mapView(_ mapView: GMSMapView, idleAt position: GMSCameraPosition) {
        
        viewModel.lat = position.target.latitude
        viewModel.lng = position.target.longitude
        let location = CLLocation(latitude:  viewModel.lat!, longitude: viewModel.lng!)
        viewModel.reverseGeocodeLocation(location) { [weak self](address, locality) in
            self?.addressLabel.text = address
            self?.localityLable.text = locality
            
            self?.clearData()
        }
    }
    
    
}

// UITableViewDataSource
extension NewAddressViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.infoArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell  = tableView.dequeueReusableCell(withIdentifier: SigninCell.reuseIdentifier, for: indexPath) as! SigninCell
        cell.selectionStyle = .none
        cell.headerLabel.textColor = .darkGray
        
        if indexPath.row == AddresTestFiledtype.houserNumber.rawValue{
            houserNumberTextFiled = cell.textFiled
            houserNumberTextFiled.returnKeyType = .next
            houserNumberTextFiled.delegate = self
            
        }
        else  if indexPath.row == AddresTestFiledtype.remark.rawValue{
            remarkTextFiled = cell.textFiled
            remarkTextFiled.delegate = self
        }
        cell.commiInit(viewModel.infoArray[indexPath.row])
        return cell
    }
}

extension NewAddressViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return CGFloat(viewModel.passwordCellHeight)
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
    }
}


extension NewAddressViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        if textField == houserNumberTextFiled {
            remarkTextFiled.becomeFirstResponder()
        }else if textField == remarkTextFiled{
            remarkTextFiled.resignFirstResponder()
        }
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
    {
        let point = tblView.convert(textField.bounds.origin, from: textField)
        let index = tblView.indexPathForRow(at: point)
        let str = (textField.text as NSString?)?.replacingCharacters(in: range, with: string)
        viewModel.infoArray[(index?.row)!].value = str ?? ""
        
        enableSaveButton()
        return true
    }
}
