//
//  ForgotPasswordViewController.swift
//  easyeat
//
//  Created by Ramniwas on 22/03/20.
//  Copyright © 2020 EasyEat. All rights reserved.
//

import UIKit
import FirebaseAuth

class ForgotPasswordViewController: BaseViewController,Storyboarded {
    var coordinator: MainCoordinator?
    
    @IBOutlet weak var tblView: UITableView!
    
    var passwordTextField: CustomTextField!
    var confirmPassword: CustomTextField!
    
    
    
    fileprivate lazy var viewModel : ForgotPasswordViewModel = {
        let viewModel = ForgotPasswordViewModel()
        return viewModel }()
    
    enum ForgotCellType : Int{
        case password = 0
        case confirmPassword
    }
    fileprivate let passwordCellHeight = 84.0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // MARK : Initial setup
        setNavigation()
        UISetup()
        
       let phoneNumber = "+919968736373"

        // This test verification code is specified for the given test phone number in the developer console.
        let testVerificationCode = "123456"

       // Auth.auth().settings?.isAppVerificationDisabledForTesting = true
        PhoneAuthProvider.provider().verifyPhoneNumber(phoneNumber, uiDelegate:nil) {
                                                                    verificationID, error in
            if ((error) != nil) {
              // Handles error
             // self.handleError(error)
              return
            }
            let credential = PhoneAuthProvider.provider().credential(withVerificationID: verificationID ?? "",
                                                                       verificationCode: testVerificationCode)
            Auth.auth().signIn(with: credential) { (AuthDataResult, Error) in
                
            }
        }
    }
    
    private func UISetup(){
        
        setNavigation()
        viewModel.infoArray = (self.viewModel.prepareInfo(dictInfo: viewModel.dictInfo))
        
        CountryCodeCell.registerWithTable(tblView)
        SigninCell.registerWithTable(tblView)
    }
    
    @IBAction func applyButtonAction(_ sender: Any) {
        
    }
}

// UITableViewDataSource
extension ForgotPasswordViewController: UITableViewDataSource {
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.infoArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell  = tableView.dequeueReusableCell(withIdentifier: SigninCell.reuseIdentifier, for: indexPath) as! SigninCell
        cell.selectionStyle = .none
        
        if indexPath.row == ForgotCellType.password.rawValue{
            passwordTextField = cell.textFiled
            passwordTextField.returnKeyType = .next
            passwordTextField.delegate = self
            cell.commiInit(viewModel.infoArray[indexPath.row])
            return cell
        }
        confirmPassword = cell.textFiled
        confirmPassword.delegate = self
        cell.commiInit(viewModel.infoArray[indexPath.row])

        return cell
    }
}

extension ForgotPasswordViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return CGFloat(passwordCellHeight)
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
    }
}

extension ForgotPasswordViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        if textField == passwordTextField {
            confirmPassword.becomeFirstResponder()
        }
        else if textField == confirmPassword{
            confirmPassword.resignFirstResponder()
        }
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
    {
        let point = tblView.convert(textField.bounds.origin, from: textField)
        let index = tblView.indexPathForRow(at: point)
        
        let str = (textField.text as NSString?)?.replacingCharacters(in: range, with: string)
        viewModel.infoArray[(index?.row)!].value = str ?? ""
        return true
    }
}
