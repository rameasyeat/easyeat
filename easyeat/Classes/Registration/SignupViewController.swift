//
//  SignupViewController.swift
//  easyeat
//
//  Created by Ramniwas on 16/03/20.
//  Copyright © 2020 EasyEat. All rights reserved.
//

import UIKit
import Lottie
class SignupViewController: BaseViewController,Storyboarded {
    var coordinator: MainCoordinator?
    
    fileprivate lazy var viewModel : SignupViewModel = {
        let viewModel = SignupViewModel()
        return viewModel }()
    
    @IBOutlet weak var signupTableView: UITableView!
    @IBOutlet weak var signinLabel: UILabel!
    
    enum SignupCellType : Int{
        case name = 0
        case phoneNumber
        case email
        case password
    }
    
    let defaultCellHeight = 84.0
    
    var nameTextFiled: CustomTextField!
    var emailTextField: CustomTextField!
    var phoneNumberTextFiled: CustomTextField!
    var passwordTextField: CustomTextField!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        UISetup()
        
    }
    
    private func UISetup(){
        
        setNavigation()
        viewModel.infoArray = viewModel.prepareInfo(dictInfo: viewModel.dictInfo)
        CountryCodeCell.registerWithTable(signupTableView)
        SigninCell.registerWithTable(signupTableView)
        
        Attributed.setText(signinLabel, kSigninlblText, korange, [kSignin],getBoldFont(CGFloat(AppFont.size12.rawValue))!)
        signinLabel.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(signupAction(gesture:))))
    }
    
    @objc func signupAction(gesture: UITapGestureRecognizer) {
               if let range = kSigninlblText.range(of: kSignin),
                  gesture.didTapAttributedTextInLabel(label: signinLabel, inRange: NSRange(range, in: kSigninlblText)) {
                backButtonAction()
              }
          }
    @IBAction func signupButtonAction(_ sender: Any) {
        
        
        viewModel.validateFields(dataStore: viewModel.infoArray) { (dict, msg, isSucess) in
           // self.coordinator?.OTPView()

           /* if isSucess {
                self.coordinator?.OTPView()
            }
            else {
                Alert(title: "", message: msg, vc: self)
            }*/
        }
    }
}
// UITableViewDataSource
extension SignupViewController: UITableViewDataSource {
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.infoArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.row == SignupCellType.phoneNumber.rawValue{
            let cell  = tableView.dequeueReusableCell(withIdentifier: CountryCodeCell.reuseIdentifier, for: indexPath) as! CountryCodeCell
            cell.selectionStyle = .none
          //  cell.headerLabel.text =  viewModel.infoArray[indexPath.row].header
            cell.mobileTextFiled.text = viewModel.infoArray[indexPath.row].value
            cell.mobileTextFiled.placeholder = viewModel.infoArray[indexPath.row].placeholder
            
            
            phoneNumberTextFiled = cell.mobileTextFiled
            phoneNumberTextFiled.delegate = self
            
            return cell
        }
        
        let cell  = tableView.dequeueReusableCell(withIdentifier: SigninCell.reuseIdentifier, for: indexPath) as! SigninCell
        cell.selectionStyle = .none
        cell.headerLabel.text =  viewModel.infoArray[indexPath.row].header
        
        switch indexPath.row {
        case SignupCellType.name.rawValue:
            nameTextFiled = cell.textFiled
            nameTextFiled.delegate = self
            
        case SignupCellType.email.rawValue:
            emailTextField = cell.textFiled
            emailTextField.delegate = self
            
        case SignupCellType.password.rawValue:

            passwordTextField = cell.textFiled
            passwordTextField.delegate = self
            
        default:
            break
        }
        
        cell.commiInit(viewModel.infoArray[indexPath.row])

        
        return cell
    }
}

extension SignupViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return CGFloat(defaultCellHeight)
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
    }
}

extension SignupViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        if textField == nameTextFiled {
            phoneNumberTextFiled.becomeFirstResponder()
        }
        else if textField == phoneNumberTextFiled{
            emailTextField.becomeFirstResponder()
        }
        else if textField == emailTextField{
            passwordTextField.becomeFirstResponder()
        }
        else if textField == passwordTextField{
            passwordTextField.resignFirstResponder()
        }
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
    {
        let point = signupTableView.convert(textField.bounds.origin, from: textField)
        let index = signupTableView.indexPathForRow(at: point)
        
        let str = (textField.text as NSString?)?.replacingCharacters(in: range, with: string)
        viewModel.infoArray[(index?.row)!].value = str ?? ""
        
        if textField == phoneNumberTextFiled {
            return (textField.text?.count ?? 0) < 10 || string == ""
        }
        return true
    }
}
