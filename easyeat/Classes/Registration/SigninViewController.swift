//
//  SigninViewController.swift
//  easyeat
//
//  Created by Ramniwas on 16/03/20.
//  Copyright © 2020 EasyEat. All rights reserved.
//

import UIKit
import Lottie
//import FacebookLogin
//import FacebookCore
import GoogleSignIn


class SigninViewController: BaseViewController,Storyboarded {
    var coordinator: MainCoordinator?
    
    @IBOutlet weak var signInTablView: UITableView!
    @IBOutlet weak var forgotPasswordLabel: CustomLabel!
    @IBOutlet weak var signupLabel: CustomLabel!
    
    var phoneNumberTextFiled: CustomTextField!
    var passwordTextField: CustomTextField!
    
    fileprivate lazy var viewModel : SigninViewModel = {
        let viewModel = SigninViewModel()
        return viewModel }()
    
    enum SigninCellType : Int{
        case country = 0
        case password
    }
    fileprivate let passwordCellHeight = 84.0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // MARK : Initial setup
        UISetup()
    }
    
    override func viewDidAppear(_ animated: Bool) {
//        GIDSignIn.sharedInstance().presentingViewController = self
//      //  GIDSignIn.sharedInstance().delegate = self
        
    }
    
    private func UISetup(){
        
        setNavigation()
        viewModel.infoArray = (self.viewModel.prepareInfo(dictInfo: viewModel.dictInfo))
        
        CountryCodeCell.registerWithTable(signInTablView)
        SigninCell.registerWithTable(signInTablView)
        
        Attributed.setText(forgotPasswordLabel, kForgotlblText, korange, [kReset],getBoldFont(CGFloat(AppFont.size12.rawValue))!)
        
        Attributed.setText(signupLabel, kSiginuplblText, korange, [kSignUp],getBoldFont(CGFloat(AppFont.size12.rawValue))!)
        
        forgotPasswordLabel.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(forgotAction(gesture:))))
        
        signupLabel.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(signupAction(gesture:))))
    }
    
    // MARK : button Action
    
    @objc func forgotAction(gesture: UITapGestureRecognizer) {
        if let range = kForgotlblText.range(of: kReset),
            gesture.didTapAttributedTextInLabel(label: forgotPasswordLabel, inRange: NSRange(range, in: kForgotlblText)) {
            coordinator!.goToForgotPassword()
        }
    }
    @objc func signupAction(gesture: UITapGestureRecognizer) {
        if let range = kSiginuplblText.range(of: kSignUp),
            gesture.didTapAttributedTextInLabel(label: signupLabel, inRange: NSRange(range, in: kSiginuplblText)) {
            coordinator?.goToSignupView()
        }
    }
    
    @IBAction func signinButtonAction(_ sender: Any) {
        
        
        /* viewModel.validateFields(dataStore: viewModel.infoArray) { (dict, msg, isSucess) in
         if isSucess {
         /*  guard let url = URL(string: "http://google.com?test=toto&test2=titi") else {
         return
         }
         NetworkManager.shared.getRequest(url, true,"") { (responce, code) in
         
         Utility.app.AlertWithAction(title: "", message: "Login", vc: self, alterHandller: {(index)in
         print(index)
         
         })
         print("responce %@",responce)
         }*/
         } else {
         Alert(title: "", message: msg, vc: self)
         }
         }*/
        
    }
    @IBAction func fbButtonAction(_ sender: Any) {
       /* let loginManager = LoginManager()
        loginManager.logOut()
        
        loginManager.logIn(
            permissions: [.publicProfile, .email],
            viewController: self
        ) { result in
            
            switch result {
            case .failed(let error):
                print(error)
            case .cancelled:
                print("User cancelled login.")
            case .success( _,  _, let accessToken):
                
                
                let facebookRequest = GraphRequest(graphPath: "/me", parameters: [:], httpMethod: .get)
                facebookRequest.start(completionHandler: {(connection,result,error) in
                    
                    let info = result as! [String : AnyObject]
                    if info["name"] as? String != nil {
                    }
                    
                })
                
                print("accessToken: " + String(describing: accessToken))
                break
                
            }
        }*/
    }
    
    @IBAction func googleButtonAction(_ sender: Any) {
        GIDSignIn.sharedInstance().signIn()
    }
}

// UITableViewDataSource
extension SigninViewController: UITableViewDataSource {
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.infoArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.row == SigninCellType.country.rawValue{
            let cell  = tableView.dequeueReusableCell(withIdentifier: CountryCodeCell.reuseIdentifier, for: indexPath) as! CountryCodeCell
            cell.selectionStyle = .none
            
            phoneNumberTextFiled = cell.mobileTextFiled
            phoneNumberTextFiled.delegate = self
            cell.commonInit(viewModel.infoArray[indexPath.row])
            
            return cell
        }
        
        let cell  = tableView.dequeueReusableCell(withIdentifier: SigninCell.reuseIdentifier, for: indexPath) as! SigninCell
        cell.selectionStyle = .none
        
        passwordTextField = cell.textFiled
        passwordTextField.delegate = self
        cell.commiInit(viewModel.infoArray[indexPath.row])
        
        return cell
    }
}

extension SigninViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == SigninCellType.password.rawValue{
            return CGFloat(passwordCellHeight)
        }
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
    }
}

extension SigninViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        if textField == phoneNumberTextFiled {
            passwordTextField.becomeFirstResponder()
        }
        else if textField == passwordTextField{
            passwordTextField.resignFirstResponder()
        }
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
    {
        let point = signInTablView.convert(textField.bounds.origin, from: textField)
        let index = signInTablView.indexPathForRow(at: point)
        
        let str = (textField.text as NSString?)?.replacingCharacters(in: range, with: string)
        viewModel.infoArray[(index?.row)!].value = str ?? ""
        
        if textField == phoneNumberTextFiled {
            return (textField.text?.count ?? 0) < 10 || string == ""
        }
        return true
    }
}


