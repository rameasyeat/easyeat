//
//  CountryCodeViewController.swift
//  easyeat
//
//  Created by Ramniwas on 28/03/20.
//  Copyright © 2020 EasyEat. All rights reserved.
//

import UIKit

protocol CountryCodeDelegate {
    func getCountryCode(_ dict : Countries)
}
class CountryCodeViewController: UIViewController,Storyboarded {
    var coordinator: MainCoordinator?
    
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var bgView: UIView!
    
     lazy var viewModel : CountryCodeViewModel = {
        let viewModel = CountryCodeViewModel()
        return viewModel }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tblView.keyboardDismissMode = .onDrag
        UISetup()

    }
    
    private func UISetup(){
        getCountryCode()
        CountryCell.registerWithTable(tblView)
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        removeFromSuperView(self)
    }
    
    
    // MARk : Apply Search
    
      // MARK : Search delegate
     func applySearch(_ searchText : String){
         if searchText != ""{
             viewModel.countryArray?.removeAll()
             
             let filteredArray = viewModel.defaultCountryArray?.filter { ($0.name?.localizedCaseInsensitiveContains(searchText))! }
             if filteredArray?.count ?? 0 > 0{
                 viewModel.countryArray?.append(contentsOf: filteredArray!)
             }}
         else {
             viewModel.countryArray = viewModel.defaultCountryArray
         }
         tblView.reloadData()
     }
    
    // MARK : Apis call
    
    fileprivate func getCountryCode(){
          let dict : [String : Any] = [String : Any]()
        viewModel.getCountryCode(APIsEndPoints.kCountryCodeList.rawValue, dict) {[weak self](statusCode) in
            self?.bgView.isHidden = false
            self?.tblView.reloadData()
             self?.bgView.animShow(0.25)

        }
    }
    
  
}

// UITableViewDataSource
extension CountryCodeViewController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
           let view = viewModel.setHeaderView(section)
           return view
           
       }
       func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
     
           return CGFloat(viewModel.headerHeight)
       }
    
        func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            return viewModel.countryArray?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell  = tableView.dequeueReusableCell(withIdentifier: CountryCell.reuseIdentifier, for: indexPath) as! CountryCell
        cell.selectionStyle = .none
        
        cell.commiInit((viewModel.countryArray?[indexPath.row])!)

        return cell
    }
    
}

extension CountryCodeViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 54
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
        viewModel.countryCodeDelegate?.getCountryCode((viewModel.countryArray?[indexPath.row])!)
            removeFromSuperView(self)
    }
    
    func removeFromSuperView(_ controller : UIViewController){
        controller.willMove(toParent: nil)
        controller.view.removeFromSuperview()
        controller.removeFromParent()
    }
}

extension CountryCodeViewController : UISearchBarDelegate{
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(ChangeAddressViewController.reload), object: nil)
        self.perform(#selector(CountryCodeViewController.reload), with: nil, afterDelay: 0.3)
    }
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        reload()
        view.endEditing(true)

    }

    @objc func reload() {
        guard let searchText = searchBar.text else { return }
        applySearch(searchText)
    }
}
