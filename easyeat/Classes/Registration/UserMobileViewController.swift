//
//  UserMobileViewController.swift
//  easyeat
//
//  Created by Ramniwas on 28/03/20.
//  Copyright © 2020 EasyEat. All rights reserved.
//

import UIKit

class UserMobileViewController: UIViewController,Storyboarded,CountryCodeDelegate {
    
    var coordinator: MainCoordinator?
   
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var proceedButton: CustomButton!
    
    @IBOutlet weak var imgView3Width: NSLayoutConstraint!
    @IBOutlet weak var imgView3Height: NSLayoutConstraint!
    @IBOutlet weak var imgView1x_axis: NSLayoutConstraint!
    @IBOutlet weak var imgView2x_axis: NSLayoutConstraint!
    
    let passwordCellHeight = 104.0

    
    fileprivate lazy var viewModel : SigninViewModel = {
        let viewModel = SigninViewModel()
        return viewModel }()
    
    var setBorderColor : Bool = false {
        didSet{
            if setBorderColor == false{
                viewModel.viewPhoneNumber?.borderColor = hexStringToUIColor(kgray)
                viewModel.hintImageWidth.constant = 16
            }
            else{
                viewModel.viewPhoneNumber?.borderColor = hexStringToUIColor(korange)
                viewModel.hintImageWidth.constant = 16
            }
        }
    }
    
    var isNumberValide : Bool = false {
        didSet{
            if isNumberValide == false{
                viewModel.hintImageView.image =  #imageLiteral(resourceName: "cross-1")
                proceedButton.alpha = 0.4
                proceedButton.isUserInteractionEnabled = false
            }
            else{
                viewModel.hintImageView.image =  #imageLiteral(resourceName: "checkmark")
                
                if viewModel.infoArray[0].countryCode != ""{
                    proceedButton.alpha = 1
                    proceedButton.isUserInteractionEnabled = true
                }
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel.centerImageWidth = imgView3Width.constant
        viewModel.centerImageheight = imgView3Height.constant
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(notification:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        
        UISetup()
    }
    
    private func UISetup(){
        
        let height = CFloat(headerView.frame.size.height) + CFloat(passwordCellHeight)
        if Float(ScreenSize.screenHeight) < height{
            
            let screenHeight  = Float(ScreenSize.screenHeight)
            let  headerHeight = screenHeight - height
            headerView.frame.size =  CGSize(width:  headerView.frame.size.width, height: (CGFloat(height + headerHeight - CFloat(passwordCellHeight))))
        }
        tblView.keyboardDismissMode = .onDrag
        viewModel.infoArray = (self.viewModel.prepareInfo(dictInfo: viewModel.dictInfo))
        CountryCodeCell.registerWithTable(tblView)
    }
        
    @objc private func keyboardWillShow(notification: NSNotification) {
        
        self.tblView.contentInset = UIEdgeInsets(top: -320, left: 0, bottom: 0, right: 0)
        self.imgView1x_axis.constant =  -20
        self.imgView2x_axis.constant = -90
        
        self.imgView3Height.constant = viewModel.centerImageheight - viewModel.animationShowHeight
        self.imgView3Width.constant = viewModel.centerImageWidth - viewModel.animationShowHeight
        
        showAnimation(notification,self)
    }
    
    @objc private func keyboardWillHide(notification: NSNotification) {
        /*tblView.contentInset = .zero
        tblView.contentOffset.y = 0
        self.imgView1x_axis.constant =  animationHideHeight
        self.imgView2x_axis.constant = animationHideHeight*/
    }
    
    @IBAction func proceedButtonAction(_ sender: Any) {
        CurrentUserInfo.dailCode = viewModel.infoArray[0].countryCode
        coordinator?.goToOTPView(viewModel.infoArray[0])
    }
    
    @objc func countryCodeButtonAction(){
        view.endEditing(true)
        let controller = CountryCodeViewController.instantiate()
        controller.viewModel.countryCodeDelegate = self
        addRootView(controller)
       
    }
    
    func getCountryCode(_ dict: Countries) {
        
        if viewModel.infoArray[0].value.count >= 8{
            isNumberValide = true
        }
        viewModel.infoArray[0].countryCode = dict.dial_code ?? ""
        viewModel.viewCountryCode?.borderColor = hexStringToUIColor(korange)
        
        tblView.reloadData()
    }
}

// UITableViewDataSource
extension UserMobileViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.infoArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell  = tableView.dequeueReusableCell(withIdentifier: CountryCodeCell.reuseIdentifier, for: indexPath) as! CountryCodeCell
        cell.selectionStyle = .none
        
        viewModel.viewCountryCode = cell.viewCountryCode
        viewModel.viewPhoneNumber = cell.viewPhoneNumber
        viewModel.hintImageView =  cell.hintImageView
       viewModel.hintImageWidth = cell.hintImageWidth
        viewModel.phoneNumberTextFiled = cell.mobileTextFiled
        viewModel.phoneNumberTextFiled.delegate = self
        cell.countryCodeButton.addTarget(self, action: #selector(countryCodeButtonAction), for: .touchUpInside)
        
        cell.commonInit(viewModel.infoArray[indexPath.row])
        
        return cell
        
    }
  
}

extension UserMobileViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return CGFloat(passwordCellHeight)
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
    }
}

extension UserMobileViewController: UITextFieldDelegate {
        func textFieldDidBeginEditing(_ textField: UITextField) {
        setBorderColor = true
        viewModel.hintImageWidth.constant = 0
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        if textField == viewModel.phoneNumberTextFiled {
            viewModel.phoneNumberTextFiled.resignFirstResponder()
        }
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
    {
        setBorderColor = true
        isNumberValide = false
        
        let point = tblView.convert(textField.bounds.origin, from: textField)
        let index = tblView.indexPathForRow(at: point)
        
        let str = (textField.text as NSString?)?.replacingCharacters(in: range, with: string)
        viewModel.infoArray[(index?.row)!].value = str ?? ""
        
        if str == "" {setBorderColor = false}
        if str?.count ?? 0 >= 8  {isNumberValide = true}
        
//        if textField == viewModel.phoneNumberTextFiled {
//            return (textField.text?.count ?? 0) < 11 || string == ""
//        }
        return true
    }
}


