//
//  OTPViewController.swift
//  easyeat
//
//  Created by Ramniwas on 17/03/20.
//  Copyright © 2020 EasyEat. All rights reserved.
//

import Foundation
import UIKit
import FirebaseAuth



class OTPViewController: BaseViewController,Storyboarded,OTPTimerDelegate {
    
    var coordinator: MainCoordinator?
    
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var phoneNummberLabel: CustomLabel!
    @IBOutlet weak var otpLabel: CustomLabel!
    @IBOutlet weak var verifyButton: CustomButton!
    @IBOutlet weak var resendButton: CustomButton!
    @IBOutlet weak var resendOTPLabel: CustomLabel!
    
    @IBOutlet weak var otpView: PHOTPView!
    
    var enteredOtp: String = ""
    var config:PinConfig! = PinConfig()
    
    lazy var viewModel : OTPViewModel = {
        let viewModel = OTPViewModel()
        return viewModel }()
    
    
    var isValidate: Bool = true

    
    var isOTPDisable: Bool = true {
        didSet {
            if isOTPDisable == true {
                verifyButton.isUserInteractionEnabled = false
                verifyButton.alpha = 0.4
            } else {
                verifyButton.isUserInteractionEnabled = true
                verifyButton.alpha = 1
            }
        }
    }
    
    var isOTPSend: Bool = false {
        didSet {
            if isOTPSend == false {
                resendOTPLabel.text = "Resend Verification Code"
                resendOTPLabel.textColor = hexStringToUIColor(korange)
                resendButton.isUserInteractionEnabled = true
                resendButton.borderColor = hexStringToUIColor(korange)
                otpLabel.text = ""
            } else {
                resendButton.isUserInteractionEnabled = false
                resendButton.borderColor = hexStringToUIColor(kgray)
                resendOTPLabel.textColor = hexStringToUIColor(kgray)
            }
        }
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.UISetup()
        setupVarificationPINView(kgray)
        setNavWithOutView()
        navView?.backgroundColor = .clear
        navView?.layer.borderWidth = 0
        titleLbl?.isHidden = true
        rightButton.isHidden = true
       self.tblView.contentInset = UIEdgeInsets(top: -340, left: 0, bottom: 0, right: 0)
        sendOTP(viewModel.dictInfo?.value ?? "")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
   
    private func UISetup(){
        
        viewModel.otpTimerDelegate = self
        tblView.keyboardDismissMode = .onDrag
        //  CountryCodeCell.registerWithTable(tblView)
        phoneNummberLabel.text = "Enter the 6 digit OTP sent on \(viewModel.dictInfo?.countryCode ?? "") \(viewModel.dictInfo?.value ?? "") to proceed"
    }
    
    
    // OTP Timer
    func grtOTPCount(_ counter: String) {
        
        if counter.count > 1{
            resendOTPLabel.text = "Resend Verification Code 00:\(counter )"
        }else  {
            resendOTPLabel.text = "Resend Verification Code 00:0\(counter)"
        }
        
        if Int(counter) == 0{
            isOTPSend = false
        }else {
            isOTPSend = true
        }
    }
    
    func startTimer(){
        viewModel.startTimer()
    }
    
    
    fileprivate func setinValidBorder(){
        otpLabel.text = "Invalid OTP"
        otpLabel.textColor = hexStringToUIColor(kred)
        setupVarificationPINView(kred)
    }
    
    func sendOTPMessage(){
       // self.firstTextFiled.becomeFirstResponder()
        otpLabel.text = "OTP sent"
        otpLabel.textColor = hexStringToUIColor(kgreen)
        
        (otpView.viewWithTag(1) as? PHOTPTextField)?.becomeFirstResponder()

    }
    
    @IBAction func verifyButtonAction(_ sender: Any) {
        veriftyOTP()
      
    }
    @IBAction func resendButtonAction(_ sender: Any) {
        self.view.endEditing(true)

        startTimer()
        isOTPDisable = true
        setupVarificationPINView(kgray)
        sendOTP(viewModel.dictInfo?.value ?? "")
    }
    
    // MARK Send OTP
    
    
    fileprivate func veriftyOTP(){
        self.view.endEditing(true)
              Hude.animation.show()
              
              let credential = PhoneAuthProvider.provider().credential(withVerificationID: viewModel.verificationID ?? "",verificationCode: enteredOtp)
              Auth.auth().signIn(with: credential) {[weak self] (AuthDataResult, Error) in
                  
                  Hude.animation.hide()
                  
                  if Error != nil {
                      self?.isOTPDisable = true
                      self?.setinValidBorder()
                      self?.isValidate = false
                  }
                  else {
                      
                      let tokenChangeListener: IDTokenDidChangeListenerHandle?
                      
                      tokenChangeListener = Auth.auth().addIDTokenDidChangeListener() { (auth, user) in
                          if let user = user {
                              user.getIDToken { idToken, error in
                                  if let error = error {
                                      // Handle error
                                      print("getIDToken error: \(error)")
                                      return;
                                  }
                                  self?.viewModel.dictInfo?.verificationCode = idToken ?? ""
                                  self?.registerUser()
                              }
                          }
                      }
                  }
              }
    }
    
    fileprivate func sendOTP(_ phoneNumber : String){
        
        Hude.animation.show()
        let numberWithCountryCode = (viewModel.dictInfo?.countryCode ?? "") + phoneNumber
        PhoneAuthProvider.provider().verifyPhoneNumber(numberWithCountryCode, uiDelegate:nil) {
            [weak self](verificationID, error) in
            Hude.animation.hide()
            
            if ((error) != nil) {
                
                Alert(title: kError, message: error?.localizedDescription.description ?? "", vc: RootViewController.controller!)
                return
            }
            
            self?.startTimer()
            
            
            self?.viewModel.verificationID = verificationID
            self?.sendOTPMessage()
            
        }
    }
    
    func backAction(){
        guard let views =  self.navigationController?.viewControllers else {return}
        for view in views{
            if view.isKind(of: CartViewController.self){
                self.navigationController?.popToViewController(view, animated: true)
                return
            }
        }
        CurrentUserInfo.isSkip = "1"
        coordinator?.goToTabbarController()
    }
    
    // MARK APIs Call
    
    fileprivate func registerUser(){
        var dict : [String : Any] = [String : Any]()
        dict[kidtoken] = viewModel.dictInfo?.verificationCode
        dict[kphone_no] = viewModel.dictInfo?.value
        dict[kdial_code] = viewModel.dictInfo?.countryCode
        dict[kplatform] = kios
        dict[kcart_token] = CurrentUserInfo.randomToken
        
        // check user name exist or not
        viewModel.registerUser(APIsEndPoints.ksignupUser.rawValue,dict, handler: {[weak self](result,statusCode)in
            
            if let status = result.status, status == 1 {
                CurrentUserInfo.userName = result.usersignUpData?.user_name
                CurrentUserInfo.authToken = result.usersignUpData?.token
                self?.backAction()
                
            }
            else {
                self?.coordinator?.goToUserNameView(dict)
            }
        })
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
       

        override func didReceiveMemoryWarning()
        {
            super.didReceiveMemoryWarning()
            // Dispose of any resources that can be recreated.
        }
}


extension OTPViewController
{
   
    private func setupVarificationPINView(_ defaultColor : String)
    {
        config.otpFieldDisplayType = .square
        config.otpFieldSeparatorSpace = 10
        config.otpFieldSize = 40
        config.otpFieldsCount = 6
        config.otpFieldEntrySecureType = false
        config.cursorColor = hexStringToUIColor(korange)
        config.otpFieldDefaultBorderColor = hexStringToUIColor(defaultColor)
        config.otpFieldEnteredBorderColor = hexStringToUIColor(korange)
        config.otpFieldErrorBorderColor = hexStringToUIColor(kred)
        config.otpFieldBorderWidth = 1
        config.shouldAllowIntermediateEditing = false

        otpView.config = config
        otpView.delegate = self
        // Create the UI
        otpView.initializeUI()
    }
    
   
  
}
extension OTPViewController
{
    ///Actions
    
}
extension OTPViewController: PHOTPViewDelegate
{
    func hasEnteredAllOTP(hasEntered: Bool) -> Bool
    {
        return true
    }
    
    func shouldBecomeFirstResponderForOTP(otpFieldIndex index: Int) -> Bool
    {
        otpLabel.text = (isValidate == false) ? "" : otpLabel.text
        return true
    }
    
    func getenteredOTP(otpString: String) {
        isValidate = true
        isOTPDisable = false
        enteredOtp = otpString
        veriftyOTP()
    }
}
