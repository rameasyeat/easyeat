//
//  UserNameViewController.swift
//  easyeat
//
//  Created by Ramniwas on 30/03/20.
//  Copyright © 2020 EasyEat. All rights reserved.
//

import UIKit

class UserNameViewController: UIViewController,Storyboarded {
    
    var coordinator: MainCoordinator?
    var previousOffset : CGFloat = 0
    var animationShowHeight : CGFloat = 50
    var animationHideHeight : CGFloat = -140
    
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var proceedButton: CustomButton!
    
    @IBOutlet weak var TCLabel: UILabel!
    @IBOutlet weak var imgView3Width: NSLayoutConstraint!
    @IBOutlet weak var imgView3Height: NSLayoutConstraint!
    @IBOutlet weak var imgView1x_axis: NSLayoutConstraint!
    @IBOutlet weak var imgView2x_axis: NSLayoutConstraint!
    @IBOutlet weak var checkMarkButton: UIButton!
    
    var centerImageWidth : CGFloat = 0
    var centerImageheight : CGFloat = 0
    
    var viewShadow : VIewShadow?
    var nameTextFiled: CustomTextField!
    
     lazy var viewModel : UserNameViewModel = {
        let viewModel = UserNameViewModel()
        return viewModel }()
    
    var setBorderColor : Bool = false {
        didSet{
            if setBorderColor == false{
                viewShadow?.borderColor = hexStringToUIColor(kgray)
            }
            else{
                viewShadow?.borderColor = hexStringToUIColor(korange)
            }
        }
    }
    
    var isCheckMarkEnable : Bool = false{
        didSet{
            if isCheckMarkEnable == true && isValide == true{
                checkMarkButton.setImage( #imageLiteral(resourceName: "CheckboxSelected"), for: .normal)
                proceedButton.alpha = 1
                proceedButton.isUserInteractionEnabled = true
            }
                else if isCheckMarkEnable == true{
                    checkMarkButton.setImage( #imageLiteral(resourceName: "CheckboxSelected"), for: .normal)
                }
            else {
                checkMarkButton.setImage( #imageLiteral(resourceName: "Checkbox"), for: .normal)
                proceedButton.alpha = 0.4
                proceedButton.isUserInteractionEnabled = false
            }
        }
    }

    
    var isValide : Bool = false {
        didSet{
            if isValide == false {
                proceedButton.alpha = 0.4
                proceedButton.isUserInteractionEnabled = false
            }
            else if isCheckMarkEnable == true{
                proceedButton.alpha = 1
                proceedButton.isUserInteractionEnabled = true
            }
        }
    }
    fileprivate let passwordCellHeight = 54.0
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        centerImageWidth = imgView3Width.constant
        centerImageheight = imgView3Height.constant
        self.tblView.contentInset = UIEdgeInsets(top: -340, left: 0, bottom: 0, right: 0)
        UISetup()
    }
    
    private func UISetup(){
        
         TCLabel.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(requestAction(gesture:))))
        tblView.keyboardDismissMode = .onDrag
        viewModel.infoArray = (self.viewModel.prepareInfo(dictInfo: viewModel.dictInfo))
        UserNameCell.registerWithTable(tblView)
    }
    
    // MARK Button Action
    
    @IBAction func checkMarkButtonAction(_ sender: Any) {
        isCheckMarkEnable = !isCheckMarkEnable
    }
    @objc func requestAction(gesture: UITapGestureRecognizer) {
         if let range = kTC.range(of: TCService),
             gesture.didTapAttributedTextInLabel(label: TCLabel, inRange: NSRange(range, in: kSpecifiRequest)) {
            coordinator?.goTOWebView(.TC)
         }
     }
   @objc private func keyboardWillShow(notification: NSNotification) {
        
        self.tblView.contentInset = UIEdgeInsets(top: -300, left: 0, bottom: 0, right: 0)
        self.imgView1x_axis.constant =  -20
        self.imgView2x_axis.constant = -90
        
        self.imgView3Height.constant = centerImageheight - animationShowHeight
        self.imgView3Width.constant = centerImageWidth - animationShowHeight
        
        showAnimation(notification,self)
    }
    
    @IBAction func proceedButtonAction(_ sender: Any) {
        view.endEditing(true)
        registerUser()
    }
    
    // MARK APIs Call
       
       fileprivate func registerUser(){
        viewModel.param?[kusername] = viewModel.infoArray[0].value
         viewModel.param?[kcart_token] = CurrentUserInfo.randomToken
        viewModel.registerUserName(APIsEndPoints.kRegisterUserName.rawValue,viewModel.param!, handler: {[weak self](result,statusCode)in
            CurrentUserInfo.userName = result.usersignUpData?.user_name
            CurrentUserInfo.authToken = result.usersignUpData?.token
            CurrentUserInfo.isSkip = "1"
            self?.coordinator?.goToTabbarController()
           })
       }
}



// UITableViewDataSource
extension UserNameViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell  = tableView.dequeueReusableCell(withIdentifier: UserNameCell.reuseIdentifier, for: indexPath) as! UserNameCell
        
        cell.selectionStyle = .none
        
        viewShadow = cell.shadowView
        cell.nameTextField.delegate = self
        nameTextFiled = cell.nameTextField
        cell.commonInit(viewModel.infoArray[0])
        
        return cell
    }
    
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
//        let diff = previousOffset - scrollView.contentOffset.y
//        previousOffset = scrollView.contentOffset.y
      }
}

extension UserNameViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return CGFloat(passwordCellHeight)
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
    }
}

extension UserNameViewController: UITextFieldDelegate {
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        setBorderColor = true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
            textField.resignFirstResponder()
        
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
    {
        setBorderColor = true
        isValide = false
        
        let point = tblView.convert(textField.bounds.origin, from: textField)
        let index = tblView.indexPathForRow(at: point)
        
        let str = (textField.text as NSString?)?.replacingCharacters(in: range, with: string)
        
        viewModel.infoArray[(index?.row)!].value = str?.firstCapitalized ?? ""
        
        if str == "" {setBorderColor = false}
        if str?.count ?? 0 >= 1  {isValide = true}
              
        return true
    }
}

extension UIImageView{
    func rotate() {
        let rotation : CABasicAnimation = CABasicAnimation(keyPath: "transform.rotation.z")
        rotation.toValue = NSNumber(value: -(Double.pi * 2))
        rotation.duration = 10
        self.layer.add(rotation, forKey: "rotationAnimation")
    }
}
