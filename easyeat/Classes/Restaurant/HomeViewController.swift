//
//  HomeViewController.swift
//  easyeat
//
//  Created by Ramniwas on 12/05/20.
//  Copyright © 2020 EasyEat. All rights reserved.
//

import UIKit
import SkeletonView

class HomeViewController: BaseViewController,Storyboarded,RuningOrderViewDelegate,MQTTDelegates {
    
    var coordinator: MainCoordinator?
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var addressLabel: CustomLabel!
    @IBOutlet weak var searchbar: UISearchBar!
    @IBOutlet weak var searchBGView: VIewShadow!
    @IBOutlet weak var topLineShadow: VIewShadow!
    @IBOutlet weak var tblBottomHeight: NSLayoutConstraint!
    @IBOutlet weak var noSearchView: UIView!
    @IBOutlet weak var noSearchMessage: CustomLabel!
    
    var viewModel : HomeViewModel = {
        let model = HomeViewModel()
        return model
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        HomeCell.registerWithTable(tblView)
        tblView.isSkeletonable = true
        view.showAnimatedSkeleton()
        
        getUserLocation()
        setupUI()

    }
    override func viewWillAppear(_ animated: Bool) {
           super.viewWillAppear(animated)
                getCartList(true)
       }
    
    override func viewDidDisappear(_ animated: Bool) {
           super.viewDidAppear(animated)
//           NotificationCenter.default.removeObserver(kMQTTNotificationCenter)
       }
    
    override func handleRefresh(_ refreshControl: UIRefreshControl) {
        refreshControl.endRefreshing()
        reSetPageIndex()
        self.getRestaurantList(false)
    }
    
    fileprivate func reSetPageIndex(){
        viewModel.pageIndex = 1
        //viewModel.restArray?.removeAll()
    }
    
    // SsetupUI
    fileprivate func setupUI(){
                
        noSearchMessage.text = kNoSearchMessage
        searchbar.delegate = self
        tblView.keyboardDismissMode = .onDrag
        tblView.addSubview(refreshControl)
        
        searchBGView.layer.applySketchShadow(color: .white, alpha: 1, x: 0, y: 0, blur: 3, spread: 0)
        
        changeSearchBarBGColor()
        getRestaurentDetails()
        reSetPageIndex()
        viewModel.distanceValue = viewModel.minDistance
        
        

    }
    
   
    
    func getUserLocation() {
        UserLocation.sharedManager.startLocationManager {[weak self] (address, locality, cord,isLocationAllow) in
           
            self?.viewModel.userCurrentLocation = cord
            self?.viewModel.currentLocationLocality = locality
            self?.reSetPageIndex()
            self?.viewModel.distanceValue = self?.viewModel.minDistance ?? 0
            self?.getRestaurantList(true)
        }
    }
    
    func changeSearchBarBGColor(){
        if let textField = searchbar.value(forKey: "searchField") as? UITextField {
            textField.backgroundColor = .white
            let backgroundView = textField.subviews.first
            if #available(iOS 11.0, *) {
                backgroundView?.subviews.forEach({ $0.removeFromSuperview() })
            }
            backgroundView?.layer.cornerRadius = 10.5
            backgroundView?.layer.masksToBounds = true
        }
    }
    
    fileprivate func updateData(_ status : Int){
        if status == 0{
            reSetPageIndex()
            self.viewModel.distanceValue = self.viewModel.maxDistance
            self.getRestaurantList(false)
        }
        else{
            tblView.isHidden = false
            addressLabel.text = viewModel.currentLocationLocality
            self.tblView.reloadData()
        }
    }
    
    private func goToMenu(_ restaurantID : String,_ tblID : String){
        CurrentUserInfo.restaurantId = restaurantID
        CurrentUserInfo.tblID = tblID
       // coordinator?.goToMenuView() // open old menu with change order type
        coordinator?.goToMenuSegment()
    }
    
    fileprivate func generateRandomToken(){
        let str  : String = ""
        
//        if CurrentUserInfo.randomToken == nil || CurrentUserInfo.randomToken == ""{
            CurrentUserInfo.randomToken = str.randomToken()
      //  }
    }
    
    @objc func directionButtonAction(_ sender : UIButton){
        guard let restLocation = viewModel.restArray?[sender.tag].location  else {return}
        coordinator?.goToMapView(viewModel.userCurrentLocation, (restLocation))
    }
        
    func viewOrderAction() {
        
       if  viewModel.isCartEmpty == false{
        CurrentUserInfo.isScanQRCode = "1"
        goToMenu(CurrentUserInfo.restaurantId, CurrentUserInfo.tblID)
        }
        else {
            guard let dict = viewModel.restaurantDict else {return}
            coordinator?.goToOrderView(dict)
        }
    }
    
    // MARK : Search delegate
    func applySearch(_ searchText : String){
        noSearchView.isHidden = true

        if searchText != ""{
            viewModel.restArray?.removeAll()
            //  let filterArray = viewModel.searchArray?.filter{($0.name?.contains(searchText))!}
            
            let filteredArray = viewModel.searchArray?.filter { ($0.name?.localizedCaseInsensitiveContains(searchText))! }
            if filteredArray?.count ?? 0 > 0{
                viewModel.restArray?.append(contentsOf: filteredArray!)
            }
            else if filteredArray?.count == 0{
                noSearchView.isHidden = false
            }
        }
        else {
            viewModel.restArray = viewModel.searchArray
        }
        tblView.reloadData()
    }
    
    // MQTT Responce
    
    func updateQMTTResponce(_ dict: [String : Any]) {
         viewModel.handleOrderStatus(dict,{[weak self](mType, orderCancelBy) in
            self?.callGetRuningStatus(mType,orderCancelBy)
        })
        
    }
//    @objc func getMQTTResponce(notification: Notification) {
//        viewModel.handleOrderStatus((notification.userInfo as? [String : Any])!,{[weak self](mType, orderCancelBy) in
//           self?.callGetRuningStatus(mType,orderCancelBy)
//        })
//       }
    
    fileprivate func callGetRuningStatus(_ mtype : MType,_ cancelOrder : OrderCancelBy){
        
        if mtype == MType.OCL || mtype == MType.RELOAD || mtype == MType.OCM{ // get runing status
            self.getCartList(false)
        }
        else if mtype == MType.OCL {
            viewModel.isRuningOrder = false
        }
    }
    
    fileprivate func updateTblBottomHeight(){
        tblBottomHeight.constant = CGFloat(viewModel.runingOrderConstraintHeight) + CGFloat(viewModel.tabbarHeight) +  CGFloat(0.getSafeAreaHeight())
    }
    
    // APIS call
    
    fileprivate func getRestaurantList(_ isHude : Bool){
        
        if isHude == false && (viewModel.totalPageCount > viewModel.pageIndex) {loadMore()}
        
        let locationDict = [viewModel.userCurrentLocation.longitude,viewModel.userCurrentLocation.latitude]
        
        var dict : [String : Any] = [String : Any]()
        dict[kfields] =  viewModel.json(from: ["name", "location", "address", "city", "logo", "nameid","id","status"])
        dict[klocation] = viewModel.json(from: locationDict)
        dict[kmax_distance] = viewModel.distanceValue
        dict[kpage_no] = viewModel.pageIndex
        dict[klimit] =  viewModel.norders
        
        print(dict)
        viewModel.getRestaurantList(APIsEndPoints.kgetRestaurantList.rawValue,dict,isHude, handler: {[weak self](statusCode)in
            self?.updateData(statusCode)
            self?.hideSkeletonLoading()
            self?.tblView.tableFooterView?.isHidden = true
            self?.tblView.tableFooterView = nil

        })
    }
    
    fileprivate func loadMore(){
        if viewModel.spiner === nil {
            viewModel.spiner = UIActivityIndicatorView(style: .gray)
        }
        viewModel.spiner?.startAnimating()
        viewModel.spiner?.frame = CGRect(x: CGFloat(0), y: CGFloat(0), width: tblView.bounds.width, height: CGFloat(44))

        self.tblView.tableFooterView = viewModel.spiner
        self.tblView.tableFooterView?.isHidden = false
    }
    
    fileprivate func getCartList(_ isLoadMQTT : Bool){
         var dict : [String : Any] = [String : Any]()
         dict[krestaurant_id] = CurrentUserInfo.restaurantId
         dict[kcart_token] = CurrentUserInfo.randomToken
         dict[ktable_id] =  CurrentUserInfo.tblID
         dict[ktoken] =  CurrentUserInfo.authToken ?? kdefaultToken
         
         getItemAddedInCart(APIsEndPoints.kGetCartList.rawValue,dict,false, handler: {[weak self](result,statusCode)in
          
            self?.view.addSubview((self?.viewModel.addRuningOrderView(result, isLoadMQTT))!)
            self?.viewModel.runingOrderView?.runingOrderDelegate = self
            self?.viewModel.socketConnection?.delegates = self
            self?.updateTblBottomHeight()

         })
     }
    
    fileprivate func getRestaurentDetails(){
        
        guard let restauranrId = CurrentUserInfo.restaurantId else {return}
           var dict : [String : Any] = [String : Any]()
           dict[krestaurant_id] = restauranrId
           viewModel.getRestaurentDetails(APIsEndPoints.krestaurantDetails.rawValue,dict, handler: {(result,statusCode)in
           })
       }
    
    fileprivate func hideSkeletonLoading(){
        searchBGView.hideSkeleton()
        tblView.hideSkeleton()
    }
}


// UITableViewDataSource
extension HomeViewController: UITableViewDataSource,UITableViewDelegate,SkeletonTableViewDataSource,SkeletonTableViewDelegate{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.restArray?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.01
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0.01
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell  = tableView.dequeueReusableCell(withIdentifier: HomeCell.reuseIdentifier, for: indexPath) as! HomeCell
        cell.selectionStyle = .none
        cell.directionButton.tag = indexPath.row
        cell.commonInit((viewModel.restArray?[indexPath.row]))
        cell.directionButton.addTarget(self, action: #selector(directionButtonAction(_:)), for: .touchUpInside)
        
        if  indexPath.row > ((viewModel.restArray?.count ?? 0) - 2){
            getRestaurantList(false)
               }
        
        return cell
    }
    
    func collectionSkeletonView(_ skeletonView: UITableView, cellIdentifierForRowAt indexPath: IndexPath) -> ReusableCellIdentifier {
       return HomeCell.reuseIdentifier
    }
    func collectionSkeletonView(_ skeletonView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return 10
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let restID = viewModel.restArray?[indexPath.row].id else {
            return
        }
        CurrentUserInfo.isScanQRCode = nil
        CurrentUserInfo.priceByOrderType = priceByOrderType.dining
        goToMenu(restID, "dl")
    }
}

extension HomeViewController: UISearchBarDelegate {
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchbar.resignFirstResponder()
        reload()
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(ChangeAddressViewController.reload), object: nil)
        self.perform(#selector(ChangeAddressViewController.reload), with: nil, afterDelay: 0.5)
    }
    @objc func reload() {
        guard let searchText = searchbar.text else { return }
        applySearch(searchText)
    }
}



