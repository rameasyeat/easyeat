//
//  MenuViewController.swift
//  easyeat
//
//  Created by Ramniwas on 26/03/20.
//  Copyright © 2020 EasyEat. All rights reserved.
//

import UIKit
import MXScroll

class MenuViewController: BaseViewController,Storyboarded,SearchItemDelegate,ViewCartdelegate,MenuIndexFilterDelegate,UpdateItemCountDelegate,TableHeaderDelegate,RuningOrderViewDelegate,MenuFilterDelegate,RestaurantTypeDelegate,VariationDelegate,MQTTDelegates,ItemsDetailsDelegates{
   
    
    var coordinator: MainCoordinator?
    
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var noRecordView: UIView!
    
    @IBOutlet weak var mainScrollView: UIScrollView!
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var searchView: SearchItemView?
    
    @IBOutlet weak var mainScrollViewTop: NSLayoutConstraint!
    @IBOutlet weak var menuHeightConstraint: NSLayoutConstraint?
    @IBOutlet weak var tblBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var headerViewHeight: NSLayoutConstraint!
    
    
    lazy var viewModel : MenuViewModel = {
        let model = MenuViewModel()
        return model
    }()
    
    var cornerLayerWidth:CGFloat = 0.0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel.setInitialData()
        
        mainScrollView?.delegate = self
        viewModel.menuPopView = MenuPopView()
        searchView?.searchDelegate = self
        
        
        viewModel.reloadMQTT = true
        viewModel.customMenuButton?.addTarget(self, action:#selector(menuButtonAction), for: .touchUpInside)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.view.isHidden = false
        tblView.isHidden = false
        hideBottomTabInCaseCameraOpen()
        UISetup()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        updateCartItems()

    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
    }
    
    
    fileprivate func UISetup(){
        RestaurantItemCell.registerWithTable(tblView)
    }
    
    fileprivate  func updateCartItems(){
        viewModel.getMenuStructure()
        updateResponce()
        getCartList()
    }
    
    fileprivate func updateResponce(){
        self.headerView.isHidden = false
        self.mainScrollView.isHidden = false
        showNorecordView()
    }
    
    // MARK Button Action
    
    @objc func menuButtonAction() {
        let controller = MenuIndexFilterViewController.instantiate()
        controller.viewModel.menuItemArray = viewModel.menuItemArray
        controller.viewModel.menuButtonYaxis = Float( (viewModel.customMenuButton?.frame.origin.y)!)
        controller.viewModel.inxedDelegate = self
        addRootView(controller)
    }
    
    @IBAction func resetFilterButtonAction(_ sender: Any) {
        
        // clear data
        viewModel.filterCategoryArray?.removeAll()
        searchView?.searchTextField.text = ""
        viewModel.searchText = ""
        searchView?.switchToggle.isOn = false
        
        // reload tbl
        viewModel.menuItemArray = viewModel.saveItemArray
        tblView.reloadData()
        showNorecordView()
    }
    
    override func backButtonAction() {
        let views  = (self.navigationController?.viewControllers)!
        for view in views{
            if view is TabbarController{
                self.navigationController?.popToViewController(view, animated: true)
                return
            }
        }
        coordinator?.goToTabbarController()
    }
    
    // MARK Delegates
    func applySearch(_ search: String) {
        viewModel.searchText = search
        viewModel.isFilterApply = true
        tblView.reloadData()
        showNorecordView()
    }
    
    func updateVarientItem(_ result : CartListData) {
        getCartList()
    }
    func updateAddItemsDetails() {
        getCartList()
    }
    
    func applyFilter(_ isFilter: Bool) {
        view.endEditing(true)
        
        let controller = MenuFilterViewController.instantiate()
        controller.viewModel.restaurantFilter = viewModel.restaurantFilter
        
        if let count = viewModel.filterCategoryArray?.count, count > 0{
            controller.viewModel.filterCategoryArray = viewModel.filterCategoryArray!
        }
        controller.viewModel.menuFilterDelegate = self
        addRootView(controller)
    }
    
    func applyMenuFilter(_ array: [FilterSubcategory]) {
        
        if array.count == 0 {return}
        let  arrayFilter = array.filter{$0.filterType == "preference"}
        
        if arrayFilter[0].filterArray[0].name?.uppercased() == "VEG" && arrayFilter[0].filterArray[0].isSelected == true{
            searchView?.switchToggle.isOn = true
        }
        else {
            searchView?.switchToggle.isOn = false
            viewModel.isVegFilter = false
        }
        
        viewModel.filterCategoryArray = array
        viewModel.isFilterApply = true
        tblView.reloadData()
        showNorecordView()
    }
    
    func vegToggleSwitch(_ isSwitchOn: Bool) {
        
        viewModel.filterCategoryArray?.removeAll()
        viewModel.isVegFilter = isSwitchOn
        viewModel.isFilterApply = true
        tblView.reloadData()
        showNorecordView()
    }
    
    func viewCart() {
        
        guard let dict = viewModel.restaurantDict else {return}
        print(dict.type!)
        coordinator?.goToCartView(dict)
    }
    
    fileprivate func showNorecordView(){
        
        Hude.animation.hide()
        
        if viewModel.menuItemArray?.count == 0 {
            noRecordView.isHidden = false
            tblView.isHidden = true
            mainScrollView.isScrollEnabled = false
            viewModel.customMenuButton?.isHidden = true
        }
        else{
            noRecordView.isHidden = true
            tblView.isHidden = false
            mainScrollView.isScrollEnabled = true
            viewModel.customMenuButton?.isHidden = false

        }
    }
    
    func getFilterIndex(_ index: Int) {
        let  indexPath = IndexPath(row: 0, section: index)
        tblView.scrollToRow(at: indexPath, at: .none, animated: false)
    }
    
    func viewOrderAction() {
        guard let dict = viewModel.restaurantDict else {return}
        coordinator?.goToOrderView(dict)
    }
    
    func getHeaderClick(_ index: Int) {
        
        guard let isClose = viewModel.menuItemArray?[index].isheaderClose else {return}
        viewModel.menuItemArray?[index].isheaderClose = !isClose
        tblView.reloadSections([index], with: .none)
    }
    
    
    func updateQMTTResponce(_ dict: [String : Any]) {
        
        viewModel.handleOrderStatus(dict,{[weak self](mType, orderCancelBy) in
            self?.callGetRuningStatus(mType,orderCancelBy)
        })
    }
    
    fileprivate func callGetRuningStatus(_ mtype : MType,_ cancelOrder : OrderCancelBy){
        
        if mtype == MType.OCL || mtype == MType.RELOAD || mtype == MType.OCM{ // get runing status
            self.getCartList()
        }
        else if mtype == MType.OCL {// cancel status
            manageBottomViewHeight(false)
        }
    }
    
    
    fileprivate func getCartList(){
        var dict : [String : Any] = [String : Any]()
        dict[krestaurant_id] = CurrentUserInfo.restaurantId
        dict[kcart_token] = CurrentUserInfo.randomToken
        dict[ktable_id] =  CurrentUserInfo.tblID
        dict[ktoken] =  CurrentUserInfo.authToken ?? kdefaultToken
        
        getItemAddedInCart(APIsEndPoints.kGetCartList.rawValue,dict,false, handler: {[weak self](result,statusCode)in
            self?.viewModel.itemAddedCartArray = result
            
            if self?.viewModel.reloadMQTT == true{
                self?.viewModel.reloadMQTT = false
                self?.viewModel.callMQTTserver()
                self?.viewModel.socketConnection?.delegates = self
            }
            CurrentUserInfo.currency = result.cart?.currency
            CurrentUserInfo.userdId = result.cart?.user_id
            
            DispatchQueue.main.async {
                self?.updateBottmView(result.cart!)
              //  self?.checkItemsInCart()
                self?.tblView.reloadData()
            }
            
        })
    }
    
    
    fileprivate func checkItemsInCart(){

        if viewModel.itemAddedCartArray?.cart?.cart_items?.count ?? 0 == 0{
            viewModel.customMenuButton?.isHidden = false
        }
        else if viewModel.itemAddedCartArray?.cart?.order?.items?.count ?? 0 > 0{
            viewModel.customMenuButton?.isHidden = false
        }
        else{
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.60, execute: {[weak self] in
                self?.viewModel.customMenuButton?.isHidden = false
            })
        }
    }
    

    
     func updateOnChangeSegmentView( _ index : Int){
        viewModel.reloadMQTT = true
        searchView?.switchToggle.isOn = false
        viewModel.restaurantDict?.type = (index == 0) ? RestaurentType.dining.rawValue : RestaurentType.delivery.rawValue
        viewWillAppear(true)
        viewDidAppear(false)
    }
    
    fileprivate func showItemView(_ isShow : Bool){
        if viewModel.cardItemView != nil && isShow == false{
            hideCardItemView(0.50)
            return
        }
    }
    
    func selectedRestaurant(restaurantType : RestaurentType, viewHeight: Int) {
        //        OrderTypeView()
        headerViewHeight.constant += CGFloat(viewHeight)
    }
    
    
    func updateCount(_ indexPath: IndexPath, _ itemCount: Int,_ addRemove : Int) {
        
        if self.viewModel.menuItemArray?.count == 0 {return}
        let item =  self.viewModel.menuItemArray?[indexPath.section].item_Array[indexPath.row]
        self.viewModel.menuItemArray?[indexPath.section].item_Array[indexPath.row].quantity = "\(itemCount)"
        let addedItems = viewModel.itemAddedCartArray?.cart?.cart_items?.filter{$0.item_id == item?.id}
        
        if addedItems?.count ?? 0 > 0  && addRemove == 1 && item?.gvariations != nil{
            let dict = viewModel.isItemContaintVariation(item!)
            let groupArray = viewModel.getvariation(dict)
            
            let controller = CustomizationViewController.instantiate()
            controller.viewModel.variationDelegate = self
            controller.viewModel.groupArray = groupArray
            controller.viewModel.selectedGroupIndex = 0
            controller.viewModel.itemDetails = viewModel.getItemDetails(item!)
            controller.viewModel.addedItem = addedItems?[0]
            controller.viewModel.restaurantDict = viewModel.restaurantDict
            addRootView(controller)
            return
            
        }
        else if viewModel.isItemContaintVariation(item!).isEmpty == false && addRemove == 1{// check varitation
            let dict = viewModel.isItemContaintVariation(item!)
            let groupArray = viewModel.getvariation(dict)
            
            let controller = VariationViewController.instantiate()
            controller.viewModel.variationDelegate = self
            controller.viewModel.groupArray = groupArray
            controller.viewModel.fromViewType = .menu
            controller.viewModel.selectedGroupIndex = 0
            controller.viewModel.itemDetails = viewModel.getItemDetails(item!)
            controller.viewModel.restaurantDict = viewModel.restaurantDict
            
            addRootView(controller)
            return
        }
        
        addItemInCart(APIsEndPoints.kItemInCart.rawValue,self.viewModel.addItemDict(item!, addRemove), handler: {[weak self](result,statusCode)in
            self?.tblView.reloadRows(at: [indexPath], with: .none)
            self?.viewModel.itemAddedCartArray = result
            self?.updateBottmView(result.cart!)
        })
    }
    
    override func viewDidLayoutSubviews(){
        let safeArea = CGFloat( 0.getSafeAreaHeight())
        let extraHeight = 78  + safeArea
        mainScrollView?.contentSize.height = ((tblView.contentSize.height + extraHeight) + headerViewHeight.constant)
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        let restViewHeight = 0
        
        if scrollView == self.tblView {
            if Int(scrollView.contentOffset.y) > viewModel.constantHeaderHeight{
                mainScrollView?.contentOffset.y = CGFloat(restViewHeight)
                return
            }
            else{
                let diff = CGFloat(viewModel.previousOffset) - (scrollView.contentOffset.y )
                viewModel.previousOffset = Int(scrollView.contentOffset.y)
                
                var newHeight = scrollView.contentOffset.y + diff
                if Int(scrollView.contentOffset.y) > Int(restViewHeight){
                    newHeight = CGFloat(restViewHeight)
                }
                mainScrollView.contentOffset.y = newHeight
            }
        }
        else {
            if Int(scrollView.contentOffset.y) > viewModel.constantHeaderHeight{
                mainScrollView?.contentOffset.y = CGFloat(restViewHeight)
                return
            }
            else{
                var newHeight = scrollView.contentOffset.y
                if Int(scrollView.contentOffset.y) > Int(restViewHeight){
                    newHeight = CGFloat(restViewHeight)
                }
                mainScrollView.contentOffset.y = newHeight
            }
        }
    }
}

// UITableViewDataSource
extension MenuViewController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.viewModel.menuItemArray!.count
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return (self.viewModel.menuItemArray?[section].item_Array.count)!
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = viewModel.setHeaderView(section)
        view.tableHeaderDelegate = self
        return view
        
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return CGFloat(viewModel.headerHeight)
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return nil
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell  = tableView.dequeueReusableCell(withIdentifier: RestaurantItemCell.reuseIdentifier, for: indexPath) as! RestaurantItemCell
        cell.selectionStyle = .none
        cell.isHidden = false
        
        cell.tblView = tblView
        cell.updateItemCountDelegate = self
        cell.restaurentDict = viewModel.restaurantDict
        
        if  self.viewModel.menuItemArray?.count == 0{return cell}
        guard let subcategory = self.viewModel.menuItemArray?[indexPath.section].item_Array else {return cell}
       
        if subcategory.count > indexPath.row {
            cell.commonInit((subcategory[indexPath.row]))
        }
        
        // hide cell
        if (self.viewModel.menuItemArray?[indexPath.section].isheaderClose)!{
            cell.isHidden = true
        }
        return cell
    }
}

extension MenuViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if self.viewModel.menuItemArray?.count ?? 0 > 0 && self.viewModel.menuItemArray?[indexPath.section].isheaderClose == false{
            return UITableView.automaticDimension
        }
        return 0
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
        

        
        guard let subcategory = self.viewModel.menuItemArray?[indexPath.section].item_Array else {return }
            let item  = subcategory[indexPath.row]
        let controller = ItemDetailsViewController.instantiate()
        controller.viewModel.itemDetails = item
        controller.viewModel.coordinator = coordinator
        controller.viewModel.variationDelegate = self
        controller.viewModel.fromViewType = .menu
        controller.viewModel.selectedGroupIndex = 0
        controller.viewModel.itemsDetailsDelegates = self
        controller.viewModel.restaurantDict = viewModel.restaurantDict
        addRootView(controller)
    }
}

extension MenuViewController:MXViewControllerViewSource{
    func viewForMixToObserveContentOffsetChange() -> UIView {
        return self.tblView
    }
}





