//
//  MenuPopView.swift
//  easyeat
//
//  Created by Ramniwas on 13/04/20.
//  Copyright © 2020 EasyEat. All rights reserved.
//

import Foundation
import UIKit

class MenuPopView : MenuViewController{
    
    
    
}

extension MenuViewController {
    // bottom order view
    
    func addOrderView(_ result : Cart){
        
        
        let view = viewModel.segmentView?.view.viewWithTag(2021) //already added runing view
        if view != nil {
            
            view?.frame.origin.y =  ScreenSize.screenHeight - CGFloat(viewModel.cardItemViewHeight)
            viewModel.runingOrderView = view as? RunningOrderView
            viewModel.setHeightAccordingRuningOrder()
            viewModel.addRuningOrderView()

            viewModel.runingOrderView?.runingOrderDelegate = self
            viewModel.runingOrderView?.animShow(0)
        }
        else  if viewModel.runingOrderView == nil{
            viewModel.setHeightAccordingRuningOrder()
            viewModel.addRuningOrderView()
            viewModel.runingOrderView?.tag = 2021
            viewModel.runingOrderView?.runingOrderDelegate = self
            viewModel.segmentView?.view.addSubview(viewModel.runingOrderView!)
            viewModel.segmentView?.view.bringSubviewToFront(viewModel.runingOrderView!)
            viewModel.runingOrderView?.animShow(Float(viewModel.animationTime))
        }
        else {
            
            if viewModel.totalQuantity == 0 || viewModel.totalQuantity == 1 {
                viewModel.setHeightAccordingRuningOrder()
                viewModel.addRuningOrderView()
                viewModel.runingOrderView?.animShow(Float(viewModel.animationTime))
            }
        }
        tblBottomConstraint.constant = CGFloat(viewModel.cardItemViewHeight + viewModel.runingOrderConstraintHeight)
        
    }
    
    func hideRuningOrderView(_ duration : Float){
        if viewModel.runingOrderView != nil{
            viewModel.isItemAddedInCart = false
            tblBottomConstraint.constant = CGFloat(viewModel.cardItemViewHeight + viewModel.runingOrderConstraintHeight)
            viewModel.runingOrderView?.animHide(duration)
        }
    }
    
    // Bottm item cart view
    func addCardItemView(){
        
        viewModel.isItemAddedInCart = true
        viewModel.addCartBottomPoView()
        viewModel.cardItemView?.tag = 2020
        
        viewModel.cardItemView?.viewCartDelegate = self
        viewModel.segmentView?.view.addSubview(viewModel.cardItemView!)
        viewModel.segmentView?.view.bringSubviewToFront(viewModel.cardItemView!)
        
        tblBottomConstraint.constant = CGFloat(viewModel.cardItemViewHeight + viewModel.runingOrderConstraintHeight)
        manageBottomViewHeight(true)
        viewModel.cardItemView?.animShow(Float(viewModel.animationTime))
    }
    
    func hideCardItemView(_ duration : Float){
        if viewModel.cardItemView != nil{
            manageBottomViewHeight(false)
            viewModel.isItemAddedInCart = false
            tblBottomConstraint.constant = CGFloat(viewModel.cardItemViewHeight + viewModel.runingOrderConstraintHeight)
            viewModel.cardItemView?.animHide(duration)
        }
    }
    
    // MARK : ViewCart
    func updateBottmView(_ result : Cart){
        
        viewModel.totalQuantity = 0
        
        for dict in (result.cart_items!){
            viewModel.totalQuantity +=  dict.quantity!
        }
        
        // cart item
        if result.cart_items?.count ?? 0 > 0{
            self.viewModel.checkAlreadyAddedItemsInCart()
            
            let view = viewModel.segmentView?.view.viewWithTag(2020) //already added bottom view
            if view != nil {
                view?.frame.origin.y = ScreenSize.screenHeight
                viewModel.cardItemView = view as? CardItemPopView
                
                viewModel.isItemAddedInCart = true
                viewModel.setHeightAccordingItemAdded()
                
                manageBottomViewHeight(true)
                tblBottomConstraint.constant = CGFloat(viewModel.cardItemViewHeight + viewModel.runingOrderConstraintHeight)
                viewModel.cardItemView?.animShow(0)
            }
            else if (viewModel.cardItemView == nil || viewModel.totalQuantity  ==  0) && view == nil {
                addCardItemView()
            }
            else  if viewModel.totalQuantity  ==  1 && viewModel.isItemCountViewShow == false{
                viewModel.isItemAddedInCart = true
                manageBottomViewHeight(true)
                tblBottomConstraint.constant = CGFloat(viewModel.cardItemViewHeight + viewModel.runingOrderConstraintHeight)
                viewModel.cardItemView?.animShow(Float(viewModel.animationTime))
            }
        }
        else { // hide cart view
            hideCardItemView(Float(viewModel.animationTime))
        }
        
        if result.order?.items?.count ?? 0 > 0{
            checkRuningOrder(result)
        }else{
            hideRuningOrder(Float(viewModel.animationTime))
        }
        
        self.viewModel.cardItemView?.currencyLabel.text = "\(viewModel.totalQuantity) ITEMS"
        hideBottomTabInCaseCameraOpen()
    }
    
     func hideBottomTabInCaseCameraOpen(){
        // hide bottom views in case camera open
              if CurrentUserInfo.isScanQRCode == nil && CurrentUserInfo.priceByOrderType == .dining  {
                         viewModel.hideMenuButton(0)
                         hideRuningOrder(0)
                         hideCardItemView(0)
                     }
              else{
                  viewModel.hideMenuButton(CurrentUserInfo.priceByOrderType.rawValue)
              }
    }
    
    fileprivate func hideRuningOrder(_ duration : Float){
        viewModel.isRuningOrder = false
        manageBottomViewHeight(false)
        tblBottomConstraint.constant = CGFloat(viewModel.cardItemViewHeight + viewModel.runingOrderConstraintHeight)
        viewModel.runingOrderView?.animHide(duration)
    }
    
    func checkRuningOrder(_ result : Cart){
        
        viewModel.isRuningOrder = true
        manageBottomViewHeight(true)
        addOrderView(result)
        setRuningOrderData(result)
    }
    
    fileprivate func setRuningOrderData(_ result : Cart){
        
        if result.order?.delivery_partner == "1"{
            viewModel.runingOrderView?.partnerDeliveryOrderStatus(result.order)
        }
        else  if viewModel.restaurantDict?.type == RestaurentType.delivery.rawValue{
            viewModel.runingOrderView?.orderStatusForDelivery(result)
        }
        else{
            viewModel.runingOrderView?.orderStatusForDinig(result)
        }
    }
    
    func manageBottomViewHeight(_ isItemCountViewShow : Bool){
        if isItemCountViewShow == true {
            menuHeightConstraint?.constant = CGFloat(viewModel.runingOrderConstraintHeight + viewModel.cardItemViewHeight + viewModel.menuMinBottomBottom)
        }else {
            menuHeightConstraint?.constant = CGFloat(viewModel.menuMinBottomBottom)
        }
        viewModel.isItemCountViewShow = isItemCountViewShow
        

    }
    
}
