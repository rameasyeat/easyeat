//
//  MenuIndexFilterViewController.swift
//  easyeat
//
//  Created by Ramniwas on 04/04/20.
//  Copyright © 2020 EasyEat. All rights reserved.
//

import UIKit

protocol MenuIndexFilterDelegate {
    func getFilterIndex(_ index : Int)
}
class MenuIndexFilterViewController:UIViewController,Storyboarded {
    @IBOutlet weak var tblBottomHeight: NSLayoutConstraint!
    @IBOutlet weak var tblHeight: NSLayoutConstraint!
    
    lazy var viewModel : InxedFilterViewModel = {
        let viewModel = InxedFilterViewModel()
        return viewModel }()
    
    var coordinator: MainCoordinator?
    @IBOutlet weak var tblView: UITableView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        UISetup()
        setTableBottomHeight()
    }
    override func viewDidLayoutSubviews(){
              let viewHeight = tblView.contentSize.height
        tblHeight.constant =  viewModel.tblMaxHeight > Int(viewHeight) ? viewHeight : CGFloat(viewModel.tblMaxHeight)
          }
    
    fileprivate func setTableBottomHeight(){
        let remainHeight = Float(ScreenSize.screenHeight) - (viewModel.menuButtonYaxis ?? 0)
        tblBottomHeight.constant = CGFloat(remainHeight + 10)
    }
    
    private func UISetup(){
        tblView.keyboardDismissMode = .onDrag
        IndexFilterCell.registerWithTable(tblView)
        
        // round up table
        
        tblView.layer.cornerRadius = 10
        tblView.layer.maskedCorners = [.layerMaxXMinYCorner,.layerMinXMaxYCorner,.layerMinXMinYCorner]

        tblView.layer.masksToBounds = true
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        removeFromView()
    }
    
     fileprivate func removeFromView(){
           for v in self.navigationController!.viewControllers{
            if v is MenuIndexFilterViewController{
                   removeFromSuperView(v)
               }
           }
       }
    
}

// UITableViewDataSource
extension MenuIndexFilterViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.menuItemArray?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell  = tableView.dequeueReusableCell(withIdentifier: IndexFilterCell.reuseIdentifier, for: indexPath) as! IndexFilterCell
        cell.selectionStyle = .none
        cell.commonInit((viewModel.menuItemArray?[indexPath.row])!)
        return cell
        
    }
    
}

extension MenuIndexFilterViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
        viewModel.inxedDelegate?.getFilterIndex(indexPath.row)
        removeFromView()

    }
}
