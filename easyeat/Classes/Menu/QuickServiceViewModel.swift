//
//  QuickServiceViewModel.swift
//  easyeat
//
//  Created by Ramniwas on 16/04/20.
//  Copyright © 2020 EasyEat. All rights reserved.
//

import Foundation
import ObjectMapper

class QuickServiceViewModel{
    var servicesArray : [Services]?
    var orderId : String?

    
    func checkServicesStatus(){
        servicesArray = servicesArray?.filter{$0.status == 1}
    }
    
    func getServiceItems(_ apiEndPoint: String,_ param : [String : Any], handler: @escaping (Int) -> Void) {
           guard let url = URL(string: Configuration().environment.baseURL + apiEndPoint) else {return}
           NetworkManager.shared.postRequest(url, true, "", params: param, networkHandler: {(responce,statusCode) in
            
            
               let dictResponce =  Mapper<RestaurantDetails>().map(JSON: responce)

               if let status = dictResponce?.status, status == 1 {
                handler(status)
               }
               else {
                   Alert(title: kError, message: dictResponce?.message ?? "", vc: RootViewController.controller!)
               }
           })
       }
}
