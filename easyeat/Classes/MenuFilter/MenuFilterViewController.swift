//
//  MenuFilterViewController.swift
//  easyeat
//
//  Created by Ramniwas on 14/04/20.
//  Copyright © 2020 EasyEat. All rights reserved.
//

import UIKit

protocol MenuFilterDelegate {
    func applyMenuFilter(_ array : [FilterSubcategory])
}
class MenuFilterViewController: BaseViewController,Storyboarded,SelectedFilterDelegate {
    
    var coordinator: MainCoordinator?
    
    @IBOutlet weak var applyButton: CustomButton!
    @IBOutlet weak var tblView: UITableView!
    
    var viewModel : FilterViewModel = {
        let model = FilterViewModel()
        return model
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        setupUI()
    }
    
    fileprivate func setupUI(){
        viewModel.convertFilterArrayToSubcategoryArray()
        FilterTblCell.registerWithTable(tblView)
    }
    
    @IBAction func applyButtonAction(_ sender: Any) {
        viewModel.menuFilterDelegate?.applyMenuFilter(viewModel.filterCategoryArray)
        removeView()
    }
   
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        removeView()
    }
    
    func removeView(){
        removeFromSuperView(self)

    }
    
    func getFilterData(_ index: Int, _ dict: Filters) {
        viewModel.getSelectedDict(index,dict)
       
    }
    
   /* fileprivate func setFilter(){
        var dict : [String : Any] = [String : Any]()
        dict[ktoken] = CurrentUserInfo.authToken ?? kdefaultToken
        dict[kfoodFilters] = viewModel.selectedArray
        viewModel.applyFilters(APIsEndPoints.kMenuFilters.rawValue,dict, handler: {[weak self](result,statusCode)in
            self?.removeView()
        })
    }*/
}

// UITableViewDataSource
extension MenuFilterViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.filterCategoryArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        let cell  = tableView.dequeueReusableCell(withIdentifier: FilterTblCell.reuseIdentifier, for: indexPath) as! FilterTblCell
        cell.filterDelegate = self
        cell.selectionStyle = .none
        cell.commonInit(viewModel.filterCategoryArray[indexPath.row])
        return cell
    }
}

extension MenuFilterViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return CGFloat(viewModel.filterCelHeight)
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
    }
}
