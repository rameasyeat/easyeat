//
//  OrderTypeViewController.swift
//  easyeat
//
//  Created by Ramniwas on 12/06/20.
//  Copyright © 2020 EasyEat. All rights reserved.
//

import UIKit

protocol OrderTypeDelegate {
    func selectedOrderType(_ type : Int)
}

class OrderTypeViewController: UIViewController,Storyboarded {
    var coordinator: MainCoordinator?
    
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var bgView: UIView!
    
     lazy var viewModel : OrderTypeViewModel = {
        let viewModel = OrderTypeViewModel()
        return viewModel }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel.getOrderType()
        UISetup()
    }
    
    private func UISetup(){
        OrderTypeCell.registerWithTable(tblView)
    }
    
    @IBAction func crossButtonAction(_ sender: Any) {
        removeFromSuperView(self)
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        removeFromSuperView(self)
    }
  
}

// UITableViewDataSource
extension OrderTypeViewController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
        func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            return viewModel.orderTypeArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell  = tableView.dequeueReusableCell(withIdentifier: OrderTypeCell.reuseIdentifier, for: indexPath) as! OrderTypeCell
        cell.selectionStyle = .none
        
        cell.commiInit(dict: viewModel.orderTypeArray[indexPath.row], indexPath.row)

        return cell
    }
    
}

extension OrderTypeViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 64
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
        viewModel.orderTypeDelegate?.selectedOrderType(indexPath.row )
            removeFromSuperView(self)
    }
    
    func removeFromSuperView(_ controller : UIViewController){
        controller.willMove(toParent: nil)
        controller.view.removeFromSuperview()
        controller.removeFromParent()
    }
}

