//
//  AddonsViewController.swift
//  easyeat
//
//  Created by Ramniwas Patidar on 14/07/20.
//  Copyright © 2020 EasyEat. All rights reserved.
//

import UIKit

protocol DelegateOnBackAddons {
    func onbackAction()
}
class AddonsViewController: BaseViewController,Storyboarded {
    
    @IBOutlet weak var saveButton: CustomButton!
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var variationTitle: CustomLabel!
    @IBOutlet weak var variationType: CustomLabel!
    @IBOutlet weak var priceLabel: CustomLabel!
    @IBOutlet weak var viewBottomHeight: NSLayoutConstraint!
    @IBOutlet weak var tblHeight: NSLayoutConstraint!

    
    var viewModel : AddonsViewModel = {
        let viewModel = AddonsViewModel()
        return viewModel }()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        UISetup()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.view.isHidden = false
        
    }
    
    private func UISetup(){
        VariationCell.registerWithTable(tblView)
        viewBottomHeight.constant += CGFloat(0.getSafeAreaHeight())/2

        variationTitle?.text = viewModel.itemDetails?.name
        variationType?.text = "Select add ons"
        viewModel.addonsArray?[0].isSelected = true
        updatePrice(0)
    }
    
    fileprivate func updatePrice(_ index : Int){
        
        let selectedVariation = viewModel.groupArray?[viewModel.selectedGroupIndex].variations?.filter{$0.isSelected == true}[0]
        priceLabel.text = CurrentUserInfo.currency + " \(selectedVariation?.price ?? 0)"
    }
    
    override func viewDidLayoutSubviews(){
           let viewHeight = tblView.contentSize.height + viewModel.subViewHeight + CGFloat(0.getSafeAreaHeight())
           tblHeight.constant =  (ScreenSize.screenHeight - viewModel.tblTopAxis) > viewHeight ? viewHeight : (ScreenSize.screenHeight - viewModel.tblTopAxis)
       }
    
    /// MARK : Button Action
    
    @IBAction func saveButtonAction(_ sender: Any) {
        addItemInCart(APIsEndPoints.kItemInCart.rawValue,self.viewModel.addItemDict(viewModel.itemDetails!), handler: {[weak self](result,statusCode)in
            self?.viewModel.variationDelegate?.updateVarientItem(result)
            
            self?.removeAllSubView(true)
        })
    }
    
    
    @IBAction func backButtonAction(_ sender: Any) {
        viewModel.delegateOnBackAddons?.onbackAction()
        removeFromSuperView(self)
    }
    
    override func crossButtonAction(_ sender: Any) {
        removeAllSubView(false)
    }
    
    fileprivate func removeAllSubView(_ reloadCartItem : Bool){
        
        for v in self.navigationController!.viewControllers{
            if v is VariationViewController || v is AddonsViewController || v is CustomizationViewController{
                removeFromSuperView(v)
            }
        }
    }
    
    
}

//// UITableViewDataSource
extension AddonsViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.addonsArray?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell  = tableView.dequeueReusableCell(withIdentifier: VariationCell.reuseIdentifier, for: indexPath) as! VariationCell
        cell.selectionStyle = .none
        
        guard let dict = viewModel.addonsArray?[indexPath.row] else {return cell}
        cell.addonsCommiInit(dict)
        
        return cell
    }
}

extension AddonsViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return CGFloat(viewModel.cellHeight)
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
        
        guard let dict = viewModel.addonsArray?[indexPath.row] else {return}
        
        if dict.isSelected == true{
            viewModel.addonsArray?[indexPath.row].isSelected = false
        }
        else{
            for (index, _) in (viewModel.addonsArray?.enumerated())!{
                viewModel.addonsArray?[index].isSelected = false
            }
            viewModel.addonsArray?[indexPath.row].isSelected = true
        }
        
        updatePrice(indexPath.row)
        tableView.reloadData()
    }
}

