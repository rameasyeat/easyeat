//
//  MyRewardsViewController.swift
//  easyeat
//
//  Created by Ramniwas on 04/06/20.
//  Copyright © 2020 EasyEat. All rights reserved.
//

import UIKit

class MyRewardsViewController: BaseViewController,Storyboarded {
    var coordinator: MainCoordinator?

    override func viewDidLoad() {
        super.viewDidLoad()
        setNavWithOutView()
        titleLbl?.text = "My Rewards"
        rightButton.isHidden = true
    }

}
