//
//  WKWebViewController.swift
//  easyeat
//
//  Created by Ramniwas on 29/04/20.
//  Copyright © 2020 EasyEat. All rights reserved.
//

import UIKit
import WebKit

enum WebViewType :Int{
    case TC
    case policy
    case aboutus
}
class WKWebViewController: BaseViewController,Storyboarded {
    var coordinator: MainCoordinator?
    
    @IBOutlet weak var webView: WKWebView!
    
    var webViewType : WebViewType?
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        setNavWithOutView()
        rightButton.isHidden = true
        loadHTMPPage()
        
    }
    
    fileprivate func loadHTMPPage(){
        if webViewType == WebViewType.TC{
            titleLbl?.text = "Terms & Condition"
            webView.load(URLRequest(url: URL(string: "https://easyeat.ai/partner/privacy-policy.php")!))
        }
    }
    
}
