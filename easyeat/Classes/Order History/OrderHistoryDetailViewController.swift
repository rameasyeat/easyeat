//
//  OrderHistoryDetailViewController.swift
//  easyeat
//
//  Created by Ramniwas on 25/04/20.
//  Copyright © 2020 EasyEat. All rights reserved.
//

import UIKit
import SDWebImage

class OrderHistoryDetailViewController: BaseViewController,Storyboarded {
    var coordinator: MainCoordinator?
    @IBOutlet weak var restaurantImageView: CustomImageView!
    @IBOutlet weak var restaurantNameLabel: CustomLabel!
    @IBOutlet weak var restaurantAddresLabel: CustomLabel!
    @IBOutlet weak var savingLabel: CustomLabel!
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var savingBGView: VIewShadow!
    @IBOutlet weak var totalPaymentLabel: CustomLabel!
    
    @IBOutlet weak var tblView: UITableView!
    var viewModel : OrderHistoryDetilViewModel = {
        let model = OrderHistoryDetilViewModel()
        return model
        
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    
    // SsetupUI
    fileprivate func setupUI(){
        
        setNavWithOutView()
        rightButton.isHidden = true
        
        OHRatingCell.registerWithTable(tblView)
        OHOrderDetailCell.registerWithTable(tblView)
        OHOrderCell.registerWithTable(tblView)

        titleLbl?.text = "Order Summary"
        getOrderHistoryDetails()
    }
    
    fileprivate func updateData(){
        
        
        let imgPath = "\(KImagesBaseUrl)\(kRestFolderName)\(viewModel.orderHistoryDict?.restaurant_logo ?? "")"
        restaurantImageView?.sd_setImage(with:  URL(string:imgPath), placeholderImage:UIImage(named: "food") , options: .progressiveDownload, completed: nil)
        
        restaurantNameLabel.text = viewModel.orderHistoryDict?.restaurant_name?.capitalized
        restaurantAddresLabel.text = viewModel.orderHistoryDict?.restaurant_city?.capitalizingFirstLetter()
        
        if let saving = viewModel.orderHistoryDict?.bill?.savings, saving > 0{
            savingBGView.isHidden = false
            savingLabel.text = "\(CurrentUserInfo.currency ?? "".capitalized)" + " \(saving)"
        }
        else{
            headerView.frame.size.height -= 40
            savingBGView.isHidden = true
        }
        
        totalPaymentLabel.text = "\(CurrentUserInfo.currency ?? "".capitalized)" + " \(viewModel.orderHistoryDict?.bill?.item_total ?? 0)"

        tblView.isHidden = false
        self.tblView.reloadData()
    }
    
    // APIS call
    
    fileprivate func getOrderHistoryDetails(){
        var dict : [String : Any] = [String : Any]()
        dict[ktoken] =  CurrentUserInfo.authToken
        dict[korder_id] = viewModel.orderId
        
        print(dict)
        viewModel.getOrderHistory(APIsEndPoints.kgetOrderHistoryDetails.rawValue,dict, handler: {[weak self](statusCode)in
            self?.updateData()
        })
    }
}


// UITableViewDataSource
extension OrderHistoryDetailViewController: UITableViewDataSource,UITableViewDelegate{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return viewModel.numberOfSection
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == OrderHistoryDetailCellType.order.rawValue{
            return viewModel.orderHistoryDict?.items?.count ?? 0
        }
        return 1
    }
    
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
          let view = viewModel.setHeaderView(section)
          return view
          
      }
      func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
    
          return CGFloat(viewModel.headerHeight)
      }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.section ==  OrderHistoryDetailCellType.orderDerail.rawValue{
            let cell  = tableView.dequeueReusableCell(withIdentifier: OHOrderDetailCell.reuseIdentifier, for: indexPath) as! OHOrderDetailCell
            cell.selectionStyle = .none
            
            guard let dict = viewModel.orderHistoryDict else {return cell}
            cell.commonInit(dict)
            return cell
        }
      /*  else if indexPath.section == OrderHistoryDetailCellType.rating.rawValue{
            let cell  = tableView.dequeueReusableCell(withIdentifier: OHRatingCell.reuseIdentifier, for: indexPath) as! OHRatingCell
            cell.selectionStyle = .none
//            cell.commiInit((viewModel.orderHistoryArray?[indexPath.row])!)
            return cell
        }*/
        let cell  = tableView.dequeueReusableCell(withIdentifier: OHOrderCell.reuseIdentifier, for: indexPath) as! OHOrderCell
        cell.selectionStyle = .none
        
        guard let dict = viewModel.orderHistoryDict?.items?[indexPath.row] else {return cell}
        cell.commonInit(dict)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section ==  OrderHistoryDetailCellType.orderDerail.rawValue{
            return CGFloat(viewModel.orderDetailCellHeight)
        }
        return CGFloat(viewModel.orderCellHeight)
    }
}
