//
//  OrderHistoryViewController.swift
//  easyeat
//
//  Created by Ramniwas on 24/04/20.
//  Copyright © 2020 EasyEat. All rights reserved.
//

import UIKit

class OrderHistoryViewController: BaseViewController,Storyboarded {
    var coordinator: MainCoordinator?
    @IBOutlet weak var NoRecordView: UIView!
    
    @IBOutlet weak var tblView: UITableView!
    var viewModel : OrderHistoryViewModel = {
        let model = OrderHistoryViewModel()
        return model
        
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    
    // SsetupUI
    fileprivate func setupUI(){
        
        setNavWithOutView()
        rightButton.isHidden = true
        
        OrderHistoryCell.registerWithTable(tblView)
        titleLbl?.text = "Order History"
        getOrderHistoryList(true)
    }
    
    fileprivate func updateData(){
        
        if viewModel.orderHistoryArray?.count ?? 0 > 0 {
            NoRecordView.isHidden = true
            tblView.isHidden = false
        }
        else {
            NoRecordView.isHidden = false
            tblView.isHidden = true
        }
        self.tblView.reloadData()

        
    }
    
    // APIS call
    
    fileprivate func getOrderHistoryList(_ isHude : Bool){
        var dict : [String : Any] = [String : Any]()
        dict[ktoken] =  CurrentUserInfo.authToken
        dict[ksindex] = "\(viewModel.pageIndex)"
        dict[knorders] = "\(viewModel.norders)"
                
        print(dict)
        viewModel.getOrderHistory(APIsEndPoints.kgetOrderHistory.rawValue,dict, pageIndex: viewModel.pageIndex,isHude, handler: {[weak self](statusCode)in
            
            self?.updateData()
        })
        
    }
}


// UITableViewDataSource
extension OrderHistoryViewController: UITableViewDataSource,UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.orderHistoryArray?.count ?? 0
    }
 
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell  = tableView.dequeueReusableCell(withIdentifier: OrderHistoryCell.reuseIdentifier, for: indexPath) as! OrderHistoryCell
        cell.selectionStyle = .none
        cell.commiInit((viewModel.orderHistoryArray?[indexPath.row])!)
        
        if indexPath.row > (viewModel.norders - 3) && indexPath.row > ((viewModel.orderHistoryArray?.count ?? 0) - 3){
            getOrderHistoryList(false)
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let orderID = viewModel.orderHistoryArray?[indexPath.row].order_id else {return}
        coordinator?.goToOrderHistoryDetailView(orderID)
    }
}
