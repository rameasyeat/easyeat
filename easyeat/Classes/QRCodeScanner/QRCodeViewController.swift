//
//  QRCodeViewController.swift
//  easyeat
//
//  Created by Ramniwas on 27/03/20.
//  Copyright © 2020 EasyEat. All rights reserved.
//

import AVFoundation
import UIKit
import Toast_Swift


class QRCodeViewController: BaseViewController, AVCaptureMetadataOutputObjectsDelegate,Storyboarded {
    var captureSession = AVCaptureSession()
    var videoPreviewLayer: AVCaptureVideoPreviewLayer?
    var coordinator: MainCoordinator?
    var fromView : FromViewType?
    @IBOutlet weak var cameraLightY_Axis: NSLayoutConstraint!
    var camera_y_axis = 0
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var headerLabel: CustomLabel!
    @IBOutlet weak var cameraLight: CustomButton!
    @IBOutlet weak var imgHeight: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        camera_y_axis = (fromView == FromViewType.menu) ? 30 : 230
        
        cameraLightY_Axis.constant = (fromView == FromViewType.menu) ? cameraLightY_Axis.constant - 210 : cameraLightY_Axis!.constant
        
         imgHeight.constant = (fromView == FromViewType.menu) ? 0 : imgHeight.constant
        
        headerLabel.isHidden = (fromView == FromViewType.menu) ? true :false
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        loadCaptionView()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        toggleTorch(on: false)
    }
    
    fileprivate func loadCaptionView(){
        
        captureSession = AVCaptureSession()
        
        guard let captureDevice = AVCaptureDevice.default(.builtInWideAngleCamera, for: .video, position: .back) else {
            print("Failed to get the camera device")
            return
        }
        do {
            let input = try AVCaptureDeviceInput(device: captureDevice)
            captureSession.addInput(input)
            
        } catch {
            print(error)
            return
        }
        
        let captureMetadataOutput = AVCaptureMetadataOutput()
        captureSession.addOutput(captureMetadataOutput)
        
        captureMetadataOutput.setMetadataObjectsDelegate(self, queue: DispatchQueue.main)
        captureMetadataOutput.metadataObjectTypes = [AVMetadataObject.ObjectType.qr]
        
        
        videoPreviewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
        videoPreviewLayer?.videoGravity = AVLayerVideoGravity.resizeAspectFill
        videoPreviewLayer?.frame = self.view.frame
        view.layer.addSublayer(videoPreviewLayer!)
        captureSession.startRunning()
        
        //// MARK : Make layer on camera view
        let path = UIBezierPath(roundedRect: CGRect(x: 0, y: 0, width: ScreenSize.screenWidth, height: ScreenSize.screenHeight), cornerRadius: 0)
        
        let roundPath = UIBezierPath(roundedRect: CGRect(x: Int((ScreenSize.screenWidth - 280)/2), y: camera_y_axis + 8, width:280, height:280), cornerRadius: 0)
        
        let cornerRadiudSize = CGSize(width: 15, height: 15) //this is your global variable that you can change based on the slider and then draw
        let cornerPath = UIBezierPath(roundedRect: roundPath.bounds, byRoundingCorners: .allCorners, cornerRadii: cornerRadiudSize)
        cornerPath.lineWidth = 1
        path.append(cornerPath)
        path.usesEvenOddFillRule = true
        
        let fillLayer = CAShapeLayer()
        fillLayer.path = path.cgPath
        fillLayer.fillRule = .evenOdd
        fillLayer.fillColor =  UIColor.black.cgColor
        fillLayer.lineJoin = .round
        fillLayer.opacity = 0.7
        
        view.layer.addSublayer(fillLayer)
        
        
        // MARK : Make round corner
        let cornerView = UIView(frame: CGRect(x: Int((ScreenSize.screenWidth - 300)/2), y: camera_y_axis, width:300, height:300))
        cornerView.drawCorners(radius: 15, color: hexStringToUIColor(korange), strokeWidth: 2, length: 15)
        self.view.addSubview(cornerView)
        
        
        setNavigation()
        headeImageview?.isHidden = true
        notificationButton.isHidden = true
        titleLbl?.isHidden = true
        navView?.backgroundColor = .clear
        
        // check is from tabbar
        if fromView == FromViewType.tabbar || fromView == FromViewType.menu {
            backButton.isHidden = true
        }
        
        self.view.bringSubviewToFront(imageView)
        self.view.bringSubviewToFront(cameraLight)
        self.view.bringSubviewToFront(headerLabel)
        
    }
    
    
    func metadataOutput(_ output: AVCaptureMetadataOutput, didOutput metadataObjects: [AVMetadataObject], from connection: AVCaptureConnection) {
        captureSession.stopRunning()
        
        videoPreviewLayer?.session = nil
        videoPreviewLayer = nil
        
        if let metadataObject = metadataObjects.first {
            guard let readableObject = metadataObject as? AVMetadataMachineReadableCodeObject else { return }
            guard let stringValue = readableObject.stringValue else { return }
            AudioServicesPlaySystemSound(SystemSoundID(kSystemSoundID_Vibrate))
            found(code: stringValue)
        }
        
        //        dismiss(animated: true)
    }
    
    func found(code: String) {
        
        let codeArray = code.components(separatedBy: "?")
        
        if codeArray.count > 1{
            let resturentId = codeArray[1].replacingOccurrences(of: "restaurant_id=", with: "")
            let filterData = resturentId.replacingOccurrences(of: "tid=", with: "")
            let ids = filterData.components(separatedBy: "&")
            
            CurrentUserInfo.restaurantId = ids[0]
            CurrentUserInfo.tblID =  "dl" //ids[1]
            CurrentUserInfo.isScanQRCode = "1"
            CurrentUserInfo.priceByOrderType = .dining

            removeSegmentView()
            coordinator?.goToMenuSegment()
            
        }
        else {
            
            let viewController = UIApplication.shared.keyWindow?.rootViewController
            viewController?.view.makeToast("Scan correct QRcode to get the Restaurant items", duration: 2)
            self.removeSession()
            
        }
    }
    
    func removeSession() {
        loadCaptionView()
    }
    
    fileprivate func removeSegmentView(){
        guard let views = self.navigationController?.viewControllers else {return}
        for view in views{
            if view is MenuSegmentController{
                removeFromSuperView(view)
            }
        }
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .portrait
    }
    
    @IBAction func cameraLightAction(_ sender: Any) {
        cameraLight.isSelected = !cameraLight.isSelected
        toggleTorch(on:cameraLight.isSelected)
    }
    func toggleTorch(on: Bool) {
        guard
            let device = AVCaptureDevice.default(for: AVMediaType.video),
            device.hasTorch
            else { return }
        
        do {
            try device.lockForConfiguration()
            device.torchMode = on ? .on : .off
            device.unlockForConfiguration()
        } catch {
            print("Torch could not be used")
        }
    }
}


extension UIView {
    
    private struct Properties {
        
        static var _radius: CGFloat = 0.0
        static var _color: UIColor = .red
        static var _strokeWidth: CGFloat = 1.0
        static var _length: CGFloat = 20.0
        
    }
    
    private var radius: CGFloat {
        get {
            return Properties._radius
        }
        set {
            Properties._radius = newValue
        }
    }
    
    private var color: UIColor {
        get {
            return Properties._color
        }
        set {
            Properties._color = newValue
        }
    }
    
    private var strokeWidth: CGFloat {
        get {
            return Properties._strokeWidth
        }
        set {
            Properties._strokeWidth = newValue
        }
    }
    
    private var length: CGFloat {
        get {
            return Properties._length
        }
        set {
            Properties._length = newValue
        }
    }
    
    func drawCorners(radius: CGFloat? = nil, color: UIColor? = nil, strokeWidth: CGFloat? = nil, length: CGFloat? = nil) {
        if let radius = radius {
            self.radius = radius
        }
        if let color = color {
            self.color = color
        }
        if let strokeWidth = strokeWidth {
            self.strokeWidth = strokeWidth
        }
        if let length = length {
            self.length = length
        }
        createTopLeft()
        createTopRight()
        createBottomLeft()
        createBottomRight()
    }
    
    private func createTopLeft() {
        let topLeft = UIBezierPath()
        topLeft.move(to: CGPoint(x: strokeWidth/2, y: radius+length))
        topLeft.addLine(to: CGPoint(x: strokeWidth/2, y: radius))
        topLeft.addQuadCurve(to: CGPoint(x: radius, y: strokeWidth/2), controlPoint: CGPoint(x: strokeWidth/2, y: strokeWidth/2))
        topLeft.addLine(to: CGPoint(x: radius+length, y: strokeWidth/2))
        setupShapeLayer(with: topLeft)
    }
    
    private func createTopRight() {
        let topRight = UIBezierPath()
        topRight.move(to: CGPoint(x: frame.width-radius-length, y: strokeWidth/2))
        topRight.addLine(to: CGPoint(x: frame.width-radius, y: strokeWidth/2))
        topRight.addQuadCurve(to: CGPoint(x: frame.width-strokeWidth/2, y: radius), controlPoint: CGPoint(x: frame.width-strokeWidth/2, y: strokeWidth/2))
        topRight.addLine(to: CGPoint(x: frame.width-strokeWidth/2, y: radius+length))
        setupShapeLayer(with: topRight)
    }
    
    private func createBottomRight() {
        let bottomRight = UIBezierPath()
        bottomRight.move(to: CGPoint(x: frame.width-strokeWidth/2, y: frame.height-radius-length))
        bottomRight.addLine(to: CGPoint(x: frame.width-strokeWidth/2, y: frame.height-radius))
        bottomRight.addQuadCurve(to: CGPoint(x: frame.width-radius, y: frame.height-strokeWidth/2), controlPoint: CGPoint(x: frame.width-strokeWidth/2, y: frame.height-strokeWidth/2))
        bottomRight.addLine(to: CGPoint(x: frame.width-radius-length, y: frame.height-strokeWidth/2))
        setupShapeLayer(with: bottomRight)
    }
    
    private func createBottomLeft() {
        let bottomLeft = UIBezierPath()
        bottomLeft.move(to: CGPoint(x: radius+length, y: frame.height-strokeWidth/2))
        bottomLeft.addLine(to: CGPoint(x: radius, y: frame.height-strokeWidth/2))
        bottomLeft.addQuadCurve(to: CGPoint(x: strokeWidth/2, y: frame.height-radius), controlPoint: CGPoint(x: strokeWidth/2, y: frame.height-strokeWidth/2))
        bottomLeft.addLine(to: CGPoint(x: strokeWidth/2, y: frame.height-radius-length))
        setupShapeLayer(with: bottomLeft)
    }
    
    private func setupShapeLayer(with path: UIBezierPath) {
        let shapeLayer = CAShapeLayer()
        shapeLayer.strokeColor = color.cgColor
        shapeLayer.fillColor = UIColor.clear.cgColor
        shapeLayer.lineWidth = strokeWidth
        shapeLayer.path = path.cgPath
        layer.addSublayer(shapeLayer)
    }
    
}
