//
//  QuickServiceViewController.swift
//  easyeat
//
//  Created by Ramniwas on 16/04/20.
//  Copyright © 2020 EasyEat. All rights reserved.
//

import UIKit

class QuickServiceViewController: BaseViewController,Storyboarded,QSubCatBackDelegate,RequestViewBackDelegate{
    
    
    var coordinator: MainCoordinator?
    @IBOutlet weak var bgView: UIView!
    
    @IBOutlet weak var collectionView: UICollectionView!
    let  viewModel : QuickServiceViewModel = {
        let model = QuickServiceViewModel()
        return model
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        viewModel.checkServicesStatus()
        bgView.animShow(0.3)

    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
    }
    
    override func backButtonAction() {
        self.view.isHidden = false
    }
    func requestItemAction() {
        self.view.isHidden = false
    }
    
    fileprivate func setupUI(){
        QuickServiceCell.registerWithCollectionView(collectionView)

        collectionView.delegate = self
        collectionView.dataSource = self
    }
    
    func getServiceType(_ indexPath : IndexPath){
        
        if let catname = viewModel.servicesArray?[indexPath.row].category_name,catname != kWifiPassword &&   (viewModel.servicesArray?[indexPath.row].subcategories?.count == 0){
            callServer(ServiceCatType.singleAction, (viewModel.servicesArray?[indexPath.row])!)
        }
        else if let catname = viewModel.servicesArray?[indexPath.row].category_name,catname == kSauces{
            gotoSubCatView(.singleAction, index: indexPath.row)
        }
        else if let catname = viewModel.servicesArray?[indexPath.row].category_name,catname == kWifiPassword{
            gotoSubCatView(.wifi, index: indexPath.row)
        }
        else {
            gotoSubCatView(.multiSelection, index: indexPath.row)
        }
    }
    
    
    fileprivate func gotoSubCatView(_ serviceType : ServiceCatType, index : Int){
        self.view.isHidden = true
        
        let controller = QSSubCategoryViewController.instantiate()
        controller.viewModel.serviceCatType =  serviceType
        controller.viewModel.orderId = viewModel.orderId
        controller.viewModel.quickServiceSubCatDelegate = self
        controller.viewModel.serviceDict = viewModel.servicesArray?[index]
        addRootView(controller)
    }
    
    fileprivate func gotoSuccessPop(_ serviceType : ServiceCatType){
        self.view.isHidden = true
        let controller = QSRequestPopView.instantiate()
        controller.viewModel.requestViewBackDelegate = self
        controller.viewModel.serviceCatType =  serviceType
        addRootView(controller)
    }
    
    // Call Apis
    fileprivate func callServer(_ serviceType : ServiceCatType,_ selectedArray : Services){
        var dict : [String : Any] = [String : Any]()
        
        dict[kuser_id] = CurrentUserInfo.userdId ?? ""
        dict[ktable_id] = CurrentUserInfo.tblID
        dict[kmessage] = selectedArray.category_name
        dict[kmessage_type] = selectedArray.category_id
        dict[krestaurant_id] = CurrentUserInfo.restaurantId 
        
        if let orderId = viewModel.orderId, orderId != ""{
            dict[korder_id] = viewModel.orderId
        }
        else {
            dict[korder_id] = "none"
        }
        
        viewModel.getServiceItems(APIsEndPoints.kCallServer.rawValue,dict, handler: {[weak self](statusCode)in
            self?.gotoSuccessPop(serviceType)
            print(statusCode)
        })
    }
    
}
extension QuickServiceViewController : UICollectionViewDataSource, UICollectionViewDelegate,UICollectionViewDelegateFlowLayout
{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return viewModel.servicesArray?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {        print((collectionView.frame.size.width / 4))
        if indexPath.row == 0{
            return (CGSize(width:collectionView.frame.size.width, height: 62))
        }
        return (CGSize(width: (collectionView.frame.size.width - 20) / 2, height: 62))
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: QuickServiceCell.reuseIdentifier, for: indexPath)as! QuickServiceCell
        cell.commonInit((viewModel.servicesArray?[indexPath.row])!)
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        getServiceType(indexPath)
    }
}
