//
//  QSRequestPopView.swift
//  easyeat
//
//  Created by Ramniwas on 17/04/20.
//  Copyright © 2020 EasyEat. All rights reserved.
//

import UIKit

protocol RequestViewBackDelegate {
    func requestItemAction()
}
class QSRequestPopView: BaseViewController,Storyboarded {
    var coordinator: MainCoordinator?
    
    var viewModel : ItemRequestViewModel = {
        let model = ItemRequestViewModel()
        return model
    }()
    
    @IBOutlet weak var animationView: UIView!
    @IBOutlet weak var descriptionLabel: CustomLabel!
    @IBOutlet weak var headerLabel: CustomLabel!
    @IBOutlet weak var requestButton: CustomButton!
    @IBOutlet weak var doneButton: CustomButton!
    @IBOutlet weak var popupBottomHeightConstraint: NSLayoutConstraint!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        popupBottomHeightConstraint.constant = CGFloat(CFloat(0.getSafeAreaHeight()))

        if  viewModel.serviceCatType == ServiceCatType.singleAction{
            doneButton.isHidden = false
        }
        viewModel.showSuccessAnimation(animationView)
    }
    
    fileprivate func removeFromView(){
        for v in self.navigationController!.viewControllers{
            if v is QSSubCategoryViewController || v is QuickServiceViewController || v is QSRequestPopView{
                removeFromSuperView(v)
            }
        }
    }
    
    // Mark Button Action
    override func crossButtonAction(_ sender: Any) {
        removeFromView()
        
    }
    @IBAction func doneButtonAction(_ sender: Any) {
        removeFromView()
    }
    @IBAction func requestButtonAction(_ sender: Any) {
        viewModel.requestViewBackDelegate?.requestItemAction()
            removeFromSuperView(self)
    }
  
}
