//
//  QSSubCategoryViewController.swift
//  easyeat
//
//  Created by Ramniwas on 16/04/20.
//  Copyright © 2020 EasyEat. All rights reserved.
//

import UIKit

protocol QSubCatBackDelegate {
    func backButtonAction()
}
class QSSubCategoryViewController: BaseViewController,Storyboarded,UpdateItemCountDelegate,RequestViewBackDelegate {
    
    var coordinator: MainCoordinator?
    
    @IBOutlet weak var requestButton: CustomButton!
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var itemCountLabel: UILabel!
    @IBOutlet weak var catNameLabel: CustomLabel!
    @IBOutlet weak var catDescriptionLabel: CustomLabel!
    @IBOutlet weak var popupBottomHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var doneButton: CustomButton!
    
    @IBOutlet weak var tblHeightConstraint: NSLayoutConstraint!
    var viewModel : QSsubcategoryViewModel = {
        let model = QSsubcategoryViewModel()
        return model
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        viewModel.checkServicesStatus()
    }
    override func viewWillAppear(_ animated: Bool) {
        
    }
    
    fileprivate func setupUI(){
        // getRestaurentDetails()
        
        popupBottomHeightConstraint.constant = CGFloat(CFloat(0.getSafeAreaHeight()))
        updateRequestButton(0)

        QSSubCategoryCell.registerWithTable(tblView)
        WifiCell.registerWithTable(tblView)
        
        // in case of wifi info
        if  viewModel.serviceCatType == ServiceCatType.wifi{
            viewModel.infoArray  = viewModel.prepareInfo(dictInfo: viewModel.serviceDict!)
            doneButton.isHidden = false
            catNameLabel.text = "Wifi"
            catDescriptionLabel.text = "Please connect to below wifi"
        }
        
    }
    
    // MARK button action
    @IBAction func RequestButtonAction(_ sender: Any) {
        callServer()
    }
    @IBAction func backButtonAction(_ sender: Any) {
        viewModel.quickServiceSubCatDelegate?.backButtonAction()
        removeFromSuperView(self)
    }
    @IBAction func doneButtonAction(_ sender: Any) {
        removeFromSuperView(self)
    }
    
    func requestItemAction(){
        
        for (index,_) in viewModel.serviceDict!.subcategories!.enumerated(){
            viewModel.serviceDict?.subcategories?[index].quantity_flag!  = 0
        }
        tblView.reloadData()
        self.view.isHidden = false
        updateRequestButton(0)

    }
    
    override func crossButtonAction(_ sender: Any) {
        
        for v in self.navigationController!.viewControllers{
            if v is QSSubCategoryViewController || v is QuickServiceViewController{
                removeFromSuperView(v)
            }
        }
    }
    
    fileprivate func gotoSuccessPop(_ serviceType : ServiceCatType){
        self.view.isHidden = true
              let controller = QSRequestPopView.instantiate()
        controller.viewModel.serviceCatType =  ServiceCatType.multiSelection
        controller.viewModel.requestViewBackDelegate = self
              addRootView(controller)
          }
    // Mark Delegate call
    func updateCount(_ indexPath: IndexPath, _ itemCount: Int, _ addRemove: Int) {
        
        if  viewModel.serviceCatType == ServiceCatType.singleAction{
            viewModel.serviceDict?.subcategories?[indexPath.row].quantity_flag! = addRemove
        }
        else {
            viewModel.serviceDict?.subcategories?[indexPath.row].quantity_flag! = itemCount
        }
        
        viewModel.itemCount = 0
        for dict in viewModel.serviceDict!.subcategories!{
            viewModel.itemCount += dict.quantity_flag ?? 0
        }
        itemCountLabel.text = "Request items now (\(viewModel.itemCount))"
        updateRequestButton(viewModel.itemCount)
        
        tblView.reloadRows(at: [indexPath], with: .none)
    }
    
    fileprivate func updateRequestButton(_ count : Int){
        if count > 0 {
            requestButton.isUserInteractionEnabled = true
            requestButton.alpha = 1
            
        }else{
            requestButton.isUserInteractionEnabled = false
            requestButton.alpha = 0.4
        }
    }
    
    /* fileprivate func getRestaurentDetails(){
     var dict : [String : Any] = [String : Any]()
     dict[krestaurant_id] = CurrentUserInfo.restaurantId ?? "5da6eceb52920c06e80ecc1e6fdf6617"
     
     viewModel.getServiceItems(APIsEndPoints.krestaurantDetails.rawValue,dict, handler: {[weak self](result,statusCode)in
     self?.viewModel.serviceDict = result
     })
     }*/
    
    
   
    
    // Call Apis
    fileprivate func callServer(){
        var dict : [String : Any] = [String : Any]()
        
        dict[ktoken] = CurrentUserInfo.authToken ?? ""
        dict[ktable_id] = CurrentUserInfo.tblID
        dict[kmessage] = viewModel.endcodeModel(viewModel.serviceDict!.subcategories!)

        dict[kmessage_type] = viewModel.serviceDict?.category_id
        dict[krestaurant_id] = CurrentUserInfo.restaurantId
        
        if let orderId = viewModel.orderId, orderId != ""{
            dict[korder_id] = viewModel.orderId
        }
        else {
            dict[korder_id] = "none"
        }
        
        viewModel.getServiceItems(APIsEndPoints.kCallServer.rawValue,dict, handler: {[weak self](statusCode) in
            print(statusCode)
            self?.gotoSuccessPop((self?.viewModel.serviceCatType!)!)

        })
    }
    
}
// UITableViewDataSource
extension QSSubCategoryViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if  viewModel.serviceCatType == ServiceCatType.wifi{
            return viewModel.infoArray.count
        }
        return viewModel.serviceDict?.subcategories?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        // wifi cell  type
        if  viewModel.serviceCatType == ServiceCatType.wifi{
            let cell  = tableView.dequeueReusableCell(withIdentifier: WifiCell.reuseIdentifier, for: indexPath) as! WifiCell
            cell.selectionStyle = .none
            cell.commonInit(viewModel.infoArray[indexPath.row])
            return cell
        }
        
        // selection cell type
        let cell  = tableView.dequeueReusableCell(withIdentifier: QSSubCategoryCell.reuseIdentifier, for: indexPath) as! QSSubCategoryCell
        cell.selectionStyle = .none
        cell.tblView = tblView
        cell.viewModel = viewModel
        cell.addButton.tag = indexPath.row
        cell.updateItemCountDelegate = self
        
        let dict = viewModel.serviceDict?.subcategories?[indexPath.row]
        cell.commonInit(dict!)
        
        return cell
    }
    
    override func viewDidLayoutSubviews(){
        print(tblView.contentSize.height)
        print(ScreenSize.screenHeight)
        
        let safeArea = CGFloat( 0.getSafeAreaHeight())
        let extraHeight = 78  + safeArea
        
        tblHeightConstraint.constant =  (tblView.contentSize.height + extraHeight) > ScreenSize.screenHeight ?  (ScreenSize.screenHeight - extraHeight) : tblView.contentSize.height  + extraHeight
        
        tblView.isScrollEnabled = ((tblView.contentSize.height + extraHeight) > ScreenSize.screenHeight) ?  true :  false
    }
}

extension QSSubCategoryViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if  viewModel.serviceCatType == ServiceCatType.wifi{
            return CGFloat(viewModel.cellHeight)
        }
        return UITableView.automaticDimension
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
    }
}
