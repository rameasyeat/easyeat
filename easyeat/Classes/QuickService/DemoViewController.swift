//
//  PageController.swift
//  easyeat
//
//  Created by Ramniwas on 17/04/20.
//  Copyright © 2020 EasyEat. All rights reserved.
//

import UIKit

    class DemoViewController: BaseViewController, UITableViewDelegate, UITableViewDataSource,Storyboarded {
        var coordinator: MainCoordinator?

        @IBOutlet weak var toptable: UITableView!
        @IBOutlet weak var containerScrollView: UIScrollView!
        @IBOutlet weak var bottomTable: UITableView!
        
        var pageContainerInitialHeight = CGFloat()
        
        override func viewWillAppear(_ animated: Bool) {
            super.viewWillAppear(animated)
            
        }
        
        override func viewDidLoad() {
            super.viewDidLoad()
            
            setNavigation()
            containerScrollView.delegate = self
            toptable.delegate = self
            toptable.dataSource = self
            
            bottomTable.delegate = self
            bottomTable.dataSource = self
        }
        
        override func viewDidLayoutSubviews(){
            print(ScreenSize.screenHeight)
            
            let safeArea = CGFloat( 0.getSafeAreaHeight())
            let extraHeight = 78  + safeArea
            print((toptable.contentSize.height + extraHeight) + bottomTable.contentSize.height)
            
            containerScrollView.contentSize.height =  1000
        }
        
        func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            let cell = UITableViewCell()
            cell.textLabel?.text = "\(indexPath.row)"
            return cell
        }
        
        func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            return 10 // random number of cells
        }
        
        func scrollViewDidScroll(_ scrollView: UIScrollView) {
            
        }
        
    }
