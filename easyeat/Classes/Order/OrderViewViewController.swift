//
//  OrderViewViewController.swift
//  easyeat
//
//  Created by Ramniwas on 07/04/20.
//  Copyright © 2020 EasyEat. All rights reserved.
//

import UIKit


enum OrderSectionType : Int {
    case item = 0
    case billDetail
    case paid
}

enum OrderType : Int {
    case dining = 0
    case delivery
    case pickup
}

class OrderViewViewController:BaseViewController,Storyboarded,OrderRestDelegate,MQTTDelegates {
 
    var coordinator: MainCoordinator?
    
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var payNow: CustomButton!
    @IBOutlet weak var addMore: CustomButton!
    @IBOutlet weak var billStatusLabel: CustomLabel!
    @IBOutlet weak var backToMenuButton: CustomButton!
    @IBOutlet weak var ordernowWidth: NSLayoutConstraint!
    @IBOutlet weak var orderNowTralling: NSLayoutConstraint!
    @IBOutlet weak var addMoreViewHeight: NSLayoutConstraint!
    @IBOutlet weak var viewAddMore: VIewShadow!
    
    var viewModel : OrderViewModel = {
        let model = OrderViewModel()
        return model
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setNavigation()
        hideServiceButton()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidAppear(animated)
        viewModel.stopCourierTimer()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        UISetup()

    }
    
   
    
    fileprivate func UISetup(){
        tblView.delegate = self
        tblView.dataSource = self
        titleLbl?.text = "Order status"
        tblView.addSubview(refreshControl) // not required when using UITableViewController
        OrderCell.registerWithTable(tblView)
        RuningOrderDeliveryCell.registerWithTable(tblView)
        BillDetailCell.registerWithTable(tblView)
        PartnerDeliveryCell.registerWithTable(tblView)
        self.getCartList(false)
    }
    
    fileprivate func hideServiceButton(){
        if viewModel.restaurantDict?.type == RestaurentType.dining.rawValue {
            notificationButton.isHidden = false
        }else {
            notificationButton.isHidden = true
        }
    }
    
    fileprivate func runingOrderCancelButton(){ // delivery & pick up order
        if (viewModel.orderData?.order_type == OrderType.delivery.rawValue || viewModel.orderData?.order_type == OrderType.pickup.rawValue){
            addMore.setTitle("Cancel Order", for: .normal)
            viewModel.hideAddMoreView(addMoreViewHeight,billStatusLabel)
        }
    }
    fileprivate func showAddMoreButton(){
        addMore.setTitleColor(hexStringToUIColor(kwhite), for: .normal)
        addMore.backgroundColor = hexStringToUIColor(korange)
        billStatusLabel.isHidden = true
        
        ordernowWidth.constant = 0
        orderNowTralling.constant = 0
    }
    
    fileprivate func updateUIData(){
        self.tblView.isHidden = false
        
        runingOrderCancelButton()
        
        if (viewModel.orderData?.order_type == OrderType.delivery.rawValue || viewModel.orderData?.order_type == OrderType.pickup.rawValue){
            billStatusLabel.isHidden = false
            
            // bill paid or not
            if viewModel.orderData?.bill?.bill_total == viewModel.orderData?.bill?.paid{
                viewAddMore.isHidden = true
            }
            else {
                viewAddMore.isHidden = false
            }
        }
        else{
            billStatusLabel.isHidden = true
            addMoreViewHeight.constant = CGFloat(viewModel.billToPayHeight)
            viewAddMore.isHidden = false
            
            if viewModel.orderData?.order_status == OrderStatus.pending.rawValue && viewModel.orderData?.bill?.bill_total == viewModel.orderData?.bill?.paid{
                showAddMoreButton()
            }else {
                getDeclinedItem()
            }
        }
        
        payNow.setTitle("\(CurrentUserInfo.currency ?? "") \(viewModel.orderData?.bill?.balance ?? 0)   Pay Now", for: .normal)
        
        if   let contain : [OrderItems] = viewModel.orderData?.orderItems?.filter({$0.item_status == 5}), contain.count > 0 {
            if let declinedReason = contain[0].decline_reason{
                checkOrderDeclined(declinedReason)
            }
        }
        else  if viewModel.orderCancelStatus == OrderCancelBy.MM{
             checkOrderDeclined(cancelOrder)
        }
        self.tblView.reloadData()
    }
    
    func getDeclinedItem(){
        
        if   let contain : [OrderItems] = viewModel.orderData?.orderItems?.filter({$0.item_status == 1 || $0.item_status == 2 || $0.item_status == 3  || $0.item_status == 0}), contain.count > 0 {
            addMore.setTitleColor(hexStringToUIColor(korange), for: .normal)
            addMore.backgroundColor = hexStringToUIColor(kwhite)
            ordernowWidth.constant = 206
            orderNowTralling.constant = 16
        }
        else {
            showAddMoreButton()
        }
    }
    
    func checkOrderDeclined<T>(_ dict : T){
        
        let controller = OrderDeclinedViewController.instantiate()
        controller.viewModel.restaurantDict = viewModel.restaurantDict
        controller.viewModel.orderId = viewModel.orderData?.order_id
        controller.viewModel.order_type = viewModel.orderData?.order_type

        
        controller.coordinator = coordinator
        
        if viewModel.orderCancelStatus == OrderCancelBy.MM && (viewModel.orderData?.order_type == priceByOrderType.delivery.rawValue || viewModel.orderData?.order_type == priceByOrderType.pickup.rawValue){
             let dictItem = self.viewModel.orderData?.bill 

            let items = viewModel.arraySection[0]
            controller.viewModel.orderArray = items.item
            controller.viewModel.toPay = dictItem?.paid
            controller.viewModel.orderCancelBy = viewModel.orderCancelStatus // delivery & Pickup
            
        }
        else if let reason  = dict as? Decline_reason{
            controller.viewModel.declinedReason = reason
            controller.viewModel.orderArray = viewModel.orderData?.orderItems
        }
        else{
            controller.viewModel.isCancelOrder = dict as? Bool // delivery & Pickup
            let items = viewModel.arraySection[0]
            controller.viewModel.orderArray = items.item
        }
        controller.viewModel.orderRestDelegate = self
        addRootView(controller)
    }
    
    
    func getRestOrderItem(_ isCancelOrder : Bool) {
        if isCancelOrder == true{
            cancelOrder()
        }
        else{
            orderRest()
        }
    }
    
    // MARK Button Action
    @IBAction func menuBackAction(_ sender: Any) {
        backAction()
    }
    
    func backAction(){
        for view in self.navigationController!.viewControllers{
            if view.isKind(of: MenuSegmentController.self){
                removeFromSuperView(view)
//                self.navigationController?.popToViewController(view, animated: true)
//                return
            }
        }
        CurrentUserInfo.priceByOrderType = priceByOrderType.dining
        coordinator?.goToMenuSegment()
    }
    
    override func backButtonAction() {
        backAction()
    }
    @IBAction func addMoreAction(_ sender: Any) {
        
        if (viewModel.orderData?.order_type == OrderType.delivery.rawValue || viewModel.orderData?.order_type == OrderType.pickup.rawValue){ // delivery order
            checkOrderDeclined(true)
        }
        else {
            backAction() // dining order
        }
    }
    
    @IBAction func payNowAction(_ sender: Any) {
        coordinator?.goToPaymentMode(viewModel.orderData!, viewModel.restaurantDict!)
    }
    
    override func notificationButtonAction() {
        let controller = QuickServiceViewController.instantiate()
        controller.viewModel.servicesArray = viewModel.getSortedArray()
        controller.viewModel.orderId = viewModel.orderData?.order_id
        addRootView(controller)
    }
    
    override func handleRefresh(_ refreshControl: UIRefreshControl) {
        refreshControl.endRefreshing()
        getCartList(false)
    }
    
    @objc func viewDetailButtonAction(){
        viewModel.isShowViewDetail = true
        tblView.reloadData()
        
    }
    
    // MARK APIs call
    
    fileprivate func getCartList(_ isMQTTConnected : Bool){
        self.viewModel.orderCancelStatus = OrderCancelBy.none
        var dict : [String : Any] = [String : Any]()
        dict[ktoken] =  CurrentUserInfo.authToken ?? kdefaultToken
        viewModel.getOrderStatus(APIsEndPoints.kgetRunningOrder.rawValue, dict) { [weak self](statusCode) in
            
            if statusCode == 1 {
                
                if isMQTTConnected == false{
                    self?.reloadMQTTServer()
                }
                self?.updateUIData()
            }
            else{
                self?.backAction()
            }
        }
    }
    
    fileprivate func ordderDetails(_ isMQTTConnected : Bool){
          
          var dict : [String : Any] = [String : Any]()
          dict[ktoken] =  CurrentUserInfo.authToken ?? kdefaultToken
           dict[korder_id] = viewModel.orderData?.order_id
        
          viewModel.getOrderStatus(APIsEndPoints.kordersDetails.rawValue, dict) { [weak self](statusCode) in
              
              if statusCode == 1 {
                  
                  if isMQTTConnected == false{
                      self?.reloadMQTTServer()
                  }
                  self?.updateUIData()
              }
          }
      }
    
    fileprivate func orderRest(){
        
        self.viewModel.clearSectionArray()

        var dict : [String : Any] = [String : Any]()
        dict[ktoken] =  CurrentUserInfo.authToken ?? kdefaultToken
        dict[korder_id] = viewModel.orderData?.order_id
        dict[kcart_token] = CurrentUserInfo.randomToken
        dict[ktable_id] = CurrentUserInfo.tblID


        viewModel.resetOrder(APIsEndPoints.kRestOrder.rawValue,dict, handler: {[weak self](result,statusCode)in
            print(statusCode)
            self?.getCartList(false)
            
        })
    }
    
    fileprivate func cancelOrder(){
        var dict : [String : Any] = [String : Any]()
        dict[ktoken] =  CurrentUserInfo.authToken
        viewModel.cancelOrder(APIsEndPoints.kCancelOrder.rawValue,dict, handler: {[weak self](statusCode)in
            self?.getCartList(false)
            print(statusCode)
        })
    }
    
    
    // MQTT Responce
    
    func updateQMTTResponce(_ dict: [String : Any]) {
        viewModel.handleOrderStatus(dict,{[weak self](mType, orderCancelBy) in
                   self?.callGetRuningStatus(mType,orderCancelBy)
               })
    }
//    @objc func getMQTTResponce(notification: Notification) {
//        viewModel.handleOrderStatus((notification.userInfo as? [String : Any])!,{[weak self](mType, orderCancelBy) in
//            self?.callGetRuningStatus(mType,orderCancelBy)
//        })
//    }
    
    func reloadMQTTServer(){
        self.viewModel.callMQTTserver()
        self.viewModel.socketConnection?.delegates = self
    }
    
    func callGetRuningStatus(_ mtype : MType,_ cancelOrder : OrderCancelBy){
        
        if mtype == MType.OID || mtype == MType.RELOAD{ // get runing status
            getCartList(true)
        }
        else if mtype == MType.OCM {// show review screen
            coordinator?.goToRatingView(viewModel.orderData!)
        }
        else if mtype == MType.OCL {// cancel status
            self.viewModel.orderCancelStatus = cancelOrder
            if viewModel.orderCancelStatus == OrderCancelBy.MM{
                ordderDetails(true)
               // checkOrderDeclined(cancelOrder)
            }
            backToMenuButton.isHidden = false
            billStatusLabel.isHidden = true
            addMoreViewHeight.constant = CGFloat(viewModel.billToPayHeight)
            
            billStatusLabel.text = ""
            tblView.reloadData()
        }
    }
}


// UITableViewDataSource
extension OrderViewViewController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if (viewModel.orderData?.order_type == OrderType.delivery.rawValue || viewModel.orderData?.order_type == OrderType.pickup.rawValue){ // delivery & pickup order
            
            if viewModel.isShowViewDetail == false && viewModel.orderData?.order_type == OrderType.delivery.rawValue && viewModel.isPartnerDelivery == 1{
                return viewModel.arraySection.count + viewModel.isPartnerDelivery
            }
            return viewModel.arraySection.count + viewModel.isPartnerDelivery +  2
        }
        return viewModel.arraySection.count + 1 // dining order
    }
    
    // Header & Footer
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if viewModel.isPartnerDelivery  == 1  && section == viewModel.isPartnerDelivery{
            let headerView =  DeliveryPartnerOrderItemHeader()
            headerView.setHeaderData(viewModel.orderData?.bill?.bill_total ?? 0, restName: viewModel.restaurantDict?.name ?? "", viewModel.isShowViewDetail)
            return headerView
        }
            
        else if viewModel.arraySection.count > 0 && (section < ( viewModel.arraySection.count )) && viewModel.isPartnerDelivery  == 0{
            let headerView =  OrderTableHeader()
            headerView.setOrderStatus(section,viewModel.orderCancelStatus,viewModel.orderData!, viewModel.restaurantDict!) { [weak self](orderStatus) in
                self?.statusLabel.text = orderStatus
            }
            headerView.backgroundColor = .red
            return headerView
        }
        return nil
    }
    
    // Header & Footer Height
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        if viewModel.isPartnerDelivery  == 1  && section == viewModel.isPartnerDelivery && viewModel.isShowViewDetail == false{
            let headerView =  DeliveryPartnerOrderItemHeader()
            headerView.setFooter(viewModel.isShowViewDetail)
            headerView.viewDetailButton.addTarget(self, action: #selector(viewDetailButtonAction), for: .touchUpInside)
            
            return headerView
        }
        return nil
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section ==  viewModel.isPartnerDelivery && viewModel.isPartnerDelivery == 1{
            return CGFloat(viewModel.headerHeight)
        }
        else if viewModel.arraySection.count > 0 && (section < ( viewModel.arraySection.count )) && viewModel.isPartnerDelivery  == 0{
            return CGFloat(viewModel.headerHeight)
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        if  section == (viewModel.isPartnerDelivery) && viewModel.isShowViewDetail == false && viewModel.isPartnerDelivery == 1{
            return CGFloat(viewModel.headerHeight)
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if (viewModel.arraySection.count + viewModel.isPartnerDelivery) > 0 && (section < ( viewModel.arraySection.count + viewModel.isPartnerDelivery)){
            
            if viewModel.isPartnerDelivery == 1 && section == 0{// add partner cell
                return 1
            }else {
                return viewModel.arraySection[section - viewModel.isPartnerDelivery].item.count
            }
        }
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if viewModel.arraySection.count > 0 && (indexPath.section < ( viewModel.arraySection.count + viewModel.isPartnerDelivery)){
            if viewModel.isPartnerDelivery == 1 && indexPath.section == 0{// add partner cell
                return partnerDeliveryCell(tableView, indexPath)
            }else {
                return addOrderCell(tableView, indexPath)
            }
        }
            
        else if (viewModel.orderData?.order_type == OrderType.delivery.rawValue || viewModel.orderData?.order_type == OrderType.pickup.rawValue) && indexPath.section == (viewModel.arraySection.count + viewModel.isPartnerDelivery){// delivery & pickup order
            
            let cell  = tableView.dequeueReusableCell(withIdentifier: RuningOrderDeliveryCell.reuseIdentifier, for: indexPath) as! RuningOrderDeliveryCell
            cell.restaurantDict = viewModel.restaurantDict
            cell.selectionStyle = .none
            cell.commonInit(viewModel.orderData!)
            return cell
        }
        let cell  = tableView.dequeueReusableCell(withIdentifier: BillDetailCell.reuseIdentifier, for: indexPath) as! BillDetailCell
        cell.selectionStyle = .none
        
        guard let dictItem = self.viewModel.orderData?.bill else {return cell}
        cell.setOrderUIData(dictItem,viewModel.orderData,viewModel.orderCancelStatus)
        return cell
    }
    
    // add Order cell
    fileprivate func addOrderCell(_ tableView : UITableView,_ indexPath : IndexPath) -> OrderCell{
        let cell  = tableView.dequeueReusableCell(withIdentifier: OrderCell.reuseIdentifier, for: indexPath) as! OrderCell
        cell.selectionStyle = .none
        
        let items = viewModel.arraySection[indexPath.section - viewModel.isPartnerDelivery].item
        cell.commonInit(items[indexPath.row],viewModel.orderCancelStatus, viewModel.orderData!,viewModel.isShowViewDetail,viewModel.isPartnerDelivery)
        return cell
    }
    
    // add  Partner DeliveryCell
    fileprivate func partnerDeliveryCell(_ tableView : UITableView,_ indexPath : IndexPath) -> PartnerDeliveryCell{
        let cell  = tableView.dequeueReusableCell(withIdentifier: PartnerDeliveryCell.reuseIdentifier, for: indexPath) as! PartnerDeliveryCell
        cell.selectionStyle = .none
        cell.viewModel = viewModel
        cell.commonInit(viewModel.orderData,viewModel.orderCancelStatus)
        
        return cell
    }
}

extension OrderViewViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
    }
}




