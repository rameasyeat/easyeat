//
//  OrderDeclinedViewController.swift
//  easyeat
//
//  Created by Ramniwas on 07/04/20.
//  Copyright © 2020 EasyEat. All rights reserved.
//

import UIKit

enum OrderDeclinedStatus : Int {
    case  itemsNotAvailable = 1
    case  availablebutdeclined
    case fakeOrde
    case otherReason
}

protocol OrderRestDelegate {
    func getRestOrderItem(_ isCancelOrder : Bool)
}

class OrderDeclinedViewController: UIViewController,Storyboarded {
    @IBOutlet weak var bgViewHeight: NSLayoutConstraint!
    
    @IBOutlet weak var addMoreButton: CustomButton!
    @IBOutlet weak var orderReset: CustomButton!
    @IBOutlet weak var orderDeclinedLabel: CustomLabel!
    @IBOutlet weak var descriptionLabel: CustomLabel!
    @IBOutlet weak var tblView: UITableView!
    var coordinator: MainCoordinator?
    @IBOutlet weak var fakeOrderView: UIView!
    @IBOutlet weak var otherReasonMsg: CustomLabel!
    @IBOutlet weak var viewDeclined: UIView!
    
    lazy var viewModel : OrderDeclinedViewModel = {
        let viewModel = OrderDeclinedViewModel()
        return viewModel }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        UISetup()
    }
    
    private func UISetup(){
        
        if viewModel.orderCancelBy == OrderCancelBy.MM && (viewModel.order_type == priceByOrderType.delivery.rawValue || viewModel.order_type == priceByOrderType.pickup.rawValue) { // pick & delivery
            viewModel.unAvailableArray = viewModel.orderArray
            orderDeclinedLabel.text = "Cancel Order"
            descriptionLabel.text = "Order was cancelled by the restaurant"
            fakeOrderView.isHidden = true
            viewDeclined.isHidden = false
            
            orderReset.setTitle("Call Restaurant", for: .normal)
            orderReset.borderWidth = 1
            orderReset.borderColor = hexStringToUIColor(korange)
            addMoreButton.setTitle("Okay", for: .normal)
            
        }
        else if viewModel.isCancelOrder == true && (viewModel.order_type == priceByOrderType.delivery.rawValue || viewModel.order_type == priceByOrderType.pickup.rawValue)
        { // pick & delivery
            viewModel.unAvailableArray = viewModel.orderArray
            orderDeclinedLabel.text = "Cancel Order"
            descriptionLabel.text = "Your order will be cancelled from the restaurant"
            fakeOrderView.isHidden = true
            viewDeclined.isHidden = false
            
            orderReset.setTitle("Go Back", for: .normal)
            orderReset.backgroundColor = hexStringToUIColor(kgreen)
            orderReset.setTitleColor(hexStringToUIColor(kwhite), for: .normal)
            
            addMoreButton.setTitle("Yes, Cancel It", for: .normal)
            addMoreButton.backgroundColor = hexStringToUIColor(kred)
            
        }
        else {
            
            if viewModel.declinedReason?.decline_item_code == OrderDeclinedStatus.fakeOrde.rawValue {
                fakeOrderView.isHidden = false
                viewDeclined.isHidden = true
            }
            else  if viewModel.declinedReason?.decline_item_code == OrderDeclinedStatus.otherReason.rawValue {
                fakeOrderView.isHidden = false
                viewDeclined.isHidden = true
                otherReasonMsg.text = viewModel.declinedReason?.decline_reason
            }
            else{
                
                orderReset.borderWidth = 1
                
                fakeOrderView.isHidden = true
                viewDeclined.isHidden = false
                
                if   let availableArray = viewModel.orderArray?.filter({$0.decline_reason?.decline_item_code == OrderDeclinedStatus.availablebutdeclined.rawValue}) {
                    viewModel.availableArray = availableArray
                }
                
                if   let notAvailable = viewModel.orderArray?.filter({$0.decline_reason?.decline_item_code == OrderDeclinedStatus.itemsNotAvailable.rawValue && $0.item_status == ItemStatus.declinedNeedAction.rawValue}) {
                    viewModel.unAvailableArray = notAvailable
                }
            }
        }
        
        OrderDeclineCell.registerWithTable(tblView)
    }
    
    fileprivate func callAction(){
        
        let number = viewModel.restaurantDict?.phone
        if let url = URL(string: "tel://\(number ?? "")"), UIApplication.shared.canOpenURL(url) {
            if #available(iOS 10, *) {
                UIApplication.shared.open(url)
            } else {
                UIApplication.shared.openURL(url)
            }
        }
    }
    
    
    // MARK Button Action
    @IBAction func callServerAction(_ sender: Any) {
        callServer()
    }
    
    @IBAction func scanQRCodeAction(_ sender: Any) {
        
        removeFromSuperView(self)
        coordinator?.goToTabbarController()
        
    }
    @IBAction func crossButonAction(_ sender: Any) {
        removeFromSuperView(self)
    }
    @IBAction func orderRestAction(_ sender: Any) {
        
        if viewModel.orderCancelBy == OrderCancelBy.MM &&  (viewModel.order_type == priceByOrderType.delivery.rawValue || viewModel.order_type == priceByOrderType.pickup.rawValue){
            callAction()
        }
            
        else if viewModel.isCancelOrder == true{ // pick & delivery
            removeFromSuperView(self)
        }
        else{
            self.viewModel.orderRestDelegate?.getRestOrderItem(false)
            removeFromSuperView(self)
        }
    }
    @IBAction func addMoreAction(_ sender: Any) {
        if viewModel.orderCancelBy == OrderCancelBy.MM &&  (viewModel.order_type == priceByOrderType.delivery.rawValue || viewModel.order_type == priceByOrderType.pickup.rawValue) {
            removeFromSuperView(self)
        }
        else if viewModel.isCancelOrder == true{ // pick & delivery
            self.viewModel.orderRestDelegate?.getRestOrderItem(true)
            removeFromSuperView(self)
        }else{
            for view in self.navigationController!.viewControllers{
                if view.isKind(of: MenuSegmentController.self){
                    removeFromSuperView(view)
                }
            }
            CurrentUserInfo.priceByOrderType = priceByOrderType.dining
            coordinator?.goToMenuSegment()
            removeFromSuperView(self)
            
            
        }
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        removeFromSuperView(self)
    }
    
    fileprivate func callServer(){
        var dict : [String : Any] = [String : Any]()
        
        dict[ktoken] = CurrentUserInfo.authToken
        dict[ktable_id] = CurrentUserInfo.tblID
        dict[kmessage] = "Service"
        dict[kmessage_type] = "service"
        dict[krestaurant_id] = CurrentUserInfo.restaurantId
        
        if let orderId = viewModel.orderId{
            dict[korder_id] = orderId
        }
        else {
            dict[korder_id] = "none"
        }
        
        viewModel.callServer(APIsEndPoints.kCallServer.rawValue,dict, handler: {(statusCode)in
            print(statusCode)
            removeFromSuperView(self)
        })
    }
    
}

//// UITableViewDataSource
extension OrderDeclinedViewController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        var count : Int = 0
        if viewModel.unAvailableArray?.count ?? 0 > 0{
            count = 1
        }
        if viewModel.availableArray?.count ?? 0 > 0{
            count += 1
        }
        return count
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = DeclinedItemHeader()
        headerView.setUIData(section,viewModel.availableArray?.count ?? 0,viewModel.unAvailableArray?.count ?? 0, isCancelOrder: viewModel.isCancelOrder ?? false, viewModel.orderCancelBy ?? OrderCancelBy.none, viewModel.toPay ?? 0, viewModel.order_type)
        return headerView
        
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        var height = 60
        if viewModel.toPay ?? 0  >  0{
            height += 60
        }
        
        return CGFloat(height)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0{
            return viewModel.unAvailableArray?.count ?? 0
        }
        return viewModel.availableArray?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell  = tableView.dequeueReusableCell(withIdentifier: OrderDeclineCell.reuseIdentifier, for: indexPath) as! OrderDeclineCell
        cell.selectionStyle = .none
        
        if indexPath.section == 0{
            cell.commonInit(viewModel.unAvailableArray![indexPath.row])
        }
        else {
            cell.commonInit(viewModel.availableArray![indexPath.row])
        }
        
        return cell
    }
    
    override func viewDidLayoutSubviews(){
        print(tblView.contentSize.height)
        print(ScreenSize.screenHeight)
        
        let safeArea = CGFloat( 0.getSafeAreaHeight())
        let extraHeight = 102  + safeArea
        
        bgViewHeight.constant =  (tblView.contentSize.height + extraHeight) > ScreenSize.screenHeight ?  (ScreenSize.screenHeight - extraHeight) : tblView.contentSize.height  + extraHeight
    }
}

extension OrderDeclinedViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 40
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
    }
}
