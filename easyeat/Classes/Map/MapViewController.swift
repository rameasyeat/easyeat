//
//  MapViewController.swift
//  easyeat
//
//  Created by Ramniwas on 13/05/20.
//  Copyright © 2020 EasyEat. All rights reserved.
//

import UIKit
import MapKit
import GoogleMaps

class MapViewController: BaseViewController,Storyboarded,GMSMapViewDelegate {
    
    @IBOutlet weak var gsmMapView: GMSMapView!
    var coordinator: MainCoordinator?
    
    var viewModel : MapViewModel = {
        let model = MapViewModel()
        return model
    }()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setNavWithOutView()
        navView?.backgroundColor = .clear
        navView?.layer.borderWidth = 0
        rightButton.isHidden = true
        getRouteDirection()
        setMapSetting()
    }
    
    // MARK : Map Setting
    fileprivate func setMapSetting(){
        let camera = GMSCameraPosition.camera(withLatitude: viewModel.userLocation?.latitude ?? 0, longitude: viewModel.userLocation?.longitude ?? 0, zoom: 12.0)
        
        self.gsmMapView.camera = camera
        self.gsmMapView.delegate = self
        self.gsmMapView?.isMyLocationEnabled = true
        self.gsmMapView.settings.myLocationButton = true
        self.gsmMapView.settings.compassButton = true
        self.gsmMapView.settings.zoomGestures = true
    }
    
    
    fileprivate func drawPath(){
        // print route using Polyline
        
        
        createMarker(titleMarker: "", iconMarker: #imageLiteral(resourceName: "marker") , latitude: viewModel.userLocation?.latitude ?? 0, longitude: viewModel.userLocation?.longitude ?? 0)
        
        createMarker(titleMarker: "", iconMarker: #imageLiteral(resourceName: "marker") , latitude: viewModel.restLocation?.lat ?? 0, longitude: viewModel.restLocation?.lon ?? 0)
        
        for route in self.viewModel.routesArray!
        {
            let routeOverviewPolyline = route["overview_polyline"] as? [String : Any]
            let points = routeOverviewPolyline?["points"] as? String
            let path = GMSPath.init(fromEncodedPath: points!)
            let polyline = GMSPolyline.init(path: path)
            polyline.strokeWidth = 4
            polyline.strokeColor = UIColor.red
            polyline.map = self.gsmMapView
        }
    }
    
    // MARK: function for create a marker pin on map
    func createMarker(titleMarker: String, iconMarker: UIImage, latitude: CLLocationDegrees, longitude: CLLocationDegrees) {
        let marker = GMSMarker()
        marker.position = CLLocationCoordinate2DMake(latitude, longitude)
        marker.title = titleMarker
        marker.icon = iconMarker
        marker.map = gsmMapView
    }
    
    // MARK : APIS call
    
    func getRouteDirection()
    {
        let origin = "\(viewModel.userLocation?.latitude ?? 0),\(viewModel.userLocation?.longitude ?? 0)"
        let destination = "\(viewModel.restLocation?.lat ?? 0),\(viewModel.restLocation?.lon ?? 0)"
        
        
        let url = "https://maps.googleapis.com/maps/api/directions/json?" +
            "origin=\(origin)&destination=\(destination)&" +
        "key=\(Configuration().environment.googleApisKey)"
        
        viewModel.getDirection(url, handler: {[weak self](status) in
            self?.drawPath()
        })
        
    }
    
}
