//
//  LoyaltyBenefitsViewController.swift
//  easyeat
//
//  Created by Ramniwas on 26/05/20.
//  Copyright © 2020 EasyEat. All rights reserved.
//

import UIKit

class LoyaltyBenefitsViewController: BaseViewController,Storyboarded {
    
    var coordinator: MainCoordinator?
    
    var viewModel : LoyalityViewModel = {
        let model = LoyalityViewModel()
        return model
    }()
    
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var previousButton: UIButton!
    @IBOutlet weak var nextButton: UIButton!
    @IBOutlet weak var dateLabel: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setNavWithOutView()
        titleLbl?.text = "Loyalty Benefits"
        rightButton.isHidden = true
        navView?.layer.borderWidth = 0

        setUI()
    }
    
    
    @IBAction func previousButtonAction(_ sender: Any) {
        if viewModel.pageCount >= 0{
             viewModel.pageCount +=  viewModel.numberOfDays
            getSavingDetails()
        }
    }
    @IBAction func nextButtonAction(_ sender: Any) {
        if viewModel.pageCount >= 7{
             viewModel.pageCount -=  viewModel.numberOfDays
            getSavingDetails()
        }
    }
    // UI setup
    fileprivate func setUI(){
        
        viewModel.getDefaultData()
        getSavingDetails()
        LoyalityCell.registerWithTable(tblView)
        
    }
    
    // APis call
    fileprivate func getSavingDetails(){
        
        var dict : [String : Any] = [String : Any]()
        dict[ktoken] = CurrentUserInfo.authToken
        dict[kgmtoffset] = -330
        dict[kdayoffset] =  viewModel.pageCount
        dict[kno_of_days] = viewModel.numberOfDays
        viewModel.getLoyalitygetLoyalityDetails(APIsEndPoints.kuserSaving.rawValue,dict, handler: {[weak self](statusCode)in
            self?.updateUI()
           
        })
    }
    
    fileprivate func updateUI(){
        self.tblView.isHidden = false
        dateLabel.text =  viewModel.getWeeklyTime()
        self.tblView.reloadData()

    }
    
}

// UITableViewDataSource
extension LoyaltyBenefitsViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (viewModel.loyalityData?.average_earning_saving?.count ?? 0 > 0) ? 2 : 0
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell  = tableView.dequeueReusableCell(withIdentifier: LoyalityCell.reuseIdentifier, for: indexPath) as! LoyalityCell
        cell.selectionStyle = .none
        
        cell.viewModel = viewModel
        cell.defaultDataArray = viewModel.earningTypeArray
        cell.commonInit(index: indexPath.row, dict: viewModel.loyalityData)
        
        return cell
    }
}

extension LoyaltyBenefitsViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}


