//
//  RatingViewController.swift
//  easyeat
//
//  Created by Ramniwas on 07/05/20.
//  Copyright © 2020 EasyEat. All rights reserved.
//

import UIKit
import Cosmos

enum RatingCellType : Int{
    case review = 0
    case item
}


class RatingViewController: UIViewController,Storyboarded,ItemRatingDelegate {
  
    
        
    @IBOutlet weak var ratingLabel: CustomLabel!
    @IBOutlet weak var ratingView: CosmosView!
    @IBOutlet weak var reviewTextField: UITextField!
    @IBOutlet weak var feedbackButton: CustomButton!
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var textViewLine: UIView!
    
    var coordinator: MainCoordinator?
    
    var viewModel : RatingViewModel = {
        let model = RatingViewModel()
        return model
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUI()
    }
    
    fileprivate func setUI(){
        getFeedbackOptions()
        
        RatingCell.registerWithTable(tblView)
        ReviewCell.registerWithTable(tblView)
        updateRating()
    }
    
    // MARK : Button action
    @IBAction func CrossButtonAction(_ sender: Any) {
        backAction()

    }
    @IBAction func feedBackButtonAction(_ sender: Any) {
        submitFeedback()
    }
    
    // Update Rating Label
    fileprivate func updateRating(){
        ratingView.didTouchCosmos = { (rating) in
            self.ratingLabel.text = self.viewModel.setMealRating(rating)
            self.feedbackButton.alpha = 1
            self.feedbackButton.isUserInteractionEnabled = true
        }
        
    }
    
    // back on menuview controller
    func backAction(){
           for view in self.navigationController!.viewControllers{
               if view.isKind(of: MenuSegmentController.self){
                removeFromSuperView(view)
                //self.navigationController?.popToViewController(view, animated: true)
               // return
               }
           }
        // no controller
        CurrentUserInfo.priceByOrderType = priceByOrderType.dining
        coordinator?.goToMenuSegment()
       }
    
     func getRating(_ rating: Double) {
       // viewModel.orderData?.orderItems?[0].rating = rating
       // tblView.reloadData()
      }
        
    // APIS call
      
      fileprivate func getFeedbackOptions(){
          var dict : [String : Any] = [String : Any]()
          dict[ktoken] =  CurrentUserInfo.authToken
          viewModel.getFeedbackOptions(APIsEndPoints.kgetReviewStructure.rawValue,dict, handler: {[weak self](statusCode)in
            self?.tblView.reloadData()
      })
    }
    
    fileprivate func submitFeedback(){
            var dict : [String : Any] = [String : Any]()
        dict[ktoken] =  CurrentUserInfo.authToken
        dict[korder_id] =  viewModel.orderData?.order_id
        dict[krating] = ratingView.rating

        dict[ksuggestion] = reviewTextField.text
        dict[kitems_rating] = viewModel.getSelectedItemRating()
        dict[kfeedback_options] = viewModel.getSelectedFeedbackOption()

            viewModel.submitFeedback(APIsEndPoints.kReviewCreate.rawValue,dict, handler: {[weak self](statusCode)in
                self?.backAction()
        })
      }
}

// UITableViewDataSource
extension RatingViewController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        guard let dict = viewModel.feedbackData?.feedback_options else { return 0}
        if dict.count > 0  {
            return  viewModel.sectionArray.count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = viewModel.setHeaderView(section)
        return view
        
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return CGFloat(viewModel.headerHeight)
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == RatingCellType.review.rawValue{
            return RatingCellType.item.rawValue
        }
        return viewModel.orderData?.orderItems?.count ?? 0
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.section == RatingCellType.review.rawValue{
            let cell  = tableView.dequeueReusableCell(withIdentifier: ReviewCell.reuseIdentifier, for: indexPath) as! ReviewCell
            cell.selectionStyle = .none
            cell.reviewTypeArray = viewModel.feedbackData?.feedback_options
            cell.commonInit(viewModel.feedbackData!.feedback_options!)
            
            return cell
            
        }
        let cell  = tableView.dequeueReusableCell(withIdentifier: RatingCell.reuseIdentifier, for: indexPath) as! RatingCell
        cell.ratingView.tag = indexPath.row
        cell.itemRatingDelegate = self
        cell.selectionStyle = .none
        let dict = viewModel.orderData?.orderItems?[indexPath.row]
        cell.commoInit(dict!)

        
        return cell
    }
}

extension RatingViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == RatingCellType.review.rawValue{
            return 110
        }
        return UITableView.automaticDimension
    }
}

extension RatingViewController: UITextFieldDelegate {
   
  func textFieldDidBeginEditing(_ textField: UITextField) {
    textViewLine.backgroundColor = hexStringToUIColor(korange)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        textViewLine.backgroundColor = hexStringToUIColor(kLightGray)
            textField.resignFirstResponder()
        
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
    {
           
        return true
    }

}
