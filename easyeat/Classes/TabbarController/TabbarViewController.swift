//
//  TabbarViewController.swift
//  easyeat
//
//  Created by Ramniwas on 26/03/20.
//  Copyright © 2020 EasyEat. All rights reserved.
//

import UIKit

class TabbarController: UITabBarController,UITabBarControllerDelegate,Storyboarded {
    var coordinator: MainCoordinator?

    enum TabbarIndex : Int {
        case home
        case cart
    }
    
    var homeView : MenuViewController?
    var cart : CartViewController?

    override func viewDidLoad() {
        super.viewDidLoad()

        cleanTitles()
        showTabbar()
        self.delegate = self
   
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        if self.selectedIndex == 1 {
        self.selectedIndex = 0
        }
    }
    
    
    fileprivate func  showTabbar(){
        // home view
        let home = HomeViewController.instantiate()
        home.coordinator = self.coordinator

       home.tabBarItem = UITabBarItem(title: "Home", image: #imageLiteral(resourceName: "SHome"), selectedImage: #imageLiteral(resourceName: "SHome"))
        
        // scan
        let scanView =  QRCodeViewController.instantiate()
       scanView.coordinator = self.coordinator
        scanView.fromView = .tabbar

        scanView.tabBarItem = UITabBarItem(title: "Scan", image: #imageLiteral(resourceName: "scan"), selectedImage: #imageLiteral(resourceName: "scan"))

        
        // profile
        let profile =  ProfileViewController.instantiate()
        profile.coordinator = self.coordinator
        profile.viewModel.fromView = .tabbar
        profile.tabBarItem = UITabBarItem(title: "Profile", image: #imageLiteral(resourceName: "profile"), selectedImage: #imageLiteral(resourceName: "profile"))

        let tabBarList = [home, scanView,profile]
        viewControllers = tabBarList
    }
    
    override func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        
        
       

    }
    
    func tabBarController(_ tabBarController: UITabBarController, didSelect viewController: UIViewController) {
        
        
        if (CurrentUserInfo.authToken == "" || CurrentUserInfo.authToken == nil) && selectedIndex == 2{
            tabBarController.selectedIndex = 0
            
                AlertWithAction(title:"Login", message: "Please login to access your profile", ["Login", "Cancel"], vc: tabBarController.selectedViewController!) { (index) -> (Void) in
                       if index == 1{
                           self.coordinator?.goToUserMobileView()
                       }
                   }
               }

    }
}


extension  TabbarController {
    func cleanTitles() {
        guard let items = self.tabBar.items else {
            return
        }
        for item in items {
            item.title = ""
            item.imageInsets = UIEdgeInsets(top: 6, left: 0, bottom: -6, right: 0)
        }
    }
}

