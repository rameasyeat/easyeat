//
//  CartViewController.swift
//  easyeat
//
//  Created by Ramniwas on 26/03/20.
//  Copyright © 2020 EasyEat. All rights reserved.
//

import UIKit
import Lottie
import Toast_Swift

enum RestaurentType : Int {
    case dining = 1
    case delivery = 2
    case both = 3
    
}
class CartViewController: BaseViewController,Storyboarded,SpecificRequestDelegate,AddSpecificNoteDelegate,UpdateItemCountDelegate,ViewCartdelegate,DeliveryTypeDelegate,DeliveryAddressDelegate,NewAddressDelegate,VariationDelegate,ItemsUpdateDelegates {
   
    
    var coordinator: MainCoordinator?
    
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var emptyView: UIView!
    @IBOutlet weak var headerView: UIView!

    @IBOutlet weak var tblBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var orderRuningAnimation: LottieView!

    var viewModel : CartViewModel = {
        let model = CartViewModel()
        return model
    }()
    
    var isItemCountViewShow : Bool = false{
        didSet{
            if isItemCountViewShow {
                tblBottomConstraint.constant = (viewModel.cardItemView?.frame.size.height)!
            }
            else {
                tblBottomConstraint.constant = 0
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setNavigation()
        hideServiceButton()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        UISetup()
    }
    
    fileprivate func UISetup(){
        
        self.headerView.isHidden = true
        self.headerView.frame.size.height = 0
        getCartList(true)
        
        
        if (viewModel.restaurantDict?.type == RestaurentType.delivery.rawValue){
            
            viewModel.restaurantDict?.orderType = (CurrentUserInfo.priceByOrderType == priceByOrderType.delivery) ? .delivery : .pickup
            checkRuningOrder()
            /*viewModel.checkIsPartnerDelivery()*/
            
            viewModel.getCurrentLocation(handler: {[weak self](isCurrentLocation) in
                
                self?.getAddressList()
                
                /*// check partner delivery
                 if self?.viewModel.deliveryPartner?.isDeliveryPartner == true{
                 self?.partnerPriceCalculation()
                 }*/
            })
        }
        else{
            self.gatCartBill(true)
        }
        
        titleLbl?.text = "Cart"
        viewModel.addSpecificRequestInfo()
        viewModel.registerCell(tblView)
    }
    
    // MARK - Button Action
    @IBAction func menuButtonAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func orderStatusView(_ sender: Any) {
        coordinator?.goToOrderView(viewModel.restaurantDict!)
    }
    override func   backButtonAction() {
        backAction()
    }
    
    func updateRemoveItems(_ isRemove: Bool) {
           UISetup()
    }
    
    fileprivate func hideServiceButton(){
        // hide right button in case of delivery and pickup
        if viewModel.restaurantDict?.type == RestaurentType.dining.rawValue {
            notificationButton.isHidden = false
        }else {
            notificationButton.isHidden = true
        }
    }
    
    override func notificationButtonAction() {
        let controller = QuickServiceViewController.instantiate()
        controller.viewModel.servicesArray = viewModel.getSortedArray()
        controller.viewModel.orderId = viewModel.cartArray?.cart?.order?.order_id ?? "none"
        addRootView(controller)
    }
    
    func addressButtonAction() {
        if viewModel.restaurantDict?.type == RestaurentType.delivery.rawValue{
            
            if CurrentUserInfo.authToken == "" || CurrentUserInfo.authToken == nil{
                RootViewController.controller?.view.makeToast("Please login to add address")
                return
            }
            addressView()
        }
    }
    
    func updateCellData(_ isUpdateCell: Bool) {
        if isUpdateCell {
            let controller = SpecificRequestViewController.instantiate()
            controller.noteDelegate = self
            controller.notes = viewModel.specificRequestDict?.description
            addRootView(controller)
        }
        else {
            sendSpecificRequest("", true)
        }
    }
    
    private func addressView(){
        let controller = DeliveryAddressViewController.instantiate()
        controller.viewModel.deliveryAddressDelegate = self
        controller.viewModel.selectedAddressId = viewModel.selectedAddressId
        addRootView(controller)
    }
    
    func addNote(_ description: String) {
        sendSpecificRequest(description, false)
    }
    
    func updateVarientItem(_ result : CartListData){
        getCartList(false)
        self.gatCartBill(false)
       }
    
    func backAction(){
        self.navigationController?.popViewController(animated: true)
        
        //        for view in self.navigationController!.viewControllers{
        //            if view.isKind(of: MenuViewController.self){
        //                self.navigationController?.popToViewController(view, animated: true)}
        //        }
    }
    
    // delivery selection type delegate
    func selectedDeliveryType(_ type: OrderType) {
        viewModel.restaurantDict?.orderType = OrderType(rawValue: type.rawValue)!
        /*viewModel.addDeliveryCharges(type)*/
        self.hideBottomView((self.viewModel.cartArray!.cart!))
        CurrentUserInfo.priceByOrderType = (type == OrderType.delivery) ? priceByOrderType.delivery : priceByOrderType.pickup
        gatCartBill(false)
    }
    
    func setAddress(_ dict: DeliveryAddress) {
        viewModel.getDefaultAddress(dict, handler:{[weak self](loadDistance) in
            self?.getDistance()
            self?.gatCartBill(false)
            
        })
    }
    
    func addNewAddress(){
        coordinator?.goToAddNewAddressView(self, viewModel.restaurantDict!)
    }
    
    // MARK APIS call
    
    fileprivate func getDistance(){
        self.getDistance((self.viewModel.getLocation(self.viewModel.restaurantDict?.location)))
        self.tblView.reloadData()
    }
    
    func updateCount(_ indexPath: IndexPath, _ itemCount: Int,_ addRemove : Int) {
        
        let item = viewModel.cartArray?.cart?.cart_items?[indexPath.row]
        let dict = viewModel.getItemCountParam(item!, quantity: addRemove)
        
        let addedItems = viewModel.cartArray?.cart?.cart_items?.filter{$0.item_id == item?.item_id}
        
        if addedItems?.count ?? 0 > 0  && addRemove == 1 && item?.item_details?.gvariations != nil{
            let dict = viewModel.isItemContaintVariation(item!)
            let groupArray = viewModel.getvariation(dict)
                       

            let controller = CustomizationViewController.instantiate()
            controller.viewModel.variationDelegate = self
            controller.viewModel.groupArray = groupArray
            controller.viewModel.selectedGroupIndex = 0
            controller.viewModel.addedItem = addedItems?[0]
            controller.viewModel.itemDetails = viewModel.getItemDetails(item!)
            controller.viewModel.restaurantDict = viewModel.restaurantDict
            addRootView(controller)
            return

        }
        else if item?.item_details?.gvariations != nil && addRemove == 1{// check varitation
          
            let dict = viewModel.isItemContaintVariation(item!)
            let groupArray = viewModel.getvariation(dict)
                            
            
            let controller = VariationViewController.instantiate()
            controller.viewModel.variationDelegate = self
            controller.viewModel.groupArray = groupArray
            controller.viewModel.selectedGroupIndex = 0
            controller.viewModel.itemDetails = viewModel.getItemDetails(item!)
            controller.viewModel.restaurantDict = viewModel.restaurantDict

            addRootView(controller)
            return
        }
        
        addItemInCart(APIsEndPoints.kItemInCart.rawValue,dict, handler: {[weak self](result,statusCode)in
            self?.viewModel.cartArray = result
            self?.gatCartBill(false)
        })
    }
   
    fileprivate func getCartList(_ isLoading : Bool){
        getItemAddedInCart(APIsEndPoints.kGetCartList.rawValue,viewModel.getCartListParam(),isLoading, handler: {[weak self](result,statusCode)in
            self?.viewModel.cartArray = result
            self?.hideBottomView(result.cart!)
            self?.tblView.reloadData()
        })
    }
    
    fileprivate func partnerPriceCalculation(){
        viewModel.partnerPriceCalculation(APIsEndPoints.kPartnerPriceCal.rawValue,viewModel.getPartnerPriceParam(),handler: {[weak self](statusCode)in
            /* self?.viewModel.addDeliveryCharges((self?.viewModel.restaurantDict!.orderType)!)*/
            self?.tblView.reloadData()
        })
    }
    
    fileprivate func getAddressList(){
        var dict : [String : Any] = [String : Any]()
        dict[ktoken] =  CurrentUserInfo.authToken ?? kdefaultToken
        viewModel.getAddress(APIsEndPoints.kuserProfileNewAddress.rawValue,dict,handler: {[weak self](statusCode)in
            self?.getDistance()
            self?.gatCartBill(false)
        })
    }
    
    fileprivate func gatCartBill(_ isLoading : Bool){
        viewModel.getBillDetails(APIsEndPoints.kcartBill.rawValue,isLoading,viewModel.getCartBillParam(),handler: {[weak self](statusCode)in
            self?.tblView.reloadData()
            guard let cart = self?.viewModel.cartArray?.cart else {return}
            self?.hideBottomView(cart)
        })
    }
    func createOrder(){
        let arrayItems = viewModel.checkIfItemNotAvailable()
        if arrayItems.count > 0 {
            let controller = ItemNotAvailableController.instantiate()
            controller.viewModel.itemsUpdateDelegates = self
            controller.viewModel.itemsArray = arrayItems
            addRootView(controller)
            return
        }
        
        // create order
        viewModel.createOrder(APIsEndPoints.kcreateOrder.rawValue,viewModel.getCreateOrderParam(), handler: {[weak self](result,statusCode)in
            if statusCode == 1 {
                self?.coordinator?.goToOrderView((self?.viewModel.restaurantDict!)!)
            }
        })
    }
    
    
    fileprivate func checkRuningOrder(){
        var dict : [String : Any] = [String : Any]()
        dict[ktoken] =  CurrentUserInfo.authToken ?? kdefaultToken
        viewModel.getOrderStatus(APIsEndPoints.kgetRunningOrder.rawValue, dict) { [weak self](runingOrder, statusCode) in
            
            if runingOrder == true{
                self?.headerView.isHidden = false
                self?.headerView.frame.size.height = 442
                
                self?.viewModel.showSuccessAnimation(self!.orderRuningAnimation)
            }
            else {
                self?.headerView.isHidden = true
                self?.headerView.frame.size.height = 0
            }
            self?.tblView.reloadData()
        }
    }
    
    fileprivate func getDistance(_ location : [Double]){
        var dict : [String : Any] = [String : Any]()
        dict[kdestinations] = viewModel.json(from: viewModel.getLocation(viewModel.restaurantDict?.userCurrentLocation))
        dict[korigins] = viewModel.json(from: location)
        
        print(dict)
        viewModel.getDistance(APIsEndPoints.kgetDistance.rawValue,dict, handler: {[weak self](statusCode)in
            /* self?.viewModel.addDeliveryCharges((self?.viewModel.restaurantDict!.orderType)!)*/
            self?.tblView.reloadData()
        })
    }
    
    fileprivate func sendSpecificRequest(_ text : String,_ remove : Bool){
        var dict : [String : Any] = [String : Any]()
        dict[kcart_token] =  CurrentUserInfo.randomToken
        dict[kspecial_note] = text
        
        viewModel.specificRequest(APIsEndPoints.kSpecilaNotes.rawValue,dict, handler: {[weak self](result,statusCode)in
            if statusCode == 1 {
                self?.addNotes(remove,text)
            }
        })
    }
    
    fileprivate func addNotes(_ removeNotes : Bool,_ description : String){
        if removeNotes == true {
            viewModel.addSpecificRequestInfo()
        }
        else {
            self.viewModel.specificRequestDict = SpecificRequest(true,description)
        }
        self.tblView.reloadData()
    }
}


// UITableViewDataSource
extension CartViewController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        if viewModel.isRuningOrder == true && (viewModel.restaurantDict?.type == RestaurentType.delivery.rawValue ) {
            return 2
            
        }
        return (viewModel.restaurantDict?.type == RestaurentType.delivery.rawValue ) ? 4 : 3
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == CartTypeCell.cart.rawValue{
            return viewModel.cartArray?.cart?.cart_items?.count ?? 0
        }
        return CartTypeCell.specificRequest.rawValue
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.section == CartTypeCell.specificRequest.rawValue{
            let cell  = tableView.dequeueReusableCell(withIdentifier: SpecificRequestCell.reuseIdentifier, for: indexPath) as! SpecificRequestCell
            cell.selectionStyle = .none
            cell.specificRequestDelegate = self
            cell.commonInit(viewModel.specificRequestDict!)
            return cell
        }
        else  if indexPath.section == CartTypeCell.delivery.rawValue && viewModel.isRuningOrder == false{
            if viewModel.restaurantDict?.type == RestaurentType.dining.rawValue{
                return addBillDetailCell(tableView, indexPath)
            }
            else if viewModel.restaurantDict?.type == RestaurentType.delivery.rawValue{
                return addDeliveryCell(tableView, indexPath)
            }
        }
        else if indexPath.section == CartTypeCell.billDetail.rawValue && viewModel.isRuningOrder == false{
            return addBillDetailCell(tableView, indexPath)
        }
        
        let cell  = tableView.dequeueReusableCell(withIdentifier: RestaurantItemCell.reuseIdentifier, for: indexPath) as! RestaurantItemCell
        cell.selectionStyle = .none
        cell.tblView = tblView
        cell.updateItemCountDelegate = self
        
        guard let dictItem = viewModel.cartArray?.cart?.cart_items?[indexPath.row] else {return cell}
        cell.commonCartInit(dictItem)
        return cell
    }
    
    
    func addDeliveryCell(_ tableView : UITableView,_ indexPath : IndexPath) -> OrderDeliveryCell{
        let cell  = tableView.dequeueReusableCell(withIdentifier: OrderDeliveryCell.reuseIdentifier, for: indexPath) as! OrderDeliveryCell
        cell.deliveryTypeDelegate = self
        cell.selectionStyle = .none
        cell.commonInit(viewModel.restaurantDict!, viewModel.distanceModel)
        return cell
    }
    
    func addBillDetailCell(_ tableView : UITableView,_ indexPath : IndexPath) -> BillDetailCell{
        let cell  = tableView.dequeueReusableCell(withIdentifier: BillDetailCell.reuseIdentifier, for: indexPath) as! BillDetailCell
        cell.selectionStyle = .none
        guard let dictItem = self.viewModel.cartArray else {return cell}
        let deliveryDict = self.viewModel.partnerDeliveryFeeData
        cell.setCartUIData(dictItem.cart!,viewModel.restaurantDict,deliveryDict,viewModel.billDetails)
        return cell
    }
    
}

extension CartViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
        if indexPath.section == CartTypeCell.specificRequest.rawValue{
            updateCellData(true)
        }
        
    }
}




