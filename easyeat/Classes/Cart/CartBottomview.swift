//
//  CartBottomview.swift
//  easyeat
//
//  Created by Ramniwas on 18/05/20.
//  Copyright © 2020 EasyEat. All rights reserved.
//

import Foundation

extension CartViewController {
    
    @available(iOS 11.0, *)
    func addCardItemView(){
        
        viewModel.cardItemView = CardItemPopView(frame: CGRect(x: 0, y: ScreenSize.screenHeight, width: ScreenSize.screenWidth, height: CGFloat(viewModel.cardItemViewHeight + 0.getSafeAreaHeight())))
        viewModel.cardItemView?.viewCartLabel.text = "Place Order"
        viewModel.cardItemView?.backgroundColor = .purple
        viewModel.cardItemView?.viewCartDelegate = self
        self.view.addSubview(viewModel.cardItemView!)
        self.view.bringSubviewToFront(viewModel.cardItemView!)
        isItemCountViewShow = true
        viewModel.cardItemView?.animShow(0.50)
    }
    
    func hideCardItemView(){
        if viewModel.cardItemView != nil{
            isItemCountViewShow = false
            viewModel.cardItemView?.animHide(0.50)
        }
    }
    
    func viewCart() {
        if (CurrentUserInfo.authToken != nil && (CurrentUserInfo.authToken != "")) {
            if (viewModel.restaurantDict!.orderType.rawValue == OrderType.delivery.rawValue) && viewModel.restaurantDict?.isDeliverable == false{
                
                if  viewModel.restaurantDict?.deliverylimit == nil{
                    self.view.makeToast(kSelectAddress)
                }
            else  if  viewModel.restaurantDict?.userCurrentDistance == nil || viewModel.restaurantDict?.userCurrentDistance == 0{
                    self.view.makeToast("Sorry! Restaurant not serving in your area")
                    }
                else{
                    
                    let restaurantDistance  = viewModel.getDeliveryDistance(viewModel.restaurantDict!)
                    let userCurrentDistance = viewModel.distanceModel?.rows?.last?.elements?.last?.distance?.text

                    self.view.makeToast("Your address is \(userCurrentDistance ?? "0 km") away from this restaurant, it delivery only upto \(restaurantDistance) km")
                }
            }
            else {
                createOrder()
            }
        }else{
            coordinator?.goToUserMobileView()
        }
    }
    
    func hideBottomView(_ cart : Cart){
        
        self.viewModel.updateBottmView(cart, handler: { (isshow) in
            
            print(self.viewModel.isRuningOrder)
            if isshow == true {
                self.tblView.isHidden = false
                self.emptyView.isHidden = true
                if self.viewModel.cardItemView == nil  && self.viewModel.isRuningOrder == false {
                    if #available(iOS 11.0, *) {
                        self.addCardItemView()
                    }
                }
                else if self.viewModel.isRuningOrder == true{
                    self.isItemCountViewShow = false
                    self.hideCardItemView()
                }
            }
            else {
                self.tblView.isHidden = true
                self.emptyView.isHidden = false
                self.hideCardItemView()
            }})
    }
}
