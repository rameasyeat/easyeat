//
//  SpecificRequestViewController.swift
//  easyeat
//
//  Created by Ramniwas on 03/04/20.
//  Copyright © 2020 EasyEat. All rights reserved.
//

import UIKit

protocol AddSpecificNoteDelegate {
    func addNote(_ description : String)
}
class SpecificRequestViewController: UIViewController,Storyboarded {
    
    var noteDelegate : AddSpecificNoteDelegate?
    var notes : String?
    var coordinator: MainCoordinator?
    let COMMENTS_LIMIT = 120
    @IBOutlet weak var safeAraiHeight: NSLayoutConstraint!
    
    
    @IBOutlet weak var textView: UITextView!
    @IBOutlet weak var addButton: CustomButton!
    
    var istextAdded : Bool = false {
        didSet{
            if istextAdded == false{
                addButton.alpha = 0.4
                addButton.isUserInteractionEnabled = false
            }
            else{
                addButton.alpha = 1
                addButton.isUserInteractionEnabled = true
            }
        }
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        textView.delegate = self
        if (notes != ""){
            textView.textColor = UIColor.black
            textView.text = notes

        }
        
        safeAraiHeight.constant = 0.getSafeAreaHeight() > 0 ? 0 : -34
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(notification:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(notification:)), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    @objc private func keyboardWillShow(notification: NSNotification) {
        
        self.view.frame.origin.y =  -256+(0.getSafeAreaHeight() > 0 ? 0 : +34)
        showAnimation(notification,self)
    }
    
    @objc private func keyboardWillHide(notification: NSNotification) {
        self.view.frame.origin.y = 0
        
    }
    
    @IBAction func closeButtonAction(_ sender: Any) {
        dismissView()
    }
    @IBAction func addButtonAction(_ sender: Any) {
        noteDelegate?.addNote(textView.text)
        dismissView()
    }
    
    fileprivate func dismissView(){
        removeFromSuperView(self)
    }
    
    func removeFromSuperView(_ controller : UIViewController){
        controller.willMove(toParent: nil)
        controller.view.removeFromSuperview()
        controller.removeFromParent()
    }
}

extension SpecificRequestViewController : UITextViewDelegate{
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.lightGray || textView.text == kAddCookingInstructions {
            textView.text = nil
            textView.textColor = UIColor.black
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = kAddCookingInstructions
            textView.textColor = UIColor.lightGray
        }
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        
        if text == ""{
            istextAdded = false
        }
        else {
            istextAdded = true
        }
        
        if(text == "\n") {
            textView.resignFirstResponder()
            return false
        }
        return textView.text.count + (text.count - range.length) <= COMMENTS_LIMIT
    }
}
