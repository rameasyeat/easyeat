//
//  ItemDetailsViewController.swift
//  easyeat
//
//  Created by Ramniwas Patidar on 23/07/20.
//  Copyright © 2020 EasyEat. All rights reserved.
//

import UIKit
import SDWebImage

protocol ItemsDetailsDelegates {
    func updateAddItemsDetails()
}

class ItemDetailsViewController: UIViewController,Storyboarded,UIScrollViewDelegate {
    var viewModel : ItemDetailViewModel = {
        let model = ItemDetailViewModel()
        return model
    }()
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var imagesView: UIView!
    @IBOutlet weak var itemNameLabel: CustomLabel!
    @IBOutlet weak var itemDescriptionLabel: CustomLabel!
    @IBOutlet weak var vegImageView: UIImageView!
    
    @IBOutlet weak var crossButton: UIButton!
    @IBOutlet weak var pageControl: UIPageControl!
    
    let scrollView: UIScrollView = {
        let scroll = UIScrollView()
        scroll.isPagingEnabled = true
        scroll.showsVerticalScrollIndicator = false
        scroll.showsHorizontalScrollIndicator = false
        return scroll
    }()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        scrollView.frame = imagesView.frame
        imagesView.addSubview(scrollView)
        
//        pageControl.bringSubviewToFront(scrollView)
//        crossButton.bringSubviewToFront(scrollView)
        
        imagesView.bringSubviewToFront(pageControl)
        imagesView.bringSubviewToFront(crossButton)

        
        crossButton.layer.applySketchShadow(color: hexStringToUIColor("#00001A"), alpha: 0.4, x: 0, y: 0, blur: 15, spread: 0)
        crossButton.layer.masksToBounds = false

        bgView.layer.applySketchShadow(color: hexStringToUIColor("#00001A"), alpha: 0.4, x: 0, y: 0, blur: 15, spread: 0)
        bgView.layer.masksToBounds = false
        

        getItemImages()
       // popAnimation()
    }
    
    fileprivate func setUI(){
        setupImages()
        
        itemNameLabel.text = viewModel.itemDetails?.name
        itemDescriptionLabel.text = viewModel.itemDetails?.item_desc
        
        if viewModel.itemDetails?.filters?.uppercased() == "|VEG|"{
            vegImageView.image =  #imageLiteral(resourceName: "veg")
        }else {
            vegImageView.image =  #imageLiteral(resourceName: "nonveg")
        }
        

        
    }
    
    // set images
    func setupImages(){
        
        let count : Int = viewModel.itemImagesArray?.count ?? 0
        pageControl.numberOfPages = count
        
        for i in 0..<count{
            
            let imageView = UIImageView()
            
            imageView.sd_setImage(with:  URL(string:(viewModel.itemImagesArray?[i])!), placeholderImage:UIImage(named: "food") , options: .progressiveDownload, completed: nil)
            
            let xPosition = imagesView.frame.size.width * CGFloat(i)
            imageView.frame = CGRect(x: xPosition, y: 0, width: imagesView.frame.size.width, height: scrollView.frame.height)
            imageView.contentMode = .scaleAspectFit
            
            scrollView.contentSize.width = imagesView.frame.size.width * CGFloat(i + 1)
            scrollView.addSubview(imageView)
            scrollView.delegate = self
        }
        
    }
    
    // Animation
    
    fileprivate func popAnimation(){
        
        let newButtonWidth: CGFloat =  self.crossButton.frame.size.width/2
        let newButtonHeight: CGFloat =  self.crossButton.frame.size.height/2

        UIView.animate(withDuration: 1.0, //1
            delay: 0.5, //2
            usingSpringWithDamping: 0.5, //3
            initialSpringVelocity: 0.5, //4
            options: UIView.AnimationOptions.curveEaseInOut, //5
            animations: ({ //6
                self.crossButton.frame = CGRect(x: 0, y: 0, width: newButtonWidth, height: newButtonHeight)
                self.crossButton.center = self.view.center
        }), completion: nil)
        
    }
    
    // MARK scroll view Delegates
   func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
            let pageWidth = self.scrollView.frame.size.width
            pageControl.currentPage = Int(self.scrollView.contentOffset.x / pageWidth)
        }
    
    @IBAction func addCartButtonAction(_ sender: Any) {
        updateCount(1)
    }
    @IBAction func crossButtonAction(_ sender: Any) {
        removeFromSuperView(self)
    }
    
    
    // Get Items
    
    func updateCount(_ addRemove : Int) {
           
        if self.viewModel.menuItemArray?.count == 0 {return}
           let addedItems = viewModel.itemAddedCartArray?.cart?.cart_items?.filter{$0.item_id == viewModel.itemDetails?.id}
           
        if addedItems?.count ?? 0 > 0  && addRemove == 1 && viewModel.itemDetails?.gvariations != nil{
            
            self.view.isHidden = true
               let dict = viewModel.isItemContaintVariation(viewModel.itemDetails!)
               let groupArray = viewModel.getvariation(dict)
               
               let controller = CustomizationViewController.instantiate()
            controller.viewModel.variationDelegate = viewModel.variationDelegate
               controller.viewModel.groupArray = groupArray
               controller.viewModel.selectedGroupIndex = 0
               controller.viewModel.itemDetails = viewModel.getItemDetails(viewModel.itemDetails!)
               controller.viewModel.addedItem = addedItems?[0]
               controller.viewModel.restaurantDict = viewModel.restaurantDict
               addRootView(controller)
               return
               
           }
           else if viewModel.isItemContaintVariation(viewModel.itemDetails!).isEmpty == false && addRemove == 1{// check varitation
               self.view.isHidden = true

               let dict = viewModel.isItemContaintVariation(viewModel.itemDetails!)
               let groupArray = viewModel.getvariation(dict)
               
               let controller = VariationViewController.instantiate()
            controller.viewModel.variationDelegate = viewModel.variationDelegate
               controller.viewModel.groupArray = groupArray
               controller.viewModel.fromViewType = .menu
               controller.viewModel.selectedGroupIndex = 0
               controller.viewModel.itemDetails = viewModel.getItemDetails(viewModel.itemDetails!)
               controller.viewModel.restaurantDict = viewModel.restaurantDict
               
               addRootView(controller)
               return
           }
           
        addItemInCart(APIsEndPoints.kItemInCart.rawValue,self.viewModel.addItemDict(viewModel.itemDetails!, addRemove), handler: {[weak self](result,statusCode)in
            self?.viewModel.itemsDetailsDelegates?.updateAddItemsDetails()
            removeFromSuperView(self!)
           })
       }
    
    // APis Call
    
    fileprivate func getItemImages(){
        
        var dict : [String : Any] = [String : Any]()
        dict[kitem_id] = viewModel.itemDetails?.id
        dict[krestaurant_id] = CurrentUserInfo.restaurantId
        
        viewModel.getItemImages(APIsEndPoints.kItemsImages.rawValue,dict, handler: {[weak self](statusCode)in
            self?.setUI()
        })
    }
    
}



