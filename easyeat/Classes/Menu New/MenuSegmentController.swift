//
//  MenuSegmentController.swift
//  easyeat
//
//  Created by Ramniwas on 08/07/20.
//  Copyright © 2020 EasyEat. All rights reserved.
//

import UIKit

import UIKit
import MXScroll



class MenuSegmentController: BaseViewController,Storyboarded, MSegmentedViewControllerDelegate {
    
    var mx: MXViewController<MSSegmentControl>!
    var coordinator: MainCoordinator?
    var segment : MSSegmentControl?
    var isScrollEnabled : Bool = true
    var isItemCountViewShow : Bool?
    var dinein : MenuViewController?
    var delivery : MenuViewController?
    var pickup : MenuViewController?

    
    @IBOutlet var containerView: UIView?
    var userDictData : UserData?
    
    lazy var viewModel : MenuSegmentViewModel = {
        let model = MenuSegmentViewModel()
        return model
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setNavigation()
        addMenuButton()
        getRestaurentDetails()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override  func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
    }
    
    fileprivate func setRightTopButtonImage(_ index : Int){
        if index == OrderType.dining.rawValue && CurrentUserInfo.isScanQRCode != nil{
            notificationButton.isHidden = false
            notificationButton.setImage(UIImage(named: "XMLID"), for: .normal)
        }
        else {
            notificationButton.setImage(UIImage(named: "profile"), for: .normal)
            notificationButton.isHidden = true
        }
    }
    
    override func backButtonAction() {
        let views  = (self.navigationController?.viewControllers)!
        for view in views{
            if view is TabbarController{
                self.navigationController?.popToViewController(view, animated: true)
                return
            }
        }
        coordinator?.goToTabbarController()
    }
    
    fileprivate func getRestaurentDetails(){
        var dict : [String : Any] = [String : Any]()
        dict[krestaurant_id] = CurrentUserInfo.restaurantId
        print(dict)
        viewModel.getRestaurentDetails(APIsEndPoints.krestaurantDetails.rawValue,dict, handler: {(result,statusCode)in
            self.getMenu()
            
        })
    }
    
    fileprivate func getMenu(){
        var dict : [String : Any] = [String : Any]()
        dict[krestaurant_id] = CurrentUserInfo.restaurantId
        
        viewModel.getMenuItem(APIsEndPoints.kGetMenu.rawValue,dict, handler: {[weak self](statusCode)in
            self?.loadVC()
        })
    }
    
    
    func getTitle()->[String]{
        let tiles = [NSLocalizedString("Dine-in", comment: ""),NSLocalizedString("Delivery", comment: ""),NSLocalizedString("Self Pickup", comment: "")]
        return tiles
    }
    
    func loadVC() {
        
        viewModel.restaurentService =  viewModel.restDict?.services
        viewModel.restaurantFilter =  viewModel.restDict?.filters
        
        segment = MSSegmentControl(sectionTitles: getTitle(), sectionImages: [#imageLiteral(resourceName: "UnSelectedDineIn"),#imageLiteral(resourceName: "UnSelectedDelivery"),#imageLiteral(resourceName: "UnSelectedPickup")], selectedImages: [#imageLiteral(resourceName: "dining-room"),#imageLiteral(resourceName: "Delivery"),#imageLiteral(resourceName: "pickup")])
        segment?.delegate = self
        segment?.type = .textImages
        segment?.selectedSegmentIndex = 1
        
        setupSegment(segmentView: segment!)
        
        let headerView = MenuHeaderViewController.instantiate()// header view
        headerView.restDict  =   viewModel.restDict
        
        dinein = MenuViewController.instantiate()// dine in
        dinein?.viewModel.menuItemArray  =  viewModel.menuItemArray
        dinein?.viewModel.menuArray  =  viewModel.menuArray
        
        dinein?.viewModel.restaurantDict  =  viewModel.restDict
        
        dinein?.coordinator = coordinator
        dinein?.viewModel.segmentView = self
        dinein?.viewModel.isItemCountViewShow = isItemCountViewShow
        dinein?.viewModel.customMenuButton = viewModel.customMenuButton
        
        
        delivery = MenuViewController.instantiate() // delivery
        delivery?.viewModel.restaurantDict  =  viewModel.restDict
        delivery?.viewModel.menuItemArray  =  viewModel.menuItemArray
        delivery?.viewModel.menuArray  =  viewModel.menuArray
        
        delivery?.coordinator = coordinator
        delivery?.viewModel.segmentView = self
        delivery?.viewModel.isItemCountViewShow = isItemCountViewShow
        delivery?.viewModel.customMenuButton = viewModel.customMenuButton
        
        
        
        pickup = MenuViewController.instantiate() // pickup
        pickup?.viewModel.restaurantDict  =  viewModel.restDict
        pickup?.viewModel.menuItemArray  =  viewModel.menuItemArray
        pickup?.viewModel.menuArray  =  viewModel.menuArray
        
        pickup?.coordinator = coordinator
        pickup?.viewModel.segmentView = self
        pickup?.viewModel.isItemCountViewShow = isItemCountViewShow
        pickup?.viewModel.customMenuButton = viewModel.customMenuButton
        
        
        
        let scanCode = QRCodeViewController.instantiate()//  scancode
        scanCode.fromView = .menu
        scanCode.coordinator = coordinator
        
        let isScanQRCode = CurrentUserInfo.isScanQRCode
        
        guard let restaurantType =  viewModel.restDict?.type else {return}
        
        if restaurantType == RestaurentType.both.rawValue{
            mx = MXViewController<MSSegmentControl>.init(headerViewController: headerView, segmentControllers: [(isScanQRCode == nil) ? scanCode : dinein!,delivery!,pickup!], segmentView: segment)
            mx.segmentView.delegate = self
            mx.segmentViewHeight = 40
            isScrollEnabled = false
            
        CurrentUserInfo.priceByOrderType = priceByOrderType.delivery // in case of both rest open delivery by default
        }
        else if  restaurantType == RestaurentType.dining.rawValue {
            mx = MXViewController<MSSegmentControl>.init(headerViewController: headerView, segmentControllers: [(isScanQRCode == nil) ? scanCode : dinein!], segmentView: segment)
            mx.segmentViewHeight = 0
            CurrentUserInfo.priceByOrderType = priceByOrderType.dining
            isScrollEnabled = true
        }
        else if  restaurantType == RestaurentType.delivery.rawValue {
            mx = MXViewController<MSSegmentControl>.init(headerViewController: headerView, segmentControllers: [delivery!], segmentView: segment)
            mx.segmentViewHeight = 0
            CurrentUserInfo.priceByOrderType = priceByOrderType.delivery
            isScrollEnabled = true
        }
        
        scrollingEnable(0)
        setRightTopButtonImage(segment?.selectedSegmentIndex ?? 0)
        mx.headerViewOffsetHeight = 0
        mx.shouldScrollToBottomAtFirstTime = false
       

        addChild(mx)
        containerView?.addSubview(mx.view)
        guard let bounds = containerView?.bounds else {return}
        mx.view.frame = bounds
        mx.didMove(toParent: self)
       
    }
    
    @IBAction func toBottom(_: Any) {
        if mx != nil {
            mx.scrollToBottom()
        }
    }
    
    override func notificationButtonAction() {
        
        if CurrentUserInfo.isScanQRCode != nil && CurrentUserInfo.priceByOrderType == priceByOrderType.dining{
            let controller = QuickServiceViewController.instantiate()
            controller.viewModel.servicesArray = viewModel.getSortedArray()
            //controller.viewModel.orderId = viewModel.itemAddedCartArray?.cart?.order?.order_id ?? "none"
            controller.viewModel.orderId =  "none"
            
            addRootView(controller)
        }
        else{
            
            if (CurrentUserInfo.authToken == "" || CurrentUserInfo.authToken == nil){
                AlertWithAction(title:"Login", message: "Please login to access your profile", ["Login", "Cancel"], vc: self) { (index) -> (Void) in
                    if index == 1{
                        self.coordinator?.goToUserMobileView()
                    }
                }
            }
            else{
                coordinator?.goToProfileView()
            }
        }
    }
    
    func setupSegment(segmentView: MSSegmentControl) {
        segmentView.borderType = .none
        segmentView.backgroundColor =  #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        
        //        segmentView.backgroundColor =  #colorLiteral(red: 0.9647058824, green: 0.631372549, blue: 0.1607843137, alpha: 0.1131987236)
        segmentView.selectionIndicatorColor = #colorLiteral(red: 0.9647058824, green: 0.631372549, blue: 0.1607843137, alpha: 1)
        segmentView.selectionIndicatorHeight = 2
        segmentView.selectedTitleTextAttributes = [NSAttributedString.Key.foregroundColor: #colorLiteral(red: 0.9647058824, green: 0.631372549, blue: 0.1607843137, alpha: 1), NSAttributedString.Key.font : UIFont.boldSystemFont(ofSize: 17)]
        segmentView.titleTextAttributes = [NSAttributedString.Key.foregroundColor: #colorLiteral(red: 0.537254902, green: 0.5725490196, blue: 0.6392156863, alpha: 1)]
        
        segmentView.segmentWidthStyle = .fixed
        segmentView.indicatorWidthPercent = 1
        segmentView.selectionStyle = .textWidth
    }
    
    
    func  didSelectSegmentAtIndex(_ index: Int) {
       
        let selectedOrderType = priceByOrderType(rawValue: index) ?? priceByOrderType.dining
        CurrentUserInfo.priceByOrderType = selectedOrderType
        
        if index == 0{
            dinein?.updateOnChangeSegmentView(index)
        }
        else if index == 1{
            delivery?.updateOnChangeSegmentView(index)
        }
        else if index == 2 {
            pickup?.updateOnChangeSegmentView(index)
        }
        scrollingEnable(index)
        setRightTopButtonImage(index)
        
    }
    
    fileprivate func addMenuButton()  {
        viewModel.customMenuButton = CustomButton(frame: CGRect(x: ScreenSize.screenWidth - 100, y: ScreenSize.screenHeight - CGFloat((50 + 0.getSafeAreaHeight())) , width: 87, height: 34))
        viewModel.customMenuButton?.setTitleColor(.white, for: .normal)
        viewModel.customMenuButton?.setImage(UIImage(named: "spoon"), for: .normal)
        viewModel.customMenuButton?.setTitle(" Menu", for: .normal)
        viewModel.customMenuButton?.backgroundColor = hexStringToUIColor(korange)
        viewModel.customMenuButton?.isHidden = true
        
        viewModel.customMenuButton?.cornerRadius = 5
        viewModel.customMenuButton?.shadowRadius = 5
        viewModel.customMenuButton?.shadowOffset = CGSize(width: 1, height: 1)
        viewModel.customMenuButton?.shadowColor = .darkGray
        viewModel.customMenuButton?.shadowOpacity = 90
        viewModel.customMenuButton?.layer.masksToBounds = false
        self.view.addSubview(viewModel.customMenuButton!)
        addButtonAnimation()
   
    }
    fileprivate func scrollingEnable(_ index : Int){
        if (CurrentUserInfo.isScanQRCode == nil && index == 0 && isScrollEnabled == false){
            mx.scrollView.isScrollEnabled = false
        }else{
            mx.scrollView.isScrollEnabled = true
        }
    }
    
    
    fileprivate func addButtonAnimation(){
        UIView.animate(withDuration: 0.6,delay: 2.0,
                          animations: {
                           self.viewModel.customMenuButton?.transform = CGAffineTransform(scaleX: 0.6, y: 0.6)
           },
                          completion: { _ in
                           UIView.animate(withDuration: 0.6) {
                               self.viewModel.customMenuButton?.transform = CGAffineTransform.identity
                           }
           })
    }
}

