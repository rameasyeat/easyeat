//
//  MenuHeaderViewController.swift
//  easyeat
//
//  Created by Ramniwas on 08/07/20.
//  Copyright © 2020 EasyEat. All rights reserved.
//

import UIKit

class MenuHeaderViewController: UIViewController,Storyboarded {
    var coordinator: MainCoordinator?
    @IBOutlet weak var restaurantView: RestaurantHeaderView?
    @IBOutlet weak var loyalityView: LoyalityHeaderView!

    var restDict : RestaurantData?
 
    override func viewDidLoad() {
        super.viewDidLoad()
        self.loyalityView.setRestaurantName(restDict?.name ?? "")
        self.restaurantView?.setRestaurantData(restDict!)
    }
}
