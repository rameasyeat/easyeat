//
//  CustomCollectionView.swift
//  Connective
//
//  Created by Ramniwas Patidar on 12/11/19.
//  Copyright © 2019 Demo. All rights reserved.
//

import UIKit

enum RoleTypeCell : Int {
    case headerRole
    case cellRole
}

protocol SelectedFilterDelegate {
    func getFilterData(_ index : Int,_ dict : Filters)
}

class GridCollectionView: UIView {
    
    @IBOutlet var contentView: UIView!
    @IBOutlet weak var collectionView: UICollectionView!
    
    var filterDelegate : SelectedFilterDelegate?
    var filterArray  :  [Filters]?

    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commoInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commoInit()
    }
    
    private func commoInit(){
        
        Bundle.main.loadNibNamed("GridCollectionView", owner: self, options: nil)
        addSubview(contentView)
        contentView.frame = self.bounds
        contentView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
    }
    
    // register cell
    func registerCell(){
        GridCell.registerWithCollectionView(collectionView)
    }
    
    func setSelection(_ dict : Filters, cell : GridCell){
        
        if  dict.isSelected == true{
            cell.viewBG.backgroundColor = hexStringToUIColor(korange)
            cell.viewBG.layer.borderColor =  hexStringToUIColor(korange).cgColor
            cell.nameLabel.textColor = hexStringToUIColor(kwhite)
        }
        else {
            cell.viewBG.backgroundColor = hexStringToUIColor(kwhite)
            cell.viewBG.layer.borderColor =  hexStringToUIColor(kgray).cgColor
            cell.nameLabel.textColor = hexStringToUIColor(kgray)
        }
    }
}

extension GridCollectionView : UICollectionViewDataSource, UICollectionViewDelegate,UICollectionViewDelegateFlowLayout
{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return filterArray?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        print((collectionView.frame.size.width / 4))
        
        let label = UILabel(frame: CGRect.zero)
        
        label.text = filterArray?[indexPath.row].name
        
        if filterArray?[indexPath.row].filtertype == "price"{
            label.text = "< \(CurrentUserInfo.currency ?? "") \(filterArray?[indexPath.row].name ?? "")"
        }
        label.sizeToFit()
        return (CGSize(width: label.frame.width + 30, height: 40))
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: GridCell.reuseIdentifier, for: indexPath)as! GridCell
        cell.commonInit((filterArray?[indexPath.row])!)
        setSelection((filterArray?[indexPath.row])!, cell: cell)
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let dict  = (filterArray?[indexPath.row])!
    
        if ((filterArray?.contains(where: {$0.filtertype == dict.filtertype && $0.isSelected == true  && $0.id == dict.id}))!){
            filterArray?[indexPath.row].isSelected = false
        }
        else{
            
            if ((filterArray?.contains(where: {$0.filtertype == dict.filtertype && $0.isSelected == true }))!){
                let index = (filterArray!.firstIndex(where: { (item) -> Bool in
                    item.filtertype == dict.filtertype && item.isSelected == true
                }))!
                filterArray?[index].isSelected = false
            }
            
            filterArray?[indexPath.row].isSelected = true
        }
        collectionView.reloadData()
        filterDelegate?.getFilterData(indexPath.row, (filterArray?[indexPath.row])!)

    }
}

