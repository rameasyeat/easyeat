//
//  CaloriesTypeCell.swift
//  easyeat
//
//  Created by Ramniwas on 21/04/20.
//  Copyright © 2020 EasyEat. All rights reserved.
//

import UIKit

class CaloriesTypeCell: ReusableCollectionViewCell {
    @IBOutlet weak var viewColor: VIewShadow!
    @IBOutlet weak var nameLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func commonInit(dict : GraphDefaultData){
        viewColor.backgroundColor = hexStringToUIColor(dict.color)
        nameLabel.text = dict.name
    }
}
