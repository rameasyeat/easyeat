//
//  CaloriesTypeCollectionView.swift
//  easyeat
//
//  Created by Ramniwas on 21/04/20.
//  Copyright © 2020 EasyEat. All rights reserved.
//

import UIKit

class CaloriesTypeCollectionView: UIView {

        @IBOutlet var contentView: UIView!
        @IBOutlet weak var collectionView: UICollectionView!
        
    var caloriesArray = [GraphDefaultData]()
        
        override init(frame: CGRect) {
            super.init(frame: frame)
            commoInit()
        }
        
        required init?(coder aDecoder: NSCoder) {
            super.init(coder: aDecoder)
            commoInit()
        }
        
         func commoInit(){
            Bundle.main.loadNibNamed("CaloriesTypeCollectionView", owner: self, options: nil)
            addSubview(contentView)
            contentView.frame = self.bounds
            contentView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
            CaloriesTypeCell.registerWithCollectionView(collectionView)
        }
        
        func getRolesData(_ rolesArray :[GraphDefaultData]){
            self.caloriesArray = rolesArray
            self.collectionView.reloadData()
        }
    }

    extension CaloriesTypeCollectionView : UICollectionViewDataSource, UICollectionViewDelegate,UICollectionViewDelegateFlowLayout
    {
        func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
            return caloriesArray.count
        }
        
        func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
            
            let label = UILabel(frame: CGRect.zero)
            label.text = caloriesArray[indexPath.row].name
            label.sizeToFit()
            return CGSize(width:label.frame.size.width, height: 25)
        }
        
        func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CaloriesTypeCell.reuseIdentifier, for: indexPath)as! CaloriesTypeCell
            cell.commonInit(dict: caloriesArray[indexPath.row])
            
            return cell
        }
        func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        }
    }

 

