//
//  MoreViewController.swift
//  easyeat
//
//  Created by Ramniwas on 24/04/20.
//  Copyright © 2020 EasyEat. All rights reserved.
//

import UIKit

class MoreViewController: BaseViewController,Storyboarded {
    var coordinator: MainCoordinator?

    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var appVersion: CustomLabel!
    var viewModel : MoreViewModel = {
        let model = MoreViewModel()
        return model
        
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    
    // SsetupUI
    fileprivate func setupUI(){
        
        setNavWithOutView()
        rightButton.isHidden = true

        MoreCell.registerWithTable(tblView)
        viewModel.prepareInfo()
        
        titleLbl?.text = "More"
        
        
        let version = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String
        let env = Configuration().environment.isProductionaENV
        
        if env == false{
            appVersion.text =  "\(version ?? "1.0.01") \("(Staging)")"
        }
    }
    
    
    fileprivate func logout(){
        AlertWithAction(title: "Logout", message: "Are you sure you want to logout?", ["YES","NO"], vc: self) {[weak self] (index) -> (Void) in
            if index == 1{
                
                CurrentUserInfo.authToken = ""
                CurrentUserInfo.restaurantId = nil
                CurrentUserInfo.userName = nil
                CurrentUserInfo.isSkip = nil

//                CurrentUserInfo.randomToken = nil
                self?.coordinator?.goToTourView()
            }
        }
    }
    
    func orderHistoryAction(){
        coordinator?.goToOrderHistoryView()
    }
    
    func openTC(){
        coordinator?.goTOWebView(WebViewType.TC)
       }
    
    func cellClick(_ indexPath : IndexPath){
        
        switch indexPath.row {
        case MoreCellType.orderHistory.rawValue:
            orderHistoryAction()
            
        case MoreCellType.logout.rawValue:
            logout()
            
        case MoreCellType.TC.rawValue:
            openTC()
            
        default:
            break
        }
        
    }

}
// UITableViewDataSource
extension MoreViewController: UITableViewDataSource,UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.infoArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell  = tableView.dequeueReusableCell(withIdentifier: MoreCell.reuseIdentifier, for: indexPath) as! MoreCell
        cell.selectionStyle = .none
        cell.commiInit(viewModel.infoArray[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        cellClick(indexPath)
        
    }
    
    
}
