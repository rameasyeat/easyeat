//
//  VariationViewController.swift
//  easyeat
//
//  Created by Ramniwas Patidar on 13/07/20.
//  Copyright © 2020 EasyEat. All rights reserved.
//

import UIKit


protocol VariationDelegate {
    func updateVarientItem(_ result : CartListData)
}
protocol OnRemoveSubviewFromParent {
    func  updateParentView()
}
class VariationViewController: BaseViewController,Storyboarded,OnRemoveSubviewFromParent,DelegateOnBackAddons {
 
    
    @IBOutlet weak var saveButton: CustomButton!
    
    @IBOutlet weak var varientBackButton: CustomButton!
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var variationTitle: CustomLabel!
    @IBOutlet weak var variationType: CustomLabel!
    @IBOutlet weak var nextButton: CustomButton!
    @IBOutlet weak var priceLabel: CustomLabel!
    @IBOutlet weak var viewBottomHeight: NSLayoutConstraint!
    @IBOutlet weak var tblHeight: NSLayoutConstraint!
    @IBOutlet weak var nextX_Axis: NSLayoutConstraint!
    
    var viewModel : VariationViewModel = {
        let viewModel = VariationViewModel()
        return viewModel }()
    
    var isContainVariation : Bool = false
    {
        didSet{
            if  viewModel.fromViewType == FromViewType.menu && isContainVariation == true{
                nextButton.isHidden = false
            }
            else if isContainVariation == true {
                nextButton.isHidden = false
                nextX_Axis.constant = varientBackButton.frame.size.width + 32
            }
            else{
                nextButton.isHidden = true
            }
        }
    }
    
    var isContainAddons : Bool = false {
        didSet{
            if isContainAddons == true {
                nextButton.isHidden = false
                nextX_Axis.constant = varientBackButton.frame.size.width + 32
            }
            else{
                nextButton.isHidden = true
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        UISetup()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.view.isHidden = false
        
    }
    override func viewDidLayoutSubviews(){
        
        let viewHeight = tblView.contentSize.height + viewModel.subViewHeight + CGFloat(0.getSafeAreaHeight())
        tblHeight.constant =  (ScreenSize.screenHeight - viewModel.tblTopAxis) > viewHeight ? viewHeight : (ScreenSize.screenHeight - viewModel.tblTopAxis)
    }
    
    private func UISetup(){
        setNavigation()
        VariationCell.registerWithTable(tblView)
        
        viewBottomHeight.constant += CGFloat(0.getSafeAreaHeight())/2
        
        variationTitle?.text = viewModel.itemDetails?.name?.capitalized
        variationType?.text = ("Choose " + (viewModel.groupArray?[viewModel.selectedGroupIndex].name ?? "")).capitalizingFirstLetter()
        viewModel.groupArray?[viewModel.selectedGroupIndex].variations?[0].isSelected = true
        
        // check next variation available or not
        if viewModel.isVariationAvailable() == true{
            isContainVariation = true
        }
        else{
            if viewModel.isAddons().count > 0{
                isContainAddons = true
            }
            else {
                isContainVariation = false
                updatePrice(0)
                tblView.reloadData()
            }
        }
        
        // nextButton.isHidden = (viewModel.fromViewType == FromViewType.menu) ? false : true
    }
    
    fileprivate func updatePrice(_ index : Int){
        priceLabel.text = CurrentUserInfo.currency + " \(viewModel.groupArray?[viewModel.selectedGroupIndex].variations?[index].price ?? 0)"
    }
    
    fileprivate func addonsController(){
        self.view.isHidden = true
        let controller = viewModel.addAddonsView()
        controller.viewModel.delegateOnBackAddons = self
        addRootView(controller)
    }
    
    /// MARK : Button Action
    
    @IBAction func saveButtonAction(_ sender: Any) {
        
        if isContainAddons == true {
           addonsController()
        }
        else {
            addItemInCart(APIsEndPoints.kItemInCart.rawValue,self.viewModel.addItemDict(viewModel.itemDetails!), handler: {[weak self](result,statusCode)in
                self?.viewModel.variationDelegate?.updateVarientItem(result)
                self?.removeAllSubView()
            })
        }
    }
    
    @IBAction func nextButtonAction(_ sender: Any) {
        self.view.isHidden = true
        
        if isContainVariation == true{
            
            if viewModel.delegatedRemoveSubview == nil {
                viewModel.delegatedRemoveSubview = self
            }
            
            viewModel.addVariationView()
        }
        else if isContainAddons == true {
            addonsController()
        }
        
    }
    @IBAction func backButtonAction(_ sender: Any) {
        viewModel.delegatedRemoveSubview?.updateParentView()
        removeFromSuperView(self)
    }
    func updateParentView() {
        self.view.isHidden = false
        
    }
    
    // delegates
    func updateVarientItem(_ result : CartListData){
        self.view.isHidden = false
    }
    
    func subcatBackAction() {
        self.view.isHidden = false
    }
    func onbackAction() {
         self.view.isHidden = false
     }
    
    override func crossButtonAction(_ sender: Any) {
        removeAllSubView()
    }
    
    fileprivate func removeAllSubView(){
        
        for v in self.navigationController!.viewControllers{
            if v is VariationViewController || v is CustomizationViewController{
                removeFromSuperView(v)
            }
        }
    }
    
    fileprivate func removeView(_ isAdded : Bool){
        removeFromSuperView(self)
    }
    
    
    
}

//// UITableViewDataSource
extension VariationViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.groupArray?[viewModel.selectedGroupIndex].variations?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell  = tableView.dequeueReusableCell(withIdentifier: VariationCell.reuseIdentifier, for: indexPath) as! VariationCell
        cell.selectionStyle = .none
        
        guard let dict = viewModel.groupArray?[viewModel.selectedGroupIndex].variations?[indexPath.row] else {return cell}
        cell.commiInit(dict, isContainVariation,isContainAddons)
        
        return cell
    }
}

extension VariationViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
        
        guard let dict = viewModel.groupArray?[viewModel.selectedGroupIndex].variations?[indexPath.row] else {return}
        
        if dict.isSelected == true{
            viewModel.groupArray?[viewModel.selectedGroupIndex].variations?[indexPath.row].isSelected = false
        }
        else{
            
            for (index, _) in (viewModel.groupArray?[viewModel.selectedGroupIndex].variations?.enumerated())!{
                viewModel.groupArray?[viewModel.selectedGroupIndex].variations?[index].isSelected = false
            }
            viewModel.groupArray?[viewModel.selectedGroupIndex].variations?[indexPath.row].isSelected = true
        }
        
        updatePrice(indexPath.row)
        tableView.reloadData()
    }
}

