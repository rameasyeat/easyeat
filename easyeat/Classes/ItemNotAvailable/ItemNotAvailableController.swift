//
//  ItemNotAvailableController.swift
//  easyeat
//
//  Created by Ramniwas Patidar on 16/07/20.
//  Copyright © 2020 EasyEat. All rights reserved.
//

import UIKit

protocol ItemsUpdateDelegates{
    func updateRemoveItems(_ isRemove : Bool)
}
class ItemNotAvailableController: BaseViewController,Storyboarded {
    
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var itemTypeLabel: CustomLabel!
    @IBOutlet weak var viewBottomHeight: NSLayoutConstraint!

    
  var viewModel : ItemNotAvailableViewModel = {
      let model = ItemNotAvailableViewModel()
      return model
  }()
        
    override func viewDidLoad() {
        super.viewDidLoad()
        viewBottomHeight.constant += CGFloat(0.getSafeAreaHeight())/2
        CustomizationCell.registerWithTable(tblView)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
    }
    
    /// MARK : Button Action
    
    @IBAction func removeAllAction(_ sender: Any) {
        var dict = [String :Any]()
        dict[krestaurant_id] = CurrentUserInfo.restaurantId
        dict[kcart_token] = CurrentUserInfo.randomToken
        dict[korder_type] = CurrentUserInfo.priceByOrderType.rawValue

        viewModel.removeItems(APIsEndPoints.kremoveUnavailable.rawValue,dict, handler: {[weak self](statusCode)in
            self?.viewModel.itemsUpdateDelegates?.updateRemoveItems(true)
            removeFromSuperView(self!)
        })
        
    }
    
    @IBAction func backButtonAction(_ sender: Any) {
        removeFromSuperView(self)
    }
  
    
    override func crossButtonAction(_ sender: Any) {
        removeFromSuperView(self)
    }

}

//// UITableViewDataSource
extension ItemNotAvailableController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.itemsArray?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell  = tableView.dequeueReusableCell(withIdentifier: CustomizationCell.reuseIdentifier, for: indexPath) as! CustomizationCell
        cell.selectionStyle = .none
        cell.commonInit(viewModel.itemsArray?[indexPath.row])
        
        return cell
    }
}

extension ItemNotAvailableController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}
