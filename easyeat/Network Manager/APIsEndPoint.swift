//
//  APIsEndPoint.swift
//  Connective
//
//  Created by Ramniwas Patidar on 05/12/19.
//  Copyright © 2019 Demo. All rights reserved.
//

import Foundation

public enum APIsEndPoints: String {
    case kGetMenu = "api/restaurant/menu/get.php"
    case kItemInCart = "api/orders/cart/update.php"
    case kGetCartList = "api/orders/cart/get.php"
    case kcreateOrder = "api/orders/details/create.php"
    case kgetRunningOrder = "api/orders/details/get_running_order.php"
    case kRestOrder = "api/orders/details/place_rest_items.php"
    case ksignupUser = "user_login/generate_token.php"
    case krestaurantDetails = "api/restaurant/details/get.php"
    case kMenuFilters = "api/restaurant/details/update_filters.php"
    case kRegisterUserName = "user_login/register_user.php"
    case kRestaurantServices = "api/restaurant/details/get_restaurant_services.php"
    case kCallServer = "messaging/send_text_message.php"
    case kbasicProfile = "user_profile_new/get_basic_user_profile.php"
    case kuserNutrition = "user_profile_new/get_nutrition_daily.php"
    case kUserEarning = "user_profile_new/get_earning_saving_total.php"
    case kUpdateBasicProfile = "user_profile_new/update_user_details.php"
    case kgetUserAllergies = "user_profile_new/allergies/get_user_allergies.php"
    case kUpdateUserAllergies = "user_profile_new/allergies/update_user_allergies.php"
    case kgetAllergiesFilter = "user_profile_new/allergies/get_allergies_filter.php"
    case kAddAllergies = "user_profile_new/allergies/add_allergy.php"
    case kDeleteAllergy = "user_profile_new/allergies/delete_user_allergy.php"
    case kgetOrderHistory = "user_profile_new/get_user_orders.php"
    case kgetOrderHistoryDetails =  "user_profile_new/get_order_details.php"
    case kCountryCodeList = "user_login/get_countries_dial_code.php"
    case kgetDistance = "get_distance.php"
    case kuserProfileNewAddress = "user_profile_new/address/get.php"
    case kcreateAddress = "user_profile_new/address/create.php"
    case kCancelOrder = "api/orders/details/cancel_order.php"
    case kcallServer = "backend/message_system/send_text_message_web.php"
    case kinitiateRazerpayPayment = "api/payment/initiate_razerpay_payment.php"
    case kgetReviewStructure = "api/review/get_structure.php"
    case kReviewCreate = "api/review/create.php"
    case kgetPaymentMethod = "api/payment/get_payment_methods.php"
    case kgetRestaurantList = "api/restaurant/details/get_nearby_restaurants.php"
    case kMessagingput = "messaging/put.php"
    case kSpecilaNotes = "api/orders/cart/update_special_note.php"
    case kPartnerPriceCal = "api/delivery_partner/mrspeedy/cal_delivery_fee.php"
    case kgetDriverLocation = "api/delivery_partner/mrspeedy/delivery_status.php"
    case kuserSaving = "user_profile_new/get_earning_savings_daily.php"
    case kcartBill = "api/orders/cart/get_cart_bill.php"
    case kremoveUnavailable = "api/orders/cart/remove_unavailable_items.php"
    case kordersDetails = "api/orders/details/get.php"
    case kMenuDish = "api/restaurant/menu/dish/get.php"
    case kItemsImages = "api/restaurant/menu/dish/get_item_images.php"
    
}
