//
//  NetworkManager.swift
//  easyeat
//
//  Created by Ramniwas on 16/03/20.
//  Copyright © 2020 EasyEat. All rights reserved.
//

import Foundation
import Alamofire

class NetworkManager {
    static var shared  = NetworkManager()
    
    private init(){}
    
    
    // Get Request
    public func getRequest(_ url : URL,_ hude : Bool,_ loadingText : String, networkHandler:@escaping ((_ responce : [String :Any], _ statusCode : Int) -> Void)){
        
        //        let headers: HTTPHeaders = [
        //            "Content-Type": "application/json",
        //        ]
        
        if hude { Hude.animation.show() }
        
        AF.request(url, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: nil).responseJSON
            { (response) in
                print(response.response?.statusCode as Any)
                print(response.value as Any)
                
                Hude.animation.hide()
                
                switch response.result {
                case .success(let value):
                    
                    if let json = value as? [String: Any] {
                        print(json)
                        networkHandler(value as! [String : Any],200)
                    }
                    
                case .failure(let error):
                    Alert(title: kError, message: error.localizedDescription, vc: RootViewController.controller!)
                    
                }
        }
    }
    
    
    // post Request
    public func postRequest(_ url : URL,_ hude : Bool,_ loadingText : String, params : [String : Any], networkHandler:@escaping ((_ responce : [String : Any], _ statusCode : Int) -> Void)){
        
        if !ReachabilityTest.isConnectedToNetwork() {
            return
        }
        //        let headers: HTTPHeaders = [
        //            "Content-Type": "application/json",
        //        ]
        
        if hude { Hude.animation.show() }

        
        AF.request(url, method: .post, parameters: params, encoding: URLEncoding.default).responseJSON { response in
            
            print(response.value as Any)
            
           Hude.animation.hide()

            switch response.result {
            case .success( _):
                    
                    if let statusCode : Int = response.response?.statusCode, statusCode == 200  {
                        do {
                            if let json = try JSONSerialization.jsonObject(with: response.data!, options : .mutableLeaves) as? [String : Any]
                            {
                                networkHandler(json, statusCode)
                            }
                        }
                        catch let error as NSError {
                            Alert(title: kError, message: error.localizedDescription, vc: RootViewController.controller!)
                            print("Parsing Failed \(error.localizedDescription)")
                        }
                    }
                
                
            case .failure(let error):
                print(error.localizedDescription)
              //  Alert(title: kError, message: error.localizedDescription, vc: RootViewController.controller!)
            }
        }
    }
    
}
