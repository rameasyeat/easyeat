//
//  tblHeaderAnimation.swift
//  easyeat
//
//  Created by Ramniwas on 30/03/20.
//  Copyright © 2020 EasyEat. All rights reserved.
//

import Foundation
import UIKit
    
    
     func showAnimation(_ notification: NSNotification,_ controller : UIViewController){
        if let userInfo = notification.userInfo {
                     let duration:TimeInterval = (userInfo[UIResponder.keyboardAnimationDurationUserInfoKey] as? NSNumber)?.doubleValue ?? 0
                     let animationCurveRawNSN = userInfo[UIResponder.keyboardAnimationCurveUserInfoKey] as? NSNumber
                     let animationCurveRaw = animationCurveRawNSN?.uintValue ?? UIView.AnimationOptions.curveEaseInOut.rawValue
                     let animationCurve:UIView.AnimationOptions = UIView.AnimationOptions(rawValue: animationCurveRaw)

                     UIView.animate(withDuration: duration,
                                                delay: TimeInterval(0),
                                                options: animationCurve,
                                                animations: { controller.view.layoutIfNeeded() },
                                                completion: nil)
                 }

    }

