//
//  Constants.swift
//  easyeat
//
//  Created by Ramniwas on 17/03/20.
//  Copyright © 2020 EasyEat. All rights reserved.
//

import Foundation


let KImagesBaseUrl = "https://d1xkxcid7icwfl.cloudfront.net/"
let kRestFolderName = "restaurant_image/"
let kQuickserviceFolderName = "user-app/icons/"

let kMQTTHost = "mqtt-dev.easyeat.ai"
let kMQTTPort = 2002

//let kApiKey = "AIzaSyBOkzIMHobipMrl3pBUzXdsdLBndum7OI4"

// signin
let kForgotlblText = "Forgot Password? Reset"
let kSiginuplblText = "Not Registered? Sign Up"
let kOTPTxt = "Didn’t Receive OTP ? Resend"
let kSigninlblText = "Already Registered? Sign In"
let kSpecifiRequest = "Additional cooking instructions? Enter here"
let kEnterhere = " Enter here"
let TCService = "Terms of Service"
let kTC = "By logging in, you agree to our Terms of Service"
let kNoSearchMessage = "We cannot find what you arelooking for.\nPlease check your search query"

let kAddCookingInstructions = "Add cooking instructions"

let kReset = "Reset"
let kSignUp = "Sign Up"
let kResend = "Resend"
let kSignin = "Sign In"

// HextColorCode
let korange = "#FEA428"
let kgray = "#999A9C"
let kred = "#F62929"
let kgreen = "#21D729"
let kwhite = "#FFFFFF"
let kLightGray = "#666C78"
let kMediumLightGray = "#F7F8F9"

// Mark Alert
let kOk = "Ok"
let kCancel = "Cancel"
let kError = ""
let kstatus = "status"
let kmessage = "message"


// Signin
let krestaurant_id = "restaurant_id"


// Cart
let kcart_token = "cart_token"
let kitem_id = "item_id"
let kquantity = "quantity"
let  ktable_id = "table_id"
let ktoken = "token"
let kfoodFilters = "food_filters"
let kuser_address = "user_address"
let krest_address = "rest_address"
let kcustomer_address = "customer_address"
let kcustomer_location = "customer_location"


// signup

let kidtoken = "idtoken"
let kphone_no = "phone_no"
let kdial_code = "dial_code"
let kplatform = "platform"
let kios = "ios"
let kusername = "username"

// Services
let kSauces = "Sauces"
let kWifiPassword = "Wifi Password"
let kcategory_name = "category_name"
let kssid = "ssid"
let kpassword = "password"
let kCallServer = "Call the Server"
let kmessage_id = "message_id"

// Sub Cat sservices
let korder_id = "order_id"
let kmessage_type = "message_type"
let ktable_no = "table_no"
let kuser_id = "user_id"


// Menu
let kfields = "fields"

// profile
let kno_of_days = "no_of_days"
let kdayoffset = "dayoffset"
let kgmtoffset = "gmtoffset"

let kEditProfileIdentifier = "EditProfileIdentifier"
let kMQTTNotificationCenter = "MQTTNotificationCenter"
let kSegmentIndexChange = "SegmentIndexChange"

let kvariation_ids = "variation_ids"
let kaddon_ids = "addon_ids"
let kgvariations = "gvariations"

let kemail = "email"
let kgender = "gender"
let kdob = "dob"
let ktype = "type"
let kallergy = "allergy"
let kcatid = "catid"


// Order history
let ksindex = "sindex"
let knorders = "norders"

let kpage_no = "page_no"
let klimit = "limit"

// New Address
let klat = "lat"
let klng = "lng"
let kflatDetails = "flat_details"
let klandmark = "landmark"
let kaddress = "address"
let korigins = "origins"
let kdestinations = "destinations"

let korder_type = "order_type"
let kdistance = "distance"
let kmtype = "mtype"
let kOID = "OID"

let kSelectAddress = "Select your Delivery Address"



// feedbacks
let krating = "rating"
let ksuggestion = "suggestion"
let kitems_rating = "items_rating"
let kfeedback_options = "feedback_options"

// Order
let kdel_order_id = "del_order_id"



let kpayment_channel = "payment_channel"

// Payment
let kNetBanking = "NET-BANKING"
let kCash = "CASH"

//let kdefaultToken = "98765ed1463cbddc61d4aba1c203a494"
let kdefaultToken = ""

let kdefaultRestaurantID = "c49dee80aeee27ab5e19689fcec88480"



// Restaurant

let klocation = "location"
let kmax_distance = "max_distance"
let kDelivery = "Delivery"
let kDining = "Dining"
let kTakeAway = "Take Away"

let kspecial_note = "special_note"

//FONTS

let kFontTextRegular  = "HelveticaNeue-Regular"
let kFontTextBold  = "HelveticaNeue-Bold"
let kFontTextSemibold = "HelveticaNeue-Semibold"
let kFontTextLight  = "HelveticaNeue-Light"
let kFontTextMedium  = "HelveticaNeue-Medium"

// Font Size
enum AppFont : Float {
    case size8 = 8
    case size9 = 9
    case size10 = 10
    case size11 = 11
    case size12 = 12
    case size13 = 13
    case size14 = 14
    case size15 = 15
    case size16 = 16
    case size17 = 17
    case size18 = 18
    case size19 = 19
    case size20 = 20
    case size21 = 21
    case size22 = 22
    case size23 = 23
    case size24 = 24
    
}

// service action
enum ServiceCatType : Int {
    case wifi = 0
    case singleAction
    case multiSelection
}

enum FromViewType : Int{
    case tabbar = 1
    case profile
    case menu
    case notAvailable
}
