//
//  CurrentUserInfo.swift
//  Connective
//
//  Created by Ramniwas Patidar on 08/12/19.
//  Copyright © 2019 Demo. All rights reserved.
//

import Foundation


final class CurrentUserInfo {
    
    private enum UserInfo: String {
        case userName
        case authToken
        case restaurantId
        case randomToken
        case tblID
        case currency
        case usedId
        case email
        case dailCode
        case phone
        case skip
        case scanQRCode
    }
    
    static var priceByOrderType : priceByOrderType = .dining
    
    static var userName: String! {
        get {
            return UserDefaults.standard.string(forKey: UserInfo.userName.rawValue)
        }
        set {
            let defaults = UserDefaults.standard
            let key = UserInfo.userName.rawValue
            
            if let name = newValue {
                defaults.set(name, forKey: key)
            } else {
                defaults.removeObject(forKey: key)
            }
        }
    }
    
    static var dailCode: String! {
           get {
               return UserDefaults.standard.string(forKey: UserInfo.dailCode.rawValue)
           }
           set {
               let defaults = UserDefaults.standard
               let key = UserInfo.dailCode.rawValue
               
               if let name = newValue {
                   defaults.set(name, forKey: key)
               } else {
                   defaults.removeObject(forKey: key)
               }
           }
       }
    
    static var userdId: String! {
           get {
               return UserDefaults.standard.string(forKey: UserInfo.usedId.rawValue)
           }
           set {
               let defaults = UserDefaults.standard
               let key = UserInfo.usedId.rawValue
               
               if let name = newValue {
                   defaults.set(name, forKey: key)
               } else {
                   defaults.removeObject(forKey: key)
               }
           }
       }
    
    static var currency: String! {
          get {
              return UserDefaults.standard.string(forKey: UserInfo.currency.rawValue)
          }
          set {
              let defaults = UserDefaults.standard
              let key = UserInfo.currency.rawValue
              
              if let name = newValue {
                  defaults.set(name, forKey: key)
              } else {
                  defaults.removeObject(forKey: key)
              }
          }
      }
    
  
    
    static var authToken: String! {
        get {
            return UserDefaults.standard.string(forKey: UserInfo.authToken.rawValue)
        }
        set {
            let defaults = UserDefaults.standard
            let key = UserInfo.authToken.rawValue
            
            if let name = newValue {
                defaults.set(name, forKey: key)
            } else {
                defaults.removeObject(forKey: key)
            }
        }
    }
    
    static var restaurantId: String! {
        get {
            return UserDefaults.standard.string(forKey: UserInfo.restaurantId.rawValue)
        }
        set {
            let defaults = UserDefaults.standard
            let key = UserInfo.restaurantId.rawValue
            
            if let name = newValue {
                defaults.set(name, forKey: key)
            } else {
                defaults.removeObject(forKey: key)
            }
        }
    }
    
    static var randomToken: String! {
          get {
              return UserDefaults.standard.string(forKey: UserInfo.randomToken.rawValue)
          }
          set {
              let defaults = UserDefaults.standard
              let key = UserInfo.randomToken.rawValue
              
              if let name = newValue {
                  defaults.set(name, forKey: key)
              } else {
                  defaults.removeObject(forKey: key)
              }
          }
      }
    
    static var isSkip: String! {
            get {
                return UserDefaults.standard.string(forKey: UserInfo.skip.rawValue)
            }
            set {
                let defaults = UserDefaults.standard
                let key = UserInfo.skip.rawValue
                
                if let name = newValue {
                    defaults.set(name, forKey: key)
                } else {
                    defaults.removeObject(forKey: key)
                }
            }
        }
    
    static var tblID: String! {
          get {
              return UserDefaults.standard.string(forKey: UserInfo.tblID.rawValue)
          }
          set {
              let defaults = UserDefaults.standard
              let key = UserInfo.tblID.rawValue
              
              if let name = newValue {
                  defaults.set(name, forKey: key)
              } else {
                  defaults.removeObject(forKey: key)
              }
          }
      }
    
    static var isScanQRCode: String! {
          get {
              return UserDefaults.standard.string(forKey: UserInfo.scanQRCode.rawValue)
          }
          set {
              let defaults = UserDefaults.standard
              let key = UserInfo.scanQRCode.rawValue
              
              if let name = newValue {
                  defaults.set(name, forKey: key)
              } else {
                  defaults.removeObject(forKey: key)
              }
          }
      }

}
