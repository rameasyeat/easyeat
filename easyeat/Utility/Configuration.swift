//
//  Configuration.swift
//  easyeat
//
//  Created by Ramniwas on 21/05/20.
//  Copyright © 2020 EasyEat. All rights reserved.
//

import Foundation
import UIKit


public enum Environment: String {
    case Staging = "staging"
    case Production = "production"

    var baseURL: String {
        switch self {
        case .Staging: return "https://api-dev.easyeat.ai/"
        case .Production: return "https://api.easyeat.ai/"
        }
    }
    
    var googleApisKey: String {
        switch self {
        case .Staging: return "AIzaSyB__kkDzuHpGrqEkySwRWrgKDiW4NsFTWQ"
        case .Production: return "AIzaSyA23kpVPqmJ40hcxUOI1U4R9lhY0Buue_Y"
        }
    }
    
    var paymentMode: Bool {
        switch self {
        case .Staging: return true
        case .Production: return false
        }
    }
    
    var isProductionaENV: Bool {
          switch self {
          case .Staging: return false
          case .Production: return true
          }
      }
    
    var MQTTServer: String {
           switch self {
           case .Staging: return "mqtt-dev.easyeat.ai"
           case .Production: return "mqtt.easyeat.ai"
           }
       }
}


struct Configuration {
     var environment: Environment = {
        if let configuration = Bundle.main.object(forInfoDictionaryKey: "Configuration") as? String {
            if configuration == "Staging" {
                return Environment.Staging
            }
        }

        return Environment.Production
    }()
}
