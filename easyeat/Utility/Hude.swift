//
//  Hude.swift
//  easyeat
//
//  Created by Ramniwas on 17/03/20.
//  Copyright © 2020 EasyEat. All rights reserved.
//

import Foundation
import UIKit
import Lottie

class Hude {
    static var animation = {return Hude()}()
    
    var bgView : UIView!
    var animationView : AnimationView!
    var transparentView : UIView!
    
    let viewWidth : Int = 100
    
    func show(){
        
        transparentView = UIView()
        transparentView.backgroundColor = .black
        transparentView.alpha = 0.5
        transparentView.tag = 2020
        transparentView.frame = UIApplication.shared.keyWindow!.frame
        
        // background View
        bgView = UIView()
        bgView.tag = 2021
        bgView.backgroundColor = .clear
        bgView.frame = UIApplication.shared.keyWindow!.frame
        
        // Animation View
        animationView = AnimationView()
        animationView.frame = CGRect(x: (Int((UIApplication.shared.keyWindow?.frame.width)!) - viewWidth) / 2, y: (Int((UIApplication.shared.keyWindow?.frame.height)!) - viewWidth) / 2, width: viewWidth, height: viewWidth)
        animationView.animation = Animation.named("loading")
        animationView.backgroundColor = .clear
        animationView.tag = 2022
        animationView.loopMode = .loop
        animationView.play()
        
        bgView.addSubview(animationView)
        
        RootViewController.controller?.view.addSubview(transparentView)
        RootViewController.controller?.view.addSubview(bgView)
    }
    
    func hide(){
        removeFromSuperView()
    }
    
    func removeFromSuperView(){
        
        if animationView != nil {
            animationView.removeFromSuperview()
        }
        for subView in (RootViewController.controller?.view.subviews)!{
            if subView.tag == 2020 || subView.tag == 2021{
                subView.removeFromSuperview()
            }
        }
    }
}
