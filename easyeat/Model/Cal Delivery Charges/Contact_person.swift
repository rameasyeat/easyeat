

import Foundation
import ObjectMapper

struct Contact_person : Mappable {
	var phone : [String]?

	init?(map: Map) {

	}

	mutating func mapping(map: Map) {

		phone <- map["phone"]
	}

}
