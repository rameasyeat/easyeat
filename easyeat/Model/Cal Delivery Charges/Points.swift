

import Foundation
import ObjectMapper

struct Points : Mappable {
	var contact_person : Contact_person?

	init?(map: Map) {

	}

	mutating func mapping(map: Map) {

		contact_person <- map["contact_person"]
	}

}
