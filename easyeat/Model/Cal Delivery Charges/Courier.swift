//
//  Courier.swift
//  easyeat
//
//  Created by Ramniwas on 19/05/20.
//  Copyright © 2020 EasyEat. All rights reserved.
//

import Foundation
import ObjectMapper

struct Courier : Mappable {
    var courier_id : Int?
    var surname : String?
    var name : String?
    var middlename : String?
    var phone : String?
    var photo_url : String?
    var latitude : String?
    var longitude : String?

    init?(map: Map) {

    }

    mutating func mapping(map: Map) {

        courier_id <- map["courier_id"]
        surname <- map["surname"]
        name <- map["name"]
        middlename <- map["middlename"]
        phone <- map["phone"]
        photo_url <- map["photo_url"]
        latitude <- map["latitude"]
        longitude <- map["longitude"]
    }

}
