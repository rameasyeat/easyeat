

import Foundation
import ObjectMapper

struct Parameter_warnings : Mappable {
	var points : [Points]?

	init?(map: Map) {

	}

	mutating func mapping(map: Map) {

		points <- map["points"]
	}

}
