

import Foundation
import ObjectMapper

struct PartnerDeliveryFeeData : Mappable {
	var is_successful : Bool?
	var partnerOrder : PartnerOrder?
	var warnings : [String]?
	var parameter_warnings : Parameter_warnings?

	init?(map: Map) {

	}

	mutating func mapping(map: Map) {

		is_successful <- map["is_successful"]
		partnerOrder <- map["order"]
		warnings <- map["warnings"]
		parameter_warnings <- map["parameter_warnings"]
	}

}
