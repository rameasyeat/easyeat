//
//  UserNameModel.swift
//  easyeat
//
//  Created by Ramniwas on 11/04/20.
//  Copyright © 2020 EasyEat. All rights reserved.
//

import Foundation
import ObjectMapper

struct UserNameAPIsModel : Mappable {
    var status : Int?
    var message : String?
    var usersignUpData : UsersignUpData?
    var found_items : Int?

    init?(map: Map) {

    }

    mutating func mapping(map: Map) {

        status <- map["success"]
        message <- map["message"]
        usersignUpData <- map["data"]
        found_items <- map["found_items"]
    }

}

struct UsersignUpData : Mappable {
    var token : String?
    var user_name : String?

    init?(map: Map) {

    }

    mutating func mapping(map: Map) {

        token <- map["token"]
        user_name <- map["user_name"]
    }

}
