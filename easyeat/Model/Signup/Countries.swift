
import Foundation
import ObjectMapper

struct Countries : Mappable {
	var sr : String?
	var name : String?
	var iso_code : String?
	var dial_code : String?
	var icon : String?
    var icon_s3 : String?

	init?(map: Map) {

	}

	mutating func mapping(map: Map) {

		sr <- map["sr"]
		name <- map["name"]
		iso_code <- map["iso_code"]
		dial_code <- map["dial_code"]
		icon <- map["icon"]
        icon_s3 <- map["icon_s3"]
	}

}
