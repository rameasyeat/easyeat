//
//  SignupInfoModel.swift
//  easyeat
//
//  Created by Ramniwas on 21/03/20.
//  Copyright © 2020 EasyEat. All rights reserved.
//

import Foundation
import UIKit

enum SignFiledType {
    case name 
    case phonenumber
    case email
    case password
}


struct SignupInfoModel{
    var type : SignFiledType
    var image: UIImage!
    var placeholder : String
    var value : String
    var countryCode : String
    var header : String

    
    
    init(type: SignFiledType,image: UIImage , placeholder: String = "", value: String = "", countryCode : String, header: String) {
        self.type = type
        self.image = image
        self.value = value
        self.placeholder = placeholder
        self.countryCode = countryCode
        self.header = header
    }
}
