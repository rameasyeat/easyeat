//
//  UserProfileModel.swift
//  easyeat
//
//  Created by Ramniwas on 21/04/20.
//  Copyright © 2020 EasyEat. All rights reserved.
//

import Foundation
import ObjectMapper

struct UserProfileModel : Mappable {
    var status : Int?
    var userData : UserData?
    var message : String?

    init?(map: Map) {

    }

    mutating func mapping(map: Map) {

        status <- map["status"]
        userData <- map["data"]
        message <- map["message"]
    }

}
