

import Foundation
import ObjectMapper

struct UserData : Mappable {
	var user_id : String?
	var name : String?
	var email : String?
	var phone : String?
	var dial_code : String?
	var dOB : String?
	var google_id : String?
	var facebook_id : String?
	var gender : String?
	var allergies : [String]?
    var total_field : Int?
    var completed_field : Int?

	init?(map: Map) {

	}

	mutating func mapping(map: Map) {

		user_id <- map["user_id"]
		name <- map["name"]
		email <- map["email"]
		phone <- map["phone"]
		dial_code <- map["dial_code"]
		dOB <- map["DOB"]
		google_id <- map["google_id"]
		facebook_id <- map["facebook_id"]
		gender <- map["gender"]
		allergies <- map["allergies"]
        total_field <- map["total_field"]
        completed_field <- map["completed_field"]
	}

}
