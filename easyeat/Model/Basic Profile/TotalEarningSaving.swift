
import Foundation
import ObjectMapper

struct TotalEarningSaving : Mappable {
	var _id : String?
	var total_saving : Double?
	var total_earning : Double?

	init?(map: Map) {

	}
	mutating func mapping(map: Map) {

		_id <- map["_id"]
		total_saving <- map["total_saving"]
		total_earning <- map["total_earning"]
	}

}
