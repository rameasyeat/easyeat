//
//  NutritionUserData.swift
//  easyeat
//
//  Created by Ramniwas on 21/04/20.
//  Copyright © 2020 EasyEat. All rights reserved.
//

import Foundation
import ObjectMapper

struct NutritionUserData : Mappable {
    var user_data : [User_data]?
    var avg_data : [Avg_data]?

    init?(map: Map) {

    }

    mutating func mapping(map: Map) {

        user_data <- map["user_data"]
        avg_data <- map["avg_data"]
    }

}
