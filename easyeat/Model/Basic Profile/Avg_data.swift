

import Foundation
import ObjectMapper

struct Avg_data : Mappable {
	var epoch : Int?
	var avg_calorie : Int?
	var avg_carb : Int?
	var avg_fat : Int?
	var avg_protein : Int?

	init?(map: Map) {

	}

	mutating func mapping(map: Map) {

		epoch <- map["epoch"]
		avg_calorie <- map["avg_calorie"]
		avg_carb <- map["avg_carb"]
		avg_fat <- map["avg_fat"]
		avg_protein <- map["avg_protein"]
	}

}
