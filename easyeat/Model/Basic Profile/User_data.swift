

import Foundation
import ObjectMapper

struct User_data : Mappable {
	var epoch : Int?
	var calorie : Int?
	var carb : Int?
	var fat : Int?
	var protein : Int?

	init?(map: Map) {

	}

	mutating func mapping(map: Map) {

		epoch <- map["epoch"]
		calorie <- map["calorie"]
		carb <- map["carb"]
		fat <- map["fat"]
		protein <- map["protein"]
	}

}
