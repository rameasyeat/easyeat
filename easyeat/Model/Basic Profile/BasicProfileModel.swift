//
//  BasicProfileViewModel.swift
//  easyeat
//
//  Created by Ramniwas on 24/03/20.
//  Copyright © 2020 EasyEat. All rights reserved.
//

import Foundation

enum BasicFiledType : Int {
    case name  = 0
    case dob
    case phonenumber
    case email
    case gender
}

struct BasicProfileModel{
    
    var type : BasicFiledType
    var placeholder : String
    var value : String
    var countryCode : String
    var header : String

    init(type: BasicFiledType, placeholder: String = "", value: String = "", countryCode : String, header: String) {
        self.type = type
        self.value = value
        self.placeholder = placeholder
        self.countryCode = countryCode
        self.header = header
    }
}
