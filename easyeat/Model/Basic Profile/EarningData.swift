
import Foundation
import ObjectMapper

struct EarningData : Mappable {
	var totalEarningSaving : [TotalEarningSaving]?
	init?(map: Map) {

	}

	mutating func mapping(map: Map) {

		totalEarningSaving <- map["total_earning_saving"]
	}

}
