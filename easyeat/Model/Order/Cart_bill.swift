

import Foundation
import ObjectMapper

struct Cart_bill : Mappable {
	var name : String?
	var value : Double?
	var id : String?
    var delivery_distance : String?
    var delivery_partner : String?

	init?(map: Map) {

	}

	mutating func mapping(map: Map) {

		name <- map["name"]
		value <- map["value"]
		id <- map["id"]
        delivery_distance <- map["delivery_distance"]
        delivery_partner <- map["delivery_partner"]

	}

}
