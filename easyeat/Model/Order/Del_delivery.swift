

import Foundation
import ObjectMapper

struct Del_delivery : Mappable {
	var delivery_id : Int?
	var delivery_type : String?
	var order_id : Int?
	var client_id : Int?
	var client_order_id : String?
	var address : String?
	var status : String?
	var status_datetime : String?
	var created_datetime : String?
	var order_name : String?
	var order_payment_amount : String?
	var delivery_price_amount : String?
	var courier : String?
	var point_id : Int?
	var contact_person : Contact_person?
	var note : String?
	var building_number : String?
	var apartment_number : String?
	var entrance_number : String?
	var intercom_code : String?
	var floor_number : String?
	var invisible_mile_navigation_instructions : String?
	var required_start_datetime : String?
	var required_finish_datetime : String?
	var taking_amount : String?
	var buyout_amount : String?
	var is_cod_cash_voucher_required : Bool?
	var is_motobox_required : Bool?
	var is_return_to_first_point_required : Bool?
	var matter : String?
	var insurance_amount : String?
	var weight_kg : Int?
	var packages : [String]?
	var checkin_issue_name : String?

	init?(map: Map) {

	}

	mutating func mapping(map: Map) {

		delivery_id <- map["delivery_id"]
		delivery_type <- map["delivery_type"]
		order_id <- map["order_id"]
		client_id <- map["client_id"]
		client_order_id <- map["client_order_id"]
		address <- map["address"]
		status <- map["status"]
		status_datetime <- map["status_datetime"]
		created_datetime <- map["created_datetime"]
		order_name <- map["order_name"]
		order_payment_amount <- map["order_payment_amount"]
		delivery_price_amount <- map["delivery_price_amount"]
		courier <- map["courier"]
		point_id <- map["point_id"]
		contact_person <- map["contact_person"]
		note <- map["note"]
		building_number <- map["building_number"]
		apartment_number <- map["apartment_number"]
		entrance_number <- map["entrance_number"]
		intercom_code <- map["intercom_code"]
		floor_number <- map["floor_number"]
		invisible_mile_navigation_instructions <- map["invisible_mile_navigation_instructions"]
		required_start_datetime <- map["required_start_datetime"]
		required_finish_datetime <- map["required_finish_datetime"]
		taking_amount <- map["taking_amount"]
		buyout_amount <- map["buyout_amount"]
		is_cod_cash_voucher_required <- map["is_cod_cash_voucher_required"]
		is_motobox_required <- map["is_motobox_required"]
		is_return_to_first_point_required <- map["is_return_to_first_point_required"]
		matter <- map["matter"]
		insurance_amount <- map["insurance_amount"]
		weight_kg <- map["weight_kg"]
		packages <- map["packages"]
		checkin_issue_name <- map["checkin_issue_name"]
	}

}
