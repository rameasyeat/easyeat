

import Foundation
import ObjectMapper

struct OrderData : Mappable {
    var _id : _id?
    var order_id : String?
    var order_no : String?
    var user_id : String?
    var restaurant_id : String?
    var order_status : Int?
    var bill_id : String?
    var created_at : Int?
    var created_mongodate : Created_mongodate?
    var date : String?
    var confirmed_at : Int?
    var estimated_time : Int?
    var completed_at : Int?
    var table_id : String?
    var table_no : String?
    var floor_id : String?
    var floor_name : String?
    var orderItems : [OrderItems]?
    var special_notes : [String]?
    var allergic_items : Allergic_items?
    var address : String?
    var distance : Int?
    var order_type : Int?
    var name : String?
    var phone : String?
    var dial_code : String?
    var bill : Bill?
    var fees : [Fees]?
    var delivery_partner : String?
    var payment_status : Int?
    var delivery : Delivery?



    init?(map: Map) {

    }

    mutating func mapping(map: Map) {

        _id <- map["_id"]
        order_id <- map["order_id"]
        order_no <- map["order_no"]
        user_id <- map["user_id"]
        restaurant_id <- map["restaurant_id"]
        order_status <- map["order_status"]
        bill_id <- map["bill_id"]
        created_at <- map["created_at"]
        created_mongodate <- map["created_mongodate"]
        date <- map["date"]
        confirmed_at <- map["confirmed_at"]
        estimated_time <- map["estimated_time"]
        completed_at <- map["completed_at"]
        table_id <- map["table_id"]
        table_no <- map["table_no"]
        floor_id <- map["floor_id"]
        floor_name <- map["floor_name"]
        orderItems <- map["items"]
        special_notes <- map["special_notes"]
        allergic_items <- map["allergic_items"]
        address <- map["address"]
        distance <- map["distance"]
        order_type <- map["order_type"]
        name <- map["name"]
        phone <- map["phone"]
        dial_code <- map["dial_code"]
        bill <- map["bill"]
        fees <- map["fees"]
        delivery_partner <- map["delivery_partner"]
        payment_status <- map["payment_status"]
        delivery <- map["delivery"]

        

    }

}
