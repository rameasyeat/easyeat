

import Foundation
import ObjectMapper

struct Decline_reason : Mappable {
	var decline_item_code : Int?
	var decline_reason : String?

   
        init?(map: Map) {
        }
        mutating func mapping(map: Map) {
            decline_item_code <- map["decline_item_code"]
            decline_reason <- map["decline_reason"]
        }
    }
