
import Foundation
import ObjectMapper

struct Fees : Mappable {
	var name : String?
	var fee : Double?

	init?(map: Map) {

	}

	mutating func mapping(map: Map) {

		name <- map["name"]
		fee <- map["fee"]
	}

}
