
import Foundation
import ObjectMapper

struct Delivery : Mappable {
	var _id : _id?
	var partner_id : String?
	var delivery_fee : String?
	var order_id : String?
	var delivery_order_id : Int?
	var del_order : Del_order?
	var del_delivery : Del_delivery?

	init?(map: Map) {

	}

	mutating func mapping(map: Map) {

		_id <- map["_id"]
		partner_id <- map["partner_id"]
		delivery_fee <- map["delivery_fee"]
		order_id <- map["order_id"]
		delivery_order_id <- map["delivery_order_id"]
		del_order <- map["del_order"]
		del_delivery <- map["del_delivery"]
	}

}
