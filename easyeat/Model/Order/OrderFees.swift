

import Foundation
import ObjectMapper

struct OrderFees : Mappable {
	var id : String?
	var fee_name : String?
	var fee : Double?
	var tax : Int?

	init?(map: Map) {

	}

	mutating func mapping(map: Map) {

		id <- map["id"]
		fee_name <- map["fee_name"]
		fee <- map["fee"]
		tax <- map["tax"]
	}

}
