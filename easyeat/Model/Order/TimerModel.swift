//
//  TimerModel.swift
//  easyeat
//
//  Created by Ramniwas on 08/04/20.
//  Copyright © 2020 EasyEat. All rights reserved.
//

import Foundation

struct TimerModel {
    private var startTime: Date?
    private var offset: TimeInterval = 0

    var elapsed : TimeInterval {
        get {
            return self.elapsed(since:Date())
        }
    }

    var isRunning = false {
        didSet {
            if isRunning  {
                self.startTime = Date()
            } else {
                if self.startTime != nil{
                    self.offset = self.elapsed
                    self.startTime = nil
                }
            }
        }
    }

    func elapsed(since: Date) -> TimeInterval {
        var elapsed = offset
        if let startTime = self.startTime {
            elapsed += -startTime.timeIntervalSince(since)
        }
        return elapsed
    }
}
