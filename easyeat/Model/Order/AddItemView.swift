//
//  AddItemView.swift
//  easyeat
//
//  Created by Ramniwas on 01/04/20.
//  Copyright © 2020 EasyEat. All rights reserved.
//

import Foundation
import UIKit


protocol AddItemClickDelegate {
    func addItem(_ sender  : UIButton,_ itemCount : Int,_ addRemove : Int)
}

class AddItemView : UIView{
    
    @IBOutlet var contentView: UIView!
    
    @IBOutlet weak var countLabel: UILabel!
    @IBOutlet weak var addButton: UIButton!
    @IBOutlet weak var minusButton: UIButton!
    
    var addItemClickDelegate : AddItemClickDelegate?
    var tblView: UITableView?
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        commonInit()
    }
    
    fileprivate func commonInit(){
        Bundle.main.loadNibNamed("AddItemView", owner: self, options: nil)
        addSubview(contentView)
        contentView.frame = self.bounds
        contentView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
    }
    
    @IBAction func minusButtonAction(_ sender: UIButton) {
        
        let count : Int = Int(countLabel.text ?? "0") ?? 0
        if count > 0{
            countLabel.text = "\(count - 1)"
        }
        
            addItemClickDelegate?.addItem(sender,count - 1, -1)
        
    }
    @IBAction func addButtonAction(_ sender: UIButton) {
        let count : Int = Int(countLabel.text ?? "0") ?? 0
        
            addItemClickDelegate?.addItem(sender,count + 1, +1)
        
    }
    
    func  addFirstItemInCart(_ sender : UIButton){
        
        let count : Int = Int(countLabel.text ?? "0") ?? 0
        addItemClickDelegate?.addItem(sender,count + 1, +1)
    }
}
