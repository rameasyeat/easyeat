

import Foundation
import ObjectMapper

struct Bill : Mappable {
    var _id : _id?
    var bill_id : String?
    var order_id : String?
    var payments : [Payments]?
    var user_id : String?
    var restaurant_id : String?
    var fees : [OrderFees]?
    var item_total : Double?
    var tax : Double?
    var bill_total : Double?
    var savings : Double?
    var earnings : Double?
    var balance : Double?
    var paid : Double?
    var date : String?
    var timestamp : Int?

    init?(map: Map) {

    }

    mutating func mapping(map: Map) {

        _id <- map["_id"]
        bill_id <- map["bill_id"]
        order_id <- map["order_id"]
        payments <- map["payments"]
        user_id <- map["user_id"]
        restaurant_id <- map["restaurant_id"]
        fees <- map["fees"]
        item_total <- map["item_total"]
        tax <- map["tax"]
        bill_total <- map["bill_total"]
        savings <- map["savings"]
        earnings <- map["earnings"]
        balance <- map["balance"]
        paid <- map["paid"]
        date <- map["date"]
        timestamp <- map["timestamp"]
    }

}
