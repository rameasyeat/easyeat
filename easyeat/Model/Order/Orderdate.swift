
import Foundation
import ObjectMapper

struct Orderdate : Mappable {
	var numberLong : String?

	init?(map: Map) {

	}

	mutating func mapping(map: Map) {

		numberLong <- map["$numberLong"]
	}

}
