

import Foundation
import ObjectMapper

struct OrderStatusModel : Mappable {
	var status : Int?
	var message : String?
	var orderData : OrderData?

	init?(map: Map) {

	}

	mutating func mapping(map: Map) {

		status <- map["status"]
		message <- map["message"]
		orderData <- map["data"]
	}

}
