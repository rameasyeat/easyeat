

import Foundation
import ObjectMapper

struct Del_order : Mappable {
	var order_id : Int?
	var order_name : String?
	var vehicle_type_id : Int?
	var created_datetime : String?
	var finish_datetime : String?
	var status : String?
	var status_description : String?
	var matter : String?
	var total_weight_kg : Int?
	var is_client_notification_enabled : Bool?
	var is_contact_person_notification_enabled : Bool?
	var loaders_count : Int?
	var backpayment_details : String?
	var points : [Points]?
	var payment_amount : String?
	var delivery_fee_amount : String?
	var intercity_delivery_fee_amount : String?
	var weight_fee_amount : String?
	var insurance_amount : String?
	var insurance_fee_amount : String?
	var loading_fee_amount : String?
	var money_transfer_fee_amount : String?
	var suburban_delivery_fee_amount : String?
	var overnight_fee_amount : String?
	var discount_amount : String?
	var backpayment_amount : String?
	var cod_fee_amount : String?
	var backpayment_photo_url : String?
	var itinerary_document_url : String?
	var waybill_document_url : String?
	var courier : Courier?
	var is_motobox_required : Bool?

	init?(map: Map) {

	}

	mutating func mapping(map: Map) {

		order_id <- map["order_id"]
		order_name <- map["order_name"]
		vehicle_type_id <- map["vehicle_type_id"]
		created_datetime <- map["created_datetime"]
		finish_datetime <- map["finish_datetime"]
		status <- map["status"]
		status_description <- map["status_description"]
		matter <- map["matter"]
		total_weight_kg <- map["total_weight_kg"]
		is_client_notification_enabled <- map["is_client_notification_enabled"]
		is_contact_person_notification_enabled <- map["is_contact_person_notification_enabled"]
		loaders_count <- map["loaders_count"]
		backpayment_details <- map["backpayment_details"]
		points <- map["points"]
		payment_amount <- map["payment_amount"]
		delivery_fee_amount <- map["delivery_fee_amount"]
		intercity_delivery_fee_amount <- map["intercity_delivery_fee_amount"]
		weight_fee_amount <- map["weight_fee_amount"]
		insurance_amount <- map["insurance_amount"]
		insurance_fee_amount <- map["insurance_fee_amount"]
		loading_fee_amount <- map["loading_fee_amount"]
		money_transfer_fee_amount <- map["money_transfer_fee_amount"]
		suburban_delivery_fee_amount <- map["suburban_delivery_fee_amount"]
		overnight_fee_amount <- map["overnight_fee_amount"]
		discount_amount <- map["discount_amount"]
		backpayment_amount <- map["backpayment_amount"]
		cod_fee_amount <- map["cod_fee_amount"]
		backpayment_photo_url <- map["backpayment_photo_url"]
		itinerary_document_url <- map["itinerary_document_url"]
		waybill_document_url <- map["waybill_document_url"]
		courier <- map["courier"]
		is_motobox_required <- map["is_motobox_required"]
	}

}
