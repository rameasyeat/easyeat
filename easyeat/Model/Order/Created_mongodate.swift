

import Foundation
import ObjectMapper

struct Created_mongodate : Mappable {
	var orderdate : Orderdate?

	init?(map: Map) {
	}

	mutating func mapping(map: Map) {
		orderdate <- map["$date"]
	}
}
