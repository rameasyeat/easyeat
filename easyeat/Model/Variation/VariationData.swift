//
//  VariationData.swift
//  easyeat
//
//  Created by Ramniwas Patidar on 16/07/20.
//  Copyright © 2020 EasyEat. All rights reserved.
//

import Foundation
import ObjectMapper

 func getvariation(_ dict : [String : Any]) -> [Gvariations]{
      let array  =  Mapper<Gvariations>().mapArray(JSONArray: dict["groups"] as! [[String : Any]])
      return array
  }
  
  func isItemContaintVariation(_ variation : String)-> [String : Any]{
             guard let dict = variation.convertToDictionary() else { return [:]}
             return dict
     }


