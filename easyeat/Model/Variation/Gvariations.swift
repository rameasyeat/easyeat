

import Foundation
import ObjectMapper

struct Gvariations : Mappable {
	var groups : [Groups]?

	init?(map: Map) {

	}

	mutating func mapping(map: Map) {

		groups <- map["groups"]
	}

}
