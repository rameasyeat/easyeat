

import Foundation
import ObjectMapper

struct Variations : Mappable {
	var name : String?
	var price : Double?
	var status : Int?
	var targetvid : Int?
	var vid : Int?
    var isSelected : Bool = false

	init?(map: Map) {

	}

	mutating func mapping(map: Map) {

		name <- map["name"]
        price <- map["price"]
		status <- map["status"]
		targetvid <- map["targetvid"]
		vid <- map["vid"]
        isSelected <- map["isSelected"]
	}

}
