

import Foundation
import ObjectMapper

struct VariationDict : Mappable {
	var gvariations : Gvariations?

	init?(map: Map) {

	}

	mutating func mapping(map: Map) {

		gvariations <- map["gvariations"]
	}

}
