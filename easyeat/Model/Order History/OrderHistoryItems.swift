

import Foundation
import ObjectMapper

struct OrderHistoryItems : Mappable {
	var itr : Int?
	var item_id : String?
	var item_name : String?
	var item_quantity : Int?
	var item_price : Double?
	var original_price : Double?
	var discount_per : Int?
	var reward_id : String?
	var created_at : String?
	var confirmed_at : String?
	var completed_at : String?
	var kitchen_counter_id : String?
	var kitchen_picked_at : String?
	var kitchen_picked_by : String?
	var kitchen_time_to_complete : Int?
	var item_status : Int?
	var order_item_id : String?
	var declined_at : Int?
	var decline_reason : [String]?
	var image : String?

	init?(map: Map) {

	}

	mutating func mapping(map: Map) {

		itr <- map["itr"]
		item_id <- map["item_id"]
		item_name <- map["item_name"]
		item_quantity <- map["item_quantity"]
		item_price <- map["item_price"]
		original_price <- map["original_price"]
		discount_per <- map["discount_per"]
		reward_id <- map["reward_id"]
		created_at <- map["created_at"]
		confirmed_at <- map["confirmed_at"]
		completed_at <- map["completed_at"]
		kitchen_counter_id <- map["kitchen_counter_id"]
		kitchen_picked_at <- map["kitchen_picked_at"]
		kitchen_picked_by <- map["kitchen_picked_by"]
		kitchen_time_to_complete <- map["kitchen_time_to_complete"]
		item_status <- map["item_status"]
		order_item_id <- map["order_item_id"]
		declined_at <- map["declined_at"]
		decline_reason <- map["decline_reason"]
		image <- map["image"]
	}

}
