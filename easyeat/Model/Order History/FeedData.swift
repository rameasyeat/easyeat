
import Foundation
import ObjectMapper

import Foundation
import ObjectMapper


struct FeedData : Mappable {
    var id : String?
    var status : Int?
    var name : String?
    var tax : Int?
    var fees : [Double]?

    init?(map: Map) {

    }

    mutating func mapping(map: Map) {

        id <- map["id"]
        status <- map["status"]
        name <- map["name"]
        tax <- map["tax"]
        fees <- map["data"]
    }

}
