

import Foundation
import ObjectMapper

struct OrderHistory : Mappable {
	var _id : _id?
	var order_id : String?
	var restaurant_id : String?
	var created_at : Int?
	var restaurant_name : String?
	var restaurant_city : String?
	var restaurant_logo : String?

	init?(map: Map) {

	}

	mutating func mapping(map: Map) {

		_id <- map["_id"]
		order_id <- map["order_id"]
		restaurant_id <- map["restaurant_id"]
		created_at <- map["created_at"]
		restaurant_name <- map["restaurant_name"]
		restaurant_city <- map["restaurant_city"]
		restaurant_logo <- map["restaurant_logo"]
	}

}
