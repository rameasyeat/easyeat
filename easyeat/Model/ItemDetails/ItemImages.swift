

import Foundation
import ObjectMapper

struct ItemImages : Mappable {
	var image_paths : [String]?
	init?(map: Map) {
	}

	mutating func mapping(map: Map) {
		image_paths <- map["image_paths"]
	}
}
