

import Foundation
import ObjectMapper

struct User_earning_saving : Mappable {
	var epoch : Double?
	var savings : Double?
	var earnings : Double?

	init?(map: Map) {

	}

	mutating func mapping(map: Map) {

		epoch <- map["epoch"]
		savings <- map["savings"]
		earnings <- map["earnings"]
	}

}
