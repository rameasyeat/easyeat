

import Foundation
import ObjectMapper

struct RestaurantModel : Mappable {
    var total_restaurants : Int?
    var total_pages : Int?
    var page_no : Int?
    var total_restautants_in_page : Int?
    var nearby_restaurants : [Nearby_restaurants]?

    init?(map: Map) {

    }

    mutating func mapping(map: Map) {

        total_restaurants <- map["total_restaurants"]
        total_pages <- map["total_pages"]
        page_no <- map["page_no"]
        total_restautants_in_page <- map["total_restautants_in_page"]
        nearby_restaurants <- map["nearby_restaurants"]
    }

}
