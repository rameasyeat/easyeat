

import Foundation
import ObjectMapper

struct Nearby_restaurants : Mappable {
	var _id : _id?
	var name : String?
	var logo : String?
	var address : String?
	var city : String?
	var location : Location?
	var status : Int?
	var nameid : String?
	var distance_km : Int?
	var avg_rating : Int?
	var total_ratings : Int?
	var delivery_minutes : Int?
	var short_url : String?
	var serviceable : Serviceable?
    var id : String?

	init?(map: Map) {

	}

	mutating func mapping(map: Map) {

		_id <- map["_id"]
		name <- map["name"]
		logo <- map["logo"]
		address <- map["address"]
		city <- map["city"]
		location <- map["location"]
		status <- map["status"]
		nameid <- map["nameid"]
		distance_km <- map["distance_km"]
		avg_rating <- map["avg_rating"]
		total_ratings <- map["total_ratings"]
		delivery_minutes <- map["delivery_minutes"]
		short_url <- map["short_url"]
		serviceable <- map["serviceable"]
        id <- map["id"]
	}

}
