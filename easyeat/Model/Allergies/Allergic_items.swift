

import Foundation
import ObjectMapper

struct Allergic_items : Mappable {
	var is_allergy : Int?
	var categories : [Categories]?

	init?(map: Map) {

	}

	mutating func mapping(map: Map) {

		is_allergy <- map["is_allergy"]
		categories <- map["categories"]
	}

}
