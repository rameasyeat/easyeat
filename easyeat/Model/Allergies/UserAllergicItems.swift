//
//  UserAllergicItems.swift
//  easyeat
//
//  Created by Ramniwas on 22/04/20.
//  Copyright © 2020 EasyEat. All rights reserved.
//

import Foundation
import UIKit
import ObjectMapper


struct UserAllergicItems : Mappable {
    var _id : _id?
    var allergic_items : Allergic_items?

    init?(map: Map) {

    }

    mutating func mapping(map: Map) {

        _id <- map["_id"]
        allergic_items <- map["allergic_items"]
    }

}
