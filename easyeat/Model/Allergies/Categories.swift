

import Foundation
import ObjectMapper

struct Categories : Mappable {
	var cat_id : String?
	var cat_name : String?
    var isSelected : Bool?
	var subcat : [Subcat]?

	init?(map: Map) {

	}

	mutating func mapping(map: Map) {

		cat_id <- map["cat_id"]
		cat_name <- map["cat_name"]
		subcat <- map["subcat"]
	}

}
