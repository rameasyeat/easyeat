

import Foundation
import ObjectMapper

struct AllegiesCategoryModel : Mappable {
	var _id : _id?
	var id : String?
	var name : String?
	var allergies : [Categories]?

	init?(map: Map) {

	}

	mutating func mapping(map: Map) {

		_id <- map["_id"]
		id <- map["id"]
		name <- map["name"]
		allergies <- map["allergies"]
	}

}
