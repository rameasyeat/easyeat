

import Foundation
import ObjectMapper

struct Subcat : Mappable {
	var subcat_id : String?
	var subcat_name : String?
    var isSelected : Bool?
	init?(map: Map) {

	}
	mutating func mapping(map: Map) {
		subcat_id <- map["subcat_id"]
		subcat_name <- map["subcat_name"]
	}

}
