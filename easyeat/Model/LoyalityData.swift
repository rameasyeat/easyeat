//
//  LoyalityData.swift
//  easyeat
//
//  Created by Ramniwas on 26/05/20.
//  Copyright © 2020 EasyEat. All rights reserved.
//

import Foundation

import Foundation
import ObjectMapper

struct LoyalityData : Mappable {
    var average_earning_saving : [Average_earning_saving]?
    var user_earning_saving : [User_earning_saving]?

    init?(map: Map) {
    }

    mutating func mapping(map: Map) {
        average_earning_saving <- map["average_earning_saving"]
        user_earning_saving <- map["user_earning_saving"]
    }
}
