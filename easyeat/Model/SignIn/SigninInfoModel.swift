//
//  LoginModel.swift
//  easyeat
//
//  Created by Ramniwas on 17/03/20.
//  Copyright © 2020 EasyEat. All rights reserved.
//

import Foundation
import UIKit

enum FiledType {
    case phonenumber
}

struct SigninInfoModel{
    var type : FiledType
    var image: UIImage!
    var placeholder : String
    var value : String
    var countryCode : String
    var header : String
    var verificationCode : String


    
    
    init(type: FiledType,image: UIImage , placeholder: String = "", value: String = "", countryCode : String,header: String,verificationCode: String) {
        self.type = type
        self.image = image
        self.value = value
        self.placeholder = placeholder
        self.countryCode = countryCode
        self.header = header
        self.verificationCode = verificationCode

    }
}
