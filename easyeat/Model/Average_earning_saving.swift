

import Foundation
import ObjectMapper

struct Average_earning_saving : Mappable {
	var epoch : Double?
	var avg_savings : Double?
	var avg_earning : Double?

	init?(map: Map) {

	}

	mutating func mapping(map: Map) {

		epoch <- map["epoch"]
		avg_savings <- map["avg_savings"]
		avg_earning <- map["avg_earning"]
	}

}
