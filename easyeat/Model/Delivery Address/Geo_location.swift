

import Foundation
import ObjectMapper

struct Geo_location : Mappable {
	var type : String?
	var coordinates : [Double]?

	init?(map: Map) {

	}

	mutating func mapping(map: Map) {

		type <- map["type"]
		coordinates <- map["coordinates"]
	}

}
