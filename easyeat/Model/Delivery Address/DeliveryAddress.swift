

import Foundation
import ObjectMapper

struct DeliveryAddress : Mappable {
	var addressid : String?
	var type : String?
	var lat : String?
	var lng : String?
	var address : String?
	var landmark : String?
	var flat_details : String?
	var is_default : Int?
    var isSelected : Bool?


	init?(map: Map) {

	}

	mutating func mapping(map: Map) {

		addressid <- map["addressid"]
		type <- map["type"]
		lat <- map["lat"]
		lng <- map["lng"]
		address <- map["address"]
		landmark <- map["landmark"]
		flat_details <- map["flat_details"]
		is_default <- map["is_default"]
	}

}
