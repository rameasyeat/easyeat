

import Foundation
import ObjectMapper

struct Elements : Mappable {
    var distance : Distance?
    var duration : Duration?
    var status : String?

    init?(map: Map) {

    }

    mutating func mapping(map: Map) {

        distance <- map["distance"]
        duration <- map["duration"]
        status <- map["status"]
    }

}
