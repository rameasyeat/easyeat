

import Foundation
import ObjectMapper

struct DistanceModel : Mappable {
        var destination_addresses : [String]?
        var origin_addresses : [String]?
        var rows : [Rows]?
        var status : String?

        init?(map: Map) {

        }

        mutating func mapping(map: Map) {

            destination_addresses <- map["destination_addresses"]
            origin_addresses <- map["origin_addresses"]
            rows <- map["rows"]
            status <- map["status"]
        }

    }
