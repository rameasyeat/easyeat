

import Foundation
import ObjectMapper

struct Rows : Mappable {
	var elements : [Elements]?

	init?(map: Map) {

	}

	mutating func mapping(map: Map) {

		elements <- map["elements"]
	}

}
