

import Foundation
import ObjectMapper

struct Delivery_partner : Mappable {
	var partner_id : String?
	var partner_name : String?
	var priority : Int?
    var isDeliveryPartner : Bool?

	init?(map: Map) {

	}

	mutating func mapping(map: Map) {

		partner_id <- map["partner_id"]
		partner_name <- map["partner_name"]
		priority <- map["priority"]
        isDeliveryPartner <- map["isDeliveryPartner"]
	}

}
