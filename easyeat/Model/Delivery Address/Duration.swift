

import Foundation
import ObjectMapper

struct Duration : Mappable {
	var text : String?
	var value : Double?

	init?(map: Map) {

	}

	mutating func mapping(map: Map) {

		text <- map["text"]
		value <- map["value"]
	}

}
