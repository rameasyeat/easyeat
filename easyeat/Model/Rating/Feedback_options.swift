

import Foundation
import ObjectMapper

struct Feedback_options : Mappable {
	var id : String?
	var text : String?
    var isSelected : Bool?

	init?(map: Map) {

	}

	mutating func mapping(map: Map) {

		id <- map["id"]
		text <- map["text"]
        isSelected <- map["isSelected"]
	}

}
