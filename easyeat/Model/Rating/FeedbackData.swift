

import Foundation
import ObjectMapper

struct FeedbackData : Mappable {
	var _id : _id?
	var type : String?
	var feedback_options : [Feedback_options]?
	var rating_text : [String]?

	init?(map: Map) {

	}

	mutating func mapping(map: Map) {

		_id <- map["_id"]
		type <- map["type"]
		feedback_options <- map["feedback_options"]
		rating_text <- map["rating_text"]
	}

}
