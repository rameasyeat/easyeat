

import Foundation
import ObjectMapper
import CoreLocation

struct RestaurantData : Mappable {
    var _id : _id?
    var id : String?
    var name : String?
    var logo : String?
    var address : String?
    var city : String?
    var area : String?
    var country : String?
    var filters : [Filters]?
    var services : [Services]?
    var discount : Int?
    var status : Int?
    var restaurant_id : String?
    var type : Int?
    var delivery_time : Int?
    var location : Location?
    var nameid : String?
    var restaursntFee : [FeedData]?
    var deliverylimit : Any?
    var phone : String?
    var dial_code : String?
    var email : String?
    var geo_location : Geo_location?
    var isDeliverable: Bool = false
    var userCurrentDistance : Int = 0
    var deliveryCharge : Double = 0
    var on_whatsapp : String?
    var userCurrentLocation : CLLocationCoordinate2D = CLLocationCoordinate2D(latitude: 0, longitude: 0)
    var currentlocationAddress : String = ""
    var currentLocationLocality : String = ""
    var orderType : OrderType = .dining
    var preparation_time : Int?
    var config_timings : [Config_timings]?
    var restaurant_open_days : [[String]]?
    var time_zone : String?
    var next_open_epoch : Int?
    var working_days : [Working_days]?
    var mall : String?
    var addons : [Addons]?
    var country_code : String?
    var serviceable : Serviceable?
    var delivery_partner : [Delivery_partner]?

    
    init?(map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        
        _id <- map["_id"]
        id <- map["id"]
        name <- map["name"]
        logo <- map["logo"]
        address <- map["address"]
        city <- map["city"]
        area <- map["area"]
        country <- map["country"]
        filters <- map["filters"]
        services <- map["services"]
        discount <- map["discount"]
        status <- map["status"]
        restaurant_id <- map["restaurant_id"]
        type <- map["type"]
        delivery_time <- map["delivery_time"]
        location <- map["location"]
        nameid <- map["nameid"]
        restaursntFee <- map["fees"]
        deliverylimit <- map["deliverylimit"]
        phone <- map["phone"]
        dial_code <- map["dial_code"]
        email <- map["email"]
        geo_location <- map["geo_location"]
        orderType <- map["orderType"]
        preparation_time <- map["preparation_time"]
        config_timings <- map["config_timings"]
        restaurant_open_days <- map["restaurant_open_days"]
        time_zone <- map["time_zone"]
        next_open_epoch <- map["next_open_epoch"]
        working_days <- map["working_days"]
        mall <- map["mall"]
        addons <- map["addons"]
        country_code <- map["country_code"]
        serviceable <- map["serviceable"]
        delivery_partner <- map["delivery_partner"]
        on_whatsapp <- map["on_whatsapp"]
        
    }
    
}
