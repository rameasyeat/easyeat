

import Foundation
import ObjectMapper

struct Config_timings : Mappable {
	var id : String?
	var name : String?
	var timing : [Int]?

	init?(map: Map) {

	}

	mutating func mapping(map: Map) {

		id <- map["id"]
		name <- map["name"]
		timing <- map["timing"]
	}

}
