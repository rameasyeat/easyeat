

import Foundation
import ObjectMapper

struct RestaurantDetails : Mappable {
	var status : Int?
	var message : String?
	var restaurantData : RestaurantData?

	init?(map: Map) {

	}

	mutating func mapping(map: Map) {

		status <- map["status"]
		message <- map["message"]
		restaurantData <- map["data"]
	}

}
