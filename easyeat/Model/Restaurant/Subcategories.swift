
import Foundation
import ObjectMapper

struct Subcategories : Mappable {
	var subcategory_id : String?
	var subcategory_name : String?
	var icon : String?
	var price_flag : Int?
	var quantity_flag : Int?
	var price : Int?
	var status : Int?

	init?(map: Map) {

	}

	mutating func mapping(map: Map) {

		subcategory_id <- map["subcategory_id"]
		subcategory_name <- map["subcategory_name"]
		icon <- map["icon"]
		price_flag <- map["price_flag"]
		quantity_flag <- map["quantity_flag"]
		price <- map["price"]
		status <- map["status"]
	}

}
