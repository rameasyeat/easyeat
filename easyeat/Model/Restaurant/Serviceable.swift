

import Foundation
import ObjectMapper

struct Serviceable : Mappable {
	var next_serviceable_day : Int?
	var next_serviceable_day_string : String?
	var next_serviceable_minute : Int?
	var next_serviceable_day_minute_id : String?
	var next_serviceable_time_string : String?
	var next_serviceable_string : String?
	var is_serviceable : Int?

	init?(map: Map) {

	}

	mutating func mapping(map: Map) {

		next_serviceable_day <- map["next_serviceable_day"]
		next_serviceable_day_string <- map["next_serviceable_day_string"]
		next_serviceable_minute <- map["next_serviceable_minute"]
		next_serviceable_day_minute_id <- map["next_serviceable_day_minute_id"]
		next_serviceable_time_string <- map["next_serviceable_time_string"]
		next_serviceable_string <- map["next_serviceable_string"]
		is_serviceable <- map["is_serviceable"]
	}

}
