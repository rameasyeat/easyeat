

import Foundation
import ObjectMapper

struct Filters : Mappable {
	var id : String?
	var filtertype : String?
	var name : String?
	var status : Int?
    var isSelected : Bool?


	init?(map: Map) {

	}

	mutating func mapping(map: Map) {

		id <- map["id"]
		filtertype <- map["filter-type"]
		name <- map["name"]
		status <- map["status"]
        isSelected <- map["isSelected"]

	}

}
