

import Foundation
import ObjectMapper

struct Services : Mappable {
	var category_id : String?
	var category_name : String!
	var icon : String?
	var subcategories : [Subcategories]?
	var status : Int?
    var password : String = ""
    var ssid : String = ""

	init?(map: Map) {

	}

	mutating func mapping(map: Map) {

		category_id <- map["category_id"]
		category_name <- map["category_name"]
		icon <- map["icon"]
		subcategories <- map["subcategories"]
		status <- map["status"]
        password <- map["password"]
        ssid <- map["ssid"]

	}

}
