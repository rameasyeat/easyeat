

import Foundation
import ObjectMapper

struct MenuModel : Mappable {
    var status : Int?
    var message : String?
    var items : [Items]?
    var found_items : Int?

    init?(map: Map) {

    }

    mutating func mapping(map: Map) {

        status <- map["status"]
        message <- map["message"]
        items <- map["items"]
        found_items <- map["found_items"]
    }

}
