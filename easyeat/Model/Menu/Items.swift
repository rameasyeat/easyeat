import ObjectMapper

struct Items : Mappable {
    var rest_id : String?
    var cat_id : String?
    var cat_name : String?
    var cat_description : String?
    var cat_priority : String?
    var subcategory_sr : String?
    var subcat_priority : String?
    var subcat_id : String?
    var subcat_name : String?
    var subcat_description : String?
    var kitchen_counter : String?
    var sr : String?
    var id : String?
    var name : String?
    var item_desc : String?
    var filters : String?
    var original_price : String?
    var discount_per : String?
    var price : String?
    var delivery_price : String?
    var delivery_discount : String?
    var takeaway_price : String?
    var takeaway_discount : String?
    var currency : String?
    var img : String?
    var availability : String?
    var availability_days : String?
    var next_available : String?
    var total_rating : String?
    var no_of_reviews : String?
    var status : String?
    var is_available : Int?
    var gvariations : String?
    var isItemAdded : Bool?
    var rest_name : String?
    var quantity : String?
    var order_type : String?
    
    
    
    
    
    init?(map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        
        rest_id <- map["rest_id"]
        cat_id <- map["cat_id"]
        cat_name <- map["cat_name"]
        cat_description <- map["cat_description"]
        cat_priority <- map["cat_priority"]
        subcategory_sr <- map["subcategory_sr"]
        subcat_priority <- map["subcat_priority"]
        subcat_id <- map["subcat_id"]
        subcat_name <- map["subcat_name"]
        subcat_description <- map["subcat_description"]
        kitchen_counter <- map["kitchen_counter"]
        sr <- map["sr"]
        id <- map["id"]
        name <- map["name"]
        item_desc <- map["item_desc"]
        filters <- map["filters"]
        original_price <- map["original_price"]
        discount_per <- map["discount_per"]
        price <- map["price"]
        delivery_price <- map["delivery_price"]
        delivery_discount <- map["delivery_discount"]
        takeaway_price <- map["takeaway_price"]
        takeaway_discount <- map["takeaway_discount"]
        currency <- map["currency"]
        img <- map["img"]
        availability <- map["availability"]
        availability_days <- map["availability_days"]
        next_available <- map["next_available"]
        total_rating <- map["total_rating"]
        no_of_reviews <- map["no_of_reviews"]
        status <- map["status"]
        is_available <- map["is_available"]
        gvariations <- map["gvariations"]
        isItemAdded <- map["isItemAdded"]
        rest_name <- map["rest_name"]
        order_type <- map["order_type"]
        
    }
        
}
