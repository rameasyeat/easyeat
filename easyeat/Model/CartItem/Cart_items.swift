
import Foundation
import ObjectMapper


struct Cart_items : Mappable {
    var item_id : String?
    var item_name : String?
    var quantity : Int?
    var discount : Int?
    var gvariations : String?
    var variation_ids : String?
    var addons : [Addons]?
    var addon_ids : String?
    var price_slash : Int?
    var delivery_discount : Int?
    var takeaway_discount : Int?
    var gvariation_name : String?
    var variation_name : String?
    var addons_name : String?
    var original_price : Int?
    var price : Double?
    var delivery_price : Double?
    var takeaway_price : Double?
    var item_details : Item_details?

    init?(map: Map) {

    }

    mutating func mapping(map: Map) {

        item_id <- map["item_id"]
        item_name <- map["item_name"]
        quantity <- map["quantity"]
        discount <- map["discount"]
        gvariations <- map["gvariations"]
        variation_ids <- map["variation_ids"]
        addons <- map["addons"]
        addon_ids <- map["addon_ids"]
        price_slash <- map["price_slash"]
        delivery_discount <- map["delivery_discount"]
        takeaway_discount <- map["takeaway_discount"]
        gvariation_name <- map["gvariation_name"]
        variation_name <- map["variation_name"]
        addons_name <- map["addons_name"]
        original_price <- map["original_price"]
        price <- map["price"]
        delivery_price <- map["delivery_price"]
        takeaway_price <- map["takeaway_price"]
        item_details <- map["item_details"]
    }

}
/*struct Cart_items : Mappable {
	var item_id : String?
	var item_name : String?
	var quantity : Int?
	var price : Double?
	var original_price : Double?
	var discount : Double?
	var price_slash : Double?
    var delivery_price : Double?
    var delivery_discount : Double?
    var takeaway_price : Double?
    var takeaway_discount : Double?
	var item_details : Item_details?

	init?(map: Map) {

	}

	mutating func mapping(map: Map) {

		item_id <- map["item_id"]
		item_name <- map["item_name"]
		quantity <- map["quantity"]
		price <- map["price"]
		original_price <- map["original_price"]
		discount <- map["discount"]
		price_slash <- map["price_slash"]
        delivery_price <- map["delivery_price"]
        delivery_discount <- map["delivery_discount"]
        takeaway_price <- map["takeaway_price"]
        takeaway_discount <- map["takeaway_discount"]
		item_details <- map["item_details"]
	}

}*/
