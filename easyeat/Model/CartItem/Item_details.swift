

import Foundation
import ObjectMapper

struct Item_details : Mappable {
	var sr : String?
	var id : String?
	var restaurant_id : String?
	var category_id : String?
	var subcategory_id : String?
	var item_name : String?
	var description : String?
	var filters : String?
	var original_price : String?
	var discount_per : String?
	var price : String?
	var currency : String?
	var image : String?
	var availability : String?
	var preparation_minutes : String?
	var no_of_reviews : String?
	var total_rating : String?
	var kitchen_counter_id : String?
	var status : String?
    var gvariations : String?
    var order_type : String?



	init?(map: Map) {

	}

	mutating func mapping(map: Map) {

		sr <- map["sr"]
		id <- map["id"]
		restaurant_id <- map["restaurant_id"]
		category_id <- map["category_id"]
		subcategory_id <- map["subcategory_id"]
		item_name <- map["item_name"]
		description <- map["description"]
		filters <- map["filters"]
		original_price <- map["original_price"]
		discount_per <- map["discount_per"]
		price <- map["price"]
		currency <- map["currency"]
		image <- map["image"]
		availability <- map["availability"]
		preparation_minutes <- map["preparation_minutes"]
		no_of_reviews <- map["no_of_reviews"]
		total_rating <- map["total_rating"]
		kitchen_counter_id <- map["kitchen_counter_id"]
		status <- map["status"]
        gvariations <- map["gvariations"]
        order_type <- map["order_type"]


	}

}
