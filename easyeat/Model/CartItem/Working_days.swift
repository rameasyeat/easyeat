
import Foundation
import ObjectMapper

struct Working_days : Mappable {
	var day : Int?
	var time : [Int]?

	init?(map: Map) {

	}

	mutating func mapping(map: Map) {

		day <- map["day"]
		time <- map["time"]
	}

}
