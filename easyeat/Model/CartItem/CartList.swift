

import Foundation
import ObjectMapper

struct CartList : Mappable {
	var message : String?
	var status : Int?
	var cartListData : CartListData?

	init?(map: Map) {

	}

	mutating func mapping(map: Map) {

		message <- map["message"]
		status <- map["status"]
		cartListData <- map["data"]
	}

}
