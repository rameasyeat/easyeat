

import Foundation
import ObjectMapper

struct BillDetails : Mappable {
	var cart_bill : [Cart_bill]?
	var bill_total : Double?
	var total_savings : Double?

	init?(map: Map) {

	}
	mutating func mapping(map: Map) {

		cart_bill <- map["cart_bill"]
		bill_total <- map["bill_total"]
		total_savings <- map["total_savings"]
	}

}
