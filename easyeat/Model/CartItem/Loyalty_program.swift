

import Foundation
import ObjectMapper

struct Loyalty_program : Mappable {
	var _id : _id?
	var rest_id : String?
	var rest_name : String?
	var loyalty_id : String?
	var loyalty_name : String?
	var is_active : String?
	var min_spent : Int?
	var date : String?
	var schemes : [Schemes]?

	init?(map: Map) {

	}

	mutating func mapping(map: Map) {

		_id <- map["_id"]
		rest_id <- map["rest_id"]
		rest_name <- map["rest_name"]
		loyalty_id <- map["loyalty_id"]
		loyalty_name <- map["loyalty_name"]
		is_active <- map["is_active"]
		min_spent <- map["min_spent"]
		date <- map["date"]
		schemes <- map["schemes"]
	}

}
