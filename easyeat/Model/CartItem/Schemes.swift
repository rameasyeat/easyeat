

import Foundation
import ObjectMapper

struct Schemes : Mappable {
	var scheme_id : String?
	var visit_type : Int?
	var no_of_visits : Int?
	var reward_items : [String]?
	var expires_after : Int?

	init?(map: Map) {

	}

	mutating func mapping(map: Map) {

		scheme_id <- map["scheme_id"]
		visit_type <- map["visit_type"]
		no_of_visits <- map["no_of_visits"]
		reward_items <- map["reward_items"]
		expires_after <- map["expires_after"]
	}

}
