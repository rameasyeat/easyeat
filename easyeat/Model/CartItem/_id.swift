
import Foundation
import ObjectMapper

struct _id : Mappable {
	var oid : String?

	init?(map: Map) {

	}

	mutating func mapping(map: Map) {

		oid <- map["$oid"]
	}

}
