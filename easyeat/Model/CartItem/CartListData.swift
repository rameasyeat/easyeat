
import Foundation
import ObjectMapper

struct CartListData : Mappable {
	var cart : Cart?
	var loyalty_program : Loyalty_program?

	init?(map: Map) {

	}
	mutating func mapping(map: Map) {

		cart <- map["cart"]
		loyalty_program <- map["loyalty_program"]
	}

}
