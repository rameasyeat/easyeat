

import Foundation
import ObjectMapper

struct Cart : Mappable {
	var _id : _id?
	var token : String?
	var user_id : String?
	var current_restaurant_id : String?
	var table_id : String?
	var cart_items : [Cart_items]?
	var redeemed_items : [String]?
	var currency : String?
	var special_note : String?
	var total_amount : Double?
	var tax : Double?
	var total_after_tax : Double?
	var total_savings : Double?
	var order : Order?

	init?(map: Map) {

	}

	mutating func mapping(map: Map) {

		_id <- map["_id"]
		token <- map["token"]
		user_id <- map["user_id"]
		current_restaurant_id <- map["current_restaurant_id"]
		table_id <- map["table_id"]
		cart_items <- map["cart_items"]
		redeemed_items <- map["redeemed_items"]
		currency <- map["currency"]
		special_note <- map["special_note"]
		total_amount <- map["total_amount"]
		tax <- map["tax"]
		total_after_tax <- map["total_after_tax"]
		total_savings <- map["total_savings"]
		order <- map["order"]
	}

}
