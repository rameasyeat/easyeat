

import Foundation
import ObjectMapper

struct Gvariations : Mappable {
	var gid : Int?
	var name : String?
	var variations : [Variations]?

	init?(map: Map) {

	}

	mutating func mapping(map: Map) {

		gid <- map["gid"]
		name <- map["name"]
		variations <- map["variations"]
	}

}
