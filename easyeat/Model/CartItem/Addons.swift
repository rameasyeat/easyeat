

import Foundation
import ObjectMapper

struct Addons : Mappable {
	var id : String?
	var name : String?
	var price : Int?
	var itemids : [String]?
    var isSelected : Bool?

	init?(map: Map) {

	}

	mutating func mapping(map: Map) {

		id <- map["id"]
		name <- map["name"]
		price <- map["price"]
		itemids <- map["itemids"]
        isSelected <- map["isSelected"]

	}

}
