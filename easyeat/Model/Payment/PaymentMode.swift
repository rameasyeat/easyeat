

import Foundation
import ObjectMapper

struct PaymentMode : Mappable {
	var _id : _id?
	var config_id : String?
	var type : String?
	var payment_method : String?
	var name : String?
	var status : Int?
	var payment_gateway : String?
	var icon : String?
	var data : ChannelIdData?
    var isSelected : Bool?

	init?(map: Map) {

	}

	mutating func mapping(map: Map) {

		_id <- map["_id"]
		config_id <- map["config_id"]
		type <- map["type"]
		payment_method <- map["payment_method"]
		name <- map["name"]
		status <- map["status"]
		payment_gateway <- map["payment_gateway"]
		icon <- map["icon"]
		data <- map["data"]
        isSelected <- map["isSelected"]
	}

}
