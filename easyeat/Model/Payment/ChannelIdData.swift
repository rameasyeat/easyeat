//
//  channel_id.swift
//  easyeat
//
//  Created by Ramniwas on 11/05/20.
//  Copyright © 2020 EasyEat. All rights reserved.
//

import Foundation
import ObjectMapper

struct ChannelIdData : Mappable {
  
    var channel_id : String?
   
    init?(map: Map) {

    }

    mutating func mapping(map: Map) {

        channel_id <- map["channel_id"]
       
    }

}
