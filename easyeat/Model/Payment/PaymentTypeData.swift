

import Foundation
import ObjectMapper

struct PaymentTypeData : Mappable {
	var type : String?
	var data : [PaymentMode]?

	init?(map: Map) {

	}

	mutating func mapping(map: Map) {

		type <- map["type"]
		data <- map["data"]
	}

}
