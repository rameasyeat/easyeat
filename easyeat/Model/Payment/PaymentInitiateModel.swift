

import Foundation
import ObjectMapper

struct PaymentInitiateModel : Mappable {
	var status : Int?
	var message : String?
	var paymentData : PaymentData?

	init?(map: Map) {
	}

	mutating func mapping(map: Map) {

		status <- map["status"]
		message <- map["message"]
		paymentData <- map["data"]
	}

}
