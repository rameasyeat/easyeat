

import Foundation
import ObjectMapper

struct Payments : Mappable {
	var payment_id : String?
	var amount : String?
	var status : Int?
	var payment_method : String?
	var config_id : Int?
	var epoch : Int?
	var transaction_id : String?

	init?(map: Map) {

	}

	mutating func mapping(map: Map) {

		payment_id <- map["payment_id"]
		amount <- map["amount"]
		status <- map["status"]
		payment_method <- map["payment_method"]
		config_id <- map["config_id"]
		epoch <- map["epoch"]
		transaction_id <- map["transaction_id"]
	}

}
