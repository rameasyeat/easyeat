

import Foundation
import ObjectMapper

struct PaymentData : Mappable {
	var amount : String?
	var orderid : String?
	var bill_name : String?
	var bill_email : String?
	var bill_mobile : String?
	var bill_desc : String?
	var country : String?
	var merchantID : String?
	var verificationKey : String?
	var vcode : String?
	var channel : String?
	var payment_page_url : String?

	init?(map: Map) {
	}

	mutating func mapping(map: Map) {

		amount <- map["amount"]
		orderid <- map["orderid"]
		bill_name <- map["bill_name"]
		bill_email <- map["bill_email"]
		bill_mobile <- map["bill_mobile"]
		bill_desc <- map["bill_desc"]
		country <- map["country"]
		merchantID <- map["merchantID"]
		verificationKey <- map["verificationKey"]
		vcode <- map["vcode"]
		channel <- map["channel"]
		payment_page_url <- map["payment_page_url"]
	}
}
