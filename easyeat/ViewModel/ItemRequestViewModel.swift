//
//  ItemRequestViewModel.swift
//  easyeat
//
//  Created by Ramniwas on 17/04/20.
//  Copyright © 2020 EasyEat. All rights reserved.
//

import Foundation
import Lottie

class ItemRequestViewModel {
    var serviceCatType : ServiceCatType?
    var status : Int?
    var animationView : AnimationView?
    var requestViewBackDelegate : RequestViewBackDelegate?
    
    
    // Food Animation
      func showSuccessAnimation(_ view : UIView){
          animationView = AnimationView()
          animationView?.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: view.frame.height)
          animationView?.animation = Animation.named("PaymentSuccess")
          animationView?.backgroundColor = .clear
          animationView?.tag = 2022
          animationView?.loopMode = .loop
          animationView?.play()
          view.addSubview(animationView!)
      }

}
