//
//  RatingViewModel.swift
//  easyeat
//
//  Created by Ramniwas on 07/05/20.
//  Copyright © 2020 EasyEat. All rights reserved.
//

import Foundation
import ObjectMapper

struct  Rating{
    var name : String
    var isSelected : Bool
}
class RatingViewModel{
    
    var orderData : OrderData?
    var sectionArray = ["WHAT DID YOU LIKE THE MOST?","RATE YOUR DISH"]
    var headerHeight = 30
    
    var feedbackData : FeedbackData?
    
    func setMealRating(_ rating : Double) -> String{
        return feedbackData?.rating_text?[Int(rating - 1)] ?? ""
    }
    
    func setHeaderView(_ sectionIndex : Int) -> TableHeaderView{
        
        let headerView = TableHeaderView()
        headerView.headerLabel.text  = sectionArray[sectionIndex]
        headerView.headerLabel.textAlignment = .center
        headerView.headerLabel.backgroundColor = .white

        headerView.headerLabel.font = UIFont.systemFont(ofSize: 10)
        headerView.imgArrowWidth.constant = 0
        return headerView
    }
    
    
    
    func getSelectedFeedbackOption() -> String{
        
        let selecteOptions = feedbackData?.feedback_options
        
        var optionsArray = [[String : Any]]()

        
        
        for option in selecteOptions!{
                   var dict = [String :Any]()
                   dict["id"] = option.id
                   dict["text"] = option.text
            dict["value"] = (option.isSelected == true) ? 1 : 0

                   optionsArray.append(dict)
               }
               return json(from: optionsArray) ?? ""
        
        
        
    }
    
    
    func getSelectedItemRating() -> String{
        var itemsArray = [[String : Any]]()
        
        print(orderData?.orderItems as Any)
        let selectedItemArray = orderData?.orderItems?.filter{Int($0.rating ?? 0) > 0 }
        
        if selectedItemArray?.count == 0 {return ""}

        
        for item in selectedItemArray!{
            var dict = [String :Any]()
            dict["id"] = item.item_id
            dict["rating"] = "\(Int(item.rating!))"
            itemsArray.append(dict)
        }
        return json(from: itemsArray) ?? ""
        
    }
    
    func json(from object:Any) -> String? {
           guard let data = try? JSONSerialization.data(withJSONObject: object, options: []) else {
               return nil
           }
           return String(data: data, encoding: String.Encoding.utf8)
       }
    
      func getFeedbackOptions(_ apiEndPoint: String,_ param : [String : Any], handler: @escaping (Int) -> Void) {
         guard let url = URL(string: Configuration().environment.baseURL + apiEndPoint) else {return}
         NetworkManager.shared.postRequest(url, true, "", params: param, networkHandler: {[weak self](responce,statusCode) in
             
             if let status: Int = responce[kstatus] as? Int, status == 1 {
                 let dict  =  Mapper<FeedbackData>().map(JSON: responce["data"] as! [String : Any])
                    self?.feedbackData = dict
                 handler(status )
             }
             else {
                 Alert(title: kError, message: responce[kmessage] as? String ?? "", vc: RootViewController.controller!)
             }
         })
     }
    
     func submitFeedback(_ apiEndPoint: String,_ param : [String : Any], handler: @escaping (Int) -> Void) {
        guard let url = URL(string: Configuration().environment.baseURL + apiEndPoint) else {return}
        NetworkManager.shared.postRequest(url, true, "", params: param, networkHandler: {(responce,statusCode) in
            if let status: Int = responce[kstatus] as? Int, status == 1 {
                handler(status )
            }
            else {
                Alert(title: kError, message: responce[kmessage] as? String ?? "", vc: RootViewController.controller!)
            }
        })
    }
    
}
