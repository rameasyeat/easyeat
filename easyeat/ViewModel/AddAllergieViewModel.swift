//
//  AddAllergieViewModel.swift
//  easyeat
//
//  Created by Ramniwas on 25/03/20.
//  Copyright © 2020 EasyEat. All rights reserved.
//

import Foundation
import ObjectMapper

class AddAllergieViewModel {
     let cellHeight = 50.0

    var dictAllergies : AllegiesCategoryModel?
    var addAllergiesDelegate : AddAllergiesDelegate?
    var addSubCatAllergiesDelegate : AddSubCatAllergiesDelegate?
    var subCatBackDelegate : SubCatBackDelegate?
    var subCatDict : Categories?


    
   func endcodeModel(_ model : Categories) -> String{
    let jsonString = Mapper().toJSONString(model, prettyPrint: true)
    return jsonString ?? ""
         }
    
    func getAllegiesCatagory(_ apiEndPoint: String,_ param : [String : Any], handler: @escaping (Int) -> Void) {
        guard let url = URL(string: Configuration().environment.baseURL + apiEndPoint) else {return}
        NetworkManager.shared.postRequest(url, true, "", params: param, networkHandler: {[weak self](responce,statusCode) in
          let dictResponce =  Mapper<AllegiesCategoryModel>().map(JSON:  responce["data"] as! [String : Any])
          if let status: Int = responce[kstatus] as? Int, status == 1 {
              self?.dictAllergies = dictResponce
              handler(status )

            }
            else {
              Alert(title: kError, message: responce[kmessage] as? String ?? "", vc: RootViewController.controller!)
            }
        })
    }
    
    
     func addUserAllergies(_ apiEndPoint: String,_ param : [String : Any], handler: @escaping (Int) -> Void) {
           guard let url = URL(string: Configuration().environment.baseURL + apiEndPoint) else {return}
           NetworkManager.shared.postRequest(url, true, "", params: param, networkHandler: {(responce,statusCode) in
             if let status: Int = responce[kstatus] as? Int, status == 1 {
                 handler(status )
               }
               else {
                 Alert(title: kError, message: responce[kmessage] as? String ?? "", vc: RootViewController.controller!)
               }
           })
       }
    
}
