//
//  ItemNotAvailableViewModel.swift
//  easyeat
//
//  Created by Ramniwas Patidar on 16/07/20.
//  Copyright © 2020 EasyEat. All rights reserved.
//

import Foundation
import ObjectMapper

class ItemNotAvailableViewModel{
    var itemsArray : [Cart_items]?
    var itemsUpdateDelegates : ItemsUpdateDelegates?
    
    func removeItems(_ apiEndPoint: String,_ param : [String : Any], handler: @escaping (Int) -> Void) {
        guard let url = URL(string: Configuration().environment.baseURL + apiEndPoint) else {return}
        NetworkManager.shared.postRequest(url, true, "", params: param, networkHandler: {(responce,statusCode) in
            if let status: Int = responce[kstatus] as? Int, status == 1 {
             handler(status)
            }
        })
    }
}
