//
//  BasicProfileViewModel.swift
//  easyeat
//
//  Created by Ramniwas on 24/03/20.
//  Copyright © 2020 EasyEat. All rights reserved.
//

import Foundation
import ObjectMapper

class BasicProfileViewModel {
    
    var dictInfo = [String : String]()
    var infoArray = [BasicProfileModel]()
    var userDictData : UserData?
    var bottomViewHeight = 78
    
    fileprivate let genderArray = ["Male","Female","Other"]
    
    
    func endcodeModel(_ model : Categories) -> String{
    let jsonString = Mapper().toJSONString(model, prettyPrint: true)
    return jsonString ?? ""
         }
    
    // Date Picer
    
    func addDatePicker(_ index : Int,_ handler : @escaping (String) -> Void){
        
        let currentDate = NSDate()
        let min = currentDate.addingTimeInterval(-3153600000)
        
        RPicker.selectDate(title: "Date of Birth", hideCancel: false, datePickerMode: .date, selectedDate: Date(), minDate: min as Date, maxDate: Date(), didSelectDate: {[weak self](date) in
            let dateStr =  AppUtility.dateToString(date: date)
            self?.infoArray[index].value = dateStr
            handler("")
        })
    }
    
    // gender Picker
    
    func addGenderPicker(_ index : Int,_ handler : @escaping (String) -> Void){
        RPicker.selectOption(dataArray: genderArray) { [weak self](str, selectedIndex) in
            self?.infoArray[index].value = str
            handler("")
        }
    }
    
    var mobileCellType = 2
    
    func prepareInfo(dictInfo : [String :String])-> [BasicProfileModel]  {
        
        infoArray.append(BasicProfileModel(type: .name, placeholder: "Name", value: userDictData?.name ?? "", countryCode: "", header: "NAME"))
        
       
        
        infoArray.append(BasicProfileModel(type: .dob, placeholder: "Date of birth", value: convertStringIntoDate(), countryCode: "", header: "DATE OF BIRTH"))
        infoArray.append(BasicProfileModel(type: .phonenumber, placeholder: "Phone", value: userDictData?.phone ?? "", countryCode: userDictData?.dial_code ?? "", header: "PHONE"))
        infoArray.append(BasicProfileModel(type: .email, placeholder: "Email", value: userDictData?.email ?? "", countryCode: userDictData?.email ?? "", header: "EMAIL"))
        infoArray.append(BasicProfileModel(type: .gender, placeholder: "Gender", value: userDictData?.gender ?? "", countryCode: userDictData?.gender ?? "", header: "GENDER"))
        
        return infoArray
    }
    
    func convertStringIntoDate() -> String{
        let date = AppUtility.getPostDateFromString(userDictData!.dOB ?? "")
        let dob = AppUtility.getStringFromDate(date: date)
        return dob
    }
    
    
    func validateFields(dataStore: [BasicProfileModel], validHandler: @escaping (_ param : [String : Any], _ msg : String, _ succes : Bool) -> Void) {
        var dictParam = [String : Any]()
        dictParam[ktoken] = CurrentUserInfo.authToken

        for index in 0..<dataStore.count {
            switch dataStore[index].type {
                
            case .name:
                if dataStore[index].value.trimmingCharacters(in: .whitespaces) == "" {
                    validHandler([:], "Please enter name", false)
                    return
                }
                dictParam[kusername] = dataStore[index].value.trimmingCharacters(in: .whitespaces) as Any
                
            case .dob:
                if dataStore[index].value.trimmingCharacters(in: .whitespaces) == "" {
                    validHandler([:], "Please select date of birth", false)
                    return
                }
                dictParam[kdob] = convertStringIntoDate()
                
            case .email:
                if dataStore[index].value.trimmingCharacters(in: .whitespaces) == "" {
                    validHandler([:], "Please enter email", false)
                    return
                }
                else if dataStore[index].value.trimmingCharacters(in: .whitespaces).isValidEmail() == false {
                    validHandler([:], "Please enter valid email", false)
                    return
                }
                
                dictParam[kemail] = dataStore[index].value.trimmingCharacters(in: .whitespaces) as Any
                
            case .gender:
                if dataStore[index].value.trimmingCharacters(in: .whitespaces) == "" {
                    validHandler([:], "Please select gender", false)
                    return
                }
                dictParam[kgender] = dataStore[index].value.trimmingCharacters(in: .whitespaces) as Any
                
            case .phonenumber: break
                
            }
            
        }
        
        validHandler(dictParam, "", true)
    }
    
    // APIs call
    
    func getBasicProfileDetails(_ apiEndPoint: String,_ param : [String : Any], handler: @escaping (Int) -> Void) {
        guard let url = URL(string: Configuration().environment.baseURL + apiEndPoint) else {return}
        NetworkManager.shared.postRequest(url, true, "", params: param, networkHandler: {[weak self](responce,statusCode) in
            let dictResponce =  Mapper<UserProfileModel>().map(JSON:  responce)
            if let status = dictResponce?.status, status == 1 {
                self?.userDictData =  dictResponce?.userData
                handler(status)
            }
            else {
                Alert(title: "", message: dictResponce?.message ?? "", vc: RootViewController.controller!)
            }
        })
    }
    
    
    func updateProfileDetails(_ apiEndPoint: String,_ param : [String : Any], handler: @escaping (Int) -> Void) {
        guard let url = URL(string: Configuration().environment.baseURL + apiEndPoint) else {return}
        NetworkManager.shared.postRequest(url, true, "", params: param, networkHandler: {(responce,statusCode) in
            let dictResponce =  Mapper<UserProfileModel>().map(JSON:  responce)
            if let status = dictResponce?.status, status == 1 {
                handler(status)
            }
            else {
                Alert(title: kError, message: dictResponce?.message ?? "", vc: RootViewController.controller!)
            }
        })
    }
    
}
