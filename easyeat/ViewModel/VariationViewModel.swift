//
//  VariationViewModel.swift
//  easyeat
//
//  Created by Ramniwas Patidar on 13/07/20.
//  Copyright © 2020 EasyEat. All rights reserved.
//

import Foundation
import ObjectMapper

class VariationViewModel{
    
    let cellHeight = 50.0
    var groupArray : [Gvariations]?
    var itemDetails : VarientItemDetails?
    var addedItem : Cart_items?
    var selectedGroupIndex : Int = 0
    var variationDelegate : VariationDelegate?
    var restaurantDict : RestaurantData?
    var fromViewType : FromViewType?
    var delegatedRemoveSubview : OnRemoveSubviewFromParent?
    
    var tblTopAxis = CGFloat(78 + 0.getSafeAreaHeight())
    var subViewHeight = CGFloat(185)

    
    /// Add Items Param
    func addItemDict(_ itemDetails : VarientItemDetails)  -> [String :Any]{
                
        var dict = [String :Any]()
        dict[krestaurant_id] = CurrentUserInfo.restaurantId
        dict[kcart_token] = CurrentUserInfo.randomToken
        dict[kitem_id] = itemDetails.id
        dict[kquantity] = 1
        dict[ktable_id] = CurrentUserInfo.tblID
        dict[ktoken] =  CurrentUserInfo.authToken ?? kdefaultToken
        dict[kvariation_ids] = getSelectedVarients()
        dict[kaddon_ids] = ""
        dict[kgvariations] = itemDetails.variation
        return dict
    }
    
    
    fileprivate func getSelectedVarients() -> String{
        var varients = ""
        for dict in groupArray! {
            let selectedVariation = dict.variations?.filter{$0.isSelected == true}
            if let varient = selectedVariation?[0]{
                varients += "|\(varient.vid ?? 0)"
            }
        }
        return varients
    }
    
    
    func addAddonsView() -> AddonsViewController {
        let controller = AddonsViewController.instantiate()
        controller.viewModel.groupArray = groupArray
        controller.viewModel.variationDelegate = variationDelegate
        controller.viewModel.restaurantDict = restaurantDict
        controller.viewModel.selectedGroupIndex = selectedGroupIndex
        controller.viewModel.itemDetails = itemDetails
        controller.viewModel.addonsArray = isAddons()
     return controller
    }
    
    func addVariationView(){
        // check vid equal to target id and vid -1
        
        let controller = VariationViewController.instantiate()
        controller.viewModel.groupArray = checkVariation(selectedGroupIndex)
        controller.viewModel.variationDelegate = variationDelegate
        controller.viewModel.restaurantDict = restaurantDict
        controller.viewModel.delegatedRemoveSubview = delegatedRemoveSubview
        controller.viewModel.selectedGroupIndex = selectedGroupIndex + 1
        controller.viewModel.itemDetails = itemDetails
        addRootView(controller)
    }
    
    // check variation exist with target id
    
    fileprivate func checkVariation(_ currentIndex : Int) -> [Gvariations]{
        
        var filterGroupeArray = groupArray
        
        let previousVid = groupArray?[currentIndex].variations?.filter{$0.isSelected == true}[0].vid
        let filterVariation = groupArray?[currentIndex + 1].variations?.filter{$0.targetvid == -1 || $0.targetvid == previousVid}
        filterGroupeArray?[currentIndex + 1].variations = filterVariation
        
        return filterGroupeArray!
    }
    
    
    func isVariationAvailable() -> Bool{
        if (groupArray?.count ?? 0  > selectedGroupIndex + 1){
            return true
        }
        return false
    }
    
    func isAddons() -> [Addons]{
        
        var addOnsArray = [Addons]()
        guard let addons = restaurantDict?.addons else { return addOnsArray}
        for dict in addons{
            let addonsArray = dict.itemids?.filter{$0 == itemDetails?.id}
            if addonsArray?.count ?? 0 > 0 {
                addOnsArray.append(dict)
            }
        }
        return addOnsArray
    }
    
    
    func getvariation(_ variation : String) -> [Gvariations]{
        let dict = variation.convertToDictionary()
        let array  =  Mapper<Gvariations>().mapArray(JSONArray: dict?["groups"] as! [[String : Any]])
        return array
    }
    func json(from object:Any) -> String? {
        guard let data = try? JSONSerialization.data(withJSONObject: object, options: []) else {
            return nil
        }
        return String(data: data, encoding: String.Encoding.utf8)
    }
    
    
    
}
