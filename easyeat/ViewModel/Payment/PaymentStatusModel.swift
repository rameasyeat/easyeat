//
//  PaymentStatusModel.swift
//  easyeat
//
//  Created by Ramniwas on 06/05/20.
//  Copyright © 2020 EasyEat. All rights reserved.
//

import Foundation

enum PaymentStatus : Int{
    case processing
    case failed
    case success
}


class PaymentStatusModel{
    var paymentStatus : PaymentStatus?
}
