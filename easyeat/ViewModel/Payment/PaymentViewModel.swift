//
//  PaymentViewModel.swift
//  easyeat
//
//  Created by Ramniwas on 06/05/20.
//  Copyright © 2020 EasyEat. All rights reserved.
//

import Foundation
import Lottie
import ObjectMapper
import Toast_Swift

class PaymentViewModel{
    
    var paymentData : PaymentData?
    var orderID : String?
    var channelName : String?
     var restaurantName : String?

    
    var animationView : AnimationView?
    var paymentStatus : PaymentStatus?
    
    let failedStatus = "Payment Failed"
    let failedDescription = "Your payment could not be processed"
    
    let sucessStatus = "Payment Successful"
    let sucessDescription = "It has been a pleasure to serve you"
    
    let processingStatus = "Processing Payment"
    let processingDescription = "Sit back and relax"
    
    var paymentText = ""
    
    
    func showSuccessAnimation(_ view : UIView,_ status : PaymentStatus){
            animationView = AnimationView()
            animationView?.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: view.frame.height)
            animationType(status)
            animationView?.backgroundColor = .clear
            animationView?.tag = 2022
//            animationView?.loopMode = .loop
            animationView?.play()
            view.addSubview(animationView!)
        }
    
    fileprivate func animationType(_ status : PaymentStatus){
        let payment = (CurrentUserInfo.currency ?? "") + " \(paymentData?.amount ?? "0")"

        if status == PaymentStatus.success{
            animationView?.animation = Animation.named("PaymentSuccess")
            paymentText = "Your transaction for \(payment) to \(restaurantName ?? "") restaurant is successful"

        }
        else if status == PaymentStatus.failed{
            animationView?.animation = Animation.named("PaymentFailed")
                   paymentText = "Your transaction for \(payment) to \(restaurantName ?? "") restaurant failed"

        }
        else if status == PaymentStatus.processing{
            animationView?.loopMode = .loop

            animationView?.animation = Animation.named("PaymnetLoading")
        }
    }
    
    func getPaymentInfo() -> [String : Any]{
        
        print(paymentData!)
        print(paymentData?.channel as Any)

            let paymentRequestDict: [String:Any] = [
                
                
                "mp_dev_mode": NSNumber.init(booleanLiteral:Configuration().environment.paymentMode),

                "mp_username":(Configuration().environment.isProductionaENV == true) ? "api_easyeat" : "api_SB_easyeatsai",
                "mp_password": (Configuration().environment.isProductionaENV == true) ? "Easyeat^&582" : "api_easTSai327n",
                "mp_merchant_ID": (Configuration().environment.isProductionaENV == true) ? "easyeat" : "SB_easyeatsai",
                "mp_app_name": (Configuration().environment.isProductionaENV == true) ? "easyeat" : "Easyeat",
                "mp_verification_key": (Configuration().environment.isProductionaENV == true) ? "f349375fd7cfb4f53f053fa5c8071ea3" : "7d9302c7920d6f9cba87c69f85fcb63a",

                // Mandatory String. Payment values.
                "mp_amount":  paymentData?.amount ?? "",
                "mp_order_ID": paymentData?.orderid ?? "",
                "mp_currency": CurrentUserInfo.currency ?? "",
                "mp_country": paymentData?.country ?? "",
                
                // Optional, but required payment values. User input will be required when values not passed.
                "mp_channel": paymentData?.channel ?? "",


                "mp_bill_description": paymentData?.bill_desc ?? "",
                "mp_bill_name": paymentData?.bill_name ?? "",
                "mp_bill_email": paymentData?.bill_email ?? "",
                "mp_bill_mobile": (paymentData!.bill_mobile ?? ""),
                
                // Optional, required a valid mp_channel value, this will skip the payment info page and go direct to the payment screen.
                    "mp_express_mode": NSNumber.init(booleanLiteral:true),

               /* // Optional, allow channel selection.
                "mp_channel_editing": NSNumber.init(booleanLiteral:false),

                // Optional, allow billing information editing.
                "mp_editing_enabled": NSNumber.init(booleanLiteral:false),

                // Optional, for Escrow.
                "mp_is_escrow": "0", // Put "1" to enable escrow

                // Optional, for credit card BIN restrictions and campaigns.
                "mp_bin_lock": ["414170", "414171"],

                // Optional, for mp_bin_lock alert error.
                "mp_bin_lock_err_msg": "Only UOB allowed",
                
                // WARNING! FOR TRANSACTION QUERY USE ONLY, DO NOT USE THIS ON PAYMENT PROCESS.
                // Optional, provide a valid cash channel transaction id here will display a payment instruction screen. Required if mp_request_type is 'Receipt'.
                "mp_transaction_id": "",
                // Optional, use 'Receipt' for Cash channels, and 'Status' for transaction status query.
                "mp_request_type": "",

                // Optional, use this to customize the UI theme for the payment info screen, the original XDK custom.css file can be obtained at https://github.com/RazerMS/rms-mobile-xdk-examples/blob/master/custom.css.
                "mp_custom_css_url": paymentData?.payment_page_url ?? "",

                // Optional, set the token id to nominate a preferred token as the default selection, set "new" to allow new card only.
                "mp_preferred_token": "",

                // Optional, credit card transaction type, set "AUTH" to authorize the transaction.
                "mp_tcctype": "",

                // Optional, required valid credit card channel, set true to process this transaction through the recurring api, please refer the MOLPay Recurring API pdf.
                "mp_is_recurring": NSNumber.init(booleanLiteral:false),

                // Optional, show nominated channels.
                "mp_allowed_channels": ["credit", "credit3"],

                // Optional, simulate offline payment, set boolean value to enable.
                "mp_sandbox_mode": NSNumber.init(booleanLiteral:true),

                // Optional, required a valid mp_channel value, this will skip the payment info page and go direct to the payment screen.
                "mp_express_mode": NSNumber.init(booleanLiteral:true),

                // Optional, extended email format validation based on W3C standards.
                "mp_advanced_email_validation_enabled": NSNumber.init(booleanLiteral:true),

                // Optional, extended phone format validation based on Google i18n standards.
                "mp_advanced_phone_validation_enabled": NSNumber.init(booleanLiteral:false),

                // Optional, explicitly force disable user input.
                "mp_bill_name_edit_disabled": NSNumber.init(booleanLiteral:true),
                "mp_bill_email_edit_disabled": NSNumber.init(booleanLiteral:true),
                "mp_bill_mobile_edit_disabled": NSNumber.init(booleanLiteral:true),
                "mp_bill_description_edit_disabled": NSNumber.init(booleanLiteral:true),

                // Optional, EN, MS, VI, TH, FIL, MY, KM, ID, ZH.
                "mp_language": "EN",

                // Optional, Cash channel payment request expiration duration in hour.
                "mp_cash_waittime": 48,
                
                // Optional, allow bypass of 3DS on some credit card channels.
                "mp_non_3DS": NSNumber.init(booleanLiteral:true),

                // Optional, disable card list option.
                "mp_card_list_disabled": NSNumber.init(booleanLiteral:true),

                // Optional for channels restriction, this option has less priority than mp_allowed_channels.
                "mp_disabled_channels": ["credit"]*/
            ]
        
        print(paymentRequestDict)
        
        return paymentRequestDict
        }
    
    
    func initiateRazerpayPayment(_ apiEndPoint: String,_ param : [String : Any], handler: @escaping (Int) -> Void) {
           guard let url = URL(string: Configuration().environment.baseURL + apiEndPoint) else {return}
           NetworkManager.shared.postRequest(url, false, "", params: param, networkHandler: {[weak self](responce,statusCode) in
            let dictResponce =  Mapper<PaymentInitiateModel>().map(JSON:  responce )
            if let status: Int = dictResponce?.status, status == 1 {
                self?.paymentData = dictResponce?.paymentData
                 handler(status )
            
               }
               else {
                 Alert(title: "", message: responce[kmessage] as? String ?? "", vc: RootViewController.controller!)
               }
           })
       }
    
    
    func collectCash(_ apiEndPoint: String,_ param : [String : Any], handler: @escaping (Int,String) -> Void) {
             guard let url = URL(string: Configuration().environment.baseURL + apiEndPoint) else {return}
             NetworkManager.shared.postRequest(url, false, "", params: param, networkHandler: {(responce,statusCode) in
                if let status: Int = responce["status"] as? Int, status == 1 {
                    handler(status,responce[kmessage] as? String ?? "" )
                 }
                 else {
                   Alert(title: kError, message: responce[kmessage] as? String ?? "", vc: RootViewController.controller!)
                 }
             })
         }

        
}
