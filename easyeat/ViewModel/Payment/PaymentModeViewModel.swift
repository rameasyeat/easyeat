//
//  PaymentModeViewModel.swift
//  easyeat
//
//  Created by Ramniwas on 05/05/20.
//  Copyright © 2020 EasyEat. All rights reserved.
//

import Foundation
import UIKit
import ObjectMapper

struct PaymenMode {
    var header : String?
    var type : [PaymenModeType]?
}

struct PaymenModeType {
    var value : String?
    var img : UIImage?
    var channelName : String?

}

class PaymentModeViewModel{
    var paymentModeArray = [PaymentTypeData]()
    var restaurantDict : RestaurantData?
    var orderData : OrderData?
    var headerHeight = 35
    var paymentData : PaymentData?
    
    var staticPaymentDict : PaymenModeType?
    
    func setHeaderView(_ sectionIndex : Int) -> TableHeaderView{
        
        let headerView = TableHeaderView()
        headerView.headerLabel.text  = paymentModeArray[sectionIndex].type
        headerView.imgArrowWidth.constant = 0
        return headerView
    }
    
    func prepareInfo()  {
         staticPaymentDict = PaymenModeType(value: "Net Banking", img:#imageLiteral(resourceName: "netbanking"))
        }
    
    
    func getPaymentOptions(_ apiEndPoint: String,_ param : [String : Any], handler: @escaping (Int) -> Void) {
        guard let url = URL(string: Configuration().environment.baseURL + apiEndPoint) else {return}
        NetworkManager.shared.postRequest(url, true, "", params: param, networkHandler: {[weak self](responce,statusCode) in
        if let status: Int = responce[kstatus] as? Int, status == 1 {
            let array  =  Mapper<PaymentTypeData>().mapArray(JSONArray: responce["data"] as! [[String : Any]])
            self?.paymentModeArray = array
                 handler(status )
             }
             else {
                 Alert(title: kError, message: responce[kmessage] as? String ?? "", vc: RootViewController.controller!)
             }
         })
    }
}
