//
//  ItemDetailViewModel.swift
//  easyeat
//
//  Created by Ramniwas Patidar on 23/07/20.
//  Copyright © 2020 EasyEat. All rights reserved.
//

import Foundation
import ObjectMapper

class ItemDetailViewModel{
    
    var itemImagesArray : [String]?
    var itemDetails : Items?
    var coordinator: MainCoordinator?
    var menuItemArray : [SubCategoryMenuItemArray]?
    var itemAddedCartArray  : CartListData?
    var variationDelegate : VariationDelegate?
    
    var groupArray : [Gvariations]?
    var addedItem : Cart_items?
    var selectedGroupIndex : Int = 0
    var restaurantDict : RestaurantData?
    var fromViewType : FromViewType?
    var itemsDetailsDelegates : ItemsDetailsDelegates?


    
    func getItemDetails(_ cartItems : Items) -> VarientItemDetails{
          
          let dict = isItemContaintVariation(cartItems)
          let groupArray = getvariation(dict)
          
          var varientItemDetails  = VarientItemDetails()
          varientItemDetails.name = cartItems.name
          varientItemDetails.id = cartItems.id
          varientItemDetails.variation = groupArray
          return varientItemDetails
      }
    
    /// Add Items Param
       func addItemDict(_ item : Items,_ addRemove : Int)  -> [String :Any]{
           
           let getAddItem = itemAddedCartArray?.cart?.cart_items?.filter{$0.item_id == item.id}
           
           var dict = [String :Any]()
           dict[krestaurant_id] = item.rest_id
           dict[kcart_token] = CurrentUserInfo.randomToken
           dict[kitem_id] = item.id
           dict[kquantity] = addRemove
           dict[ktable_id] = CurrentUserInfo.tblID
           dict[ktoken] =  CurrentUserInfo.authToken ?? kdefaultToken
           dict[kvariation_ids] = (getAddItem?.count == 0) ? "" : getAddItem?[0].variation_ids ?? ""
           dict[kaddon_ids] = (getAddItem?.count == 0) ? "" : getAddItem?[0].addon_ids ?? ""
           dict[kgvariations] = (item.gvariations == nil) ? "" : item.gvariations
           return dict
       }
    
    func isItemContaintVariation(_ item : Items)-> [String : Any]{
           if item.gvariations != nil  {
               guard let dict = item.gvariations?.convertToDictionary() else { return [:]}
               return dict
           }
           return [:]
       }
       
       func getvariation(_ dict : [String : Any]) -> [Gvariations]{
           let array  =  Mapper<Gvariations>().mapArray(JSONArray: dict["groups"] as! [[String : Any]])
           return array
       }

    
    func getItemDetails(_ apiEndPoint: String,_ param : [String : Any], handler: @escaping (Int) -> Void) {
        guard let url = URL(string: Configuration().environment.baseURL + apiEndPoint) else {return}
        NetworkManager.shared.postRequest(url, true, "", params: param, networkHandler: {(responce,statusCode) in
            if let status: Int = responce[kstatus] as? Int, status == 1 {
            }
        })
    }
    
    func getItemImages(_ apiEndPoint: String,_ param : [String : Any], handler: @escaping (Int) -> Void) {
        guard let url = URL(string: Configuration().environment.baseURL + apiEndPoint) else {return}
        NetworkManager.shared.postRequest(url, false, "", params: param, networkHandler: {[weak self](responce,statusCode) in
            if let status: Int = responce[kstatus] as? Int, status == 1 {
                let dict  =  Mapper<ItemImages>().map(JSON: responce["data"] as! [String : Any])
                self?.itemImagesArray = dict?.image_paths
                handler(status )
            }
        })
    }
}
