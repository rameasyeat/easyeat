//
//  CustomizationModelView.swift
//  easyeat
//
//  Created by Ramniwas Patidar on 16/07/20.
//  Copyright © 2020 EasyEat. All rights reserved.
//

import Foundation

class CustomizationModelView{
    
      var groupArray : [Gvariations]?
      var addedItem : Cart_items?
      var selectedGroupIndex : Int = 0
      var variationDelegate : VariationDelegate?
      var restaurantDict : RestaurantData?
      var itemDetails : VarientItemDetails?
      var fromView : FromViewType?
}
