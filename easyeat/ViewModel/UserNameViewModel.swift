//
//  UserNameViewModel.swift
//  easyeat
//
//  Created by Ramniwas on 30/03/20.
//  Copyright © 2020 EasyEat. All rights reserved.
//

import Foundation
import ObjectMapper

struct UserNameModel{
    var placeholder : String
    var value : String
    init( placeholder: String = "",value: String) {
        self.value = value
        self.placeholder = placeholder
    }
}

class UserNameViewModel{
    
    var dictInfo = [String : String]()
    var infoArray = [UserNameModel]()
    var param : [String : Any]?

       func prepareInfo(dictInfo : [String :String])-> [UserNameModel]  {
           infoArray.append(UserNameModel(placeholder: "Enter your name", value: ""))
            return infoArray
       }
    
    
    func registerUserName(_ apiEndPoint: String,_ param : [String : Any], handler: @escaping (UserNameAPIsModel,Int) -> Void) {
        guard let url = URL(string: Configuration().environment.baseURL + apiEndPoint) else {return}
        NetworkManager.shared.postRequest(url, true, "", params: param, networkHandler: {(responce,statusCode) in
            let dictResponce =  Mapper<UserNameAPIsModel>().map(JSON: responce)
            if let status = dictResponce?.status, status == 1 {
                handler(dictResponce!,status)
            }
            else {
                Alert(title: kError, message: dictResponce?.message ?? "", vc: RootViewController.controller!)
            }
        })
    }

}
