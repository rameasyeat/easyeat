//
//  NoAllergiesViewModel.swift
//  easyeat
//
//  Created by Ramniwas on 22/04/20.
//  Copyright © 2020 EasyEat. All rights reserved.
//

import Foundation
import ObjectMapper

class NoAllergiesViewModel{
    
    var delegate :AllergiesDelegate!
    var allgergieType :AllergiesType!
    var userDictData : UserData?
    var isEditProfile : Bool?
    var userAllergiesDict : UserAllergicItems?

       
    
    
    func resetAllergies(_ apiEndPoint: String,_ param : [String : Any], handler: @escaping (Int) -> Void) {
             guard let url = URL(string: Configuration().environment.baseURL + apiEndPoint) else {return}
             NetworkManager.shared.postRequest(url, true, "", params: param, networkHandler: {(responce,statusCode) in
               if let status: Int = responce[kstatus] as? Int, status == 1 {
                   handler(status )
                 }
                 else {
                   Alert(title: kError, message: responce[kmessage] as? String ?? "", vc: RootViewController.controller!)
                 }
             })
         }
}
