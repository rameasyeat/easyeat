//
//  ProfileViewModel.swift
//  easyeat
//
//  Created by Ramniwas on 21/04/20.
//  Copyright © 2020 EasyEat. All rights reserved.
//

import Foundation
import ObjectMapper
import LMGraphView

struct GraphDefaultData {
    var name : String
    var color : String
    var type : String
    var title : String

    
}
class ProfileViewModel{
    
    var cellHeight = 320
    var caloriesTypeArray = [GraphDefaultData]()
    var userDictData : UserData?
    var userNutritionData : [User_data]?
    var userAvgData : [Avg_data]?
    var arrayUserEarning : [TotalEarningSaving]?
    var fromView : FromViewType?
    var graphMaxValue : Int = 0

    
    let list = ["Your Calories","Average Calories","Average Calories","Fats - 200","Carbs - 500","Protein - 700"]
    let color = ["#FEC356","#FD5035","#F98C3C","#FD6C40","#FD8F6D","#FEBD98"]
    
    
    func getCalories(){
        
        for (index,str) in list.enumerated(){
            print(str)
            var dict = GraphDefaultData(name: list[index], color: color[index], type: "cal", title: "")
            if index > 2{
                dict.type = "fat"
            }
            caloriesTypeArray.append(dict)
        }
    }
    
    
    func getMaxSavingValue(_ currentValue : Int){
           if currentValue > graphMaxValue {
               graphMaxValue = currentValue
           }
       }
       
       // User  calories
       func convertDataIntoWeekArray(_ type : UserSavingOREarningType) -> [[Int : String]]{
           
           guard let count = userAvgData?.count else {return [[:]]}
           print(count)
           
           var array = [[Int : String]]()
           for (index,_) in userAvgData!.enumerated(){
               var dict = [Int : String]()
               dict[index + 1] = getDaysValue(index,type)
               array.append(dict)
           }
           return array
       }
       
       
       func getDaysValue(_ index : Int, _ type : UserSavingOREarningType) -> (String){
           
           var  dayOfWeek : String  = "0"
           if type == .axisData{
               let day = userAvgData?[index].epoch ?? 0
               dayOfWeek = AppUtility.getdayFromDate(date: AppUtility.date(timestamp:
                   Double(day)))
           }
           else if type == .earning{
            dayOfWeek = "\(userAvgData?[index].avg_carb ?? 0)"
           }
           else if type == .saving{
            let saving = userAvgData?[index].avg_calorie ?? 0
               dayOfWeek = "\(saving)"
               getMaxSavingValue(Int(saving))
           }
           return (dayOfWeek)
       }
       
       // get Graph points
       
       func getGraphPoints(_ arrayData : [[Int : String]]) -> [LMGraphPoint]{
           
           var arrayGraphPoints = [LMGraphPoint]()
           for (index,value) in arrayData.enumerated(){
               let price : String = (value[index+1] ?? "0") as String
               
               let dict = LMGraphPoint(point: CGPoint(x: index+1, y: Int(Double(price)!)), title: "\(index+1)", value: price)!
               arrayGraphPoints.append(dict)
           }
           return arrayGraphPoints
       }
    
    
    // validate data
    func validateSavingData() -> Bool{
        let filterUsercarb = userAvgData?.filter{$0.avg_carb ?? 0 > 0}
        let filterUserCal = userAvgData?.filter{$0.avg_calorie ?? 0 > 0}
        if filterUsercarb?.count == 0 && filterUserCal?.count == 0{
            return true
        }
        return false
    }
       
    
    // APIs call
    
    func getBasicProfileDetails(_ apiEndPoint: String,_ param : [String : Any], handler: @escaping (Int) -> Void) {
          guard let url = URL(string: Configuration().environment.baseURL + apiEndPoint) else {return}
          NetworkManager.shared.postRequest(url, true, "", params: param, networkHandler: {[weak self](responce,statusCode) in
              if let status: Int = responce[kstatus] as? Int, status == 1 {
                let dictResponce =  Mapper<UserProfileModel>().map(JSON:  responce)

                self?.userDictData =  dictResponce?.userData
                handler(status)

              }
              else {
                  Alert(title: "", message: responce[kmessage] as? String ?? "", vc: RootViewController.controller!)
              }
          })
      }
    
    // Nutrition data
    
    func getNutritionData(_ apiEndPoint: String,_ param : [String : Any], handler: @escaping (Int) -> Void) {
             guard let url = URL(string: Configuration().environment.baseURL + apiEndPoint) else {return}
             NetworkManager.shared.postRequest(url, true, "", params: param, networkHandler: {[weak self](responce,statusCode) in
                if let status: Int = responce[kstatus] as? Int, status == 1 {
                    let dictResponce =  Mapper<NutritionUserData>().map(JSON:  responce["data"] as! [String : Any])

                    self?.userNutritionData =  (dictResponce?.user_data)!
                    self?.userAvgData =  (dictResponce?.avg_data)!
                    handler(status)

                 }
                 else {
                    Alert(title: kError, message: responce[kmessage] as? String ?? "", vc: RootViewController.controller!)
                 }
             })
         }
    
    // earining
    
    func getUserEarining(_ apiEndPoint: String,_ param : [String : Any], handler: @escaping (Int) -> Void) {
             guard let url = URL(string: Configuration().environment.baseURL + apiEndPoint) else {return}
             NetworkManager.shared.postRequest(url, true, "", params: param, networkHandler: {[weak self](responce,statusCode) in
                if let status: Int = responce[kstatus] as? Int, status == 1 {
                    let dictResponce =  Mapper<EarningData>().map(JSON: responce["data"] as! [String : Any])

                    self?.arrayUserEarning =  (dictResponce?.totalEarningSaving)!
                    handler(status)

                 }
                 else {
                    Alert(title: kError, message: responce[kmessage] as? String ?? "", vc: RootViewController.controller!)
                 }
             })
         }
    
}
