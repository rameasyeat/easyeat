//
//  CountryCodeViewModel.swift
//  easyeat
//
//  Created by Ramniwas on 28/03/20.
//  Copyright © 2020 EasyEat. All rights reserved.
//

import Foundation
import UIKit
import ObjectMapper

//struct CountryCodeModel{
//    var image: UIImage!
//    var countryName : String
//    var countryCode : String
//
//    init(image: UIImage , countryName: String = "", countryCode : String) {
//        self.image = image
//        self.countryCode = countryCode
//        self.countryName = countryName
//    }
//}

class CountryCodeViewModel {
    var dictInfo = [String : String]()
    var countryArray : [Countries]?
    var defaultCountryArray : [Countries]?
    var countryCodeDelegate: CountryCodeDelegate?
    var headerHeight = 40

    
    
    func setHeaderView(_ sectionIndex : Int) -> TableHeaderView{
          
          let headerView = TableHeaderView()
          headerView.headerLabel.text  = "Select the country/region for your number"
          headerView.imgArrowWidth.constant = 0
          return headerView
      }
    
    
    func setCountryCoe(_ array : [Countries]){
        
        var newArray = array
        
        let filterMalaysia : Countries = array.filter{$0.dial_code == "+60"}[0]
        let indexMalaysia  = array.firstIndex(where: {$0.dial_code == filterMalaysia.dial_code})
        newArray.remove(at: indexMalaysia!)

        
        let filterSingapor : Countries = array.filter{$0.dial_code == "+65"}[0]
        let indexSingapor   = array.firstIndex(where: {$0.dial_code == filterSingapor.dial_code})
        newArray.remove(at: indexSingapor!)

        
        let filterIndia : Countries = array.filter{$0.dial_code == "+91"}[0]
        let indexIndia   = array.firstIndex(where: {$0.dial_code == filterIndia.dial_code})
        newArray.remove(at: indexIndia!)
               
        newArray.insert(filterMalaysia, at: 0)
        newArray.insert(filterSingapor, at: 1)
        newArray.insert(filterIndia, at: 2)
        
        self.countryArray = newArray
        self.defaultCountryArray = newArray
    }
    
    
       func getCountryCode(_ apiEndPoint: String,_ param : [String : Any], handler: @escaping (Int) -> Void) {
        guard let url = URL(string: Configuration().environment.baseURL + apiEndPoint) else {return}
        NetworkManager.shared.postRequest(url, true, "", params: param, networkHandler: {[weak self](responce,statusCode) in
            
            let array  =  Mapper<Countries>().mapArray(JSONArray:  responce["countries"] as! [[String : Any]])

            if let status: Int = responce[kstatus] as? Int, status == 1  && array.count > 0{
                self?.setCountryCoe(array)
                handler(status)
            }
            else {
                if let status: Int = responce[kstatus] as? Int, status == 0  && array.count > 0{
                    Alert(title: kError, message: responce[kmessage] as? String ?? "", vc: RootViewController.controller!)
                }
            }
        })
    }
    
}
