//
//  SigninViewModel.swift
//  easyeat
//
//  Created by Ramniwas on 20/03/20.
//  Copyright © 2020 EasyEat. All rights reserved.
//

import Foundation
import UIKit




class SigninViewModel {
    var dictInfo = [String : String]()
    var infoArray = [SigninInfoModel]()
    
    var centerImageWidth : CGFloat = 0
    var centerImageheight : CGFloat = 0
    
    var previousOffset : CGFloat = 0
    var animationShowHeight : CGFloat = 50
    var animationHideHeight : CGFloat = -140
    
    var viewCountryCode : VIewShadow?
    var viewPhoneNumber : VIewShadow?
    var hintImageView: UIImageView!
    var hintImageWidth: NSLayoutConstraint!
    
    var phoneNumberTextFiled: CustomTextField!
    
    func prepareInfo(dictInfo : [String :String])-> [SigninInfoModel]  {
        
        infoArray.append(SigninInfoModel(type: .phonenumber, image: UIImage(named: "profilePlaceholder") ??  #imageLiteral(resourceName: "profilePlaceholder"), placeholder: "Enter mobile number", value: "", countryCode: "+60", header: "",verificationCode: ""))
        return infoArray
    }
    
    func validateFields(dataStore: [SigninInfoModel], validHandler: @escaping (_ param : [String : AnyObject], _ msg : String, _ succes : Bool) -> Void) {
        var dictParam = [String : AnyObject]()
        for index in 0..<dataStore.count {
            switch dataStore[index].type {
                
            case .phonenumber:
                if dataStore[index].value.trimmingCharacters(in: .whitespaces) == "" || (dataStore[index].value.trimmingCharacters(in: .whitespaces).count < 10) {
                    validHandler([:], "Please enter mobile number", false)
                    return
                }
                else if dataStore[index].countryCode.trimmingCharacters(in: .whitespaces) == "" {
                    validHandler([:], "Please enter Country code", false)
                    return
                }
                dictParam["mobileNumber"] = dataStore[index].value.trimmingCharacters(in: .whitespaces) as AnyObject
                dictParam["countryCode"] = dataStore[index].countryCode as AnyObject
                
            }
        }
        
        validHandler(dictParam, "", true)
    }
}
