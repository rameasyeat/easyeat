//
//  OrderDeclinedViewModel.swift
//  easyeat
//
//  Created by Ramniwas on 07/04/20.
//  Copyright © 2020 EasyEat. All rights reserved.
//

import Foundation
import ObjectMapper

struct  DeclinedInfo {
    var header : String?
    var description : String?
}
class OrderDeclinedViewModel{
    var declinedReason : Decline_reason?
    var orderArray : [OrderItems]?
    var availableArray : [OrderItems]?
    var unAvailableArray : [OrderItems]?
    var orderRestDelegate : OrderRestDelegate?
    var isCancelOrder : Bool?
    var orderCancelBy : OrderCancelBy?
    var restaurantDict : RestaurantData?
    var toPay : Double?
    var orderId : String?
    var order_type : Int?

    
    var tblTopAxis = CGFloat(0.getSafeAreaHeight())
    var subViewHeight = CGFloat(50)



    func callServer(_ apiEndPoint: String,_ param : [String : Any], handler: @escaping (Int) -> Void) {
             guard let url = URL(string: Configuration().environment.baseURL + apiEndPoint) else {return}
             NetworkManager.shared.postRequest(url, true, "", params: param, networkHandler: {(responce,statusCode) in
                 if let status: Int = responce[kstatus] as? Int, status == 1 {
                     handler(status )
                 }
                 else {
                     Alert(title: kError, message: responce[kmessage] as? String ?? "", vc: RootViewController.controller!)
                 }
             })
         }
    
}
