

import Foundation
import CoreLocation
class MapViewModel{
    
    var routesArray : [[String : Any]]?
    
    var restLocation : Location?
    var userLocation : CLLocationCoordinate2D?
    
    func getDirection(_ apiEndPoint: String, handler: @escaping (String) -> Void) {
           guard let url = URL(string:apiEndPoint) else {return}
        NetworkManager.shared.getRequest(url, true, "", networkHandler: {[weak self](responce,statusCode) in
               
               if let status = responce[kstatus] as? String, status == "OK" {
                self?.routesArray = responce["routes"] as? [[String : Any]]
                   handler(status )
               }
               else {
                   Alert(title: kError, message: responce[kmessage] as? String ?? "", vc: RootViewController.controller!)
               }
           })
       }
    
}
