//
//  NewAddressViewModel.swift
//  easyeat
//
//  Created by Ramniwas on 29/04/20.
//  Copyright © 2020 EasyEat. All rights reserved.
//

import Foundation
import UIKit
import GoogleMaps

struct NewAddressModel {
    var header : String?
    var value : String?
    
}

struct SelectedDeliveryAddress  {
    var addressid : String?
    var type : String?
    var lat : String?
    var lng : String?
    var address : String?
    var landmark : String?
    var flat_details : String?
    var is_default : Int?
    var isSelected : Bool?
    
}

class NewAddressViewModel {
    
    var newAddressDelegate : NewAddressDelegate?
    var infoArray = [NewAddressModel]()
    let passwordCellHeight = 84.0
    var restaurantDict : RestaurantData?
    var deliveryAddress  = SelectedDeliveryAddress()
    
    
    var googleMapView:GMSMapView!
    var geoCoder :CLGeocoder?
    var lat : Double?
    var lng : Double?
    
    
    func setRestaurentLocation(){
        lat = restaurantDict?.location?.lat
        lng = restaurantDict?.location?.lon
    }
    
    func setdeliveryAddress(_ address : String) -> SelectedDeliveryAddress{
        
        deliveryAddress.address = address
        deliveryAddress.lat = "\(lat ?? 0)"
        deliveryAddress.lng = "\(lng ?? 0)"
        deliveryAddress.landmark = infoArray[1].value
        deliveryAddress.flat_details = infoArray[0].value
        deliveryAddress.isSelected = true
        
        return deliveryAddress
    }
    
    func reverseGeocodeLocation(_ location : CLLocation, handler : @escaping ((String,String)-> Void)){
        // Geocode Location
        geoCoder?.reverseGeocodeLocation(location) { (placemarks, error) in
            if let placemarks = placemarks{
                let pm = placemarks as [CLPlacemark]
                
                if pm.count > 0 {
                    let pm = placemarks[0]
                    
                    var addressString : String = ""
                    var locality : String = ""
                    
                    if pm.thoroughfare != nil {
                        addressString = addressString + pm.thoroughfare! + ", "
                    }
                    if pm.subLocality != nil {
                        addressString = addressString + pm.subLocality! + ", "
                        locality = locality + pm.subLocality! + ", "
                    }
                    if pm.locality != nil {
                        addressString = addressString + pm.locality! + ", "
                        locality = locality  + pm.locality! + ", "
                    }
                    
                    if let subAdministrativeArea = pm.subAdministrativeArea {
                        addressString = addressString + subAdministrativeArea + ", "
                    }
                    
                    if let administrativeArea = pm.administrativeArea {
                        addressString = addressString + administrativeArea + ", "
                    }
                    if pm.country != nil {
                        addressString = addressString + pm.country! + ", "
                    }
                    if pm.postalCode != nil {
                        addressString = addressString + pm.postalCode! + " "
                    }
                    
                    handler(addressString,locality)
                    
                }
            }
        }
    }
    
    func prepareInfo()-> [NewAddressModel]  {
        infoArray.append(NewAddressModel(header: "HOUSE/FLAT/BLOCK NO.", value: ""))
        infoArray.append(NewAddressModel(header: "LANDMARK", value: ""))
        
        return infoArray
    }
    
    func saveNewAddress(_ apiEndPoint: String,_ param : [String : Any], handler: @escaping (Int) -> Void) {
        guard let url = URL(string: Configuration().environment.baseURL + apiEndPoint) else {return}
        NetworkManager.shared.postRequest(url, true, "", params: param, networkHandler: { (responce,statusCode) in
            
            if let status: Int = responce[kstatus] as? Int, status == 1 {
                handler(status )
            }
            else {
                Alert(title: kError, message: responce[kmessage] as? String ?? "", vc: RootViewController.controller!)
            }
        })
    }
}
