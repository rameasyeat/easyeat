//
//  MenuSegmentViewModel.swift
//  easyeat
//
//  Created by Ramniwas on 09/07/20.
//  Copyright © 2020 EasyEat. All rights reserved.
//

import Foundation
import ObjectMapper


class MenuSegmentViewModel {
    
    var restaurentService : [Services]?
    var restaurantFilter : [Filters]?
    var menuArray : [Items]?
    var restDict : RestaurantData?
    
    var menuItemArray = [SubCategoryMenuItemArray]()
    var saveItemArray = [SubCategoryMenuItemArray]()
    var customMenuButton : CustomButton?
    
    var handler :((Int) -> Void)?
    
    
    func getSortedArray() -> [Services]{
        let  items = restaurentService!.sorted(by: { (item1, item2) -> Bool in
            return item1.category_name?.compare(item2.category_name) == ComparisonResult.orderedAscending
        })
        return items
    }
    
    
    
    func getRestaurentDetails(_ apiEndPoint: String,_ param : [String : Any], handler: @escaping (RestaurantData,Int) -> Void) {
        guard let url = URL(string: Configuration().environment.baseURL + apiEndPoint) else {return}
        NetworkManager.shared.postRequest(url, true, "", params: param, networkHandler: {[weak self](responce,statusCode) in
            
            if let status: Int = responce[kstatus] as? Int, status == 1 {
                let dict  =  Mapper<RestaurantData>().map(JSON: responce["data"] as! [String : Any])
                self?.restDict = dict!
                handler(dict!,status)
            }
            else {
                Alert(title: kError, message: responce[kmessage] as? String ?? "", vc: RootViewController.controller!)
            }
        })
    }
    
    func getMenuItem(_ apiEndPoint: String,_ param : [String : Any], handler: @escaping (Int) -> Void) {
        
        self.handler  = handler
        guard let url = URL(string: Configuration().environment.baseURL + apiEndPoint) else {return}
        NetworkManager.shared.postRequest(url, true, "", params: param, networkHandler: {(responce,statusCode) in
            if let status: Int = responce[kstatus] as? Int, status == 1 {
                let dict  =  Mapper<MenuModel>().map(JSON: responce["data"] as! [String : Any])
                
                self.getMenuStructure((dict?.items)!)
                //   handler(status)
            }
            else {
                Alert(title: kError, message: responce[kmessage] as? String ?? "", vc: RootViewController.controller!)
            }
        })
    }
    
    func getMenuStructure(_ menuArray : [Items]){
        
        self.menuArray = menuArray
        
        for dict in  menuArray{
            
            var subCategory = SubCategoryMenuItemArray()
            
            if !menuItemArray.contains(where: {$0.subcat_id == dict.subcat_id}){
                subCategory.subcat_name = dict.subcat_name
                subCategory.subcat_id = dict.subcat_id
                
                if isItemsExistInOrderType(order_type: dict.order_type ?? "0") == true{
                    subCategory.item_Array.append(dict)
                }
                subCategory.cat_id = dict.cat_id
                subCategory.cat_name = dict.cat_name
                menuItemArray.append(subCategory)
            }
            else {
                
                for dictMenuModel in menuItemArray{
                    if dictMenuModel.subcat_id == dict.subcat_id{
                        
                        let index = menuItemArray.firstIndex(where: { (item) -> Bool in
                            item.subcat_id == dict.subcat_id
                        })
                        
                        if isItemsExistInOrderType(order_type: dict.order_type ?? "0") == true{
                            menuItemArray[index!].item_Array.append(dict)
                        }
                    }
                }
            }
        }
        
        saveItemArray = menuItemArray
        self.handler!(1)
    }
}
