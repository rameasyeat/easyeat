//
//  ChangeAddressViewModel.swift
//  easyeat
//
//  Created by Ramniwas on 30/04/20.
//  Copyright © 2020 EasyEat. All rights reserved.
//

import Foundation

struct PlacesAddress {
    var address : String?
    var locality : String?
    var placeID : String?
}
class ChangeAddressViewModel{
    var placesArray : [PlacesAddress]?
    var changeAddressDelegate  : ChangeAddressDelegate?
}
