//
//  OrderViewModel.swift
//  easyeat
//
//  Created by Ramniwas on 07/04/20.
//  Copyright © 2020 EasyEat. All rights reserved.
//

import Foundation
import ObjectMapper


enum OrderStatus : Int {
    case pending = 1
    case confirmed
    case inprogress
    case completed
    case moreItemAdded
}

class OrderViewModel{
    
    let cellHeight  = 100
    let billDetailHeight  = 320
    let billToPayHeight  = 80
    
    var orderData : OrderData?
    let headerHeight  = 50
    var arraySection = [OrderStruct]()
    var socketConnection : SocketConnection?
    var restaurantDict : RestaurantData?
    var orderCancelStatus : OrderCancelBy = OrderCancelBy.none
    var isPartnerDelivery : Int = 0
    var isShowViewDetail : Bool = false
    var courierLocationTimer : Timer?
    
    var courierLocation : Courier?
    
    struct OrderStruct {
        var itr  = 0
        var  item = [OrderItems]()
    }
    
    
    
    fileprivate func checkDeliveryType(){
        let partnerDelivery =  orderData?.delivery_partner ?? "0"
        if Int(partnerDelivery) == 1{
            isPartnerDelivery = 1
        }
    }
    
    // Call MQTT
    
    func callMQTTserver(){
        if socketConnection == nil{
            socketConnection?.mqtt = nil
        }
        socketConnection = SocketConnection()
        socketConnection?.callMQTTSocket(orderData?.order_id ?? "")
    }
    
    
    // Handle responce
    func handleOrderStatus(_ json : [String :Any],_ handller :@escaping (MType,OrderCancelBy)->Void){
        
        let mtype : String = json["mtype"] as? String ?? ""
        
        if mtype == "OID"{
            handller(MType.OID,OrderCancelBy.none)
        }
        else  if mtype == "OCM"{
            handller(MType.OCM,OrderCancelBy.none)
        }
        else  if mtype == "OCL"{
            
            let  data : [String : Any]  = (json["data"] as? [String :Any])!
            let cancelCode : Int = data["cancel_code"] as? Int ?? 0
            
            if(cancelCode == 2){ // cancel by MM
                handller(MType.OCL,OrderCancelBy.MM)
            }
            else  if(cancelCode == 1){// cancel by user
                handller(MType.OCL,OrderCancelBy.user)
            }
        }
        else
        {
            handller(MType.RELOAD,OrderCancelBy.none)
        }
    }
    
    
    func getOrderItemWithItrKey(_ itemArray : [OrderItems]){
        
        clearSectionArray()
        
        
        for itemDict in itemArray {
            
            var dict = OrderStruct()
            
            if !arraySection.contains(where: {$0.itr == itemDict.itr}){
                dict.itr = itemDict.itr!
                dict.item.append(itemDict)
                arraySection.append(dict)
            }
            else {
                
                for dict in arraySection{
                    if dict.itr == itemDict.itr{
                        let index = arraySection.firstIndex(where: { (item) -> Bool in
                            item.itr == dict.itr
                        })
                        arraySection[index!].item.append(itemDict)
                    }
                }
            }
        }
        
    }
    
    // hide bottom view if paid
    func hideAddMoreView(_ addMoreViewHeight: NSLayoutConstraint,_ statusLabel : UILabel){
        if orderData?.bill?.paid == orderData?.bill?.bill_total{
            addMoreViewHeight.constant = 0
            statusLabel.isHidden = true
        }
        
    }
    
    func clearSectionArray(){
        if arraySection.count > 0{
            arraySection.removeAll()
        }
    }
    
    // MARK : Set timer to nil
    
    func stopCourierTimer(){
        if courierLocationTimer != nil{
            courierLocationTimer?.invalidate()
            courierLocationTimer = nil
        }
    }
    
    func getSortedArray() -> [Services]{
          let  items = restaurantDict?.services!.sorted(by: { (item1, item2) -> Bool in
                return item1.category_name?.compare(item2.category_name) == ComparisonResult.orderedAscending
            })
          return items!
        }
    
    // Apis call
    
    
    func resetOrder(_ apiEndPoint: String,_ param : [String : Any], handler: @escaping ([String : Any],Int) -> Void) {
        guard let url = URL(string: Configuration().environment.baseURL + apiEndPoint) else {return}
        NetworkManager.shared.postRequest(url, true, "", params: param, networkHandler: {(responce,statusCode) in
            
            let code : Int = responce[kstatus] as? Int ?? 0
            if code == 1 {
                handler([:],code)
            }
            else {
                Alert(title: kError, message: responce["kmessage"] as? String ?? "", vc: RootViewController.controller!)
            }
        })
    }
    
    func cancelOrder(_ apiEndPoint: String,_ param : [String : Any], handler: @escaping (Int) -> Void) {
        guard let url = URL(string: Configuration().environment.baseURL + apiEndPoint) else {return}
        NetworkManager.shared.postRequest(url, true, "", params: param, networkHandler: {(responce,statusCode) in
            if let status: Int = responce[kstatus] as? Int, status == 1 {
                handler(status )
            }
            else {
                Alert(title: kError, message: responce[kmessage] as? String ?? "", vc: RootViewController.controller!)
            }
        })
    }
    
    func getDriverLocation(_ apiEndPoint: String,_ param : [String : Any], handler: @escaping (Int) -> Void) {
        guard let url = URL(string: Configuration().environment.baseURL + apiEndPoint) else {return}
        NetworkManager.shared.postRequest(url, false, "", params: param, networkHandler: {[weak self](responce,statusCode) in
            
            
            if let status: Int = responce[kstatus] as? Int, status == 1 {
                self?.courierLocation  =  Mapper<Courier>().map(JSON: responce["data"] as! [String : Any])
                
                handler(status )
            }
            else {
                Alert(title: kError, message: responce[kmessage] as? String ?? "", vc: RootViewController.controller!)
            }
        })
    }
    
    
    
    func getOrderStatus(_ apiEndPoint: String,_ param : [String : Any], handler: @escaping (Int) -> Void) {
      
        
        guard let url = URL(string: Configuration().environment.baseURL + apiEndPoint) else {return}
        NetworkManager.shared.postRequest(url, false, "", params: param, networkHandler: {[weak self](responce,statusCode) in
            let dictResponce =  Mapper<OrderStatusModel>().map(JSON: responce)
            if let status = dictResponce?.status, status == 1 {
                
                self?.orderData = dictResponce?.orderData
                self?.checkDeliveryType()
                self?.getOrderItemWithItrKey((dictResponce?.orderData?.orderItems)!)
                handler(status)
            }
            else {
                
                // let orderData = OrderData(map: .init(mappingType: .fromJSON, JSON: [:]))
                 handler(0)
            }
        })
    }
    
    
    
    
}




