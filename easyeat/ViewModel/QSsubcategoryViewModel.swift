//
//  QSsubcategoryViewModel.swift
//  easyeat
//
//  Created by Ramniwas on 16/04/20.
//  Copyright © 2020 EasyEat. All rights reserved.
//

import Foundation
import ObjectMapper


struct WifiService{
var type : ServiceCatType
var value : String
var header : String
}

class QSsubcategoryViewModel{
    
    var serviceDict : Services?
    var itemCount : Int = 0
    var serviceCatType : ServiceCatType?
    var infoArray = [WifiService]()
    let cellHeight = 65
    var orderId : String?
    var  quickServiceSubCatDelegate : QSubCatBackDelegate?
    var tblTopAxis = CGFloat(78 + 0.getSafeAreaHeight())
    var subViewHeight = CGFloat(185)
    
    
    func endcodeModel(_ model : [Subcategories]) -> String{
     let jsonString = Mapper().toJSONString(model, prettyPrint: true)
     return jsonString ?? ""
          }
    
    func checkServicesStatus(){
        print(serviceDict?.subcategories as AnyObject)
        if serviceCatType != ServiceCatType.wifi{
            let array = serviceDict?.subcategories?.filter{$0.status == 1}
            serviceDict?.subcategories = array
        }
      }
    func prepareInfo(dictInfo : Services)-> [WifiService]  {

        infoArray.append(WifiService(type: serviceCatType!, value: dictInfo.ssid , header: "Wifi name"))
        infoArray.append(WifiService(type: serviceCatType!, value: dictInfo.password , header: "Password"))
         return infoArray
     }
    
    func getServiceItems(_ apiEndPoint: String,_ param : [String : Any], handler: @escaping (Int) -> Void) {
          guard let url = URL(string: Configuration().environment.baseURL + apiEndPoint) else {return}
          NetworkManager.shared.postRequest(url, true, "", params: param, networkHandler: {(responce,statusCode) in
              let dictResponce =  Mapper<RestaurantDetails>().map(JSON: responce)
              if let status = dictResponce?.status, status == 1 {
                  handler(status)
              }
              else {
                  Alert(title: kError, message: dictResponce?.message ?? "", vc: RootViewController.controller!)
              }
          })
      }

}
