//
//  ForgotPasswordViewModel.swift
//  easyeat
//
//  Created by Ramniwas on 22/03/20.
//  Copyright © 2020 EasyEat. All rights reserved.
//

import Foundation
import UIKit

class ForgotPasswordViewModel {
    var dictInfo = [String : String]()
    var infoArray = [ForgotPasswordModel]()
    
    
    func prepareInfo(dictInfo : [String :String])-> [ForgotPasswordModel]  {
        
        infoArray.append(ForgotPasswordModel(type: .password, placeholder: "Enter a new Password", value: "", header: "NEW PASSWORD"))
        
        infoArray.append(ForgotPasswordModel(type: .confirmPassword, placeholder: "Enter a new Password", value: "", header: "RE-ENTER PASSWORD"))
        
        return infoArray
    }
    
    func validateFields(dataStore: [ForgotPasswordModel], validHandler: @escaping (_ param : [String : AnyObject], _ msg : String, _ succes : Bool) -> Void) {
        var dictParam = [String : AnyObject]()
        for index in 0..<dataStore.count {
            switch dataStore[index].type {
           
                case .password:
                if dataStore[index].value.trimmingCharacters(in: .whitespaces) == "" {
                    validHandler([:], "Please enter password", false)
                    return
                }
                dictParam["password"] = dataStore[index].value.trimmingCharacters(in: .whitespaces) as AnyObject
                
                case .confirmPassword:
                    if dataStore[index].value.trimmingCharacters(in: .whitespaces) == "" {
                        validHandler([:], "Please enter confirm password", false)
                        return
                    }
                    
                    else if dataStore[index].value.trimmingCharacters(in: .whitespaces) == "" {
                        validHandler([:], "Please enter confirm password", false)
                        return
                    }
                    dictParam["password"] = dataStore[index].value.trimmingCharacters(in: .whitespaces) as AnyObject
                
            }
        }
        
        validHandler(dictParam, "", true)
    }
}
