//
//  OrderHistoryViewModel.swift
//  easyeat
//
//  Created by Ramniwas on 25/04/20.
//  Copyright © 2020 EasyEat. All rights reserved.
//

import Foundation
import ObjectMapper


class OrderHistoryViewModel{
    var orderHistoryArray : [OrderHistory]?
    var pageIndex : Int = 1
    var norders : Int = 10
    
  
    
    func getOrderHistory(_ apiEndPoint: String,_ param : [String : Any],pageIndex : Int,_ showHUde : Bool, handler: @escaping (Int) -> Void) {
        guard let url = URL(string: Configuration().environment.baseURL + apiEndPoint) else {return}
        NetworkManager.shared.postRequest(url, showHUde, "", params: param, networkHandler: {[weak self](responce,statusCode) in
            
            
            let array  =  Mapper<OrderHistory>().mapArray(JSONArray:  responce["data"] as! [[String : Any]])

            if let status: Int = responce[kstatus] as? Int, status == 1  && array.count > 0{
                
                if self?.pageIndex == 1{
                    self?.orderHistoryArray = array
                }
                else{
                        for dic in array{
                            
                            if !(self?.orderHistoryArray?.contains(where: { (orderHistory) -> Bool in
                                if orderHistory.order_id == dic.order_id{return true}else{
                                    return false}
                            }))!{
                                self?.orderHistoryArray?.append(dic)
                            }
                            else {
                                return
                            }
                    }
                }
                
                self?.pageIndex += self!.norders
                handler(status )
                
            }
            else if let status: Int = responce[kstatus] as? Int, status == 1 && (self?.orderHistoryArray?.count == 0 || self?.orderHistoryArray == nil){
                handler(status )
            }
            else if let status: Int = responce[kstatus] as? Int, status == 0{
                    Alert(title: kError, message: responce[kmessage] as? String ?? "", vc: RootViewController.controller!)
                }
        })
    }
}
