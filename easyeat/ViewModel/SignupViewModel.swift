//
//  SigninViewModel.swift
//  easyeat
//
//  Created by Ramniwas on 20/03/20.
//  Copyright © 2020 EasyEat. All rights reserved.
//

import Foundation
import UIKit




class SignupViewModel {
    var dictInfo = [String : String]()
    var infoArray = [SignupInfoModel]()
    
    
    func prepareInfo(dictInfo : [String :String])-> [SignupInfoModel]  {
        
        infoArray.append(SignupInfoModel(type: .name, image: UIImage(named: "profilePlaceholder") ??  #imageLiteral(resourceName: "profilePlaceholder"), placeholder: "Please enter name", value: "", countryCode: "", header: "Name"))
        infoArray.append(SignupInfoModel(type: .phonenumber, image: UIImage(named: "profilePlaceholder") ??  #imageLiteral(resourceName: "profilePlaceholder"), placeholder: "Please enter mobile number", value: "", countryCode: "+91", header: "Phone Number"))
        infoArray.append(SignupInfoModel(type: .email, image: UIImage(named: "profilePlaceholder") ??  #imageLiteral(resourceName: "profilePlaceholder"), placeholder: "Please enter email", value: "", countryCode: "", header: "Email"))
        infoArray.append(SignupInfoModel(type: .password, image: UIImage(named: "profilePlaceholder") ??  #imageLiteral(resourceName: "profilePlaceholder"), placeholder: "Please enter password", value: "", countryCode: "", header: "Password"))
        
        return infoArray
    }
    
    func validateFields(dataStore: [SignupInfoModel], validHandler: @escaping (_ param : [String : AnyObject], _ msg : String, _ succes : Bool) -> Void) {
        var dictParam = [String : AnyObject]()
        for index in 0..<dataStore.count {
            switch dataStore[index].type {
                
            case .name:
                if dataStore[index].value.trimmingCharacters(in: .whitespaces) == "" {
                    validHandler([:], "Please enter name", false)
                    return
                }
                dictParam["name"] = dataStore[index].value.trimmingCharacters(in: .whitespaces) as AnyObject
                
            case .email:
                if dataStore[index].value.trimmingCharacters(in: .whitespaces) == "" {
                    validHandler([:], "Please enter email", false)
                    return
                }
                else if dataStore[index].value.trimmingCharacters(in: .whitespaces).isValidEmail() == false {
                    validHandler([:], "Please enter email", false)
                    return
                }
                
                dictParam["email"] = dataStore[index].value.trimmingCharacters(in: .whitespaces) as AnyObject
                
            case .phonenumber:
                if dataStore[index].value.trimmingCharacters(in: .whitespaces) == "" || (dataStore[index].value.trimmingCharacters(in: .whitespaces).count < 10) {
                    validHandler([:], "Please enter mobile number", false)
                    return
                }
                else if dataStore[index].countryCode.trimmingCharacters(in: .whitespaces) == "" {
                    validHandler([:], "Please enter Country code", false)
                    return
                }
                dictParam["mobileNumber"] = dataStore[index].value.trimmingCharacters(in: .whitespaces) as AnyObject
                dictParam["countryCode"] = dataStore[index].countryCode as AnyObject
                
            case .password:
                if dataStore[index].value.trimmingCharacters(in: .whitespaces) == "" {
                    validHandler([:], "Please enter password", false)
                    return
                }
                dictParam["password"] = dataStore[index].value.trimmingCharacters(in: .whitespaces) as AnyObject
            }
            
        }
        
        validHandler(dictParam, "", true)
    }
}
