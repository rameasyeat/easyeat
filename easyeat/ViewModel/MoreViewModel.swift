//
//  MoreViewModel.swift
//  easyeat
//
//  Created by Ramniwas on 24/04/20.
//  Copyright © 2020 EasyEat. All rights reserved.
//

import Foundation
import UIKit

struct MoreModel {
    var value : String?
    var img : UIImage?
}

enum MoreCellType : Int{
    case orderHistory = 0
//    case setting
    case TC
//    case notification
//    case changePassword
//    case language
    case logout
}

class MoreViewModel{
    
    var infoArray = [MoreModel]()
    
    func prepareInfo()  {
        
        infoArray.append(MoreModel(value: "Order History", img:#imageLiteral(resourceName: "clock")))
//        infoArray.append(MoreModel(value: "Settings", img:#imageLiteral(resourceName: "settings")))
        infoArray.append(MoreModel(value: "Terms & Condition", img: #imageLiteral(resourceName: "accept")))
//        infoArray.append(MoreModel(value: "Notification", img:#imageLiteral(resourceName: "bell")))
//        infoArray.append(MoreModel(value: "Change Password", img:#imageLiteral(resourceName: "password")))
//        infoArray.append(MoreModel(value: "Language", img:#imageLiteral(resourceName: "subject")))
        infoArray.append(MoreModel(value: "Log Out", img:#imageLiteral(resourceName: "log-out")))

    }
}
