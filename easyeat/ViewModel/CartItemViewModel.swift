

import Foundation
import ObjectMapper

func getItemAddedInCart(_ apiEndPoint: String,_ param : [String : Any],_ isFromCart : Bool, handler: @escaping (CartListData,Int) -> Void) {
    guard let url = URL(string: Configuration().environment.baseURL + apiEndPoint) else {return}
    NetworkManager.shared.postRequest(url,false, "", params: param, networkHandler: {(responce,statusCode) in
        let dictResponce =  Mapper<CartList>().map(JSON: responce)
        if let status = dictResponce?.status, status == 1 {
          handler((dictResponce?.cartListData)!,status)
        }
        else {
            Alert(title: kError, message: dictResponce?.message ?? "", vc: RootViewController.controller!)
        }
    })
}

func addItemInCart(_ apiEndPoint: String,_ param : [String : Any], handler: @escaping (CartListData,Int) -> Void) {
          guard let url = URL(string: Configuration().environment.baseURL + apiEndPoint) else {return}
          NetworkManager.shared.postRequest(url, false, "", params: param, networkHandler: {(responce,statusCode) in
              let dictResponce =  Mapper<CartList>().map(JSON: responce)
              if let status = dictResponce?.status, status == 1 {
                
                guard let dict = dictResponce?.cartListData else {return }
               handler(dict,status)
              }
              else {
                  Alert(title: kError, message: dictResponce?.message ?? "", vc: RootViewController.controller!)
              }
          })
      }

// check items exist in order or not

 func isItemsExistInOrderType(order_type : String) -> Bool{
    
    var currentOrderType: Int  = 0
    
    if CurrentUserInfo.priceByOrderType == .dining{
        currentOrderType = OrderTypeInBinary.dinein.rawValue
    }
    else   if CurrentUserInfo.priceByOrderType == .delivery{
        currentOrderType = OrderTypeInBinary.delivery.rawValue
    }
    else   if CurrentUserInfo.priceByOrderType == .pickup{
        currentOrderType = OrderTypeInBinary.takeaway.rawValue
    }
    
    let  orderTypeInBinary : Int = Int(order_type) ?? 0
    let itemOrderTypeBinary = UInt64(orderTypeInBinary)
    
    let currentorderTypeInBinary = UInt64(currentOrderType)
    
    let binaryOrderType = itemOrderTypeBinary & currentorderTypeInBinary
    
    if binaryOrderType == currentOrderType{
        return true
    }
    return false
}
