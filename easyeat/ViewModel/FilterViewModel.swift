//
//  FilterViewModel.swift
//  easyeat
//
//  Created by Ramniwas on 14/04/20.
//  Copyright © 2020 EasyEat. All rights reserved.
//

import Foundation
import UIKit

struct FilterSubcategory {
    var filterType : String?
    var filterArray = [Filters]()
    
}
class FilterViewModel{
    var restaurantFilter : [Filters]?
    var menuFilterDelegate : MenuFilterDelegate?
    var filterCategoryArray = [FilterSubcategory]()
    var selectedArray  =  [String]()

    var filterCelHeight = 100

    func getSelectedDict(_ index: Int, _ dict: Filters) {
        
        let catIndex = (filterCategoryArray.firstIndex(where: { (item) -> Bool in
            item.filterType == dict.filtertype
        }))!
        
        for (tag, _) in filterCategoryArray[catIndex].filterArray.enumerated() {
          filterCategoryArray[catIndex].filterArray[tag].isSelected = false
        }
        
        filterCategoryArray[catIndex].filterArray[index].isSelected = dict.isSelected
        selectedArray.removeAll()
               for selectedDict in filterCategoryArray{
                   let filterArray = selectedDict.filterArray.filter{$0.isSelected == true}
                
                if filterArray.count > 0{
                selectedArray.append(filterArray[0].id ?? "")
                }
               }
    }
    func convertFilterArrayToSubcategoryArray(){
        
        if filterCategoryArray.count > 0 {
            return
        }
        
        for dict in restaurantFilter!{
            
            var filterDict = FilterSubcategory()
            if !filterCategoryArray.contains(where: {$0.filterType == dict.filtertype}){
                filterDict.filterType = dict.filtertype
                if dict.status == 1{
                    filterDict.filterArray.append(dict)
                }
                filterCategoryArray.append(filterDict)

            }
            else {
                for dictMenuModel in filterCategoryArray{
                    if dictMenuModel.filterType == dict.filtertype{
                        
                        let index = filterCategoryArray.firstIndex(where: { (item) -> Bool in
                            item.filterType == dict.filtertype
                        })
                        if dict.status == 1{
                        filterCategoryArray[index!].filterArray.append(dict)
                    }
                    }
                }
                
            }
            
        }
    }
    
    
    func applyFilters(_ apiEndPoint: String,_ param : [String : Any], handler: @escaping (RestaurantData,Int) -> Void) {
        guard let url = URL(string: Configuration().environment.baseURL + apiEndPoint) else {return}
        NetworkManager.shared.postRequest(url, true, "", params: param, networkHandler: {(responce,statusCode) in
            
            print(responce)
//            let dictResponce =  Mapper<RestaurantDetails>().map(JSON: responce)
//            if let status = dictResponce?.status, status == 1 {
//                let item =  dictResponce?.restaurantData
//                handler(item!,status)
//            }
//            else {
//                Alert(title: kError, message: dictResponce?.message ?? "", vc: RootViewController.controller!)
//            }
        })
    }

    
}
