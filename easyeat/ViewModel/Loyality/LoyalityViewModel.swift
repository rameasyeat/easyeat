//
//  LoyalityViewModel.swift
//  easyeat
//
//  Created by Ramniwas on 26/05/20.
//  Copyright © 2020 EasyEat. All rights reserved.
//

import Foundation
import ObjectMapper
import LMGraphView

enum UserSavingOREarningType : Int{
    case saving = 0
    case earning
    case axisData
}
class LoyalityViewModel {
    var earningTypeArray = [GraphDefaultData]()
    var loyalityData : LoyalityData?
    
    var pageCount = 0
    var numberOfDays = 7
    
    let list = ["Your Savings","Average Savings","Your Savings","Average Savings"]
    let color = ["#2CD9C5","#032581","#2D99FC","#032581"]
    
    
    var graphMaxValue : Int = 0
    
    
    
    func getMaxSavingValue(_ currentValue : Int){
        if currentValue > graphMaxValue {
            graphMaxValue = currentValue
        }
    }
    
    // User Saving
    func convertDataIntoWeekArray(_ type : UserSavingOREarningType) -> [[Int : String]]{
        
        guard let count = loyalityData?.user_earning_saving?.count else {return [[:]]}
        print(count)
        
        var array = [[Int : String]]()
        for (index,_) in loyalityData!.user_earning_saving!.enumerated(){
            var dict = [Int : String]()
            dict[index + 1] = getDaysValue(index,type)
            array.append(dict)
        }
        return array
    }
    
    
    func getDaysValue(_ index : Int, _ type : UserSavingOREarningType) -> (String){
        
        var  dayOfWeek : String  = "0"
        if type == .axisData{
            let day = loyalityData?.user_earning_saving?[index].epoch ?? 0
            dayOfWeek = AppUtility.getdayFromDate(date: AppUtility.date(timestamp:
                Double(day)))
        }
        else if type == .earning{
            dayOfWeek = "\(loyalityData?.user_earning_saving?[index].earnings ?? 0)"
        }
        else if type == .saving{
            let saving = loyalityData?.user_earning_saving?[index].savings ?? 0
            dayOfWeek = "\(saving)"
            getMaxSavingValue(Int(saving))
        }
        return (dayOfWeek)
    }
    
    // get Graph points
    
    func getGraphPoints(_ arrayData : [[Int : String]]) -> [LMGraphPoint]{
        
        var arrayGraphPoints = [LMGraphPoint]()
        for (index,value) in arrayData.enumerated(){
            let price : String = (value[index+1] ?? "0") as String
            
            let dict = LMGraphPoint(point: CGPoint(x: index+1, y: Int(Double(price)!)), title: "\(index+1)", value: price)!
            arrayGraphPoints.append(dict)
        }
        return arrayGraphPoints
    }
    
    func getWeeklyTime() -> (String){
        
        guard let firstdayOfWeek = loyalityData?.user_earning_saving?[0].epoch else {return ("")}
        guard let lastdayOfWeek = loyalityData?.user_earning_saving?.last?.epoch else {return ("")}
        
        let startDay = AppUtility.getdayFromDate(date: AppUtility.date(timestamp:
            Double(firstdayOfWeek)))
        
        let lasttDay = AppUtility.getdayFromDate(date: AppUtility.date(timestamp: Double(lastdayOfWeek)))
        
        let str = startDay + " - " + lasttDay
        
        return (str)
    }
    
    func getDefaultData(){
        
        for (index,str) in list.enumerated(){
            print(str)
            var dict = GraphDefaultData(name: list[index], color: color[index], type: "saving", title: "Savings on Easy Eat")
            if index > 1{
                dict.type = "loyality"
                dict.title  = "Earnings through Loyalty"
            }
            earningTypeArray.append(dict)
        }
    }
    
    
    // Loyality saving & earning
    
    func convertLoyalityDataIntoWeekArray(_ type : UserSavingOREarningType) -> [[Int : String]]{
           
        guard let count = loyalityData?.average_earning_saving?.count else {return [[:]]}
           print(count)
           
           var array = [[Int : String]]()
           for (index,_) in loyalityData!.average_earning_saving!.enumerated(){
               var dict = [Int : String]()
               dict[index + 1] = getLoyalityDaysValue(index,type)
               array.append(dict)
           }
           return array
       }
    
    func getLoyalityDaysValue(_ index : Int, _ type : UserSavingOREarningType) -> (String){
        
        var  dayOfWeek : String  = "0"
        if type == .axisData{
            let day = loyalityData?.average_earning_saving?[index].epoch ?? 0
            dayOfWeek = AppUtility.getdayFromDate(date: AppUtility.date(timestamp:
                Double(day)))
        }
        else if type == .earning{
            dayOfWeek = "\(loyalityData?.average_earning_saving?[index].avg_earning ?? 0)"
        }
        else if type == .saving{
            let saving = loyalityData?.average_earning_saving?[index].avg_savings ?? 0
            dayOfWeek = "\(saving)"
            getMaxSavingValue(Int(saving))
        }
        return (dayOfWeek)
    }
    
    
    // validate data
    func validateSavingData() -> Bool{
        let filterUserSaving = loyalityData?.user_earning_saving?.filter{$0.savings ?? 0 > 0}
        let filterUserEarning = loyalityData?.user_earning_saving?.filter{$0.earnings ?? 0 > 0}
        if filterUserSaving?.count == 0 && filterUserEarning?.count == 0{
            return true
        }
        return false
    }
    
    
    func validateLoyality() -> Bool{
        let filterUserSaving = loyalityData?.average_earning_saving?.filter{$0.avg_savings ?? 0 > 0}
           let filterUserEarning = loyalityData?.average_earning_saving?.filter{$0.avg_earning ?? 0 > 0}
           if filterUserSaving?.count == 0 && filterUserEarning?.count == 0{
               return true
           }
           return false
       }
    
    // APIs call
    
    func getLoyalitygetLoyalityDetails(_ apiEndPoint: String,_ param : [String : Any], handler: @escaping (Int) -> Void) {
        guard let url = URL(string: Configuration().environment.baseURL + apiEndPoint) else {return}
        NetworkManager.shared.postRequest(url, false, "", params: param, networkHandler: {[weak self](responce,statusCode) in
            if let status: Int = responce[kstatus] as? Int, status == 1 {
                let dict  =  Mapper<LoyalityData>().map(JSON: responce["data"] as! [String : Any])
                self?.loyalityData = dict!
                handler(status)
            }
            else {
                Alert(title: kError, message: responce[kmessage] as? String ?? "", vc: RootViewController.controller!)
            }
        })
    }
    
    
}
