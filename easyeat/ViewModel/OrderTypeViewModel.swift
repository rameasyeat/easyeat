//
//  OrderTypeViewModel.swift
//  easyeat
//
//  Created by Ramniwas on 12/06/20.
//  Copyright © 2020 EasyEat. All rights reserved.
//

import Foundation


struct OrderTypeSelection {
    var typle : String?
    var img : UIImage?
}
class OrderTypeViewModel {
    
    var orderTypeDelegate : OrderTypeDelegate?
    var orderTypeArray = [OrderTypeSelection]()
    
    func getOrderType(){
        orderTypeArray.append(OrderTypeSelection(typle:"Dining at the Restaurant" , img: #imageLiteral(resourceName: "dining-room")))
        orderTypeArray.append(OrderTypeSelection(typle:"Delivery from the Restaurant" , img: #imageLiteral(resourceName: "Delivery")))
        orderTypeArray.append(OrderTypeSelection(typle:"PickUp at the Restaurant" , img: #imageLiteral(resourceName: "pickup")))
    }

    
}
