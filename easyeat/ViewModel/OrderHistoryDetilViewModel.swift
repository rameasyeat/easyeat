//
//  OrderHistoryDetilViewModel.swift
//  easyeat
//
//  Created by Ramniwas on 25/04/20.
//  Copyright © 2020 EasyEat. All rights reserved.
//

import Foundation
import ObjectMapper


enum OrderHistoryDetailCellType : Int {
    case orderDerail
    case order
    case rating
}

class OrderHistoryDetilViewModel{
    
    var orderHistoryDict : OrderHistoryDetail?
    var orderId : String?
    var numberOfSection  = 2
    var orderCellHeight = 80
    var orderDetailCellHeight = 130

    var headerHeight = 40
    
    let headerArray = ["Order details", "Your Order"]
    
    func setHeaderView(_ sectionIndex : Int) -> TableHeaderView{
        
        let headerView = TableHeaderView()
        headerView.headerLabel.text  = headerArray[sectionIndex]
        headerView.imgArrowWidth.constant = 0
        return headerView
    }
    
    func getOrderHistory(_ apiEndPoint: String,_ param : [String : Any], handler: @escaping (Int) -> Void) {
        guard let url = URL(string: Configuration().environment.baseURL + apiEndPoint) else {return}
        NetworkManager.shared.postRequest(url, true, "", params: param, networkHandler: {[weak self](responce,statusCode) in
            
            if let status: Int = responce[kstatus] as? Int, status == 1 {
                let dict  =  Mapper<OrderHistoryDetail>().map(JSON: responce["data"] as! [String : Any])

                self?.orderHistoryDict = dict
                handler(status )
            }
            else {
                Alert(title: kError, message: responce[kmessage] as? String ?? "", vc: RootViewController.controller!)
            }
        })
    }
}
