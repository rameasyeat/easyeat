

import Foundation
import ObjectMapper
import Lottie
import MXScroll

struct MenuItemArray {
    
    var cat_name : String?
    var cat_id : String?
    var subcat_item_array = [SubCategoryMenuItemArray]()
}

struct SubCategoryMenuItemArray {
    
    var subcat_name : String?
    var subcat_id : String?
    var cat_name : String?
    var cat_id : String?
    var isheaderClose : Bool = false
    var item_Array =  [Items]()
    
}

enum MenuSectionType : Int{
    case restaurant = 0
    case welcome
    case rewards
    case menuItems
}

enum OrderTypeInBinary : Int {
    case dinein = 1
    case delivery = 2
    case takeaway = 4
}

enum priceByOrderType : Int{
    case dining = 0
    case delivery
    case pickup
}

class MenuViewModel{
    
    var previousOffset  = 0
    let headerHeight  = 50
    let headerSearchHeight  = 140
    
    var cardItemViewHeight = 0
    
    let cellHeight = 100
    let menuMinBottomBottom = 16
    
    let constantHeaderHeight = 252
    var cardItemView :CardItemPopView?
    var runingOrderView :RunningOrderView?
    
    var menuArray : [Items]?
    var categoryArray : [String]?
    var menuItemArray : [SubCategoryMenuItemArray]?
    var saveItemArray : [SubCategoryMenuItemArray]?
    
    var reloadMQTT : Bool = false
    
    //    var saveItemArray = [SubCategoryMenuItemArray]?
    
    var restuarantID : String?
    var tblId : String?
    var totalQuantity : Int = 0
    var itemAddedCartArray  : CartListData?
    //    var restaurentService : [Services]?
    var restaurantFilter : [Filters]?
    var menuPopView : MenuPopView?
    var animationView : AnimationView?
    var socketConnection : SocketConnection?
    var filterCategoryArray : [FilterSubcategory]?
    var isVegFilter : Bool?
    var searchText   : String = ""
    var isFilterCountNil : Bool = true
    var restaurantDict : RestaurantData?
    var segmentView : MenuSegmentController?
    
    var orderType : OrderType = .dining
    
    var runingOrderConstraintHeight  = 0
    var isItemCountViewShow : Bool?
    
    var customMenuButton : CustomButton?
    
    var animationTime = 0.50
    
    var isRuningOrder : Bool = false{
        didSet{
            if isRuningOrder == false{
                runingOrderConstraintHeight = 0
            }else {
                runingOrderConstraintHeight = 53
            }
            customMenuButton?.frame.origin.y = ScreenSize.screenHeight - CGFloat(runingOrderConstraintHeight + cardItemViewHeight + ((isItemAddedInCart == true) ? 20 : 50) + 34)
        }
    }
    
    var isItemAddedInCart : Bool = false{
        didSet{
            if isItemAddedInCart == false{
                cardItemViewHeight = 0
            }else {
                cardItemViewHeight = 70
            }
            customMenuButton?.frame.origin.y = ScreenSize.screenHeight - CGFloat(runingOrderConstraintHeight + cardItemViewHeight + ((isRuningOrder == true) ? 20 : 50) + 34)
        }
    }
    
    var isFilterApply : Bool = false{
        didSet{
            setMenuFilters()
        }
    }
    
    
  
    
    /// Add Items Param
    func addItemDict(_ item : Items,_ addRemove : Int)  -> [String :Any]{
        
        let getAddItem = itemAddedCartArray?.cart?.cart_items?.filter{$0.item_id == item.id}
        
        var dict = [String :Any]()
        dict[krestaurant_id] = item.rest_id
        dict[kcart_token] = CurrentUserInfo.randomToken
        dict[kitem_id] = item.id
        dict[kquantity] = addRemove
        dict[ktable_id] = CurrentUserInfo.tblID
        dict[ktoken] =  CurrentUserInfo.authToken ?? kdefaultToken
        dict[kvariation_ids] = (getAddItem?.count == 0) ? "" : getAddItem?[0].variation_ids
        dict[kaddon_ids] = (getAddItem?.count == 0) ? "" : getAddItem?[0].addon_ids
        dict[kgvariations] = (item.gvariations == nil) ? "" : item.gvariations
        return dict
    }
    
//    func addButtonAnimation(_ delay : TimeInterval){
//        UIView.animate(withDuration: 0.3,delay: delay,
//                              animations: {[weak self] in
//                                self?.customMenuButton?.transform = CGAffineTransform(scaleX: 0.8, y: 0.8)
//               },
//                              completion: { _ in
//                                UIView.animate(withDuration: 0.2) {
//                                   self.customMenuButton?.transform = CGAffineTransform.identity
//                               }
//               })
//        }
    /// check if item constain variation
    
    func isItemContaintVariation(_ item : Items)-> [String : Any]{
        if item.gvariations != nil  {
            guard let dict = item.gvariations?.convertToDictionary() else { return [:]}
            return dict
        }
        return [:]
    }
    
    func getvariation(_ dict : [String : Any]) -> [Gvariations]{
        let array  =  Mapper<Gvariations>().mapArray(JSONArray: dict["groups"] as! [[String : Any]])
        return array
    }
    
    // MARK : MQTT cell
    func callMQTTserver(){
        if socketConnection == nil{
            socketConnection?.mqtt = nil
        }
        socketConnection = SocketConnection()
        socketConnection?.callMQTTSocket(itemAddedCartArray?.cart?.order?.order_id ?? "")
    }
    
    // Handle responce
    func handleOrderStatus(_ json : [String :Any],_ handller :@escaping (MType,OrderCancelBy)->Void){
        
        let mtype : String = json["mtype"] as? String ?? ""
        
        if mtype == "OID"{
            handller(MType.OID,OrderCancelBy.none)
        }
        else  if mtype == "OCM"{
            handller(MType.OCM,OrderCancelBy.none)
        }
        else  if mtype == "OCL"{
            
            let  data : [String : Any]  = (json["data"] as? [String :Any])!
            let cancelCode : Int = data["cancel_code"] as? Int ?? 0
            
            if(cancelCode == 2){ // cancel by MM
                handller(MType.OCL,OrderCancelBy.MM)
            }
            else  if(cancelCode == 1){// cancel by user
                handller(MType.OCL,OrderCancelBy.user)
            }
        }
        else
        {
            handller(MType.RELOAD,OrderCancelBy.none)
        }
    }
    
    
    // setInitial Data
    func setInitialData(){
        self.restaurantFilter = restaurantDict?.filters
        
        // Check restaurant Type
        guard let restaurantType = restaurantDict?.type else {return}
        if restaurantType == RestaurentType.both.rawValue || restaurantType == RestaurentType.delivery.rawValue{
            restaurantDict?.type = RestaurentType.delivery.rawValue
        }
        else {
            CurrentUserInfo.priceByOrderType =  .dining
        }
    }
    
    func getItemDetails(_ cartItems : Items) -> VarientItemDetails{
        
        let dict = isItemContaintVariation(cartItems)
        let groupArray = getvariation(dict)
        
        var varientItemDetails  = VarientItemDetails()
        varientItemDetails.name = cartItems.name
        varientItemDetails.id = cartItems.id
        varientItemDetails.variation = groupArray
        return varientItemDetails
    }
    
    // headerView
    func setHeaderView(_ sectionIndex : Int) -> TableHeaderView{
        
        let headerView = TableHeaderView()
        headerView.headerButton.tag = sectionIndex
        if menuItemArray?.count == 0{return headerView}
        
        headerView.animateImage((menuItemArray?[sectionIndex].isheaderClose)!)
        headerView.headerLabel.text  = menuItemArray?[sectionIndex].subcat_name?.capitalized
        return headerView
    }
    
    
    func setSearchItemHeaderView(_ sectionIndex : Int) -> SearchItemView{
        let searchItemView = SearchItemView()
        searchItemView.backgroundColor = .purple
        return searchItemView
    }
    
    // Food Animation
    func addRunningOrderFoodAnimation(_ view : UIView,_ type : String){
        
        if animationView != nil{
            animationView?.removeFromSuperview()
        }
        animationView = AnimationView()
        animationView?.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: view.frame.height)
        animationView?.animation = Animation.named(type)
        animationView?.backgroundColor = .clear
        animationView?.tag = 2022
        animationView?.loopMode = .loop
        animationView?.play()
        view.addSubview(animationView!)
    }
    
    
    // check item are added in cart
    func checkAlreadyAddedItemsInCart(){
        
        guard let addedItemArray = itemAddedCartArray?.cart?.cart_items  else {return}
        
        guard  let arrayCount = menuItemArray?.count else {return}
        
        for count in 0..<arrayCount {
            
            for  addedItemDict in addedItemArray{
                
                let index = menuItemArray?[count].item_Array.firstIndex(where: { (item) -> Bool in
                    item.id == addedItemDict.item_id
                })
                if index != nil {
                    menuItemArray?[count].item_Array[index ?? 0].quantity = "\( addedItemDict.quantity ?? 0)"}
            }
        }
        
    }
    
    func getMenuStructure(){
        
        if menuItemArray?.count ?? 0 > 0 {
            menuItemArray?.removeAll()
        }
        
        for dict in  menuArray!{
            
            var subCategory = SubCategoryMenuItemArray()
            
            if !(menuItemArray?.contains(where: {$0.subcat_id == dict.subcat_id}))!{
                subCategory.subcat_name = dict.subcat_name
                subCategory.subcat_id = dict.subcat_id
                
                if isItemsExistInOrderType(order_type: dict.order_type ?? "0") == true{
                    subCategory.item_Array.append(dict)
                }
                subCategory.cat_id = dict.cat_id
                subCategory.cat_name = dict.cat_name
                menuItemArray?.append(subCategory)
            }
            else {
                
                for dictMenuModel in menuItemArray!{
                    if dictMenuModel.subcat_id == dict.subcat_id{
                        
                        let index = menuItemArray?.firstIndex(where: { (item) -> Bool in
                            item.subcat_id == dict.subcat_id
                        })
                        
                        if isItemsExistInOrderType(order_type: dict.order_type ?? "0") == true{
                            menuItemArray?[index!].item_Array.append(dict)
                        }
                    }
                }
            }
        }
        
        saveItemArray = menuItemArray
    }
    
    
    func setHeightAccordingRuningOrder(){
        if isItemAddedInCart == false {
            runingOrderConstraintHeight += 0.getSafeAreaHeight()
            print(runingOrderConstraintHeight)
        }
    }
    func setHeightAccordingItemAdded(){
        cardItemViewHeight += 0.getSafeAreaHeight()
        
    }
    
    // show cart bottom pop
    
    func addCartBottomPoView(){
        
        setHeightAccordingItemAdded()
        if cardItemView == nil{
            cardItemView = CardItemPopView(frame: CGRect(x: 0, y: ScreenSize.screenHeight, width: ScreenSize.screenWidth, height: CGFloat(cardItemViewHeight)))
        }
        else {
            cardItemView?.frame =  CGRect(x: 0, y: ScreenSize.screenHeight, width: ScreenSize.screenWidth, height: CGFloat(cardItemViewHeight))
        }
    }
    
    func addRuningOrderView(){
        
        if runingOrderView == nil{
            runingOrderView = RunningOrderView(frame: CGRect(x: 0, y: ScreenSize.screenHeight - CGFloat(cardItemViewHeight), width: ScreenSize.screenWidth, height: CGFloat(runingOrderConstraintHeight)))
            
        }else {
            runingOrderView?.frame =  CGRect(x: 0, y: ScreenSize.screenHeight - CGFloat(cardItemViewHeight), width: ScreenSize.screenWidth, height: CGFloat(runingOrderConstraintHeight))
        }
    }
    
    
    
    // Hide MenuButton
    
    func hideMenuButton(_ index : Int){
        if (CurrentUserInfo.isScanQRCode == nil && index == 0){
            customMenuButton?.isHidden = true
            isRuningOrder = false
            isItemAddedInCart = false
            
        }else{
            customMenuButton?.isHidden = false
        }
    }
    
    
    // menu header
    
    func setMenuFilters() {
        menuItemArray?.removeAll()
        
        
        var filterArray = [SubCategoryMenuItemArray]()
        for dict in  saveItemArray!{
            
            isFilterCountNil = true
            var subcategoryDict = SubCategoryMenuItemArray()
            
            subcategoryDict.subcat_name = dict.subcat_name
            subcategoryDict.subcat_id = dict.subcat_id
            subcategoryDict.cat_id = dict.cat_id
            subcategoryDict.cat_name = dict.cat_name
            // veg filter
            
            var mainFilterArray = dict.item_Array
            if isVegFilter == true{
                let filterSubArray = mainFilterArray.filter{($0.filters?.uppercased() == "|VEG|")}
                if filterSubArray.count > 0{
                    mainFilterArray = filterSubArray
                    isFilterCountNil = false
                }
                else {
                    isFilterCountNil = true
                    mainFilterArray.removeAll()
                }
            }
            
            
            if filterCategoryArray?.count ?? 0 > 0{
                for dictFilter in filterCategoryArray!{
                    let  selectedArray = dictFilter.filterArray.filter{$0.isSelected  == true}
                    if selectedArray.count > 0{
                        let preference  = selectedArray[0].name ?? "0"
                        
                        var vegFilter = ""
                        
                        if preference == "Non Veg"{
                            vegFilter = "|NON-VEG|"
                        }
                        else  if preference == "Veg"{
                            vegFilter = "|VEG|"
                        }
                        
                        if selectedArray[0].filtertype == "preference"{
                            
                            let filterSubArray = mainFilterArray.filter{($0.filters?.uppercased() ?? ""  == vegFilter)}
                            if filterSubArray.count > 0{
                                mainFilterArray = filterSubArray
                                isFilterCountNil = false
                            }
                            else{
                                mainFilterArray.removeAll()
                                isFilterCountNil = true
                            }
                        }
                        
                        if selectedArray[0].filtertype == "price"{
                            let price : Int = Int(selectedArray[0].name ?? "0") ?? 0
                            
                            let filterSubArray = mainFilterArray.filter({(itemPrice) in
                                let strPrice : String = itemPrice.price ?? "0"
                                if Int(Float(strPrice)!) < price{
                                    return true
                                }
                                else {
                                    return false
                                }
                            })
                            if filterSubArray.count > 0{
                                mainFilterArray = filterSubArray
                                isFilterCountNil = false
                            }
                            else{
                                mainFilterArray.removeAll()
                                isFilterCountNil = true
                            }
                        }
                    }
                    else {
                        isFilterCountNil = true
                        
                    }
                }
            }
            
            // Apply search
            if searchText != ""{
                //                let filterSubArray = mainFilterArray.filter{($0.name!.contains("\(searchText )"))}
                
                let filterSubArray = mainFilterArray.filter { ($0.name?.localizedCaseInsensitiveContains(searchText))! }
                
                if filterSubArray.count > 0{
                    mainFilterArray = filterSubArray
                    isFilterCountNil = false
                }
                
                if isFilterCountNil == true || filterSubArray.count == 0{
                    mainFilterArray = filterSubArray
                }
            }
            
            if mainFilterArray.count > 0 && isFilterCountNil == false{
                subcategoryDict.item_Array = mainFilterArray
                filterArray.append(subcategoryDict)
            }
            else  if searchText == "" && filterCategoryArray?.count ?? 0  == 0 && isVegFilter == false{
                subcategoryDict.item_Array = mainFilterArray
                filterArray.append(subcategoryDict)
            }
            else if isFilterCountNil == true && searchText == "" && mainFilterArray.count > 0 {
                subcategoryDict.item_Array = mainFilterArray
                filterArray.append(subcategoryDict)
            }
            
            
        }
        
        menuItemArray = filterArray
        
        
    }
    
    func getMenuItem(_ apiEndPoint: String,_ param : [String : Any], handler: @escaping ([Items],Int) -> Void) {
        guard let url = URL(string: Configuration().environment.baseURL + apiEndPoint) else {return}
        NetworkManager.shared.postRequest(url, false, "", params: param, networkHandler: {(responce,statusCode) in
            if let status: Int = responce[kstatus] as? Int, status == 1 {
                let dict  =  Mapper<MenuModel>().map(JSON: responce["data"] as! [String : Any])
                handler((dict?.items)!,status)
            }
            else {
                Alert(title: kError, message: responce[kmessage] as? String ?? "", vc: RootViewController.controller!)
            }
        })
    }
    
    
}
extension String {
    func convertToDictionary() -> [String: Any]? {
        if let data = data(using: .utf8) {
            return try? JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
        }
        return nil
    }
    
    
}
