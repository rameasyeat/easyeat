//
//  OTPViewModel.swift
//  easyeat
//
//  Created by Ramniwas on 28/03/20.
//  Copyright © 2020 EasyEat. All rights reserved.
//

import Foundation
import ObjectMapper

protocol OTPTimerDelegate {
    func grtOTPCount(_ count :String)
}
class OTPViewModel {
    
    var otpTimerDelegate : OTPTimerDelegate?
    var dictInfo : SigninInfoModel?
    var otpTimer: Timer?
    var counter = 30
    var verificationID: String?
   
    
    func startTimer() {
        
        counter = 30
        otpTimer =  Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(updateCounter), userInfo: nil, repeats: true)
    }
    @objc func updateCounter() {
        if counter > 0 {
            counter -= 1
        otpTimerDelegate?.grtOTPCount("\(counter)")
        }
        else {
            endTimer()
        }
    }
    
    func endTimer() {
        otpTimer?.invalidate()
    }
    
    func registerUser(_ apiEndPoint: String,_ param : [String : Any], handler: @escaping (UserNameAPIsModel,Int) -> Void) {
        guard let url = URL(string: Configuration().environment.baseURL + apiEndPoint) else {return}
        NetworkManager.shared.postRequest(url, true, "", params: param, networkHandler: {(responce,statusCode) in
            let dictResponce =  Mapper<UserNameAPIsModel>().map(JSON: responce)
                handler(dictResponce!,0)
        })
    }
}
