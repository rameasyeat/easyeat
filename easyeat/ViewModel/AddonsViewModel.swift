//
//  VariationViewModel.swift
//  easyeat
//
//  Created by Ramniwas Patidar on 13/07/20.
//  Copyright © 2020 EasyEat. All rights reserved.
//

import Foundation
import ObjectMapper

class AddonsViewModel{
    
    let cellHeight = 50.0
    var addonsArray : [Addons]?
    var groupArray : [Gvariations]?
    var itemDetails : VarientItemDetails?
    var selectedGroupIndex : Int = 0
    var variationDelegate : VariationDelegate?
    var restaurantDict : RestaurantData?
    var tblTopAxis = CGFloat(78 + 0.getSafeAreaHeight())
    var subViewHeight = CGFloat(185)
    var delegateOnBackAddons : DelegateOnBackAddons?
    
    
    /// Add Items Param
    func addItemDict(_ itemDetails : VarientItemDetails)  -> [String :Any]{
        
        let selectedAddons = addonsArray?.filter{$0.isSelected == true}
        
        var dict = [String :Any]()
        dict[krestaurant_id] = CurrentUserInfo.restaurantId
        dict[kcart_token] = CurrentUserInfo.randomToken
        dict[kitem_id] = itemDetails.id
        dict[kquantity] = 1
        dict[ktable_id] = CurrentUserInfo.tblID
        dict[ktoken] =  CurrentUserInfo.authToken ?? kdefaultToken
        dict[kvariation_ids] = getSelectedVarients()
        dict[kaddon_ids] =  (selectedAddons?.count ?? 0 == 0) ? "" : "|\(selectedAddons?[0].id ?? "")"
        dict[kgvariations] = itemDetails.variation
        return dict
    }
    
    fileprivate func getSelectedVarients() -> String{
        var varients = ""
        for dict in groupArray! {
            let selectedVariation = dict.variations?.filter{$0.isSelected == true}
            if let varient = selectedVariation?[0]{
                varients += "|\(varient.vid ?? 0)"
            }
        }
        return varients
    }
    
    func isAddons() -> [Addons]{
        
        var addOnsArray = [Addons]()
        guard let addons = restaurantDict?.addons else { return addOnsArray}
        for dict in addons{
            let addonsArray = dict.itemids?.filter{$0 == itemDetails?.id}
            if addonsArray?.count ?? 0 > 0 {
                addOnsArray.append(dict)
            }
        }
        return addOnsArray
    }
    
    func getvariation(_ variation : String) -> [Gvariations]{
        let dict = variation.convertToDictionary()
        let array  =  Mapper<Gvariations>().mapArray(JSONArray: dict?["groups"] as! [[String : Any]])
        return array
    }
    
    func json(from object:Any) -> String? {
        guard let data = try? JSONSerialization.data(withJSONObject: object, options: [.prettyPrinted]) else {
            return nil
        }
        return String(data: data, encoding: String.Encoding.utf8)
    }
}
