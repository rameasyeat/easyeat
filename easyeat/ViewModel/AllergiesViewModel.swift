//
//  AllergiesViewModel.swift
//  easyeat
//
//  Created by Ramniwas on 25/03/20.
//  Copyright © 2020 EasyEat. All rights reserved.
//

import Foundation
import ObjectMapper

class AllergiesViewModel {
     let cellHeight = 84.0
    let deleteButtonWidth = 18

    var dictInfo = [String : String]()
    var userDictData : UserData?
    var userAllergiesDict : UserAllergicItems?
    
    
    
    
    func getUserAllergies(_ apiEndPoint: String,_ param : [String : Any], handler: @escaping (Int) -> Void) {
          guard let url = URL(string: Configuration().environment.baseURL + apiEndPoint) else {return}
          NetworkManager.shared.postRequest(url, true, "", params: param, networkHandler: {[weak self](responce,statusCode) in
            let dictResponce =  Mapper<UserAllergicItems>().map(JSON:  responce["data"] as! [String : Any])
            if let status: Int = responce[kstatus] as? Int, status == 1 {
                self?.userAllergiesDict = dictResponce
                handler(status )

              }
              else {
                Alert(title: kError, message: responce[kmessage] as? String ?? "", vc: RootViewController.controller!)
              }
          })
      }
    
    func deleteUserAllergies(_ apiEndPoint: String,_ param : [String : Any], handler: @escaping (Int) -> Void) {
             guard let url = URL(string: Configuration().environment.baseURL + apiEndPoint) else {return}
             NetworkManager.shared.postRequest(url, true, "", params: param, networkHandler: {(responce,statusCode) in
               if let status: Int = responce[kstatus] as? Int, status == 1 {
                   handler(status )
                 }
                 else {
                   Alert(title: kError, message: responce[kmessage] as? String ?? "", vc: RootViewController.controller!)
                 }
             })
         }
    
    
    func updateAllergies(_ apiEndPoint: String,_ param : [String : Any], handler: @escaping (Int) -> Void) {
             guard let url = URL(string: Configuration().environment.baseURL + apiEndPoint) else {return}
             NetworkManager.shared.postRequest(url, true, "", params: param, networkHandler: {(responce,statusCode) in
               if let status: Int = responce[kstatus] as? Int, status == 1 {
                   handler(status )
                 }
                 else {
                   Alert(title: kError, message: responce[kmessage] as? String ?? "", vc: RootViewController.controller!)
                 }
             })
         }

}
