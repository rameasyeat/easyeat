//
//  AddressViewModel.swift
//  easyeat
//
//  Created by Ramniwas on 29/04/20.
//  Copyright © 2020 EasyEat. All rights reserved.
//

import Foundation
import ObjectMapper

class AddressViewModel{
    var addressArray : [DeliveryAddress]?
    var deliveryAddressDelegate : DeliveryAddressDelegate?
    var selectedAddressId : String?
    var selectedAddress = SelectedDeliveryAddress()

  
    
    func getAddress(_ apiEndPoint: String,_ param : [String : Any], handler: @escaping (Int) -> Void) {
        guard let url = URL(string: Configuration().environment.baseURL + apiEndPoint) else {return}
        NetworkManager.shared.postRequest(url, true, "", params: param, networkHandler: {[weak self](responce,statusCode) in
            
            if let status: Int = responce[kstatus] as? Int, status == 1 {
                let array  =  Mapper<DeliveryAddress>().mapArray(JSONArray:  responce["data"] as! [[String : Any]])
                self?.addressArray = array
                handler(status )
            }
            else {
               // Alert(title: kError, message: responce[kmessage] as? String ?? "", vc: RootViewController.controller!)
            }
        })
    }
}
