//
//  CartViewModel.swift
//  easyeat
//
//  Created by Ramniwas on 02/04/20.
//  Copyright © 2020 EasyEat. All rights reserved.
//

import ObjectMapper
import CoreLocation
import Lottie
import Toast_Swift

enum CartTypeCell : Int {
    case cart = 0
    case specificRequest
    case delivery
    case billDetail
}

struct SpecificRequest {
    var isSpecifiRequest : Bool = false
    var description : String = ""
    
    init(_ isRequest : Bool,_ description : String) {
        self.isSpecifiRequest = isRequest
        self.description = description
    }
    
}

struct VarientItemDetails {
    var name : String?
    var id : String?
    var variation :[Gvariations]?
}


class CartViewModel{
    
    var cardItemViewHeight  = 70
    let cellHeight  = 100
    let specificCellHeight  = 56
    let constantHeaderHeight  = 252
    
    var deliveryCharge : Double?
    var totalQuantity : Int = 0
    
    var cardItemView :CardItemPopView?
    var specificRequestDict  : SpecificRequest?
    var restaurantDict : RestaurantData?
    var cartArray  : CartListData?
    var distanceModel : DistanceModel?
    var partnerDeliveryFeeData : PartnerDeliveryFeeData?
    
    var userCurrentLocation : CLLocationCoordinate2D?
    var currentlocationAddress : String?
    var currentLocationLocality : String?
    var selectedAddressId : String?
    var deliveryPartner : Delivery_partner?
    
    var isDeliverable : Bool = false
    var isRuningOrder : Bool = false
    
    var billDetails : BillDetails?
    
    
    func registerCell(_ tblView : UITableView){
        RestaurantItemCell.registerWithTable(tblView)
        SpecificRequestCell.registerWithTable(tblView)
        OrderDeliveryCell.registerWithTable(tblView)
        BillDetailCell.registerWithTable(tblView)
    }
    
    func getCurrentLocation(handler :@escaping (Bool)-> Void){
        UserLocation.sharedManager.startLocationManager { [weak self](locality,address, cord,isLocationAllow) in
            self?.userCurrentLocation = cord
            self?.currentlocationAddress = address
            self?.currentLocationLocality = locality
            handler(isLocationAllow)
        }
    }
    func getLocation<T>(_ location : T) -> [Double]{
        
        if let dict = location as? SelectedDeliveryAddress{
            let lat : String = dict.lat ?? ""
            let lng : String = dict.lng ?? ""
            var location  = [Double]()
            location.append(Double(lat)!)
            location.append(Double(lng)!)
            return location
        }
        else  if let dict = location as? Location{
            var location  = [Double]()
            location.append(dict.lat ?? 0)
            location.append(dict.lon ?? 0)
            return location
        }
            
        else  if let dict = location as? CLLocationCoordinate2D{
            var location  = [Double]()
            location.append(dict.latitude)
            location.append(dict.longitude)
            return location
        }
        return[0,0]
    }
    
    
    
    // MARK : ViewCart
    func updateBottmView(_ result : Cart,handler : @escaping (Bool)-> Void){
        totalQuantity = 0
        for dict in result.cart_items!{
            totalQuantity +=  dict.quantity!
        }
        if result.cart_items?.count ?? 0 > 0{
            handler(true)
        }
        else {
            handler(false)
        }
        
        let totoalAmount : Double = Double(billDetails?.bill_total ?? 0)
        let roundPrice = String(format: "%.2f",totoalAmount)
        
//        cardItemView?.currencyLabel.text = "\(result.currency ?? "") \(roundPrice)"
//        cardItemView?.itemCountLabel.text = "\(totalQuantity) ITEMS"

        cardItemView?.currencyLabel.text = "\(result.currency ?? "") \(roundPrice)"

        
        if (CurrentUserInfo.authToken != nil && CurrentUserInfo.authToken != "") {
            cardItemView?.viewCartLabel.text = "Place Order"
        }else{
            cardItemView?.viewCartLabel.text = "Login to Place Order"
        }
    }
    
    
    func json(from object:Any) -> String? {
        guard let data = try? JSONSerialization.data(withJSONObject: object, options: []) else {
            return nil
        }
        return String(data: data, encoding: String.Encoding.utf8)
    }
    
    
    
    func getvariation(_ dict : [String : Any]) -> [Gvariations]{
        let array  =  Mapper<Gvariations>().mapArray(JSONArray: dict["groups"] as! [[String : Any]])
        return array
    }
    
    
    func isItemContaintVariation(_ item : Cart_items?)-> [String : Any]{
        if item?.item_details?.gvariations != nil  {
            guard let dict = item?.item_details?.gvariations?.convertToDictionary() else { return [:]}
            return dict
        }
        return [:]
    }
    
    
    func checkDeliveryDistance(){
        var restaurantDistance = getDeliveryDistance(restaurantDict!)
        
//        if let distance = restaurantDict?.deliverylimit as? Double {
//            restaurantDistance = distance
//        }
//        else if let distance = restaurantDict?.deliverylimit as? String {
//            restaurantDistance = Double(distance) ?? 0
//        }
//        else if let distance = restaurantDict?.deliverylimit as? Double {
//            restaurantDistance = Double(distance)
//        }
        
        let userCurrentDistance = (distanceModel?.rows?.last?.elements?.last?.distance?.value ?? 0) / 1000
        
        
        let roundOffDistance = Double(round(1000*userCurrentDistance)/1000)
        
        if restaurantDistance  >= roundOffDistance &&  roundOffDistance > 0{
            restaurantDict?.isDeliverable = true
        }
        else{
            restaurantDict?.isDeliverable = false
        }
        restaurantDict?.userCurrentDistance = Int(round(userCurrentDistance))
    }
    
     func getDeliveryDistance(_ dict: RestaurantData) -> Double{
         var restaurantDistance :  Double = 0
         
         if let distance = dict.deliverylimit as? Double {
             restaurantDistance = distance
         }
         else if let distance = dict.deliverylimit as? String {
             restaurantDistance = Double(distance) ?? 0
         }
         else if let distance = dict.deliverylimit as? Double {
             restaurantDistance = Double(distance)
         }
         return restaurantDistance
     }
    
    
    /*func addDeliveryCharges(_ type : OrderType){
     guard  let dictData  = (restaurantDict?.restaursntFee?[0]) else {return}
     guard let index = restaurantDict?.userCurrentDistance else { return }
     if (type == OrderType.delivery) && (restaurantDict?.isDeliverable == true){
     
     if deliveryPartner?.isDeliveryPartner == true {
     let charges : String = partnerDeliveryFeeData?.partnerOrder?.delivery_fee_amount ?? "0"
     deliveryCharge = Double(charges)
     }
     else if let charge = dictData.fees?[index], charge > 0{
     deliveryCharge = charge
     }
     }
     else{
     deliveryCharge = 0
     }
     
     let totoalAmount : Double = Double(cartArray?.cart!.total_amount ?? 0)  + Double(deliveryCharge ?? 0)
     cardItemView?.currencyLabel.text = "\(CurrentUserInfo.currency ?? "") \(totoalAmount)"
     
     restaurantDict?.deliveryCharge = deliveryCharge ?? 0
     }*/
    
    // MARK : Check is Partner delivery type
    
    func checkIsPartnerDelivery(){
        
        guard let arrayDelivery = restaurantDict?.delivery_partner else {return
        }
        
        // check priority
        if  arrayDelivery.count > 1{
            let priorityParnter = arrayDelivery.filter{$0.priority == 1}
            if priorityParnter.count > 0 {
                deliveryPartner = priorityParnter[0]
            }
            deliveryPartner?.isDeliveryPartner = true
        }
        else if arrayDelivery.count > 0 {
            deliveryPartner = arrayDelivery[0]
            deliveryPartner?.isDeliveryPartner = true
        }
        deliveryPartner?.isDeliveryPartner = false
    }
    
    // Mark add specific Request
    func addSpecificRequestInfo(){
        specificRequestDict = SpecificRequest(false,"")
    }
    
    // Food Animation
    func showSuccessAnimation(_ view : UIView){
        let animationView  = AnimationView()
        animationView.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: view.frame.height)
        animationView.animation = Animation.named("Delivery")
        animationView.backgroundColor = .clear
        animationView.tag = 2024
        animationView.loopMode = .loop
        animationView.play()
        view.addSubview(animationView)
    }
    
    func getSortedArray() -> [Services]{
        let  items = restaurantDict?.services!.sorted(by: { (item1, item2) -> Bool in
            return item1.category_name?.compare(item2.category_name) == ComparisonResult.orderedAscending
        })
        return items!
    }
    
    // APIS param Dict :
    
    func getPartnerPriceParam() -> [String : Any]{
        var dict : [String : Any] = [String : Any]()
        dict[krest_address] =  restaurantDict?.address
        //dict[kuser_address] =  restaurantDict?.address // testing
        dict[kuser_address] = restaurantDict?.currentlocationAddress
        return dict
    }
    
    func getCreateOrderParam() -> [String : Any]{
        var dict : [String : Any] = [String : Any]()
        dict[kcart_token] =  CurrentUserInfo.randomToken
        dict[ktoken] =   CurrentUserInfo.authToken ?? kdefaultToken
        dict[korder_type] =  CurrentUserInfo.priceByOrderType.rawValue //restaurantDict?.orderType.rawValue
        dict[kdistance] = restaurantDict?.userCurrentDistance
        dict[ktable_id] = CurrentUserInfo.tblID

        dict[kaddress] =  restaurantDict?.currentlocationAddress // ?? restaurantDict?.address
        return dict
    }
    
    func getCartBillParam() -> [String : Any]{
        var dict : [String : Any] = [String : Any]()
        
        
        dict[kcustomer_address] = restaurantDict?.currentlocationAddress ?? ""
        dict[kcart_token] = CurrentUserInfo.randomToken
        dict[krestaurant_id] = CurrentUserInfo.restaurantId
        dict[korder_type] = CurrentUserInfo.priceByOrderType.rawValue
        
        let lat  = restaurantDict?.userCurrentLocation.latitude ?? 0
        let lng   =  restaurantDict?.userCurrentLocation.longitude ?? 0
        
        let strLat : String = lat.description
        let strlong : String = lng.description
        
        let locationArray : [String] = [strLat,strlong]
        dict[kcustomer_location] =  json(from: locationArray)
        
        return dict
        
    }
    
    func getCartListParam() -> [String :Any]{
        var dict : [String : Any] = [String : Any]()
        dict[krestaurant_id] = CurrentUserInfo.restaurantId
        dict[kcart_token] = CurrentUserInfo.randomToken
        dict[ktable_id] = CurrentUserInfo.tblID
        dict[ktoken] =  CurrentUserInfo.authToken ?? kdefaultToken
        return dict
    }
    
    func getItemCountParam(_ item : Cart_items,quantity : Int) -> [String :Any]{
        
        let getAddItem = cartArray?.cart?.cart_items?.filter{$0.item_id == item.item_id}
        
        var dict = [String :Any]()
        dict[krestaurant_id] = CurrentUserInfo.restaurantId
        dict[kcart_token] = CurrentUserInfo.randomToken
        dict[kitem_id] = item.item_id
        dict[kquantity] = quantity
        dict[ktable_id] = CurrentUserInfo.tblID ?? cartArray?.cart?.table_id
        dict[ktoken] =  CurrentUserInfo.authToken ?? kdefaultToken
        dict[kvariation_ids] = (getAddItem?.count == 0) ? "" : getAddItem?[0].variation_ids
        dict[kaddon_ids] = (getAddItem?.count == 0) ? "" : getAddItem?[0].addon_ids
        dict[kgvariations] = (item.gvariations == nil) ? "" : item.gvariations
        
        return dict
    }
    
    func getItemDetails(_ cartItems : Cart_items) -> VarientItemDetails{
        
        let dict = isItemContaintVariation(cartItems)
        let groupArray = getvariation(dict)
        
        var varientItemDetails  = VarientItemDetails()
        varientItemDetails.name = cartItems.item_name
        varientItemDetails.id = cartItems.item_id
        varientItemDetails.variation = groupArray
        return varientItemDetails
    }
    
    
    // check if item not available
    
    func checkIfItemNotAvailable() -> [Cart_items]{
        let cartItems = cartArray?.cart?.cart_items
        var cartArray = [Cart_items]()
        
        for dict in cartItems!{
            if isItemsExistInOrderType(order_type: (dict.item_details?.order_type)!) == false {
                cartArray.append(dict)
            }
        }
        
        return cartArray
    }
    
    
    // APIs call
    func createOrder(_ apiEndPoint: String,_ param : [String : Any], handler: @escaping ([String : Any],Int) -> Void) {
        guard let url = URL(string: Configuration().environment.baseURL + apiEndPoint) else {return}
        NetworkManager.shared.postRequest(url, true, "", params: param, networkHandler: {(responce,statusCode) in
            
            let code : Int = responce[kstatus] as? Int ?? 0
            if code == 1 {
                handler([:],code)
            }
            else {
                
                RootViewController.controller?.view.makeToast(responce[kmessage] as? String ?? "")
                //                Alert(title: kError, message: responce["kmessage"] as? String ?? "", vc: RootViewController.controller!)
            }
        })
    }
    
    func specificRequest(_ apiEndPoint: String,_ param : [String : Any], handler: @escaping (Bool,Int) -> Void) {
        
        guard let url = URL(string: Configuration().environment.baseURL + apiEndPoint) else {return}
        NetworkManager.shared.postRequest(url, false, "", params: param, networkHandler: {(responce,statusCode) in
            if let status: Int = responce[kstatus] as? Int, status == 1 {
                handler(true,status)
            }
            else {
                Alert(title: kError, message: responce[kmessage] as? String ?? "", vc: RootViewController.controller!)
            }
        })
    }
    
    func getDistance(_ apiEndPoint: String,_ param : [String : Any], handler: @escaping (Int) -> Void) {
        guard let url = URL(string: Configuration().environment.baseURL + apiEndPoint) else {return}
        NetworkManager.shared.postRequest(url, false, "", params: param, networkHandler: {[weak self](responce,statusCode) in
            
            
            if let status: Int = responce[kstatus] as? Int, status == 1 {
                
                let dict  =  Mapper<DistanceModel>().map(JSON: responce["data"] as! [String : Any])
                self?.distanceModel = dict
                self?.checkDeliveryDistance()
                handler(status )
            }
            else {
                Alert(title: kError, message: responce[kmessage] as? String ?? "", vc: RootViewController.controller!)
            }
        })
    }
    
    func partnerPriceCalculation(_ apiEndPoint: String,_ param : [String : Any], handler: @escaping (Int) -> Void) {
        guard let url = URL(string: Configuration().environment.baseURL + apiEndPoint) else {return}
        NetworkManager.shared.postRequest(url, false, "", params: param, networkHandler: {[weak self](responce,statusCode) in
            
            if let status: Int = responce[kstatus] as? Int, status == 1 {
                self?.partnerDeliveryFeeData  =  Mapper<PartnerDeliveryFeeData>().map(JSON: responce["data"] as! [String : Any])
                handler(status )
            }
            else {
                Alert(title: kError, message: responce[kmessage] as? String ?? "", vc: RootViewController.controller!)
            }
        })
    }
    
    
    func getOrderStatus(_ apiEndPoint: String,_ param : [String : Any], handler: @escaping (Bool,Int) -> Void) {
        
        guard let url = URL(string: Configuration().environment.baseURL + apiEndPoint) else {return}
        NetworkManager.shared.postRequest(url, false, "", params: param, networkHandler: {[weak self](responce,statusCode) in
            let dictResponce =  Mapper<OrderStatusModel>().map(JSON: responce)
            if let orderItems = dictResponce?.orderData?.orderItems, orderItems.count > 0 {
                self?.isRuningOrder = true
                handler(true ,0)
            }
            else {
                self?.isRuningOrder = false
                handler(false ,0)
            }
        })
    }
    
    
    func getAddress(_ apiEndPoint: String,_ param : [String : Any], handler: @escaping (Int) -> Void) {
        guard let url = URL(string: Configuration().environment.baseURL + apiEndPoint) else {return}
        NetworkManager.shared.postRequest(url, false, "", params: param, networkHandler: {[weak self](responce,statusCode) in
            
            if let status: Int = responce[kstatus] as? Int, status == 1 {
                let array  =  Mapper<DeliveryAddress>().mapArray(JSONArray:  responce["data"] as! [[String : Any]])
                if array.count > 0 {
                    let defaultAddress = array.filter{$0.is_default == 1}[0]
                    self?.getDefaultAddress(defaultAddress, handler: {(load) in })
                    handler(status)
                }
            }
            else {
                handler(0)
                // Alert(title: kError, message: responce[kmessage] as? String ?? "", vc: RootViewController.controller!)
            }
        })
    }
    
    func getBillDetails(_ apiEndPoint: String,_ isLoading : Bool,_ param : [String : Any], handler: @escaping (Int) -> Void) {
        guard let url = URL(string: Configuration().environment.baseURL + apiEndPoint) else {return}
        NetworkManager.shared.postRequest(url, false, "", params: param, networkHandler: {[weak self](responce,statusCode) in
            
            if let status: Int = responce[kstatus] as? Int, status == 1 || status == 2 {
                self?.billDetails =  Mapper<BillDetails>().map(JSON: responce["data"] as! [String : Any])
            }
            handler(0)
        })
    }
    
    func getDefaultAddress(_ defaultAddress :DeliveryAddress, handler : @escaping (Bool)-> Void){
        
        if (defaultAddress.is_default != nil){// check default location selected or not
            self.restaurantDict?.userCurrentLocation = CLLocationCoordinate2D(latitude: Double(defaultAddress.lat!)!, longitude: Double(defaultAddress.lng!)!)
            self.restaurantDict?.currentlocationAddress = (defaultAddress.flat_details ?? "") + ", " + (defaultAddress.address ?? "")
            self.restaurantDict?.currentLocationLocality = defaultAddress.flat_details ?? ""
            self.selectedAddressId = defaultAddress.addressid
            
        }
        else { // if not show user current location
            self.restaurantDict?.userCurrentLocation = userCurrentLocation!
            self.restaurantDict?.currentlocationAddress = currentlocationAddress ?? ""
            self.restaurantDict?.currentLocationLocality = currentLocationLocality ?? ""
        }
        handler(true)
    }
    
}
extension  Int{
    func getSafeAreaHeight() -> Int{
        let window = UIApplication.shared.keyWindow
        if #available(iOS 11.0, *) {
            return Int(window?.safeAreaInsets.bottom ?? 0)
        }
        return 0
    }
}

