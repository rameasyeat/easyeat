//
//  Homeswift
//  easyeat
//
//  Created by Ramniwas on 12/05/20.
//  Copyright © 2020 EasyEat. All rights reserved.
//

import Foundation
import ObjectMapper
import CoreLocation
import Lottie

class HomeViewModel{
    
    var restArray : [Nearby_restaurants]?
    var searchArray : [Nearby_restaurants]?
    var runingOrderView :RunningOrderView?
    var socketConnection : SocketConnection?
    var restaurantDict :RestaurantData?
    
    let maxDistance = 4000000000000000
    let minDistance = 4000000000000000 // testing
    //    let minDistance = 40000
    
    var distanceValue : Int = 0
    
    var userCurrentLocation : CLLocationCoordinate2D = CLLocationCoordinate2D(latitude: 0, longitude: 0)
    var currentlocationAddress : String?
    var currentLocationLocality : String?
    var animationView : AnimationView?
    var tabbarHeight = 49
    var isCartEmpty : Bool = true
    var spiner : UIActivityIndicatorView?
    
    var pageIndex : Int = 1
    var norders : Int = 10
    
    var totalPageCount : Int = 0
    
    
    var runingOrderConstraintHeight  = 0
    
    var isRuningOrder : Bool = false{
        didSet{
            if isRuningOrder == false{
                runingOrderConstraintHeight = 0
            }
            else {
                runingOrderConstraintHeight = 53
            }
            runingOrderViewHeight()
        }
    }
    
    
    // show cart bottom pop
    func addRuningOrderView(_ cartListData : CartListData,_ isLoadMQTT : Bool) -> RunningOrderView{
        if cartListData.cart?.cart_items?.count ?? 0 > 0 {
            isRuningOrder = true
            isCartEmpty = false
            runingOrderView?.itemCountLabel.text = "Go back to what tasty food’s menu"
            runingOrderView?.orderLabel.text = "Start Ordering"
            runingOrderView?.addRunningOrderFoodAnimation(runingOrderView!.foodImageView,"cook")
            
        }
            
        else if let orderID = cartListData.cart?.order?.order_id, orderID != ""{
            isRuningOrder = true
            isCartEmpty = true
            setRuningOrderData(cartListData.cart!)
            
            if isLoadMQTT == true {
                callMQTTserver(orderID)
            }
        }
        else {
            isRuningOrder = false
        }
        
        
        
        return runingOrderView!
    }
    
    fileprivate func runingOrderViewHeight(){
        if runingOrderView == nil{
            runingOrderView = RunningOrderView(frame: CGRect(x: 0, y: ScreenSize.screenHeight - (CGFloat(runingOrderConstraintHeight + 49
                + 0.getSafeAreaHeight())), width: ScreenSize.screenWidth, height: CGFloat(runingOrderConstraintHeight)))
            
        }else {
            runingOrderView?.frame =  CGRect(x: 0, y: ScreenSize.screenHeight - (CGFloat(runingOrderConstraintHeight + 49 + 0.getSafeAreaHeight())), width: ScreenSize.screenWidth, height: CGFloat(runingOrderConstraintHeight))
        }
    }
    
    // MQTT Server
    func callMQTTserver(_ orderId : String){
        
        if socketConnection != nil{
            socketConnection?.mqtt = nil
        }
        socketConnection = SocketConnection()
        socketConnection?.callMQTTSocket(orderId)

    }
    
    
    // Handle responce
    func handleOrderStatus(_ json : [String :Any],_ handller :@escaping (MType,OrderCancelBy)->Void){
        
        let mtype : String = json["mtype"] as? String ?? ""
        
        if mtype == "OID"{
            handller(MType.OID,OrderCancelBy.none)
        }
        else  if mtype == "OCM"{
            handller(MType.OCM,OrderCancelBy.none)
        }
        else  if mtype == "OCL"{
            
            let  data : [String : Any]  = (json["data"] as? [String :Any])!
            let cancelCode : Int = data["cancel_code"] as? Int ?? 0
            
            if(cancelCode == 2){ // cancel by MM
                handller(MType.OCL,OrderCancelBy.MM)
            }
            else  if(cancelCode == 1){// cancel by user
                handller(MType.OCL,OrderCancelBy.user)
            }
        }
        else
        {
            handller(MType.RELOAD,OrderCancelBy.none)
        }
    }
    
    
    
    func json(from object:Any) -> String? {
        guard let data = try? JSONSerialization.data(withJSONObject: object, options: []) else {
            return nil
        }
        return String(data: data, encoding: String.Encoding.utf8)
    }
    
    
    func getRestaurentDetails(_ apiEndPoint: String,_ param : [String : Any], handler: @escaping (RestaurantData,Int) -> Void) {
        
        guard let url = URL(string:Configuration().environment.baseURL + apiEndPoint) else {return}
        NetworkManager.shared.postRequest(url, false, "", params: param, networkHandler: {[weak self](responce,statusCode) in
            
            if let status: Int = responce[kstatus] as? Int, status == 1 {
                let dict  =  Mapper<RestaurantData>().map(JSON: responce["data"] as! [String : Any])
                self?.restaurantDict = dict!
            }
            else {
                // Alert(title: kError, message: responce[kmessage] as? String ?? "", vc: RootViewController.controller!)
            }
        })
    }
    
    
    
    
    func getRestaurantList(_ apiEndPoint: String,_ param : [String : Any],_ hude : Bool, handler: @escaping (Int) -> Void) {
        
        print(param)
        guard let url = URL(string: Configuration().environment.baseURL + apiEndPoint) else {return}
        NetworkManager.shared.postRequest(url, false, "", params: param, networkHandler: {[weak self](responce,statusCode) in
            
            
            if let status: Int = responce[kstatus] as? Int, status == 1 {
                
                let dict  =  Mapper<RestaurantModel>().map(JSON: responce["data"] as! [String : Any])
                
                self?.totalPageCount = dict?.total_pages ?? 0
                
                let array : [Nearby_restaurants] = (dict?.nearby_restaurants)!
                
                if self?.pageIndex == 1{
                    self?.restArray = array
                }
                else{
                    for dic in array{
                        if !(self?.restArray?.contains(where: { (restDict) -> Bool in
                            if restDict.id == dic.id{return true}else{
                                return false}
                        }))!{
                            self?.restArray?.append(dic)
                        }
                        else {
                            return
                        }
                    }
                }
                self?.searchArray = self?.restArray
                self?.pageIndex += 1
                handler(status )
            }
                
            else {
                if self?.pageIndex == 1 && self?.totalPageCount == 0 {
                    handler(0)
                }
            }
        })
    }
    
    fileprivate func setRuningOrderData(_ result : Cart){
        
        guard let type = restaurantDict?.type else {return}
        
        if result.order?.delivery_partner == "1"{
            runingOrderView?.partnerDeliveryOrderStatus(result.order)
        }
        else if Int(type) == RestaurentType.delivery.rawValue{
            runingOrderView?.orderStatusForDelivery(result)
        }
            
        else{
            runingOrderView?.orderStatusForDelivery(result)
        }
    }
}
