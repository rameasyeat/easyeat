//
//  IndexFilterCell.swift
//  easyeat
//
//  Created by Ramniwas on 04/04/20.
//  Copyright © 2020 EasyEat. All rights reserved.
//

import UIKit

class IndexFilterCell: ReusableTableViewCell {

    @IBOutlet weak var inxedCountLabel: CustomLabel!
    @IBOutlet weak var itemName: CustomLabel!
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    
    func commonInit(_ dict : SubCategoryMenuItemArray){
        itemName.text = dict.subcat_name?.capitalizingFirstLetter()
        inxedCountLabel.text = "\(dict.item_Array.count)"
        
    }
}
