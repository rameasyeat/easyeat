//
//  PartnerDeliveryCell.swift
//  easyeat
//
//  Created by Ramniwas on 19/05/20.
//  Copyright © 2020 EasyEat. All rights reserved.
//

import UIKit
import Lottie
import SDWebImage
import MapKit
import GoogleMaps

struct CourierDummy  {
var courier_id : Int?
var surname : String?
var name : String?
var middlename : String?
var phone : String?
var photo_url : String?
var latitude : Double?
var longitude : Double?
}

class PartnerDeliveryCell: ReusableTableViewCell,GMSMapViewDelegate {
    @IBOutlet weak var lottieView: LottieView!
    @IBOutlet weak var orderStatus: CustomLabel!
    @IBOutlet weak var descLabel: CustomLabel!
    @IBOutlet weak var driverViewHeight: NSLayoutConstraint!
    @IBOutlet weak var driverView: VIewShadow!
    @IBOutlet weak var driverImageView: UIImageView!
    @IBOutlet weak var driverName: CustomLabel!
    @IBOutlet weak var vehicleType: CustomLabel!
    @IBOutlet weak var callButton: CustomButton!
    @IBOutlet weak var gsmMapView: GMSMapView!
    
    var animationView = AnimationView()
    var animationViewHeight = 200
    var driverViewDefaultHeight = 67
    var dictCourier : Courier?
    var viewModel : OrderViewModel?
    var courierDummy : CourierDummy?
    
    var mapViewModel : MapViewModel = {
          let model = MapViewModel()
          return model
      }()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        lottieView.addSubview(animationView)
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    // MARK : Button Action
    @IBAction func callButtonAction(_ sender: Any) {
        callAction()
    }
    
    fileprivate func callAction(){
        
        if let url = URL(string: "tel://\(dictCourier?.phone ?? "")"), UIApplication.shared.canOpenURL(url) {
            if #available(iOS 10, *) {
                UIApplication.shared.open(url)
            } else {
                UIApplication.shared.openURL(url)
            }
        }
    }
    
    // MARK : Set Timer
    fileprivate func addlocationTimer(){
        viewModel?.courierLocationTimer = Timer.scheduledTimer(timeInterval: 15, target: self, selector: #selector(runTimedCode), userInfo: nil, repeats: true)
      }
      @objc func runTimedCode(){// set timer
        getDriverLocation(viewModel?.orderData?.delivery?.del_order?.order_id)
      }
    
    
    // MARK : Order Status
    func commonInit(_  orderData : OrderData?,_ orderCancel : OrderCancelBy){
        
        gsmMapView.isHidden = true
        lottieView.isHidden = false
        
        if let dict  = orderData?.delivery?.del_order?.courier{
            driverViewHeight.constant = CGFloat(driverViewDefaultHeight)
            driverView.isHidden = false
            dictCourier = dict
            driverImageView?.sd_setImage(with:  URL(string: dict.photo_url ?? ""), placeholderImage:UIImage(named: "header") , options: .progressiveDownload, completed: nil)
            driverName.text = dict.name
            
        }
        else {
            driverViewHeight.constant = 0
            driverView.isHidden = true
        }
        
        // Cancel order status
        if  orderCancel == OrderCancelBy.user{
            orderStatus.text = "Order cancelled"
            descLabel.text = "Your order is cancelled"
            addRunningOrderFoodAnimation("wfoc")
            return
            
        }
        else  if orderData?.bill?.paid == orderData?.bill?.bill_total && orderCancel == OrderCancelBy.MM{ // cancel by MM and amount paid
            
            orderStatus.text = "Order cancelled"
            descLabel.text = "Order was cancelled by the restaurant"
            addRunningOrderFoodAnimation("wfoc")
            return
        }
        else if  orderCancel == OrderCancelBy.MM{ // cancel by MM
            orderStatus.text = "Order cancelled"
            descLabel.text = "Order was cancelled by the restaurant"
            addRunningOrderFoodAnimation("wfoc")
            return
        }
        else{
            setOrderStatus(orderData)
        }
        
    }
   
    func setOrderStatus(_ orderData : OrderData?){
        if orderData?.order_status == OrderStatus.pending.rawValue{
            
            if Int(orderData?.bill?.balance ?? 0) <= 0{
                orderStatus.text = "Order Pending Confirmation"
                descLabel.text = "Restaurant is confirming your order"
                addRunningOrderFoodAnimation("wfoc")
            }
            else {
                orderStatus.text = "Waiting For Payment"
                descLabel.text = "Please pay to send your order to kitchen"
                addRunningOrderFoodAnimation("payment_waiting")
            }
        }
        else if orderData?.order_status == OrderStatus.confirmed.rawValue || orderData?.order_status == OrderStatus.inprogress.rawValue{
            
            if orderData?.delivery?.del_order?.status == "new" || orderData?.delivery?.del_order?.status == "available"{
                orderStatus.text = "Delicious Food Is Being Prepared"
                descLabel.text = "Restaurant is preparing your food"
                addRunningOrderFoodAnimation("fp")
            }
            else if orderData?.delivery?.del_order?.status == "active" {
                
                if orderData?.delivery?.del_delivery?.status == "planned" {
                    orderStatus.text = "Delicious Food Is Being Prepared"
                    descLabel.text = "Restaurant is preparing your food"
                    addRunningOrderFoodAnimation("fp")
                }
                else if orderData?.delivery?.del_delivery?.status == "courier_assigned" {
                    orderStatus.text = "Driver Has Been Assigned"
                    descLabel.text = "Restaurant is preparing your food"
                    addRunningOrderFoodAnimation("driver_assigned")
                }
                else if orderData?.delivery?.del_delivery?.status == "courier_departed" {
                    orderStatus.text = "Driver Is On The Way To Restaurant"
                    descLabel.text = "Restaurant is preparing your food"
                    addRunningOrderFoodAnimation("dowr")
                }
                    
                else if orderData?.delivery?.del_delivery?.status == "parcel_picked_up" {
                    
                    // MARK : Courier Delivery traking
                 //   getDriverLocation(orderData?.delivery?.del_order?.order_id) //
                    
                    orderStatus.text = "Driver Is On His Way To Drop Your Order"
                    descLabel.text = "Delicious food is on the way"
                    addRunningOrderFoodAnimation("driver_going_for_delivery")
                }
                else if orderData?.delivery?.del_delivery?.status == "courier_arrived" {
                    orderStatus.text = "Your Order Has Been Arrived"
                    descLabel.text = "Driver is waiting at your doorstep"
                    addRunningOrderFoodAnimation("driver_waiting_at_home")
                }
                else if orderData?.delivery?.del_delivery?.status == "active" {
                    orderStatus.text = "Driver Is On His Way To Drop Your Order"
                    descLabel.text = "Delicious food is on the way"
                    addRunningOrderFoodAnimation("driver_going_for_delivery")
                }
                else if orderData?.delivery?.del_delivery?.status == "finished" {
                    orderStatus.text = "Your Order Has Been Delivered"
                    descLabel.text = "Enjoy your meal"
                    addRunningOrderFoodAnimation("wfoc")
                }
                else if orderData?.delivery?.del_delivery?.status == "canceled" {
                    
                }
                else if orderData?.delivery?.del_delivery?.status == "failed" {
                }
                else {
                    orderStatus.text = "Delicious Food Is Being Prepared"
                    descLabel.text = "Restaurant is preparing your food"
                    addRunningOrderFoodAnimation("fp")
                }
            }
            else if orderData?.delivery?.del_order?.status == "completed"{
                orderStatus.text = "Your Order Has Been Delivered"
                descLabel.text = "Enjoy your meal"
                addRunningOrderFoodAnimation("wfoc")
            }
        }
    }
    
    // Food Animation
       func addRunningOrderFoodAnimation(_ type : String){
           
           DispatchQueue.main.async {
               self.animationView.frame = CGRect(x:0, y:0, width: CGFloat(self.animationViewHeight), height: CGFloat(self.animationViewHeight))
               self.animationView.animation = Animation.named(type)
               self.animationView.backgroundColor = .clear
               self.animationView.tag = 2022
               self.animationView.loopMode = .loop
               self.animationView.play()
               
           }
       }
    
    
    // MARK : Driver APIs call for traking
    fileprivate func getDriverLocation(_ orderId : Int?){
        var dict : [String : Any] = [String : Any]()
        dict[kdel_order_id] = "\(orderId ?? 0)"
        viewModel?.getDriverLocation(APIsEndPoints.kgetDriverLocation.rawValue,dict, handler: {[weak self](statusCode)in
            self?.updateUI()
        })
    }
    
    func updateUI(){
//        gsmMapView.isHidden = true
//        lottieView.isHidden = false
        
        gsmMapView.isHidden = false
        lottieView.isHidden = true
        
        addlocationTimer() // timer
        courierDummy = CourierDummy(courier_id: 10, surname: "Gulati", name: "Dev", middlename: "", phone: "9968736373", photo_url: "", latitude:28.4403, longitude: 77.0009)
        
        setMapSetting()
        getRouteDirection()

    }

    // MARK : Map Setting
    fileprivate func setMapSetting(){
        let camera = GMSCameraPosition.camera(withLatitude: viewModel?.restaurantDict?.location?.lat ?? 0, longitude: viewModel?.restaurantDict?.location?.lon ?? 0, zoom: 12.0)
        
        self.gsmMapView.camera = camera
        self.gsmMapView.delegate = self
        self.gsmMapView?.isMyLocationEnabled = true
        self.gsmMapView.settings.myLocationButton = true
        self.gsmMapView.settings.compassButton = true
        self.gsmMapView.settings.zoomGestures = true
    }
    
    
    fileprivate func drawPath(){
        // print route using Polyline
        
        
        createMarker(titleMarker: "", iconMarker: #imageLiteral(resourceName: "marker") , latitude: viewModel?.restaurantDict?.location?.lat ?? 0, longitude: viewModel?.restaurantDict?.location?.lon ?? 0) // rest pin
        
        createMarker(titleMarker: "", iconMarker: #imageLiteral(resourceName: "marker") ,  latitude: 28.4603, longitude: 77.0000) // user location pin
        
        createMarker(titleMarker: "", iconMarker: #imageLiteral(resourceName: "scooter") ,  latitude: courierDummy?.latitude ?? 0, longitude: courierDummy?.longitude ?? 0) // driver pin

        
        for route in self.mapViewModel.routesArray!
        {
            let routeOverviewPolyline = route["overview_polyline"] as? [String : Any]
            let points = routeOverviewPolyline?["points"] as? String
            let path = GMSPath.init(fromEncodedPath: points!)
            let polyline = GMSPolyline.init(path: path)
            polyline.strokeWidth = 4
            polyline.strokeColor = UIColor.black
            polyline.map = self.gsmMapView
        }
}
    
    
    // MARK: function for create a marker pin on map
    func createMarker(titleMarker: String, iconMarker: UIImage, latitude: CLLocationDegrees, longitude: CLLocationDegrees) {
        let marker = GMSMarker()
        marker.position = CLLocationCoordinate2DMake(latitude, longitude)
        marker.title = titleMarker
        marker.icon = iconMarker
        marker.map = gsmMapView
    }
    
    // MARK : APIS call
    
    func getRouteDirection()
    {
        let origin = "\(viewModel?.restaurantDict?.location?.lat ?? 0),\(viewModel?.restaurantDict?.location?.lon ?? 0)" // user delivery location
        
        let destination = "\(courierDummy?.latitude ?? 0),\(courierDummy?.longitude ?? 0)" // driver location
        
        let url = "https://maps.googleapis.com/maps/api/directions/json?" +
            "origin=\(origin)&destination=\(destination)&" +
        "key=\(Configuration().environment.googleApisKey)"
        
        mapViewModel.getDirection(url, handler: {[weak self](status) in
            self?.drawPath()
        })
        
    }
    
}

