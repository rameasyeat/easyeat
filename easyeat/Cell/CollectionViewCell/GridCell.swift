

import UIKit

class GridCell: ReusableCollectionViewCell {
    @IBOutlet weak var nameLabel: UILabel!
    
    @IBOutlet weak var viewBG: VIewShadow!
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    func commonInit(_ dict : Filters){
        if dict.filtertype == "price"{
            nameLabel.text = "< \(CurrentUserInfo.currency ?? "") \(dict.name ?? "")"
        }
        else{
            nameLabel.text = dict.name
        }
    }
}

