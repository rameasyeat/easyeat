//
//  CalorieCollectionCell.swift
//  easyeat
//
//  Created by Ramniwas on 21/04/20.
//  Copyright © 2020 EasyEat. All rights reserved.
//

import UIKit
import LMGraphView


class CalorieCollectionCell: ReusableCollectionViewCell {
    var graphView1 : LMLineGraphView?
    var viewModel : ProfileViewModel?
    @IBOutlet weak var graphView: UIView!
    @IBOutlet weak var noRecord: CustomLabel!
    @IBOutlet weak var notifyView: NotifymeView!
    

    @IBOutlet weak var collectionView: CaloriesTypeCollectionView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func commonInit(index : Int){
        collectionView.commoInit()

        if index == 0{
            let filterArray = (viewModel?.caloriesTypeArray.filter{$0.type == "cal"})!
            collectionView.getRolesData(filterArray)
        }
        else{
            let filterArray = (viewModel?.caloriesTypeArray.filter{$0.type == "fat"})!
            collectionView.getRolesData(filterArray)
        }
        
    
    
          noRecord.isHidden = true
          
          if graphView1 != nil {
              graphView1?.removeFromSuperview()
          }
          
          graphView1  = LMLineGraphView()
         graphView1?.frame = graphView.frame
          
          graphView1?.yAxisUnit = ""
          graphView1?.title = ""
          graphView1?.layout.xAxisScrollableOnly = false
          graphView1?.layout.xAxisLabelFont = UIFont.systemFont(ofSize: 8)
          graphView1?.layout.xAxisMargin = 0
          graphView1?.layout.titleLabelHeight = 0
          let earning = plotGraph("002581", fillColor: "032581", .earning, index)
          
//          if index == 1{
//            collectionView.caloriesArray = (viewModel.defaultDataArray?.filter{$0.type == "loyality"})!
//              savingTypeLabel.text = "Earnings through Loyalty"
//
//              if viewModel?.validateLoyality() == true{
//                  noRecord.isHidden = false
//                  graphView1?.isHidden = true
//              }else {
//                  graphView1?.isHidden = false
//              }
//
//              let array = viewModel?.convertLoyalityDataIntoWeekArray(.axisData)
//              if array?.count ?? 0 > 1{
//                  graphView1?.xAxisValues = array
//                  let saving = plotGraph("2D99FC", fillColor: "C8E1FA", .saving, index)
//                  graphView1?.graphPlots = [saving,earning]
//              }
//          }
//          else {
              
              
              
              if viewModel?.validateSavingData() == true{
                  noRecord.isHidden = false
                  graphView1?.isHidden = true
              }else {
                  graphView1?.isHidden = false
              }
              
              let array = viewModel?.convertDataIntoWeekArray(.axisData)
              if array?.count ?? 0 > 1{
                  graphView1?.xAxisValues = array
                   let saving = plotGraph("2CD9C5", fillColor: "CBF2EE", .saving, index)
                  graphView1?.graphPlots = [saving,earning]
              }
         // }
        graphView.addSubview(graphView1!)
      }
      
      
      func plotGraph(_ stroke : String, fillColor : String, _ type : UserSavingOREarningType, _ index :Int) -> LMGraphPlot{
          let ploat : LMGraphPlot = LMGraphPlot()
          ploat.strokeColor = hexStringToUIColor(stroke)
          ploat.graphPointSize = 0
          
          if type == .saving{
              ploat.fillColor = hexStringToUIColor(fillColor)
          }
          else {
              ploat.fillColor = .clear
          }
          
          viewModel?.graphMaxValue = 0
//          let  array =  (index == 0) ? viewModel?.convertDataIntoWeekArray(type) : viewModel?.convertLoyalityDataIntoWeekArray(type)
        
        let  array =  viewModel?.convertDataIntoWeekArray(type)
          ploat.graphPoints = viewModel?.getGraphPoints(array!)
          
          print(viewModel?.graphMaxValue as Any)
          graphView1?.yAxisMaxValue = CGFloat(CFloat(viewModel!.graphMaxValue)) + 10
          
          return ploat
          
      }

}
