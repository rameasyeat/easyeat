//
//  QuickServiceCell.swift
//  easyeat
//
//  Created by Ramniwas on 16/04/20.
//  Copyright © 2020 EasyEat. All rights reserved.
//

import UIKit

class QuickServiceCell: ReusableCollectionViewCell {
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var serviceNameLabel: CustomLabel!
    @IBOutlet weak var bgView: UIView!


    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    
        layer.masksToBounds = false
    }
    
    func commonInit(_ dict : Services){
        
        imgView?.sd_setImage(with:  URL(string: "\(KImagesBaseUrl)\(kQuickserviceFolderName)\(dict.icon ?? "")"), placeholderImage:UIImage(named: "header") , options: .progressiveDownload, completed: nil)
        serviceNameLabel.text = dict.category_name ?? ""
    }

}
