//
//  AllergiesCell.swift
//  easyeat
//
//  Created by Ramniwas on 25/03/20.
//  Copyright © 2020 EasyEat. All rights reserved.
//

import UIKit
import SDWebImage

class AllergiesCell: ReusableTableViewCell {

    @IBOutlet weak var categoryImgView: UIImageView!
    @IBOutlet weak var categoryName: CustomLabel!
    @IBOutlet weak var selectedImageView: UIImageView!
    @IBOutlet weak var imgViewLeadingmargin: NSLayoutConstraint!
    @IBOutlet weak var catImageViewWidth: NSLayoutConstraint!
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    // catagory
    func commiInit(_ dictionary : Categories){
        categoryName.text = dictionary.cat_name?.capitalized
        if dictionary.isSelected == true{
            selectedImageView.image = #imageLiteral(resourceName: "RadioSelected")
        }else {
            selectedImageView.image = #imageLiteral(resourceName: "Radio")
        }
    }
    
    // subcategory
    
    func subCatCommiInit(_ dictionary : Subcat){
        categoryName.text = dictionary.subcat_name?.capitalized
           if dictionary.isSelected == true{
               selectedImageView.image = #imageLiteral(resourceName: "RadioSelected")
           }else {
               selectedImageView.image = #imageLiteral(resourceName: "Radio")
           }
       }
    
    // Netbanking Payment mode
    func paymentMode(_ dictionary : PaymentMode){
        categoryName.text = dictionary.name
         categoryImgView?.sd_setImage(with:  URL(string: dictionary.icon ?? ""), placeholderImage:UIImage(named: "") , options: .progressiveDownload, completed: nil)
        catImageViewWidth.constant = 26
        imgViewLeadingmargin.constant = 20
        
        if dictionary.isSelected == true{
            selectedImageView.image = #imageLiteral(resourceName: "RadioSelected")
        }else {
            selectedImageView.image = #imageLiteral(resourceName: "Radio")
        }
    }
    
    
    /// Variation
    
    func variationInit(_ dictionary : Variations){
        categoryName.text = dictionary.name
           if dictionary.isSelected == true{
               selectedImageView.image = #imageLiteral(resourceName: "RadioSelected")
           }else {
               selectedImageView.image = #imageLiteral(resourceName: "Radio")
           }
       }
}
