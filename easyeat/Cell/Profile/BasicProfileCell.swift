//
//  BasicProfileCell.swift
//  easyeat
//
//  Created by Ramniwas on 22/04/20.
//  Copyright © 2020 EasyEat. All rights reserved.
//

import UIKit

class BasicProfileCell: ReusableTableViewCell {
    
    @IBOutlet weak var headerLabel: CustomLabel!
    @IBOutlet weak var downArrowImage: UIImageView!
    @IBOutlet weak var txtField: CustomTextField!
    @IBOutlet weak var bgView: VIewShadow!
    
    var changetxtWidth = 42
    
    @IBOutlet weak var changeTextWidth: NSLayoutConstraint!
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
    }
    
    func commiInit<T>(_ dictionary :T,_ isEdit : Bool){
        
        if let dict = dictionary as? SigninInfoModel{
            txtField.text = dict.value
            txtField.placeholder = dict.placeholder
            headerLabel.text = dict.header
        }
        else if let dict = dictionary as? BasicProfileModel{
            
            txtField.text = dict.value
            txtField.placeholder = dict.placeholder
            headerLabel.text = dict.header
            txtField.returnKeyType = .next
            txtField.isUserInteractionEnabled = true
            downArrowImage.isHidden = true
            changeTextWidth.constant = 0

            switch dict.type {
                
            case BasicFiledType.name :
                txtField.keyboardType = .default
                txtField.text = dict.value.capitalized
                hideArrow(isEdit)
                
            case BasicFiledType.dob:
                txtField.keyboardType = .default
                txtField.isUserInteractionEnabled = false
                hideChabgeText(isEdit)
                
            case BasicFiledType.phonenumber:
                txtField.keyboardType = .phonePad
                txtField.isUserInteractionEnabled = false

                
            case BasicFiledType.email:
                txtField.keyboardType = .emailAddress
                 hideArrow(isEdit)
                
            case BasicFiledType.gender:
                txtField.returnKeyType = .done
                txtField.isUserInteractionEnabled = false
                hideChabgeText(isEdit)

            }
            
            if isEdit == false{
                txtField.isUserInteractionEnabled = false
            }
            
        }
        
        
    }
    
    
    func hideArrow(_ isEdit : Bool){
        if isEdit == true{
         changeTextWidth.constant = CGFloat(changetxtWidth)
        downArrowImage.isHidden = true
            bgView.backgroundColor = hexStringToUIColor(kwhite)

        }
        else {
            bgView.backgroundColor = hexStringToUIColor("EDEDED")
        }
    }
    
    func hideChabgeText(_ isEdit : Bool){
        if isEdit == true{
         changeTextWidth.constant = 0
        downArrowImage.isHidden = false
        bgView.backgroundColor = hexStringToUIColor(kwhite)

        }
        else{
            bgView.backgroundColor = hexStringToUIColor("EDEDED")

        }
    }
}
