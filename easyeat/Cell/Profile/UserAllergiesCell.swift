//
//  UserAllergiesCell.swift
//  easyeat
//
//  Created by Ramniwas on 22/04/20.
//  Copyright © 2020 EasyEat. All rights reserved.
//

import UIKit

class UserAllergiesCell: ReusableTableViewCell {
    @IBOutlet weak var deleteButtonWidth: NSLayoutConstraint!
    @IBOutlet weak var allergiesNameLabel: CustomLabel!
    @IBOutlet weak var headerNameLabel: CustomLabel!
    @IBOutlet weak var deleteButton: CustomButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func commiInit(_ dict :Categories, index : Int){
            headerNameLabel.text = "ALLERGY \(index + 1)"
        
        let catFilter = dict.subcat?.map{$0.subcat_name} as? [String]
        if catFilter?.count ?? 0 > 0 {
            allergiesNameLabel.text = (dict.cat_name ?? "").firstCapitalized + "- \(catFilter?.joined(separator: " ") ?? "")".capitalized
        }
        else {
            allergiesNameLabel.text = (dict.cat_name ?? "").capitalized
        }
    }
}
    
