//
//  ProfileCell.swift
//  easyeat
//
//  Created by Ramniwas on 21/04/20.
//  Copyright © 2020 EasyEat. All rights reserved.
//

import UIKit

class ProfileCell: ReusableTableViewCell {
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var pageControl: UIPageControl!
    var viewModel : ProfileViewModel?
    @IBOutlet weak var profileCellBgView: VIewShadow!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        CalorieCollectionCell.registerWithCollectionView(collectionView)
        collectionView.delegate = self
        collectionView.dataSource = self
        pageControl.numberOfPages = 2
        
        profileCellBgView.layer.applySketchShadow(color: hexStringToUIColor("#0000002E"), alpha: 0.4, x: 0, y: 0, blur: 6, spread: 0)

    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func commonInit(){
        collectionView.reloadData()
    }
}

    extension ProfileCell : UICollectionViewDataSource, UICollectionViewDelegate,UICollectionViewDelegateFlowLayout
    {
        func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
            return 1
        }
        
        func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
            return collectionView.frame.size
        }
        
        func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CalorieCollectionCell.reuseIdentifier, for: indexPath)as! CalorieCollectionCell
            
            cell.viewModel = viewModel
            cell.commonInit(index: indexPath.row)
            
            
            return cell
        }
        func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        }
        
        func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
            for cell in collectionView.visibleCells {
                let indexPath = collectionView.indexPath(for: cell)
                pageControl.currentPage = indexPath?.row ?? 0

            }
        }
    }
