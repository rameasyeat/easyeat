//
//  CountryCodeCell.swift
//  easyeat
//
//  Created by Ramniwas on 18/03/20.
//  Copyright © 2020 EasyEat. All rights reserved.
//

import UIKit

class CountryCodeCell: ReusableTableViewCell {
    
    @IBOutlet weak var mobileTextFiled : CustomTextField!
    @IBOutlet weak var countryCodeLabel: CustomLabel!
    
    @IBOutlet weak var hintImageView: UIImageView!
    @IBOutlet weak var hintImageWidth: NSLayoutConstraint!
    @IBOutlet weak var viewPhoneNumber: VIewShadow!
    @IBOutlet weak var viewCountryCode: VIewShadow!
    @IBOutlet weak var countryCodeButton: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func commonInit(_ dict : SigninInfoModel){
        mobileTextFiled.text = dict.value
        mobileTextFiled.placeholder = dict.placeholder
        countryCodeLabel.text = dict.countryCode
    }
    
    // basic info
    
    func commonBasicProfileInit(_ dict : BasicProfileModel){
          mobileTextFiled.text = dict.value
          mobileTextFiled.placeholder = dict.placeholder
          countryCodeLabel.text = dict.countryCode
        viewCountryCode.backgroundColor = hexStringToUIColor("EDEDED")
        viewPhoneNumber.backgroundColor = hexStringToUIColor("EDEDED")


      }

    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}
