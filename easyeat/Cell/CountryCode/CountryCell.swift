//
//  CountryCell.swift
//  easyeat
//
//  Created by Ramniwas on 28/03/20.
//  Copyright © 2020 EasyEat. All rights reserved.
//

import UIKit

class CountryCell: ReusableTableViewCell {
    @IBOutlet weak var flagImageView: UIImageView!
    @IBOutlet weak var countryName: UILabel!
    @IBOutlet weak var countryCode: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func  commiInit(_ dict : Countries){
        flagImageView?.sd_setImage(with:  URL(string: dict.icon_s3 ?? ""), placeholderImage:UIImage(named: "") , options: .progressiveDownload, completed: nil)
        countryName.text = dict.name
        countryCode.text = dict.dial_code
    }
}
