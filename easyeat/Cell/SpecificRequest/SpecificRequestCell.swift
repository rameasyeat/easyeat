//
//  SpecificRequestCell.swift
//  easyeat
//
//  Created by Ramniwas on 02/04/20.
//  Copyright © 2020 EasyEat. All rights reserved.
//

import UIKit

protocol SpecificRequestDelegate {
    func updateCellData(_ isUpdateCell : Bool)
}
class SpecificRequestCell: ReusableTableViewCell {
    
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var removeButton: UIButton!
    @IBOutlet weak var greenDotImage: UIImageView!
    @IBOutlet weak var removeButtonWidth: NSLayoutConstraint!
    var specificRequestDelegate : SpecificRequestDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
    }
    
    // Mark SetCell Data
    
    func commonInit(_ dict : SpecificRequest){
        
        if dict.isSpecifiRequest {
            removeButton.setAttributedTitle(NSAttributedString(string: "Remove"), for: .normal)
              removeButtonWidth.constant = 63
            removeButton.tintColor = hexStringToUIColor(korange)
            greenDotImage.image =   #imageLiteral(resourceName: "checkmark")
            descriptionLabel.attributedText = NSAttributedString(string: dict.description)
            descriptionLabel.isUserInteractionEnabled = false
            
        }
        else {
            removeButtonWidth.constant = 0
            removeButton.setAttributedTitle(NSAttributedString(string: ""), for: .normal)
            greenDotImage.image = nil
            descriptionLabel.isUserInteractionEnabled = true

            Attributed.setText(descriptionLabel, kSpecifiRequest, korange, [kEnterhere],descriptionLabel.font)
            descriptionLabel.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(requestAction(gesture:))))
            
        }
    }
    
    // MARK Button Action
    
    @objc func requestAction(gesture: UITapGestureRecognizer) {
        if let range = kSpecifiRequest.range(of: "instructions? Enter here"),
            gesture.didTapAttributedTextInLabel(label: descriptionLabel, inRange: NSRange(range, in: kSpecifiRequest)) {
            specificRequestDelegate?.updateCellData(true)
        }
    }
    
    
    @IBAction func removeButtonAction(_ sender: Any) {
        specificRequestDelegate?.updateCellData(false)
    }
}
