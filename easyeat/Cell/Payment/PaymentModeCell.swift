//
//  PaymentModeCell.swift
//  easyeat
//
//  Created by Ramniwas on 05/05/20.
//  Copyright © 2020 EasyEat. All rights reserved.
//

import UIKit

class PaymentModeCell: ReusableTableViewCell {

    @IBOutlet weak var iconImageView: UIImageView!
    @IBOutlet weak var txtLabel: CustomLabel!
    @IBOutlet weak var arrowImage: NSLayoutConstraint!
    @IBOutlet weak var arrowTrailingConstraint: NSLayoutConstraint!
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func commiInit(_ dict : PaymentMode){
        iconImageView?.sd_setImage(with:  URL(string: dict.icon ?? ""), placeholderImage:UIImage(named: "") , options: .progressiveDownload, completed: nil)
        txtLabel?.text = dict.name
    }
    
    func netBankingInit(_ dict : PaymenModeType){
        iconImageView.image = dict.img
        txtLabel?.text = dict.value
    }
    
}
