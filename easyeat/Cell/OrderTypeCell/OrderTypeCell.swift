//
//  OrderTypeCell.swift
//  easyeat
//
//  Created by Ramniwas on 12/06/20.
//  Copyright © 2020 EasyEat. All rights reserved.
//

import UIKit

class OrderTypeCell: ReusableTableViewCell {
    
    @IBOutlet weak var imgView: CustomImageView!
    @IBOutlet weak var typeLabel: UILabel!
    @IBOutlet weak var lineView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func commiInit(dict : OrderTypeSelection,_ index : Int){
        typeLabel.text = dict.typle
        imgView.image = dict.img
        
        if index == 2{
            lineView.isHidden = true
        }
    }
    
}
