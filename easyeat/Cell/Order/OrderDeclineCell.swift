//
//  OrderDeclineCell.swift
//  easyeat
//
//  Created by Ramniwas on 07/04/20.
//  Copyright © 2020 EasyEat. All rights reserved.
//

import UIKit
import SDWebImage

class OrderDeclineCell: ReusableTableViewCell {
    @IBOutlet weak var itemImageView: CustomImageView!
    @IBOutlet weak var itemNameLabel: CustomLabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func commonInit(_ dict : OrderItems){
        
        itemImageView?.sd_setImage(with:  URL(string: dict.image ?? ""), placeholderImage:UIImage(named: "header") , options: .progressiveDownload, completed: nil)
        itemNameLabel.text = "\(dict.item_quantity ?? 0) X \(dict.item_name ?? "")"
    }
    
}
