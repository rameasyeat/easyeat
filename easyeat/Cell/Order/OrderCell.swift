//
//  OrderCell.swift
//  easyeat
//
//  Created by Ramniwas on 07/04/20.
//  Copyright © 2020 EasyEat. All rights reserved.
//

import UIKit



enum ItemStatus : Int {
    case waiting = 0
    case confirmed
    case preparing
    case prepared
    case served
    case declinedNeedAction
    case declinedNoFurtherAction
}

class OrderCell: ReusableTableViewCell {
    
    @IBOutlet weak var statusLabel: CustomLabel!
    @IBOutlet weak var itemImageView: CustomImageView!
    @IBOutlet weak var itemCountLabel: CustomLabel!
    @IBOutlet weak var itemNameLabel: CustomLabel!
    @IBOutlet weak var viewStatus: VIewShadow!
    @IBOutlet weak var descriptionLableHeight: NSLayoutConstraint!
    @IBOutlet weak var descriptionLabel: CustomLabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
    }
    
    func commonInit(_ dict : OrderItems,_ orderCancel : OrderCancelBy,_ orderData : OrderData,_ isShowViewDetail :Bool?,_ isPartnerDelivery : Int?){
        
        
        itemImageView?.sd_setImage(with:  URL(string: dict.image ?? ""), placeholderImage:UIImage(named: "food") , options: .progressiveDownload, completed: nil)
        itemNameLabel.text = dict.item_name?.capitalized
        itemCountLabel.text = "X\(dict.item_quantity ?? 0)"
        
        if orderCancel == OrderCancelBy.MM || orderCancel == OrderCancelBy.user{
            viewStatus.borderColor = hexStringToUIColor(kred)
            statusLabel.textColor = hexStringToUIColor(kred)
            statusLabel.text = "Cancelled"
        }
        else{
            if orderData.order_type == OrderType.delivery.rawValue {
                deliveryOrderStatus(dict.item_status ?? 0)
            }
            else if orderData.order_type == OrderType.pickup.rawValue{
                orderStatusView(dict.item_status ?? 0)
            }
            else{
                orderStatusView(dict.item_status ?? 0)
            }
        }
        
        if isShowViewDetail == false && isPartnerDelivery == 1 {
            self.backgroundColor = hexStringToUIColor(kMediumLightGray)
        }else {
            self.backgroundColor = hexStringToUIColor(kwhite)
        }
        
        // check variation contains
        let variationName = dict.variation_name ?? ""
        var addons = dict.addons_name ?? ""
        
        addons  = (addons == "") ? "" : ("," + addons.capitalized)
        let desc = variationName.removeWhiteSpace().capitalized + addons
        
        if desc != "," {
            descriptionLabel.text = desc
        }
        else{
            let heightConstraint = NSLayoutConstraint(item: itemNameLabel!, attribute: .height, relatedBy: .equal, toItem:nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 43)
            itemNameLabel.addConstraint(heightConstraint)
        }
    }
    
    func deliveryOrderStatus(_ itemOrderStatus : Int) {
        
        switch itemOrderStatus {
        case ItemStatus.waiting.rawValue:
            viewStatus.borderColor = hexStringToUIColor(korange)
            statusLabel.textColor = hexStringToUIColor(korange)
            statusLabel.text = "Waiting"
            
        case ItemStatus.confirmed.rawValue:
            viewStatus.borderColor = hexStringToUIColor(kgray)
            statusLabel.textColor = hexStringToUIColor(kgreen)
            statusLabel.text = "Confirmed"
            
        case ItemStatus.preparing.rawValue:
            viewStatus.borderColor = hexStringToUIColor(kgreen)
            statusLabel.textColor = hexStringToUIColor(kgreen)
            statusLabel.text = "Preparing"
            
            
        case ItemStatus.prepared.rawValue:
            viewStatus.borderColor = hexStringToUIColor(kgreen)
            statusLabel.textColor = hexStringToUIColor(kgreen)
            statusLabel.text = "Dispatched"
            
        case ItemStatus.served.rawValue:
            viewStatus.backgroundColor = hexStringToUIColor(kgreen)
            viewStatus.borderColor = hexStringToUIColor(kgreen)
            statusLabel.textColor = hexStringToUIColor(kwhite)
            statusLabel.borderColor = hexStringToUIColor(kgreen)
            statusLabel.text = "Served"
            
        case ItemStatus.declinedNeedAction.rawValue:
            viewStatus.borderColor = hexStringToUIColor(kred)
            statusLabel.textColor = hexStringToUIColor(kred)
            statusLabel.text = "Declined"
            
        case ItemStatus.declinedNoFurtherAction.rawValue:
            viewStatus.borderColor = hexStringToUIColor(kred)
            statusLabel.textColor = hexStringToUIColor(kred)
            statusLabel.text = "Declined"
            
        default:
            break
        }
        
    }
    func orderStatusView(_ itemOrderStatus : Int) {
        
        switch itemOrderStatus {
        case ItemStatus.waiting.rawValue:
            viewStatus.borderColor = hexStringToUIColor(korange)
            statusLabel.textColor = hexStringToUIColor(korange)
            statusLabel.text = "Waiting"
            
        case ItemStatus.confirmed.rawValue:
            viewStatus.borderColor = hexStringToUIColor(kgray)
            statusLabel.textColor = hexStringToUIColor(kgreen)
            statusLabel.text = "Confirmed"
            
        case ItemStatus.preparing.rawValue:
            viewStatus.borderColor = hexStringToUIColor(kgreen)
            statusLabel.textColor = hexStringToUIColor(kgreen)
            statusLabel.text = "Preparing"
            
            
        case ItemStatus.prepared.rawValue:
            viewStatus.borderColor = hexStringToUIColor(kgreen)
            statusLabel.textColor = hexStringToUIColor(kgreen)
            statusLabel.text = "Prepared"
            
        case ItemStatus.served.rawValue:
            viewStatus.backgroundColor = hexStringToUIColor(kgreen)
            viewStatus.borderColor = hexStringToUIColor(kgreen)
            statusLabel.textColor = hexStringToUIColor(kwhite)
            statusLabel.borderColor = hexStringToUIColor(kgreen)
            statusLabel.text = "Served"
            
        case ItemStatus.declinedNeedAction.rawValue:
            viewStatus.borderColor = hexStringToUIColor(kred)
            statusLabel.textColor = hexStringToUIColor(kred)
            statusLabel.text = "Declined"
            
        case ItemStatus.declinedNoFurtherAction.rawValue:
            viewStatus.borderColor = hexStringToUIColor(kred)
            statusLabel.textColor = hexStringToUIColor(kred)
            statusLabel.text = "Declined"
            
        default:
            break
        }
        
    }
}
