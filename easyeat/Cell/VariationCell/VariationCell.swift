//
//  VariationCell.swift
//  easyeat
//
//  Created by Ramniwas Patidar on 14/07/20.
//  Copyright © 2020 EasyEat. All rights reserved.
//

import UIKit

class VariationCell: ReusableTableViewCell {

    @IBOutlet weak var imgView: CustomImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var priceWidth: NSLayoutConstraint!
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func commiInit(_ dictionary : Variations,_ isContainVariation : Bool,_ isContainAddons : Bool){
        nameLabel.text = dictionary.name?.capitalized
        if dictionary.isSelected == true{
            imgView.image = #imageLiteral(resourceName: "RadioSelected")
        }else {
            imgView.image = #imageLiteral(resourceName: "Radio")
        }
        
        if isContainVariation == false && isContainAddons == false{
            
            let roundPrice = String(format: "%.2f",Double(dictionary.price ?? 0))
            priceLabel.text = CurrentUserInfo.currency  + " " + roundPrice
        }
        else{
            priceWidth.constant = 0
        }
    }
    
    func addonsCommiInit(_ dictionary : Addons){
        nameLabel.text = dictionary.name?.capitalized
           if dictionary.isSelected == true{
               imgView.image = #imageLiteral(resourceName: "RadioSelected")
           }else {
               imgView.image = #imageLiteral(resourceName: "Radio")
           }
            priceLabel.text = CurrentUserInfo.currency + " \(dictionary.price ?? 0)"
       }

}
