//
//  ReviewTypeCell.swift
//  easyeat
//
//  Created by Ramniwas on 07/05/20.
//  Copyright © 2020 EasyEat. All rights reserved.
//

import UIKit

class ReviewTypeCell: ReusableCollectionViewCell {

    @IBOutlet weak var reviewTypeLabel: CustomLabel!
    @IBOutlet weak var checkMarkImage: CustomImageView!
    @IBOutlet weak var bgView: VIewShadow!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func commonInit(_ dict : Feedback_options){
        reviewTypeLabel.text = dict.text
        
        if dict.isSelected == true{
            bgView.borderColor = hexStringToUIColor(korange)
            checkMarkImage.image = #imageLiteral(resourceName: "selectedTikMark")
        }
        else {
            bgView.borderColor = hexStringToUIColor(kLightGray)
            checkMarkImage.image = #imageLiteral(resourceName: "tikMark")

        }
    }
}
