//
//  RatingCell.swift
//  easyeat
//
//  Created by Ramniwas on 07/05/20.
//  Copyright © 2020 EasyEat. All rights reserved.
//

import UIKit
import Cosmos
import SDWebImage


protocol ItemRatingDelegate {
    func getRating(_ rating : Double)
}
class RatingCell: ReusableTableViewCell{

    @IBOutlet weak var itemNameLabel: CustomLabel!
    @IBOutlet weak var itemImgView: CustomImageView!
    @IBOutlet weak var ratingView: CosmosView!
    var itemRatingDelegate :ItemRatingDelegate?
   

    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
        
    func commoInit(_ dict : OrderItems){
             itemImgView?.sd_setImage(with:  URL(string: dict.image ?? ""), placeholderImage:UIImage(named: "header") , options: .progressiveDownload, completed: nil)
             itemNameLabel.text = dict.item_name
        ratingView.rating = round(Double(dict.rating ?? 0))
        
        ratingView.didTouchCosmos = { (rating) in
            self.itemRatingDelegate?.getRating(rating)
                }
    }
}
