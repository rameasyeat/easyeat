//
//  ReviewCell.swift
//  easyeat
//
//  Created by Ramniwas on 07/05/20.
//  Copyright © 2020 EasyEat. All rights reserved.
//

import UIKit

class ReviewCell: ReusableTableViewCell {
    @IBOutlet weak var collectionView: UICollectionView!
    
        var reviewTypeArray : [Feedback_options]?
        
        override func awakeFromNib() {
            super.awakeFromNib()
            ReviewTypeCell.registerWithCollectionView(collectionView)
            collectionView.delegate = self
            collectionView.dataSource = self
        }

        override func setSelected(_ selected: Bool, animated: Bool) {
            super.setSelected(selected, animated: animated)
        }
        
    func commonInit(_ array : [Feedback_options]){
//        reviewTypeArray = array
        collectionView.reloadData()
        }
    }

        extension ReviewCell : UICollectionViewDataSource, UICollectionViewDelegate,UICollectionViewDelegateFlowLayout
        {
            func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
                return reviewTypeArray?.count ?? 0
            }
            
            func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
                return (CGSize(width: (collectionView.frame.size.width - 10) / 2, height: 45))
            }
            
            func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
                
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ReviewTypeCell.reuseIdentifier, for: indexPath)as! ReviewTypeCell
                cell.commonInit((reviewTypeArray?[indexPath.row])!)
                
                return cell
            }
            func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
                
                if reviewTypeArray?[indexPath.row].isSelected == true{
                    reviewTypeArray?[indexPath.row].isSelected = false
                }
                else{
                    reviewTypeArray?[indexPath.row].isSelected = true
                }
                
                collectionView.reloadData()
            }
        }
