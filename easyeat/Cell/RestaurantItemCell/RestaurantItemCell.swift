//
//  RestaurantItemCell.swift
//  easyeat
//
//  Created by Ramniwas on 31/03/20.
//  Copyright © 2020 EasyEat. All rights reserved.
//

import UIKit
import Cosmos
import SDWebImage
import ObjectMapper

protocol UpdateItemCountDelegate {
    func updateCount(_ indexPath :IndexPath,_ itemCount: Int,_ addRemove: Int)
}
class RestaurantItemCell: ReusableTableViewCell,AddItemClickDelegate {
    
    @IBOutlet weak var itemName: CustomLabel!
    @IBOutlet weak var reviewLabel: CustomLabel!
    @IBOutlet weak var priceLabel: CustomLabel!
    @IBOutlet weak var descriptionLabel: CustomLabel!
    @IBOutlet weak var reviewCountLabel: CustomLabel!
    @IBOutlet weak var itemNotAvailableLabel: CustomLabel!
        
    @IBOutlet weak var lineView2: UILabel!
    @IBOutlet weak var lineView1: UILabel!
    
    
    @IBOutlet weak var itemCountView: AddItemView!
    @IBOutlet weak var addButton: CustomButton!
    //    @IBOutlet weak var ratingView: CosmosView!
    @IBOutlet weak var itemImageView: CustomImageView!
    @IBOutlet weak var vegImageView: UIImageView!
    
    
    
    var restaurentDict : RestaurantData?
    
    
    var tblView: UITableView?
    var updateItemCountDelegate : UpdateItemCountDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        itemCountView.addItemClickDelegate = self
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
    }
    
    @IBAction func addButtonAction(_ sender: UIButton) {
        
        // check restaurant service available or not
        if restaurentDict?.status == 1 && restaurentDict?.serviceable?.is_serviceable == 0{// 1 & 2  normal flow
            addButton.alpha = 0.4
            if let msg = restaurentDict?.serviceable?.next_serviceable_string{
                Alert(title: "", message:msg.capitalizingFirstLetter(), vc: RootViewController.controller!)
            }
        }
        else  if restaurentDict?.status == 2{// disable
            if let msg = restaurentDict?.serviceable?.next_serviceable_string{
                Alert(title: "", message:msg.capitalizingFirstLetter(), vc: RootViewController.controller!)
            }
        }
        else {
            itemCountView.addFirstItemInCart(sender)
        }
    }
    
    
    
    func addItem(_ sender: UIButton, _ itemCount: Int, _ addRemove: Int) {
        
        if let cell = sender.superview?.superview as? RestaurantItemCell {
            getCell(cell, itemCount, addRemove)
        }
        else if let cell = sender.superview?.superview?.superview?.superview?.superview as? RestaurantItemCell {
            getCell(cell, itemCount, addRemove)
        }
    }
    
    fileprivate func getCell(_ cell : RestaurantItemCell,_ itemCount: Int, _ addRemove: Int){
        if let indexPath = tblView?.indexPath(for: cell){
            updateItemCountDelegate?.updateCount(indexPath,itemCount, addRemove)
        }
    }
    
    
    // Menu Array
    func commonInit(_ dict : Items?){
        itemName.text = dict?.name?.capitalized
        itemCountView.tblView = tblView
        setItemPrice(dict)
        
        lineView2.isHidden = false
        if  let review = dict?.no_of_reviews, Int(review) ?? 0 > 1{
            reviewLabel.text = "\(dict?.no_of_reviews ?? "0") Reviews"
        }
        else if  let review = dict?.no_of_reviews, Int(review) ?? 0 == 1{
            reviewLabel.text = "\(dict?.no_of_reviews ?? "0") Review"
        }
        else {
            lineView2.isHidden = true
            reviewLabel.text = ""
        }
        
        
        // rating
        if let ratingCount : String = dict?.total_rating, Int(ratingCount) ?? 0 > 0{
            reviewCountLabel.text = ratingCount
            lineView1.isHidden = false
        }
        else {
            reviewCountLabel.text = ""
            lineView1.isHidden = true
        }
        
        //        ratingView.rating = Double(ratingCount)!
        
        if let img = dict?.img {
            itemImageView?.sd_setImage(with:  URL(string:img), placeholderImage:UIImage(named: "food") , options: .progressiveDownload, completed: nil)
        }
        itemCountView.countLabel.text = dict?.quantity
        
        // check item availability
        
        if dict?.filters?.uppercased() == "|VEG|"{
            vegImageView.image =  #imageLiteral(resourceName: "veg")
        }else {
            vegImageView.image =  #imageLiteral(resourceName: "nonveg")
        }
        
        // check Item added in cart
        if Int(dict?.quantity ?? "0")! > 0 {
            addButton.isHidden = true
            itemCountView.isHidden = false
        }
        else {
            addButton.isHidden = false
            itemCountView.isHidden = true
        }
        
        // check is item available or not
        if   dict?.is_available == 0 {
            addButton.isUserInteractionEnabled = false
            addButton.backgroundColor = .gray
            addButton.setTitleColor(.white, for: .normal)
            addButton.setTitle("Out of Stock", for: .normal)
            addButton.titleLabel?.font = UIFont.systemFont(ofSize: 8)
            addButton.layer.borderWidth = 0
        }
        else{
            addButton.isUserInteractionEnabled = true
            addButton.backgroundColor = hexStringToUIColor(kwhite)
            addButton.setTitleColor(hexStringToUIColor(korange), for: .normal)
            addButton.setTitle("Add +", for: .normal)
            addButton.layer.borderWidth = 1
        }
        
        if restaurentDict?.status == 1 && restaurentDict?.serviceable?.is_serviceable == 0{// 1 & 2  normal flow
            addButton.alpha = 0.4
            
        }else if restaurentDict?.status == 1 {// is_serviceable 1 & 2  normal flow
            addButton.alpha = 1
            
        }
        else if restaurentDict?.status == 2{// disable
            addButton.alpha = 0.4
        }
        else { // status 0  remove from list
        }
        
        
    }
    
    
    fileprivate func setItemPrice(_ dict : Items?){ // Memu Items
        
        let currency = "\(dict?.currency ?? "") "
        var amount : String = "0"
        if CurrentUserInfo.priceByOrderType == priceByOrderType.dining{
            amount = dict?.price ?? "0"
        }
        else  if CurrentUserInfo.priceByOrderType == priceByOrderType.delivery{
            amount = dict?.delivery_price ?? "0"
        }
        else if CurrentUserInfo.priceByOrderType == priceByOrderType.pickup{
            amount = dict?.takeaway_price ?? "0"
        }
        
        // check if variation avaiable
        
        if dict?.gvariations != nil{
            guard let groupe = getvariation(isItemContaintVariation((((dict?.gvariations)!)))).last else {return}
            let roundPrice = String(format: "%.2f",groupe.variations?[0].price ?? 0)
            priceLabel.text = currency + roundPrice
        }
        else {
            let roundPrice = String(format: "%.2f",Double(amount) ?? 0)
            priceLabel.text = currency + roundPrice
        }
        
    }
    
    
    // Cart Array
    func commonCartInit(_ dict : Cart_items){
        itemName.text = dict.item_name?.capitalized
        itemCountView.tblView = tblView
        cartItemPrice(dict)
        
        lineView2.isHidden = false
        if  let review = dict.item_details?.no_of_reviews, Int(review) ?? 0 > 1{
            reviewLabel.text = "\(review) Reviews"
        }
        else if  let review = dict.item_details?.no_of_reviews, Int(review) ?? 0 == 1{
            reviewLabel.text = "\(review) Review"
        }
        else {
            lineView2.isHidden = true
            reviewLabel.text = ""
        }
        
        // rating
        if let ratingCount : String = dict.item_details?.total_rating, Int(ratingCount) ?? 0 > 0{
            reviewCountLabel.text = ratingCount
            lineView1.isHidden = false
        }
        else {
            reviewCountLabel.text = ""
            lineView1.isHidden = true
        }
        
        //        ratingView.rating = Double(ratingCount)!
        
        itemImageView?.sd_setImage(with:  URL(string: dict.item_details?.image ?? ""), placeholderImage:UIImage(named: "food") , options: .progressiveDownload, completed: nil)
        itemCountView.countLabel.text = "\(dict.quantity ?? 0)"
        
        
        if dict.item_details?.availability == "1"{
            vegImageView.image =  #imageLiteral(resourceName: "veg")
        }else {
            vegImageView.image =  #imageLiteral(resourceName: "nonveg")
        }
        // check Item added in cart
        
        if dict.quantity ?? 0 > 0 {
            addButton.isHidden = true
            itemCountView.isHidden = false
        }
        else {
            addButton.isHidden = false
            itemCountView.isHidden = true
        }
        
        // check item exist for order
        itemNotAvailableLabel.isHidden =  (isItemsExistInOrderType(order_type: dict.item_details?.order_type ?? "0") == true) ? true : false
        
        
        // check variation contains
        let variationName = dict.variation_name ?? ""
        var addons = dict.addons_name ?? ""
        
       addons  = (addons == "") ? "" : ("," + addons.capitalized)
        let desc = variationName.removeWhiteSpace().capitalized + addons
        
        if desc != "," {
            descriptionLabel.text = desc
        }
    }
    
   
    
    fileprivate func cartItemPrice(_ dict : Cart_items){ // Memu Items
        
        
        let currency = "\(dict.item_details?.currency ?? "") "
        
        var amount : Double?
        if CurrentUserInfo.priceByOrderType == priceByOrderType.dining{
            amount = dict.price ?? 0
        }
        else  if CurrentUserInfo.priceByOrderType == priceByOrderType.delivery{
            amount = dict.delivery_price
        }
        else if CurrentUserInfo.priceByOrderType == priceByOrderType.pickup{
            amount = dict.takeaway_price 
        }
        
        // check if variation avaiable
        if dict.item_details?.gvariations != nil{
            let groupe = getvariation(isItemContaintVariation((dict.item_details?.gvariations!)!)).last
            let roundPrice = String(format: "%.2f",groupe?.variations?[0].price ?? 0)
            priceLabel.text = currency + roundPrice
        }
        else {
            let roundPrice = String(format: "%.2f",amount ?? 0)
            priceLabel.text = currency + roundPrice
        }
    }
    
}


