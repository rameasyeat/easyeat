//
//  UserNameCell.swift
//  easyeat
//
//  Created by Ramniwas on 30/03/20.
//  Copyright © 2020 EasyEat. All rights reserved.
//

import UIKit

class UserNameCell: ReusableTableViewCell {
    @IBOutlet weak var shadowView: VIewShadow!
    @IBOutlet weak var nameTextField: CustomTextField!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    func commonInit(_ dict : UserNameModel){
        nameTextField.text = dict.value
        nameTextField.placeholder = dict.placeholder
        
    }
    
}
