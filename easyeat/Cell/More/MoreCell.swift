//
//  MoreCell.swift
//  easyeat
//
//  Created by Ramniwas on 24/04/20.
//  Copyright © 2020 EasyEat. All rights reserved.
//

import UIKit

class MoreCell: ReusableTableViewCell {

    @IBOutlet weak var iconImageView: UIImageView!
    @IBOutlet weak var txtLabel: CustomLabel!
    @IBOutlet weak var bgView: VIewShadow!
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func commiInit(_ dict : MoreModel){
        iconImageView.image = dict.img
        txtLabel?.text = dict.value
        bgView.layer.applySketchShadow(color: hexStringToUIColor("#0000002E"), alpha: 0.4, x: 0, y: 0, blur: 6, spread: 0)

    }
    
}
