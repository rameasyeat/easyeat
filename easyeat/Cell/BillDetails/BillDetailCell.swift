//
//  BillDetailCell.swift
//  easyeat
//
//  Created by Ramniwas on 08/04/20.
//  Copyright © 2020 EasyEat. All rights reserved.
//

import UIKit

class BillDetailCell: ReusableTableViewCell {
    
    @IBOutlet weak var totalSavingLabel: CustomLabel!
    @IBOutlet weak var totalPriceLabel: CustomLabel!
    
    
    @IBOutlet weak var paidLabel: CustomLabel!
    @IBOutlet weak var toPayLabel: CustomLabel!
    
    @IBOutlet weak var waveView: UIView!
    @IBOutlet weak var waveViewTop: UIView!
    @IBOutlet weak var payView: UIView!
    
    
    @IBOutlet weak var serviceTaxLabel: CustomLabel!
    @IBOutlet weak var sstLabel: CustomLabel!
    @IBOutlet weak var roundOffLabel: CustomLabel!
    @IBOutlet weak var deliveryFeeLabel: CustomLabel!
    @IBOutlet weak var totalItem: CustomLabel!
    
    @IBOutlet weak var sstViewHeight: NSLayoutConstraint!
    @IBOutlet weak var roundOffViewHeight: NSLayoutConstraint!
    @IBOutlet weak var serviceTaxViewHeight: NSLayoutConstraint!
    @IBOutlet weak var deliveryFeedHeight: NSLayoutConstraint!
    @IBOutlet weak var totalItemViewHeight: NSLayoutConstraint!
    
    @IBOutlet weak var savingViewHeight: NSLayoutConstraint!
    @IBOutlet weak var payViewHeight: NSLayoutConstraint!
    
    @IBOutlet weak var orderStatusImageView: UIImageView!
    @IBOutlet weak var paidImageView: UIImageView!
    
    
    var viewFeeHeight = 39
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func setCartUIData<T>(_ dictItem : Cart,_  restaurantDict : RestaurantData?,_ partnerDeliveryFeeData : T,_ cartBillDetails : BillDetails?){
        
        
        
        let cart_item_total = cartBillDetails?.cart_bill?.filter{$0.id == "cart_item_total"}
        if cart_item_total?.count ?? 0 > 0 {
            let roundtotalAmount = String(format: "%.2f",cart_item_total?[0].value ?? 0)
            totalItem.text = "\(CurrentUserInfo.currency ?? "") \(roundtotalAmount)"
        }
        else {
            totalItem.text = "\(CurrentUserInfo.currency ?? "") \(0.0)"
        }
        
        
        // sst tax
        let sst_tax = cartBillDetails?.cart_bill?.filter{$0.id == "sst_tax"}
        if sst_tax?.count ?? 0 > 0 {
            let tax = String(format: "%.2f",sst_tax?[0].value ?? 0)
            sstLabel.text  = "\(CurrentUserInfo.currency ?? "") \(tax)"
            sstViewHeight.constant = CGFloat(viewFeeHeight)
        }else {
            sstViewHeight.constant = 0
        }
        
        // service tax
        let service_tax = cartBillDetails?.cart_bill?.filter{$0.id == "service_tax"}
        if service_tax?.count ?? 0 > 0 {
            let tax = String(format: "%.2f",service_tax?[0].value ?? 0)
            serviceTaxLabel.text  = "\(CurrentUserInfo.currency ?? "") \(tax)"
            serviceTaxViewHeight.constant =  CGFloat(viewFeeHeight)
            
        } else {
            serviceTaxViewHeight.constant = 0
        }
        
        // delivery fee
        let delivery_fee = cartBillDetails?.cart_bill?.filter{$0.id == "delivery_fee"}
        
        if delivery_fee?.count ?? 0 > 0 {
            let tax = String(format: "%.2f",delivery_fee?[0].value ?? 0)
            deliveryFeeLabel.text  = "\(CurrentUserInfo.currency ?? "") \(tax)"
            deliveryFeedHeight.constant =  CGFloat(viewFeeHeight)
        }else{
            deliveryFeedHeight.constant = 0
        }
        
        // round off
        
        let round_off = cartBillDetails?.cart_bill?.filter{$0.id == "round_off"}
        
        if round_off?.count ?? 0 > 0 {
            let round_off = String(format: "%.2f",round_off?[0].value ?? 0)
            roundOffLabel.text  = "\(CurrentUserInfo.currency ?? "") \(round_off)"
            roundOffViewHeight.constant =  CGFloat(viewFeeHeight)
            
        }else{
            roundOffViewHeight.constant = 0
        }
        
        
        
        if let saving = cartBillDetails?.total_savings, saving > 0{
            let saving = ((saving)*100).rounded()/100
            totalSavingLabel.text = "\(CurrentUserInfo.currency ?? "") \(saving)"
        }
        else {
            savingViewHeight.constant = 0
        }
        
        totalPriceLabel.text = "\(CurrentUserInfo.currency ?? "") \(cartBillDetails?.bill_total ?? 0)"
        
        
        waveView.backgroundColor = UIColor(patternImage: #imageLiteral(resourceName: "lineWave"))
        waveViewTop.backgroundColor = UIColor(patternImage: #imageLiteral(resourceName: "lineWave"))
        
        payViewHeight.constant = 0
        payView.isHidden = true
    }
    
    fileprivate func cartDeliveryFee(_ totalAmount : Double,_ tax : Double,_ totalAfterTax : Double,_ totalSaving :Double,_ restaurantDict : RestaurantData?){
        
        let roundtotalAmount = String(format: "%.2f",totalAmount)
        
        totalItem.text = "\(CurrentUserInfo.currency ?? "") \(roundtotalAmount)"
        
        
        
        
        var totoalAmountWithTax : Double = Double(totalAmount)
        
        if  ((restaurantDict?.isDeliverable == true) && (restaurantDict!.orderType.rawValue == OrderType.delivery.rawValue)){
            totoalAmountWithTax  += Double(totalAfterTax)
        }
        
        let roundtotoalAmountWithTax = String(format: "%.2f",totoalAmountWithTax)
        
        totalPriceLabel.text = "\(CurrentUserInfo.currency ?? "") \(roundtotoalAmountWithTax)"
        
        if  totalSaving > 0{
            totalSavingLabel.text = "\(CurrentUserInfo.currency ?? "") \(totalSaving)"
        }
        else {
            savingViewHeight.constant = 0
        }
    }
    
    
    // order
    func setOrderUIData(_ dictItem : Bill,_  orderData : OrderData?,_ orderCancel : OrderCancelBy){
        
        waveView.backgroundColor = UIColor(patternImage:#imageLiteral(resourceName: "lineWave"))
        waveViewTop.backgroundColor = UIColor(patternImage: #imageLiteral(resourceName: "lineWave"))
        
        totalItem.text = "\(CurrentUserInfo.currency ?? "") \(dictItem.item_total ?? 0 )"
        paidLabel.text = "\(CurrentUserInfo.currency ?? "") \(dictItem.paid ?? 0)"
        totalPriceLabel.text = "\(CurrentUserInfo.currency ?? "") \(dictItem.bill_total ?? 0)"
        toPayLabel.text = "\(CurrentUserInfo.currency ?? "") \(dictItem.balance ?? 0)"
        
        
        // sst tax
        let sst_tax = dictItem.fees?.filter{$0.id == "sst_tax"}
        if sst_tax?.count ?? 0 > 0 {
            let tax = String(format: "%.2f",sst_tax?[0].fee ?? 0)
            sstLabel.text  = "\(CurrentUserInfo.currency ?? "") \(tax)"
            sstViewHeight.constant = CGFloat(viewFeeHeight)
        }else {
            sstViewHeight.constant = 0
        }
        
        // service tax
        let service_tax = dictItem.fees?.filter{$0.id == "service_tax"}
        if service_tax?.count ?? 0 > 0 {
            let tax = String(format: "%.2f",service_tax?[0].fee ?? 0)
            serviceTaxLabel.text  = "\(CurrentUserInfo.currency ?? "") \(tax)"
            serviceTaxViewHeight.constant =  CGFloat(viewFeeHeight)
            
        } else {
            serviceTaxViewHeight.constant = 0
        }
        
        
        // delivery fee
        let delivery_fee = dictItem.fees?.filter{$0.id == "delivery"}
        
        if delivery_fee?.count ?? 0 > 0 {
            let tax = String(format: "%.2f",delivery_fee?[0].fee ?? 0)
            deliveryFeeLabel.text  = "\(CurrentUserInfo.currency ?? "") \(tax)"
            deliveryFeedHeight.constant =  CGFloat(viewFeeHeight)
        }else{
            deliveryFeedHeight.constant = 0
        }
        
        // round off
        let round_off = dictItem.fees?.filter{$0.id == "round_off"}
        
        if round_off?.count ?? 0 > 0 {
            let round_off = String(format: "%.2f",round_off?[0].fee ?? 0)
            roundOffLabel.text  = "\(CurrentUserInfo.currency ?? "") \(round_off)"
            roundOffViewHeight.constant =  CGFloat(viewFeeHeight)
            
        }else{
            roundOffViewHeight.constant = 0
        }
        
        
        // saving
        if let saving = dictItem.savings, saving > 0{
            let saving = ((saving)*100).rounded()/100
            totalSavingLabel.text = "\(CurrentUserInfo.currency ?? "") \(saving)"
        }else {
            savingViewHeight.constant = 0
        }
        
        
        // order status image
        if  orderCancel == OrderCancelBy.user{
            orderStatusImageView.isHidden = false
        }
        else  if dictItem.paid == dictItem.bill_total && orderCancel == OrderCancelBy.MM{
            orderStatusImageView.isHidden = false
            orderStatusImageView.image =  #imageLiteral(resourceName: "orderCancel")
            paidImageView.isHidden = false
            paidImageView.image =  #imageLiteral(resourceName: "Paid")
        }
        else if  orderCancel == OrderCancelBy.MM{
            orderStatusImageView.isHidden = false
        }
        else  if Float(dictItem.paid ?? 0) >= Float(dictItem.bill_total ?? 0) && dictItem.bill_total ?? 0 != 0{
            orderStatusImageView.isHidden = false
            orderStatusImageView.image =  #imageLiteral(resourceName: "Paid")
        }
        else{
            orderStatusImageView.isHidden = true
        }
    }
}


extension Double{
    
    func roundedPrice(_ price : Double) -> Double{
        return (price*100).rounded()/100
    }
}
