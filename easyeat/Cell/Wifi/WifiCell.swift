//
//  WifiCell.swift
//  easyeat
//
//  Created by Ramniwas on 17/04/20.
//  Copyright © 2020 EasyEat. All rights reserved.
//

import UIKit

class WifiCell: ReusableTableViewCell {
    @IBOutlet weak var headerLabel: CustomLabel!
    @IBOutlet weak var nameLabel: CustomLabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func commonInit(_ dict : WifiService){
        headerLabel.text = dict.header
        nameLabel.text = dict.value
    }
}
