//
//  SigninCell.swift
//  easyeat
//
//  Created by Ramniwas on 18/03/20.
//  Copyright © 2020 EasyEat. All rights reserved.
//

import UIKit

class SigninCell: ReusableTableViewCell {
    
    @IBOutlet weak var textFiled : CustomTextField!
    @IBOutlet weak var headerLabel: CustomLabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    
    func commiInit<T>(_ dictionary :T){
        
        if let dict = dictionary as? SigninInfoModel{
            textFiled.text = dict.value
            textFiled.placeholder = dict.placeholder
            headerLabel.text = dict.header
        }
        else if let dict = dictionary as? SignupInfoModel{
            textFiled.text = dict.value
            textFiled.placeholder = dict.placeholder
            headerLabel.text = dict.header
        }
        else if let dict = dictionary as? ForgotPasswordModel{
            textFiled.text = dict.value
            textFiled.placeholder = dict.placeholder
            headerLabel.text = dict.header
        }
        else if let dict = dictionary as? NewAddressModel{
            textFiled.text = dict.value
            headerLabel.text = dict.header
        }
}

fileprivate func setValue(){
    
}

override func setSelected(_ selected: Bool, animated: Bool) {
    super.setSelected(selected, animated: animated)
    
    // Configure the view for the selected state
}

}
