//
//  OHRatingCell.swift
//  easyeat
//
//  Created by Ramniwas on 25/04/20.
//  Copyright © 2020 EasyEat. All rights reserved.
//

import UIKit

class OHRatingCell: ReusableTableViewCell {

    @IBOutlet weak var ratingLabel: CustomLabel!
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    
}
