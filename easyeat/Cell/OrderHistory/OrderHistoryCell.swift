//
//  OrderHistoryCell.swift
//  easyeat
//
//  Created by Ramniwas on 24/04/20.
//  Copyright © 2020 EasyEat. All rights reserved.
//

import UIKit

class OrderHistoryCell: ReusableTableViewCell {

    @IBOutlet weak var itemImageView: CustomImageView!
    @IBOutlet weak var dateLabel: CustomLabel!
    @IBOutlet weak var locationLabel: CustomLabel!
    @IBOutlet weak var restaurantName: CustomLabel!
    @IBOutlet weak var ratingDescriptionLabel: CustomLabel!
    @IBOutlet weak var ratingLabel: CustomLabel!

    @IBOutlet weak var ratingButton: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func commiInit(_ dict : OrderHistory){
        
        itemImageView?.sd_setImage(with:URL(string: "\(KImagesBaseUrl)\(kRestFolderName)\(dict.restaurant_logo ?? "")"), placeholderImage:UIImage(named: "food") , options: .progressiveDownload, completed: nil)
        
        restaurantName.text = dict.restaurant_name?.capitalized
        locationLabel.text = dict.restaurant_city?.capitalizingFirstLetter()
        dateLabel.text = AppUtility.dateToString(date: AppUtility.date(timestamp: Double(dict.created_at ?? 0)))
    }
}
