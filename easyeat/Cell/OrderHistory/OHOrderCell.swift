//
//  OHOrderCell.swift
//  easyeat
//
//  Created by Ramniwas on 25/04/20.
//  Copyright © 2020 EasyEat. All rights reserved.
//

import UIKit

class OHOrderCell: ReusableTableViewCell {
    @IBOutlet weak var itemImageView: CustomImageView!
    @IBOutlet weak var itemNameLabel: CustomLabel!
    @IBOutlet weak var ratingCountLabel: CustomLabel!
    @IBOutlet weak var vegImageView: UIImageView!
    @IBOutlet weak var itemCountLabel: CustomLabel!
    @IBOutlet weak var priceLabel: CustomLabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
    }
    
    func commonInit(_ dict : OrderHistoryItems){
        itemNameLabel.text = dict.item_name?.capitalized
        priceLabel.text =  "\(CurrentUserInfo.currency ?? "".capitalized)" + " "  + "\(dict.item_price ?? 0)" 
        itemCountLabel.text = "\(dict.item_quantity ?? 0)" + " X " + "\(dict.item_price ?? 0)"

        
//        if  let review = dict.{
//            ratingCountLabel.text = "\(review)"
//        }
        
        if  let review = dict.item_quantity{
            ratingCountLabel.text = "\(review)"
        }
        
        let imgPath = dict.image ?? ""
        itemImageView?.sd_setImage(with:  URL(string: imgPath), placeholderImage:UIImage(named: "food") , options: .progressiveDownload, completed: nil)
        
//        if dict.filters == "|veg|"{
//            vegImageView.image =  #imageLiteral(resourceName: "veg")
//        }else {
//            vegImageView.image =  #imageLiteral(resourceName: "nonveg")
//        }
        
        
    }
    
}
