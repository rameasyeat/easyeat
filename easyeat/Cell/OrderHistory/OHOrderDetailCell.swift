//
//  OHOrderDetailCell.swift
//  easyeat
//
//  Created by Ramniwas on 25/04/20.
//  Copyright © 2020 EasyEat. All rights reserved.
//

import UIKit

class OHOrderDetailCell: ReusableTableViewCell {

    @IBOutlet weak var orderTimeLabel: CustomLabel!
    @IBOutlet weak var orderNumberLabel: CustomLabel!
    @IBOutlet weak var paymentModeLabel: CustomLabel!
    @IBOutlet weak var paymentIdLabel: CustomLabel!



    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func commonInit(_ dict : OrderHistoryDetail){
        orderTimeLabel.text = AppUtility.dateToString(date: AppUtility.date(timestamp: Double(dict.bill?.timestamp ?? 0)))
        orderNumberLabel.text = dict.order_no
        
        if dict.bill?.payments?.count ?? 0 > 0 {
            guard let paymentdict =   dict.bill?.payments?[0] else {return}
            paymentModeLabel.text = paymentdict.payment_method?.capitalized
        paymentIdLabel.text = paymentdict.payment_id
        }
        
    }
    
}
