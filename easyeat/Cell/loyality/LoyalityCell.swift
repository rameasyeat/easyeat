//
//  LoyalityCell.swift
//  easyeat
//
//  Created by Ramniwas on 26/05/20.
//  Copyright © 2020 EasyEat. All rights reserved.
//

import UIKit
import LMGraphView

class LoyalityCell: ReusableTableViewCell{
    @IBOutlet weak var savingTypeLabel: CustomLabel!
    @IBOutlet weak var priceLabel: CustomLabel!
    @IBOutlet weak var savingView: UIView!
    @IBOutlet weak var collectionView: CaloriesTypeCollectionView!
    @IBOutlet weak var graphView: LMLineGraphView!
    @IBOutlet weak var noRecord: CustomLabel!
    @IBOutlet weak var bgView: VIewShadow!
    
    
    var defaultDataArray : [GraphDefaultData]?
    var loyalityData : LoyalityData?
    var viewModel : LoyalityViewModel?
    var graphView1 : LMLineGraphView?
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    func commonInit(index : Int, dict : LoyalityData?){
        noRecord.isHidden = true
        
        if graphView1 != nil {
            graphView1?.removeFromSuperview()
        }
        
        graphView1  = LMLineGraphView()
        graphView1?.frame = CGRect(x: 0, y: graphView.frame.origin.y, width: ScreenSize.screenWidth - 48, height: graphView.frame.size.height)
        
        graphView1?.yAxisUnit = ""
        graphView1?.title = ""
        graphView1?.layout.xAxisScrollableOnly = false
        graphView1?.layout.xAxisLabelFont = UIFont.systemFont(ofSize: 8)
        graphView1?.layout.xAxisMargin = 0
        graphView1?.layout.titleLabelHeight = 0
        let earning = plotGraph("002581", fillColor: "032581", .earning, index)
        
        if index == 1{
            collectionView.caloriesArray = (defaultDataArray?.filter{$0.type == "loyality"})!
            savingTypeLabel.text = "Earnings Through Loyalty"
            
            if viewModel?.validateLoyality() == true{
                noRecord.isHidden = false
                graphView1?.isHidden = true
            }else {
                graphView1?.isHidden = false
            }
            
            let array = viewModel?.convertLoyalityDataIntoWeekArray(.axisData)
            if array?.count ?? 0 > 1{
                graphView1?.xAxisValues = array
                let saving = plotGraph("2D99FC", fillColor: "C8E1FA", .saving, index)
                graphView1?.graphPlots = [saving,earning]
            }
        }
        else {
            
            collectionView.caloriesArray = (defaultDataArray?.filter{$0.type == "saving"})!
            
            
            if viewModel?.validateSavingData() == true{
                noRecord.isHidden = false
                graphView1?.isHidden = true
            }else {
                graphView1?.isHidden = false
            }
            
            let array = viewModel?.convertDataIntoWeekArray(.axisData)
            if array?.count ?? 0 > 1{
                graphView1?.xAxisValues = array
                 let saving = plotGraph("2CD9C5", fillColor: "CBF2EE", .saving, index)
                graphView1?.graphPlots = [saving,earning]
            }
        }
        bgView.addSubview(graphView1!)
    }
    
    
    func plotGraph(_ stroke : String, fillColor : String, _ type : UserSavingOREarningType, _ index :Int) -> LMGraphPlot{
        let ploat : LMGraphPlot = LMGraphPlot()
        ploat.strokeColor = hexStringToUIColor(stroke)
        ploat.graphPointSize = 0
        
        if type == .saving{
            ploat.fillColor = hexStringToUIColor(fillColor)
        }
        else {
            ploat.fillColor = .clear
        }
        
        viewModel?.graphMaxValue = 0
        let  array =  (index == 0) ? viewModel?.convertDataIntoWeekArray(type) : viewModel?.convertLoyalityDataIntoWeekArray(type)
        ploat.graphPoints = viewModel?.getGraphPoints(array!)
        
        print(viewModel?.graphMaxValue as Any)
        graphView1?.yAxisMaxValue = CGFloat(CFloat(viewModel!.graphMaxValue)) + 10
        
        return ploat
        
    }
    
}
