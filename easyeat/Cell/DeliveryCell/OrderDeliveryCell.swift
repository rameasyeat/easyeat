//
//  OrderDeliveryCell.swift
//  easyeat
//
//  Created by Ramniwas on 29/04/20.
//  Copyright © 2020 EasyEat. All rights reserved.
//

import UIKit

protocol DeliveryTypeDelegate {
    func selectedDeliveryType(_ type : OrderType)
    func addressButtonAction()
    
}
class OrderDeliveryCell: ReusableTableViewCell {
    
    var deliveryTypeDelegate : DeliveryTypeDelegate?
    
    @IBOutlet weak var deliveryButton: CustomButton!
    @IBOutlet weak var pickupButton: CustomButton!
    @IBOutlet weak var noDeliveryLabel: CustomLabel!
    @IBOutlet weak var addressLabel: CustomLabel!
    @IBOutlet weak var lineView: UIView!
    @IBOutlet weak var bgViewHeight: NSLayoutConstraint!
    @IBOutlet weak var noDeliveryMessageHeight: NSLayoutConstraint!
    @IBOutlet weak var addressButton: UIButton!
    @IBOutlet weak var addressViewLine: UIView!
    @IBOutlet weak var viewBG: VIewShadow!
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    @IBAction func addressButtonAction(sender : UIButton){
        deliveryTypeDelegate?.addressButtonAction()
        
    }
    
    func commonInit(_ dict : RestaurantData,_ distance : DistanceModel?){
        
        if dict.currentlocationAddress != ""{
            addressLabel.text = dict.currentlocationAddress // show restaurant location if user current location and default location not available
            addressLabel.textColor = .black
        }
        
        if dict.orderType.rawValue == OrderType.delivery.rawValue{
            showDeliveryOption()
            deliveryButton.setImage(#imageLiteral(resourceName: "RadioSelected"), for: .normal)
            pickupButton.setImage(#imageLiteral(resourceName: "Radio"), for: .normal)
            showNoDeliveryMessage(dict, distance)
        }
        else{
            hideDeliveryOption()
            pickupButton.setImage(#imageLiteral(resourceName: "RadioSelected"), for: .normal)
            deliveryButton.setImage(#imageLiteral(resourceName: "Radio"), for: .normal)
        }
    }
    
    fileprivate func showNoDeliveryMessage(_ dict: RestaurantData,_ distance : DistanceModel?){
        if dict.isDeliverable == true || dict.currentlocationAddress == ""{
            noDeliveryLabel.isHidden = true
            bgViewHeight.constant = 70
            addressViewLine.isHidden = true
        }
        else{
            noDeliveryLabel.isHidden = false
            addressViewLine.isHidden = false
            
            if  dict.userCurrentDistance == 0{
                noDeliveryLabel.text = "Sorry! Restaurant not serving in your area"
            }
            else{
                
                let userCurrentDistance = distance?.rows?.last?.elements?.last?.distance?.text

                noDeliveryLabel.text = "Your address is \(userCurrentDistance ?? "0 km") away from this restaurant, it delivery only upto \(getDeliveryDistance(dict)) km"
            }
        }
    }
    
    fileprivate func getDeliveryDistance(_ dict: RestaurantData) -> Int{
        var restaurantDistance :  Int = 0
        
        if let distance = dict.deliverylimit as? Int {
            restaurantDistance = distance
        }
        else if let distance = dict.deliverylimit as? String {
            restaurantDistance = Int(distance) ?? 0
        }
        else if let distance = dict.deliverylimit as? Double {
            restaurantDistance = Int(distance)
        }
        return restaurantDistance
    }
    
    func hideDeliveryOption(){
        bgViewHeight.constant = 0
        viewBG.isHidden = true
        lineView.isHidden = true
    }
    func showDeliveryOption(){
        bgViewHeight.constant = 100
        viewBG.isHidden = false
        lineView.isHidden = true
        
    }
    
    
    @IBAction func deliveryButtonAction(_ sender: Any) {
        deliveryTypeDelegate?.selectedDeliveryType(.delivery)
    }
    
    @IBAction func pickupButtonAction(_ sender: Any) {
        deliveryTypeDelegate?.selectedDeliveryType(.pickup)
    }
}
