//
//  DeliveryAddressCell.swift
//  easyeat
//
//  Created by Ramniwas on 29/04/20.
//  Copyright © 2020 EasyEat. All rights reserved.
//

import UIKit

class DeliveryAddressCell: ReusableTableViewCell {

    @IBOutlet weak var houseNumber: CustomLabel!
    @IBOutlet weak var addressLabel: CustomLabel!
    @IBOutlet weak var landMarkLabel: CustomLabel!
    @IBOutlet weak var imgView: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func commonInit(_ dict : DeliveryAddress){
        houseNumber.text = dict.flat_details
        addressLabel.text = dict.address
        landMarkLabel.text = dict.landmark
        
        
        if dict.isSelected == true{
            imgView.image = #imageLiteral(resourceName: "RadioSelected")
        }else {
            imgView.image = #imageLiteral(resourceName: "Radio")
        }
    }
}
