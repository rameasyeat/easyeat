//
//  ChangeAddressCell.swift
//  easyeat
//
//  Created by Ramniwas on 30/04/20.
//  Copyright © 2020 EasyEat. All rights reserved.
//

import UIKit

class ChangeAddressCell: ReusableTableViewCell {
    @IBOutlet weak var localityLabel: CustomLabel!
    @IBOutlet weak var addressLabel: CustomLabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    
    func commonInt(_ dict : PlacesAddress){
        localityLabel.text = dict.locality
        addressLabel.text = dict.address
    }
    
}
