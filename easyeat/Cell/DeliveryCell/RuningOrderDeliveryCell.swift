//
//  RuningOrderDeliveryCell.swift
//  easyeat
//
//  Created by Ramniwas on 01/05/20.
//  Copyright © 2020 EasyEat. All rights reserved.
//

import UIKit




class RuningOrderDeliveryCell: ReusableTableViewCell {
    @IBOutlet weak var orderTypeLabel: CustomLabel!
    @IBOutlet weak var restaurantAddressLabel: CustomLabel!
    @IBOutlet weak var contactNumberLabel: CustomLabel!
    @IBOutlet weak var callLabel: CustomLabel!
    @IBOutlet weak var callImageView: UIImageView!
    @IBOutlet weak var callViewWidth: NSLayoutConstraint!
    @IBOutlet weak var headerLabel: CustomLabel!
    @IBOutlet weak var callView: UIView!
    @IBOutlet weak var callButton: UIButton!
    
    var  pickup = "Order Type - Pick Up"
    var  delivery = "Order Type - Delivery"
    
    var restaurantDict : RestaurantData?
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
    }
    
    func commonInit(_ orderdata : OrderData){
        
        if orderdata.order_type == OrderType.delivery.rawValue{
            orderTypeLabel.text = delivery
            headerLabel.text = "DELIVERY ADDRESS"
            restaurantAddressLabel.text = orderdata.address

        }else {
            orderTypeLabel.text = pickup
            headerLabel.text = "RESTAURANT ADDRESS"
            restaurantAddressLabel.text = restaurantDict?.address
        }
        
        let contact = "\(restaurantDict?.name ?? "") - " + "\(restaurantDict?.dial_code ?? "")" + " \(restaurantDict?.phone ?? "")"
        contactNumberLabel.text = contact
        updateCallActionUI()
        
    }
    
    fileprivate func callAction(_ url : URL){
        if UIApplication.shared.canOpenURL(url) {
            if #available(iOS 10, *) {
                UIApplication.shared.open(url)
            } else {
                UIApplication.shared.openURL(url)
            }
        }
    }
    
    func updateCallActionUI(){
        if restaurantDict?.on_whatsapp == "1" {
            callImageView.image =  #imageLiteral(resourceName: "whatsapp")
            callLabel.text = "Whatsapp"
        callViewWidth.constant = 100
            
        }else{
            callImageView.image =  #imageLiteral(resourceName: "phone")
            callLabel.text = "CALL"
        }

    }
    
    @IBAction func callButtonAction(_ sender: Any) {
        
        if restaurantDict?.on_whatsapp == "1"{
            let appURL = URL(string: "https://api.whatsapp.com/send?phone=\(restaurantDict?.phone ?? "")")
            callAction(appURL!)
        }
        else {
            let contact =  "\(restaurantDict?.phone ?? "")"
            guard let number = URL(string: "tel://" + contact) else { return }
            callAction(number)
        }
        
    }
    
    
}
