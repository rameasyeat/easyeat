//
//  CustomizationCell.swift
//  easyeat
//
//  Created by Ramniwas Patidar on 14/07/20.
//  Copyright © 2020 EasyEat. All rights reserved.
//

import UIKit
import SDWebImage
import ObjectMapper

class CustomizationCell: ReusableTableViewCell {
    @IBOutlet weak var itemName: CustomLabel!
    @IBOutlet weak var itemType: CustomLabel!
    @IBOutlet weak var itemPrice: CustomLabel!
    @IBOutlet weak var itemImageView: CustomImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

   // Menu Array
        func commonInit(_ dict : Cart_items?){
            itemName.text = dict?.item_name
       
            if let img = dict?.item_details?.image {
                itemImageView?.sd_setImage(with:  URL(string:img), placeholderImage:UIImage(named: "food") , options: .progressiveDownload, completed: nil)
            }
            setItemPrice(dict)
        }
    
    fileprivate func setItemPrice(_ dict : Cart_items?){ // Memu Items
          
        let currency = "\(dict?.item_details?.currency ?? "") "
          var amount : Double = 0
          if CurrentUserInfo.priceByOrderType == priceByOrderType.dining{
            amount = dict?.price ?? 0
          }
          else  if CurrentUserInfo.priceByOrderType == priceByOrderType.delivery{
            amount = dict?.delivery_price ?? 0
          }
          else if CurrentUserInfo.priceByOrderType == priceByOrderType.pickup{
            amount = dict?.takeaway_price ?? 0
          }
    
        // check if variation avaiable
        if dict?.item_details?.gvariations != nil{
            let groupe = getvariation(isItemContaintVariation((dict?.item_details?.gvariations)!)).last
            let roundPrice = String(format: "%.2f",groupe?.variations?[0].price ?? 0)
            itemPrice.text = currency + roundPrice
        }
        else {
            let roundPrice = String(format: "%.2f",amount )
            itemPrice.text = currency + roundPrice
        }
    }
    
}
