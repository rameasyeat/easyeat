//
//  HomeCell.swift
//  easyeat
//
//  Created by Ramniwas on 12/05/20.
//  Copyright © 2020 EasyEat. All rights reserved.
//

import UIKit
import Cosmos

class HomeCell: ReusableTableViewCell {
    
    @IBOutlet weak var nameLabel: CustomLabel!
    @IBOutlet weak var addressLabel: CustomLabel!
    @IBOutlet weak var ratingLabel: CustomLabel!
    @IBOutlet weak var openTimeLabel: CustomLabel!
    @IBOutlet weak var distanceLabel: CustomLabel!
    
    @IBOutlet weak var directionView: UIView!
    @IBOutlet weak var ratingView: CosmosView!
    
    @IBOutlet weak var restImageView: CustomImageView!
    @IBOutlet weak var directionButton: UIButton!
    
    @IBOutlet weak var ratingViewHeight: NSLayoutConstraint!
    @IBOutlet weak var ratingLabelHeight: NSLayoutConstraint!
    @IBOutlet weak var directionViewWidth: NSLayoutConstraint!
    
    
    var isRating : Bool = false {
        didSet{
            if isRating{
                ratingView.isHidden = false
                ratingViewHeight.constant = 9
                ratingLabelHeight.constant = 12
            }
            else{
                ratingViewHeight.constant = 0
                ratingLabelHeight.constant = 0
                ratingView.isHidden = true
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    fileprivate func getDistance(_ dict : Nearby_restaurants?){
        // Distance
        if let distance = dict?.distance_km, distance > 40000{
            let roundUp = String(format: "%.1f", (distance))
            distanceLabel.text = "\(roundUp) Kms"
            directionViewWidth.constant = 60
            
        }else{
            directionView.isHidden = true
            directionViewWidth.constant = 0
        }
    }
    
    fileprivate func setReview(_ dict : Nearby_restaurants?){
        // Review
        if  let review = dict?.total_ratings, Int(review) > 1{
            ratingLabel.text = "\(dict?.total_ratings ?? 0) Reviews"
            
        }else {
            ratingLabel.text = "\(dict?.total_ratings ?? 0) Review"
        }
        
        
        //Rating Count
        let ratingCount  = dict?.avg_rating ?? 0
        
        if ratingCount > 0{
            ratingView.rating = Double(ratingCount)
            isRating = true
            
        }else{
            isRating = false
        }
    }
    
    
    fileprivate func checkRestOpeningHours(_ dict : Nearby_restaurants?){
        // Check restautant open or not
        if dict?.serviceable?.is_serviceable == 0 && dict?.status == 1{
            self.contentView.alpha = 0.5
            openTimeLabel.text = "Open next at \(dict?.serviceable?.next_serviceable_time_string ?? "") \(dict?.serviceable?.next_serviceable_day_string ?? "")".capitalizingFirstLetter()
            openTimeLabel.textColor = hexStringToUIColor(kred)
        }
        else  if  dict?.status == 2{
            self.contentView.alpha = 0.5
            openTimeLabel.text = "Coming soon"
            openTimeLabel.textColor = hexStringToUIColor(kred)
        }
        else{
            openTimeLabel.text = ""
            self.contentView.alpha = 1
        }
    }
    
    
    // Menu Array
    func commonInit(_ dict : Nearby_restaurants?){
        
        nameLabel.text = dict?.name?.capitalized
        addressLabel.text = dict?.city?.removeWhiteSpace().capitalizingFirstLetter()
        getDistance(dict)
        setReview(dict)
        checkRestOpeningHours(dict)
        
        // rest img
        restImageView?.sd_setImage(with:  URL(string: (dict?.logo!)!), placeholderImage:UIImage(named: "food") , options: .progressiveDownload, completed: nil)
    }
}
