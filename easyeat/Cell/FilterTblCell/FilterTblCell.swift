//
//  FilterTblCell.swift
//  easyeat
//
//  Created by Ramniwas on 14/04/20.
//  Copyright © 2020 EasyEat. All rights reserved.
//

import UIKit

class FilterTblCell: ReusableTableViewCell {
    
    @IBOutlet weak var headerLabel: CustomLabel!
    var customGridView : GridCollectionView!
    var filterDelegate : SelectedFilterDelegate?

    
    override func awakeFromNib() {
        super.awakeFromNib()
        addGridImageCollection()
    }
    
    func addGridImageCollection(){
        
        customGridView = GridCollectionView(frame: CGRect(x: 28, y: 35, width:ScreenSize.screenWidth - 54, height:40))
        self.contentView.addSubview(customGridView)
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func commonInit(_ dict : FilterSubcategory){
        headerLabel.text = dict.filterType?.firstCapitalized
        customGridView.registerCell()
        customGridView.filterDelegate = filterDelegate
        customGridView.filterArray = dict.filterArray
    }
    
}
