//
//  QSSubCategoryCell.swift
//  easyeat
//
//  Created by Ramniwas on 16/04/20.
//  Copyright © 2020 EasyEat. All rights reserved.
//

import UIKit

class QSSubCategoryCell: ReusableTableViewCell,AddItemClickDelegate {
    
    
    
    @IBOutlet weak var itemImageView: CustomImageView!
    @IBOutlet weak var itemName: CustomLabel!
    @IBOutlet weak var itemCountView: AddItemView!
    @IBOutlet weak var addButton: CustomButton!
    var viewModel : QSsubcategoryViewModel?
    
    var tblView: UITableView?
    var updateItemCountDelegate : UpdateItemCountDelegate?
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        itemCountView.addItemClickDelegate = self
    }
    
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
    }
    
    func addItem(_ sender: UIButton, _ itemCount: Int, _ addRemove: Int) {
        
        if let cell = sender.superview?.superview as? QSSubCategoryCell {
            getCell(cell, itemCount, addRemove)
        }
        else if let cell = sender.superview?.superview?.superview?.superview?.superview as? QSSubCategoryCell {
            getCell(cell, itemCount, addRemove)
        }
    }
    
    fileprivate func getCell(_ cell : QSSubCategoryCell,_ itemCount: Int, _ addRemove: Int){
        if let indexPath = tblView?.indexPath(for: cell){
            updateItemCountDelegate?.updateCount(indexPath,itemCount, addRemove)
        }
    }
    @IBAction func addButtonAction(_ sender: UIButton) {
        
        if  let catName = viewModel?.serviceDict?.category_name, catName == kSauces{
            if let cell = sender.superview?.superview as? QSSubCategoryCell {
                if  let dict = viewModel?.serviceDict?.subcategories?[sender.tag]{
                    getCell(cell, dict.quantity_flag!, ( dict.quantity_flag == 0) ? 1 : 0 )
                }
            }
        }
        else {
            itemCountView.addFirstItemInCart(sender)
        }
    }
    
    
    
    
    func commonInit(_ dict : Subcategories){
        itemName.text = dict.subcategory_name
        itemCountView.tblView = tblView
        
        
        itemImageView?.sd_setImage(with: URL(string: "\(KImagesBaseUrl)\(kQuickserviceFolderName)\(dict.icon ?? "")"), placeholderImage:UIImage(named: "header") , options: .progressiveDownload, completed: nil)
        itemCountView.countLabel.text = "\(dict.quantity_flag ?? 0)"
        
        
        // signle item selection
        if  viewModel?.serviceCatType == ServiceCatType.singleAction{
            if  let count = dict.quantity_flag, count > 0 {
                addButton.isHidden = false
                itemCountView.isHidden = true
                addButton.borderColor = hexStringToUIColor(kred)
                addButton.setTitleColor(hexStringToUIColor(kred), for: .normal)
                addButton.setTitle("Remove -", for: .normal)
            }
            else {
                itemCountView.isHidden = true
                addButton.isHidden = false
                addButton.borderColor = hexStringToUIColor(korange)
                addButton.setTitleColor(hexStringToUIColor(korange), for: .normal)
                addButton.setTitle("Add +", for: .normal)
            }
        }
            
    // number of item selection more then one
        else {
            if  let count = dict.quantity_flag, count > 0 {
                addButton.isHidden = true
                itemCountView.isHidden = false
            }
            else {
                addButton.isHidden = false
                itemCountView.isHidden = true
            }
        }
    }
}
