//
//  BillDetailView.swift
//  easyeat
//
//  Created by Ramniwas on 03/04/20.
//  Copyright © 2020 EasyEat. All rights reserved.
//

import Foundation
import UIKit


class BillDetailView : VIewShadow{
   
    
    @IBOutlet var contentView: VIewShadow!
    @IBOutlet weak var totalItem: CustomLabel!
    @IBOutlet weak var taxLabel: CustomLabel!
    @IBOutlet weak var totalSavingLabel: CustomLabel!
    @IBOutlet weak var totalPriceLabel: CustomLabel!
    @IBOutlet weak var savingViewHeight: NSLayoutConstraint!
    
    @IBOutlet weak var paidLabel: CustomLabel!
    @IBOutlet weak var toPayLabel: CustomLabel!
    @IBOutlet weak var viewPaid: NSLayoutConstraint!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    

    internal override func commonInit(){
        Bundle.main.loadNibNamed("BillDetailView", owner: self, options: nil)
        addSubview(contentView)
        contentView.frame = self.bounds
        contentView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
    }
    
    func setCartUIData(_ dictItem : Cart){

        totalItem.text = "\(CurrentUserInfo.currency ?? "") \(dictItem.total_amount ?? 0.0)"
        taxLabel.text  = "\(CurrentUserInfo.currency ?? "") \(dictItem.tax ?? 0.0)"
        totalPriceLabel.text = "\(CurrentUserInfo.currency ?? "") \(dictItem.total_after_tax ?? 0.0)"
        
        if let saving = dictItem.total_savings, saving > 0{
            totalSavingLabel.text = "\(CurrentUserInfo.currency ?? "") \(dictItem.total_savings ?? 0.0)"
        }
        else {
            savingViewHeight.constant = 0
        }
    }
    
    // order
    func setCartUIData(_ dictItem : Bill){
          totalItem.text = "\(dictItem.item_total ?? 0)"
          taxLabel.text  = "\(dictItem.tax ?? 0)"
          totalPriceLabel.text = "\(dictItem.bill_total ?? 0)"
          
          if let saving = dictItem.savings, saving > 0{
              totalSavingLabel.text = "\(dictItem.savings ?? 0)"
          }
          else {
              savingViewHeight.constant = 0
          }
      }
}
