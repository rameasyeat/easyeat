//
//  OrderBottomPopView.swift
//  easyeat
//
//  Created by Ramniwas on 11/04/20.
//  Copyright © 2020 EasyEat. All rights reserved.
//

import Foundation
import UIKit
import Lottie

protocol RuningOrderViewDelegate {
    func viewOrderAction()
}
class RunningOrderView : VIewShadow{
    
    @IBOutlet weak var orderLabel: CustomLabel!
    @IBOutlet weak var itemCountLabel: CustomLabel!
    @IBOutlet weak var viewOrderLabel: CustomLabel!
    
    @IBOutlet weak var foodImageView: UIView!
    @IBOutlet var contentView: VIewShadow!

    @IBOutlet weak var viewOrderButton: UIButton!

    var runingOrderDelegate: RuningOrderViewDelegate?
    var animationView : AnimationView?
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    
    internal override func commonInit(){
        Bundle.main.loadNibNamed("RunningOrderView", owner: self, options: nil)
        addSubview(contentView)
        contentView.frame = self.bounds
        contentView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
    }
    
    @IBAction func viewRuningOrderAction(_ sender: Any) {
        runingOrderDelegate?.viewOrderAction()
    }
    
    // Food Animation
    func addRunningOrderFoodAnimation(_ view : UIView,_ type : String){
        if animationView != nil{
            animationView?.removeFromSuperview()
        }
        animationView = AnimationView()
        animationView?.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: view.frame.height)
        animationView?.animation = Animation.named(type)
        animationView?.backgroundColor = .clear
        animationView?.tag = 2022
        animationView?.loopMode = .loop
        animationView?.play()
        view.addSubview(animationView!)
    }
    
    
    // DeliveryType
    func orderStatusForDelivery(_ result : Cart){
        
        if  result.order?.order_status == OrderStatus.pending.rawValue{
            itemCountLabel.text = "Please hold on!"
            orderLabel.text = "Waiting For Confirmation"
            
            addRunningOrderFoodAnimation(foodImageView,"WaitingForConfirmation")
            
        }
        else if  result.order?.order_status == OrderStatus.confirmed.rawValue{
            itemCountLabel.text = "Order is confirmed and will be start preparing soon"
            orderLabel.text = "Order Confirmed"
            
            addRunningOrderFoodAnimation(foodImageView,"FoodPreparing")
        }
        else if  result.order?.order_status == OrderStatus.inprogress.rawValue{
            
            let items =  result.order?.items?[0]
            
            if result.order?.address == "pickup take away"{
                if items?.item_status == ItemStatus.served.rawValue{
                    itemCountLabel.text = "Order has been picked"
                    orderLabel.text = "Order Picked"
                    addRunningOrderFoodAnimation(foodImageView,"FoodPreparing")
                    
                }
                else if items?.item_status == ItemStatus.prepared.rawValue{
                    itemCountLabel.text = "Order has been prepared. Ready to be picked"
                    orderLabel.text = "Order Prepared"
                    addRunningOrderFoodAnimation(foodImageView,"DriverGoingToRestaurant")
                }
                else   {
                    itemCountLabel.text = "Your order is being prepared"
                    orderLabel.text = "Preparing Order"
                    
                    addRunningOrderFoodAnimation(foodImageView,"FoodPreparing")
                }
            }
                
            else{
                if items?.item_status == ItemStatus.served.rawValue{
                    itemCountLabel.text = "Your order has been delivered"
                    orderLabel.text = "Order Delivered"
                    
                    addRunningOrderFoodAnimation(foodImageView,"FoodPreparing")
                    
                }
                else if items?.item_status == ItemStatus.prepared.rawValue{
                    itemCountLabel.text = "Your order is on the way"
                    orderLabel.text = "Order Dispatched"
                    
                    addRunningOrderFoodAnimation(foodImageView,"DriverGoingToRestaurant")
                }
                else   {
                    itemCountLabel.text = "Your order is being prepared"
                    orderLabel.text = "Preparing Order"
                    
                    addRunningOrderFoodAnimation(foodImageView,"FoodPreparing")
                }
            }
        }
            
        else if  result.order?.order_status == OrderStatus.completed.rawValue{
            itemCountLabel.text = "Your order is completed"
            orderLabel.text = "Order Completed"
            
            addRunningOrderFoodAnimation(foodImageView,"DriverApproved")
        }
    }
    
    
    // Dining Type
    func orderStatusForDinig(_ result : Cart){
        let orderStatus =  result.order?.order_status
        if let orderCount = result.order?.items?.count, orderCount > 0{
            switch orderStatus {
            case OrderStatus.pending.rawValue:
                itemCountLabel.text = "Please hold on!"
                orderLabel.text = "Waiting For Confirmation"
                addRunningOrderFoodAnimation(foodImageView,"WaitingForConfirmation")
                
                let items =  (result.order?.items)!
                
                for dict in items {
                    
                    if dict.decline_reason?.decline_item_code == 3 && dict.item_status == 5{
                        itemCountLabel.text = "Please add more items"
                        orderLabel.text = "Order Declined"
                    }
                        
                    else if  dict.item_status == 5 && dict.decline_reason?.decline_item_code  == 1{
                        itemCountLabel.text = "Please add more items"
                        orderLabel.text = "Order Declined"
                    }
                    else  if dict.decline_reason?.decline_item_code  == 2 && dict.item_status == 5{
                        orderLabel.text = "Some Items Declined"
                        itemCountLabel.text = "Please add more items"
                        
                    }
                    else if dict.item_status == 5 && dict.decline_reason?.decline_item_code ?? 0 == 0{
                        orderLabel.text = "Waiting For Confirmation"
                        itemCountLabel.text = "New items added!"
                    }
                }
                
            case OrderStatus.confirmed.rawValue:
                orderLabel.text = "Order Confirmed"
                itemCountLabel.text = "Order is confirmed and will be start preparing soon"
                
                addRunningOrderFoodAnimation(foodImageView,"FoodPreparing")
                
                
            case OrderStatus.inprogress.rawValue:
                let items =  (result.order?.items)!
                
                let filterItem = items.filter{$0.item_status == 3}
                let itemStatus2 = items.filter{$0.item_status == 2}
                
                
                if filterItem.count > 0{
                    orderLabel.text = "Preparing Order"
                    itemCountLabel.text = "\(itemStatus2.count) item in kitchen"
                }
                else{
                    orderLabel.text = "Preparing Order"
                    itemCountLabel.text = "\(itemStatus2.count)item in kitchen"
                    
                }
                addRunningOrderFoodAnimation(foodImageView,"FoodPreparing")
                
                
                
            case OrderStatus.moreItemAdded.rawValue:
                orderLabel.text = "Order Preparing"
                itemCountLabel.text = "\(result.order?.items?.count ?? 0) item in kitchen"
                addRunningOrderFoodAnimation(foodImageView,"FoodPreparing")
                
                break
                
            default:
                break
            }
            
        }
    }
    
    func partnerDeliveryOrderStatus(_  orderData : Order?){
        
        /* // Cancel order status
         if  orderCancel == OrderCancelBy.user{
         orderLabel.text = "Order cancelled"
         itemCountLabel.text = "Your order is cancelled"
         addRunningOrderFoodAnimation(foodImageView,"wfoc")
         return
         
         }
         else  if orderData?.bill?.paid == orderData?.bill?.bill_total && orderCancel == OrderCancelBy.MM{ // cancel by MM and amount paid
         
         orderLabel.text = "Order cancelled"
         itemCountLabel.text = "Order was cancelled by the restaurant"
         addRunningOrderFoodAnimation(foodImageView,"wfoc")
         return
         }
         else if  orderCancel == OrderCancelBy.MM{ // cancel by MM
         orderLabel.text = "Order cancelled"
         itemCountLabel.text = "Order was cancelled by the restaurant"
         addRunningOrderFoodAnimation(foodImageView,"wfoc")
         return
         }
         else{*/
        setOrderStatus(orderData)
        //   }
        
        
    }
    
    
    func setOrderStatus(_ orderData : Order?){
        
        if orderData?.order_status == OrderStatus.pending.rawValue{
            
            if Int(orderData?.bill?.balance ?? 0) <= 0{
                orderLabel.text = "Order Pending Confirmation"
                itemCountLabel.text = "Restaurant is confirming your order"
                addRunningOrderFoodAnimation(foodImageView,"wfoc")
            }
            else {
                orderLabel.text = "Waiting For Payment"
                itemCountLabel.text = "Please pay to send your order to kitchen"
                addRunningOrderFoodAnimation(foodImageView,"payment_waiting")
            }
        }
        else if orderData?.order_status == OrderStatus.confirmed.rawValue || orderData?.order_status == OrderStatus.inprogress.rawValue{
            
            if orderData?.delivery?.del_order?.status == "new" || orderData?.delivery?.del_order?.status == "available"{
                orderLabel.text = "Delicious Food Is Being Prepared"
                itemCountLabel.text = "Restaurant is preparing your food"
                addRunningOrderFoodAnimation(foodImageView,"fp")
            }
            else if orderData?.delivery?.del_order?.status == "active" {
                
                if orderData?.delivery?.del_delivery?.status == "planned" {
                    orderLabel.text = "Delicious Food Is Being Prepared"
                    itemCountLabel.text = "Restaurant is preparing your food"
                    addRunningOrderFoodAnimation(foodImageView,"fp")
                }
                else if orderData?.delivery?.del_delivery?.status == "courier_assigned" {
                    orderLabel.text = "Driver Has Been Assigned"
                    itemCountLabel.text = "Restaurant is preparing your food"
                    addRunningOrderFoodAnimation(foodImageView,"driver_assigned")
                }
                else if orderData?.delivery?.del_delivery?.status == "courier_departed" {
                    orderLabel.text = "Driver Is On The Way To Restaurant"
                    itemCountLabel.text = "Restaurant is preparing your food"
                    addRunningOrderFoodAnimation(foodImageView,"dowr")
                }
                    
                else if orderData?.delivery?.del_delivery?.status == "parcel_picked_up" {
                    orderLabel.text = "Driver Is On His Way To Drop Your Order"
                    itemCountLabel.text = "Delicious food is on the way"
                    addRunningOrderFoodAnimation(foodImageView,"driver_going_for_delivery")
                }
                else if orderData?.delivery?.del_delivery?.status == "courier_arrived" {
                    orderLabel.text = "Your Order Has Been Arrived"
                    itemCountLabel.text = "Driver is waiting at your doorstep"
                    addRunningOrderFoodAnimation(foodImageView,"driver_waiting_at_home")
                }
                else if orderData?.delivery?.del_delivery?.status == "active" {
                    orderLabel.text = "Driver Is On His Way To Drop Your Order"
                    itemCountLabel.text = "Delicious food is on the way"
                    addRunningOrderFoodAnimation(foodImageView,"driver_going_for_delivery")
                }
                else if orderData?.delivery?.del_delivery?.status == "finished" {
                    orderLabel.text = "Your Order Has Been Delivered"
                    itemCountLabel.text = "Enjoy your meal"
                    addRunningOrderFoodAnimation(foodImageView,"wfoc")
                }
                else if orderData?.delivery?.del_delivery?.status == "canceled" {
                    
                }
                else if orderData?.delivery?.del_delivery?.status == "failed" {
                }
                else {
                    orderLabel.text = "Delicious Food Is Being Prepared"
                    itemCountLabel.text = "Restaurant is preparing your food"
                    addRunningOrderFoodAnimation(foodImageView,"fp")
                }
            }
            else         if orderData?.order_status == OrderStatus.completed.rawValue{
                orderLabel.text = "Your Order Has Been Delivered"
                itemCountLabel.text = "Enjoy your meal"
                addRunningOrderFoodAnimation(foodImageView,"wfoc")
            }
        }
        
    }
}
