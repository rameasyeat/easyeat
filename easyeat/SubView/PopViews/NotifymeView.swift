//
//  NotifymeView.swift
//  easyeat
//
//  Created by Ramniwas Patidar on 22/07/20.
//  Copyright © 2020 EasyEat. All rights reserved.
//

import Foundation

class NotifymeView : VIewShadow{
    
    @IBOutlet var contentView: VIewShadow!
    @IBOutlet weak var soonNotifyView: UIView!
    
    @IBOutlet weak var notifyButton: CustomButton!
    
    @IBOutlet weak var imgView: UIImageView!
    override init(frame: CGRect) {
       super.init(frame: frame)
       commonInit()
   }
   
   required init(coder aDecoder: NSCoder) {
       super.init(coder: aDecoder)
       commonInit()
   }
   

    @IBAction func notifyButtonAction(_ sender: Any) {
        soonNotifyView.isHidden = false
        notifyButton.isHidden = false

    }
    internal override func commonInit(){
       Bundle.main.loadNibNamed("NotifymeView", owner: self, options: nil)
       addSubview(contentView)
       contentView.frame = self.bounds
       contentView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
   }
    
   
   
}
