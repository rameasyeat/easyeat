//
//  ViewBottomPop.swift
//  easyeat
//
//  Created by Ramniwas on 02/04/20.
//  Copyright © 2020 EasyEat. All rights reserved.
//

import Foundation
import UIKit

protocol ViewCartdelegate {
    func viewCart()
}
class CardItemPopView : VIewShadow{
   
    
    @IBOutlet var contentView: VIewShadow!
    @IBOutlet weak var itemCountLabel: CustomLabel!
    @IBOutlet weak var currencyLabel: CustomLabel!
    @IBOutlet weak var viewCardButton: UIButton!
    @IBOutlet weak var viewCartLabel: CustomLabel!
    var viewCartDelegate: ViewCartdelegate?

    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    

    internal override func commonInit(){
        Bundle.main.loadNibNamed("CardItemPopView", owner: self, options: nil)
        addSubview(contentView)
        contentView.frame = self.bounds
        contentView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
    }
    
    @IBAction func viewCardButtonAction(_ sender: Any) {
        viewCartDelegate?.viewCart()
    }
}
