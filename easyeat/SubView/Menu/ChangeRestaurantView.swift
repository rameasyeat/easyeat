//
//  RestaurantType.swift
//  easyeat
//
//  Created by Ramniwas on 15/05/20.
//  Copyright © 2020 EasyEat. All rights reserved.
//

import Foundation

protocol RestaurantTypeDelegate {
    func selectedRestaurant(restaurantType : RestaurentType, viewHeight : Int)
}
class ChangeRestaurantView : UIView{
    @IBOutlet var contentView: UIView!
    @IBOutlet weak var orderTypeLabel: CustomLabel!
    @IBOutlet weak var changeButton: UIButton!
    @IBOutlet weak var deliveryButton: CustomButton!
    @IBOutlet weak var buttonView: UIView!
    @IBOutlet weak var dineinButton: CustomButton!
    @IBOutlet weak var orderTypeImage: UIImageView!
    
    var viewHeight = 50
    var restaurantType : RestaurentType?
    
    var restaurantTypeDelegate : RestaurantTypeDelegate?
    
   /* var isDining : Bool = true {
        didSet{
            if isDining == true{
                deliveryButton.setImage(#imageLiteral(resourceName: "Radio"), for: .normal)
                dineinButton.setImage(#imageLiteral(resourceName: "RadioSelected"), for: .normal)
                orderTypeLabel.text = kDining

            }
            else{
                deliveryButton.setImage(#imageLiteral(resourceName: "RadioSelected"), for: .normal)
                dineinButton.setImage(#imageLiteral(resourceName: "Radio"), for: .normal)
                orderTypeLabel.text = kDelivery

            }
            viewHeight = -50
            changeButton.isHidden = false
            buttonView.isHidden = true
        }
    }*/
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        commonInit()
    }
    
    fileprivate func commonInit(){
        Bundle.main.loadNibNamed("ChangeRestaurantView", owner: self, options: nil)
        addSubview(contentView)
        contentView.frame = self.bounds
        contentView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
    }
    
    @IBAction func changeButtonAction(_ sender: Any) {
//        viewHeight = 50
//        changeButton.isHidden = true
//        buttonView.isHidden = false

        restaurantTypeDelegate?.selectedRestaurant(restaurantType: restaurantType!, viewHeight: 0)
    }
   /* @IBAction func deliveryButtonAction(_ sender: Any) {
        isDining = false
        restaurantTypeDelegate?.selectedRestaurant(restaurantType: .delivery, viewHeight: viewHeight)
       }
    @IBAction func dineinButtonAction(_ sender: Any) {
        isDining = true
        restaurantTypeDelegate?.selectedRestaurant(restaurantType: .dining, viewHeight: viewHeight)
        }*/
    
    func setDefaultOrderType(_ type : priceByOrderType){
        buttonView.isHidden = true
        if type ==  .dining{
            orderTypeLabel.text = kDining
            dineinButton.setImage(#imageLiteral(resourceName: "RadioSelected"), for: .normal)
            orderTypeImage.image =  #imageLiteral(resourceName: "dining-room")

            
        }
        else if type ==  .delivery{
                orderTypeLabel.text = kDelivery
                dineinButton.setImage(#imageLiteral(resourceName: "RadioSelected"), for: .normal)
            orderTypeImage.image =  #imageLiteral(resourceName: "Delivery")

            }
        else{
            orderTypeImage.image =  #imageLiteral(resourceName: "pickup")
            orderTypeLabel.text = kTakeAway
            deliveryButton.setImage(#imageLiteral(resourceName: "RadioSelected"), for: .normal)
        }
    }
    
   
    
}
