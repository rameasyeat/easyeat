//
//  RestaurantHeaderView.swift
//  easyeat
//
//  Created by Ramniwas on 31/03/20.
//  Copyright © 2020 EasyEat. All rights reserved.
//

import Foundation
import UIKit
import SDWebImage

class RestaurantHeaderView : UIView{
    @IBOutlet var contentView: UIView!
    @IBOutlet weak var restaurantImage: UIImageView!
    @IBOutlet weak var restaurantName: CustomLabel!
    @IBOutlet weak var noPortLabel: CustomLabel!
    
    @IBOutlet weak var ratingLabel: CustomLabel!
    @IBOutlet weak var noParkImageView: UIImageView!
    @IBOutlet weak var locationLabel: CustomLabel!
    @IBOutlet weak var reviewCountLabel: UILabel!
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        commonInit()
    }
    
    fileprivate func commonInit(){
        Bundle.main.loadNibNamed("RestaurantHeaderView", owner: self, options: nil)
        addSubview(contentView)
        contentView.frame = self.bounds
        contentView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
    }
    
    func setRestaurantData(_ dict : RestaurantData){
        
        restaurantImage?.sd_setImage(with:URL(string: "\(KImagesBaseUrl)\(kRestFolderName)\(dict.logo ?? "")"), placeholderImage:UIImage(named: "header") , options: .progressiveDownload, completed: nil)
        restaurantName.text = dict.name?.capitalized
        
        var location = "\(dict.area ?? "")" + "\(dict.city ?? "")"
        if (dict.country != ""){
            location +=  ", \(dict.country ?? "")"
        }
        
        locationLabel.text = location.removeWhiteSpace().capitalizingFirstLetter()
        
        if dict.serviceable?.is_serviceable == 0 && dict.status == 1{
            noPortLabel.text = "Open next at \(dict.serviceable?.next_serviceable_time_string ?? "") \(dict.serviceable?.next_serviceable_day_string ?? "")".capitalizingFirstLetter()
            noPortLabel.textColor = hexStringToUIColor(kred)
        }
        else  if  dict.status == 2{
            noPortLabel.text = "Currently not accepting orders"
            noPortLabel.textColor = hexStringToUIColor(kred)
        }
        else {
            noPortLabel.text = ""
        }
    }
    
}
