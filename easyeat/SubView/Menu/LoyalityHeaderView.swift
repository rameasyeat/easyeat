
//
//  LoyalityHeaderView.swift
//  easyeat
//
//  Created by Ramniwas on 31/03/20.
//  Copyright © 2020 EasyEat. All rights reserved.
//

import Foundation
import UIKit

class LoyalityHeaderView : UIView{
    @IBOutlet var contentView: UIView!

    @IBOutlet weak var headerLabel: CustomLabel!
    @IBOutlet weak var descriptionLabel: CustomLabel!
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        commonInit()
    }
    
    fileprivate func commonInit(){
        Bundle.main.loadNibNamed("LoyalityHeaderView", owner: self, options: nil)
        addSubview(contentView)
        contentView.frame = self.bounds
        contentView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
    }
    
    func setRestaurantName(_ restaurantName : String){
        
        headerLabel.text = "Welcome To \(restaurantName) Loyalty Program".capitalized
        descriptionLabel.text = "Get exciting rewards on your frequent visits to \(restaurantName)".capitalizingFirstLetter()
        
    }
    
}
