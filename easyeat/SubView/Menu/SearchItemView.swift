//
//  SearchItemView.swift
//  easyeat
//
//  Created by Ramniwas on 02/04/20.
//  Copyright © 2020 EasyEat. All rights reserved.
//

import Foundation
import UIKit

protocol SearchItemDelegate {
    func applySearch(_ search : String)
    func applyFilter(_ isFilter : Bool)
    func vegToggleSwitch(_ isSwitchOn : Bool)
    
}
class SearchItemView : UIView{
    
    @IBOutlet var contentView: UIView!
    @IBOutlet var vegLabel: CustomLabel!
    @IBOutlet var searchTextField: CustomTextField!
    @IBOutlet var switchToggle: UISwitch!
    @IBOutlet var viewBG: VIewShadow!
    @IBOutlet var search: CustomButton!
    @IBOutlet var filterButton: CustomButton!
    var searchDelegate : SearchItemDelegate?
    @IBOutlet weak var filterWidth: NSLayoutConstraint!
    
    var isSearching : Bool = false {
        didSet{
            if isSearching {
                viewBG.isHidden = false
                switchToggle.isHidden = true
                vegLabel.isHidden = true
                searchTextField.becomeFirstResponder()
            }
            else {
                viewBG.isHidden = true
                switchToggle.isHidden = false
                vegLabel.isHidden = false
                searchTextField.resignFirstResponder()

            }
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        commonInit()
    }
    
    fileprivate func commonInit(){
        Bundle.main.loadNibNamed("SearchItemView", owner: self, options: nil)
        addSubview(contentView)
        contentView.frame = self.bounds
        contentView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        searchTextField.delegate = self
    }
    
    func checkQuickService(_ isQuickService : Bool){
        if isQuickService ==  true{
            filterWidth.constant = 0
        }
    }
    
    @IBAction func SwitchToggleAction(_ sender: Any) {
        searchDelegate?.vegToggleSwitch(switchToggle.isOn)
    }
    
    @IBAction func searchButtonAction(_ sender: Any) {
        isSearching = !isSearching
    }
    
    @IBAction func filterButtonAction(_ sender: Any) {
        searchDelegate?.applyFilter(true)
        
    }
}

extension SearchItemView: UITextFieldDelegate {
        func textFieldDidBeginEditing(_ textField: UITextField) {

    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        searchTextField.resignFirstResponder()
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
    {
        let str = (textField.text as NSString?)?.replacingCharacters(in: range, with: string)
        searchDelegate?.applySearch(str ?? "")

        return true
    }
}
