//
//  HeaderOrderTable.swift
//  easyeat
//
//  Created by Ramniwas on 08/04/20.
//  Copyright © 2020 EasyEat. All rights reserved.
//

import Foundation
import UIKit

class HeaderOrderTable : UIView{
    @IBOutlet var contentView: UIView!
    @IBOutlet weak var headerLabel: CustomLabel!
    
    @IBOutlet weak var etaLabel: CustomLabel!
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        commonInit()
    }
    
    fileprivate func commonInit(){
        Bundle.main.loadNibNamed("HeaderOrderTable", owner: self, options: nil)
        addSubview(contentView)
        contentView.frame = self.bounds
        contentView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
    }
    
}
