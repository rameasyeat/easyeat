//
//  DeliveryPartnerOrderItemHeader.swift
//  easyeat
//
//  Created by Ramniwas on 19/05/20.
//  Copyright © 2020 EasyEat. All rights reserved.
//

import Foundation

class DeliveryPartnerOrderItemHeader : UIView{
    @IBOutlet var contentView: UIView!
    @IBOutlet weak var restaurantName: CustomLabel!
    @IBOutlet weak var priceLabel: CustomLabel!
    @IBOutlet weak var viewDetailButton: CustomButton!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        commonInit()
    }
    
    fileprivate func commonInit(){
        Bundle.main.loadNibNamed("DeliveryPartnerOrderItemHeader", owner: self, options: nil)
        addSubview(contentView)
        contentView.frame = self.bounds
        contentView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
    }
    
    func setHeaderData(_ amount : Double, restName : String,_ isShowDetails : Bool){
        
        restaurantName.text = restName
        let totalAmount = "\(CurrentUserInfo.currency ?? "") \(amount)"
        Attributed.setText(priceLabel, "Total: \(totalAmount)", "#000000", [totalAmount],getBoldFont(CGFloat(AppFont.size16.rawValue))!)
        
        setBGViewColor(isShowDetails)
        restaurantName.isHidden = false
        priceLabel.isHidden = false
        viewDetailButton.isHidden = true
    }
    
    func setFooter(_ isShowDetails : Bool){
        setBGViewColor(isShowDetails)
        restaurantName.isHidden = true
        priceLabel.isHidden = true
        viewDetailButton.isHidden = false
    }
    
    fileprivate func setBGViewColor(_ isShowDetails : Bool){
        if isShowDetails == false {
            contentView.backgroundColor = hexStringToUIColor(kMediumLightGray)
        }
        else {
        contentView.backgroundColor = hexStringToUIColor(kwhite)
        }
    }
    
}
