//
//  DeclinedItemHeader.swift
//  easyeat
//
//  Created by Ramniwas on 09/04/20.
//  Copyright © 2020 EasyEat. All rights reserved.
//

import Foundation
import UIKit


class  DeclinedItemHeader: UIView{
    @IBOutlet var contentView: UIView!
    @IBOutlet weak var headerLabel: CustomLabel!
    @IBOutlet weak var descriptionLabel: CustomLabel!
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        commonInit()
    }
    
    fileprivate func commonInit(){
        Bundle.main.loadNibNamed("DeclinedItemHeader", owner: self, options: nil)
        addSubview(contentView)
        contentView.frame = self.bounds
        contentView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
    }
    
    func setUIData(_ index : Int,_ available : Int,_ unAvailable : Int, isCancelOrder : Bool,_ orderCancelBy : OrderCancelBy,_ toPay : Double,_ order_type : Int?){
        
        if orderCancelBy == OrderCancelBy.MM && (order_type == priceByOrderType.delivery.rawValue || order_type == priceByOrderType.pickup.rawValue)
        {
            paidCancelOrderMessage(toPay)
        }
        else if isCancelOrder == true{
            headerLabel.text = "Do you want to cancel your order?"
            descriptionLabel.text = ""
        }
        else {
            
            if orderCancelBy == OrderCancelBy.MM && order_type == priceByOrderType.dining.rawValue && toPay > 0
            {
                paidCancelOrderMessage(toPay)
           
            }else if index  == 0 {
                headerLabel.text = "\(unAvailable) Items Unavailable"
                descriptionLabel.text = "All items unavailable at this moment"
            }
            else if index  == 1 {
                headerLabel.text = "\(available) Item available"
                descriptionLabel.text =  "Only \(available) item available"
            }
        }
    }
    
    
    fileprivate func paidCancelOrderMessage(_ toPay : Double){
        if toPay > 0{
            headerLabel.text = " Sorry your order was cancelled by the Restaurant.We have initiated a refund for your order. Your money will be refunded within 2-7 days."
            descriptionLabel.text = ""
            
        }
        else {
            headerLabel.text = "Sorry your order was cancelled by the Restaurant."
            descriptionLabel.text = ""
        }
    }
}
