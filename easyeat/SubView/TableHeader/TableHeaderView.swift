//
//  TableHeaderView.swift
//  easyeat
//
//  Created by Ramniwas on 01/04/20.
//  Copyright © 2020 EasyEat. All rights reserved.
//

import Foundation
import UIKit

protocol TableHeaderDelegate {
    func getHeaderClick(_ index : Int)
}
class TableHeaderView : UIView{
    @IBOutlet var contentView: UIView!
    @IBOutlet weak var headerLabel: CustomLabel!
    @IBOutlet weak var arrowImageView: UIImageView!
    @IBOutlet weak var headerButton: UIButton!
    @IBOutlet weak var lineView: UIView!
    @IBOutlet weak var imgArrowWidth: NSLayoutConstraint!
    
    var tableHeaderDelegate : TableHeaderDelegate?

    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        commonInit()
    }
    
    fileprivate func commonInit(){
        Bundle.main.loadNibNamed("TableHeaderView", owner: self, options: nil)
        addSubview(contentView)
        contentView.frame = self.bounds
        contentView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
    }
    
    func animateImage(_ isClose : Bool ){
        
        if isClose{
            UIView.animate(withDuration: 0) {
                self.arrowImageView.transform = CGAffineTransform(rotationAngle: (3 * .pi/2))
               // self.lineView.isHidden = false
            }
        }
        else {
            UIView.animate(withDuration: 0) {
                self.arrowImageView.transform = CGAffineTransform.identity
              //  self.lineView.isHidden = true

            }
        }
    }
    
    
    @IBAction func headerClickAction(_ sender: UIButton) {
        tableHeaderDelegate?.getHeaderClick(sender.tag)
    }
}
