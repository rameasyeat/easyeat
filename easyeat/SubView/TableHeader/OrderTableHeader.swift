//
//  OrderTableHeader.swift
//  easyeat
//
//  Created by Ramniwas on 08/04/20.
//  Copyright © 2020 EasyEat. All rights reserved.
//

import Foundation
import UIKit
import CountdownLabel

class OrderTableHeader : UIView{
    @IBOutlet var contentView: UIView!
    @IBOutlet weak var headerLabel: CustomLabel!
    @IBOutlet weak var etaLabel: CountdownLabel!
    @IBOutlet weak var lineView: UIView!
    
    var orderStatusHandler : ((String)-> Void)!
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        commonInit()
    }
    
    
    
    
    fileprivate func commonInit(){
        Bundle.main.loadNibNamed("OrderTableHeader", owner: self, options: nil)
        addSubview(contentView)
        contentView.frame = self.bounds
        contentView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
    }
    
    func startTimer(_ kitchentime: TimeInterval){
        
        etaLabel.setCountDownTime(minutes:kitchentime)
        etaLabel.timeFormat = "mm:ss"
        etaLabel.countdownAttributedText = CountdownAttributedText(text: "ETA HERE", replacement: "HERE")
        etaLabel.start()
    }
    
    fileprivate func setTimer(_ remainingTime : Int){
        
        etaLabel.isHidden  = false
        if remainingTime <  60 {
            etaLabel.text = "less than min"
        }else {
            startTimer(TimeInterval(remainingTime))
        }
    }
    
    func setOrderStatus(_ sectionIndex : Int,_ orderCencel : OrderCancelBy,_ orderDict : OrderData,_ restaurantDict : RestaurantData,_ handler : @escaping (String)-> Void){
        orderStatusHandler = handler
        
        if orderCencel == OrderCancelBy.MM{
            
            lineView.isHidden = true
            headerLabel.isHidden = false
            headerLabel.text = "Order was cancelled by the restaurant"
            handler("Order Cancelled")
            return
            
        }
        else if orderCencel == OrderCancelBy.user{
            lineView.isHidden = true
            headerLabel.isHidden = false
            headerLabel.text = "You cancelled the order"
            
            handler("Order Cancelled")
            return
        }
        else{
            updateOrderStatus(sectionIndex,orderDict,restaurantDict)
        }
    }
    
    
    func updateOrderStatus(_ sectionIndex : Int,_ orderDict : OrderData,_ restaurantDict : RestaurantData){
        
        if sectionIndex == OrderSectionType.item.rawValue{
            lineView.isHidden = true
            headerLabel.isHidden = false
        }
        else {
            lineView.isHidden = false
            headerLabel.isHidden = true
        }
        
        guard let currentTime = Date().toMillis() else { return }
        
        etaLabel.isHidden  = true
        
        if orderDict.order_type == OrderType.delivery.rawValue || orderDict.order_type == OrderType.pickup.rawValue{ //order type 1
            
            let kitchekPickedTime = Int(orderDict.orderItems?[sectionIndex].kitchen_picked_at ?? "0") ?? 0
            
            let diff = currentTime - kitchekPickedTime
            var remainingTime = (Int(orderDict.orderItems?[sectionIndex].kitchen_time_to_complete ?? 0)*60) - (diff)
            
            let restaurantPrepareTime = (restaurantDict.delivery_time ?? 0) * 60
            remainingTime += restaurantPrepareTime
            orderDeliveryStatus(sectionIndex, remainingTime: remainingTime, orderDict)
        }
            
        else {// order type 0
            
            let filterItems = orderDict.orderItems?.filter{$0.item_status == 2}
            
            if filterItems?.count ?? 0 > 0{
                let kitchekPickedTime = Int(filterItems?[0].kitchen_picked_at ?? "0") ?? 0
                
                let diff = currentTime - kitchekPickedTime
                let remainingTime = (Int(orderDict.orderItems?[sectionIndex].kitchen_time_to_complete ?? 0)*60) - (diff)
                
                diningorderStatus(filterItems?[0].item_status ?? 0, remainingTime: remainingTime, orderDict)
            }
            else {
                diningorderStatus(orderDict.orderItems?[0].item_status ?? 0, remainingTime: 0, orderDict)
            }
        }
    }
    
    
    
    func orderDeliveryStatus(_ sectionIndex : Int, remainingTime : Int,_ orderDict :OrderData){
        
        if orderDict.order_status == OrderStatus.pending.rawValue{
            
            if(orderDict.orderItems?.count == 0){
             headerLabel.text = "Order were declined by manager"
             orderStatusHandler("Order Declined")
             }
//             else if(cancelled_item_count > 0){
//             headerLabel.text = "Order will be confirmed by the manager. Some Items were declined"
//             orderStatusHandler("Order pending Confirmation")
//             }
//             else {*/
            
           else if(Int(orderDict.bill?.balance ?? 0) > 0){
                headerLabel.text = "You will need to pay before restaurant can start preparing your order"
                orderStatusHandler("Payment Pending")
            }
            else{
                headerLabel.text = "Order will be confirmed by the manager"
                orderStatusHandler("Order Pending Confirmation")
            }
            
        }
        else if orderDict.order_status == OrderStatus.confirmed.rawValue{
            headerLabel.text = "Your order is confirmed and will be start preparing soon"
            orderStatusHandler("Order Placed")
        }
        else if orderDict.order_status == OrderStatus.inprogress.rawValue{
            
            let item = orderDict.orderItems?[sectionIndex]
            if(orderDict.address == "pickup take away"){
                
                setTimer(remainingTime)
                
                //                if(window.served_item_count > 0){
                //                    order_status_title_div.innerHTML= 'Order Picked';
                //                    order_status_div.innerHTML = 'Order has been picked';
                //                }
                //                else
                if(item?.item_status == ItemStatus.prepared.rawValue){
                    headerLabel.text = "Order has been prepared. Ready to be picked"
                    orderStatusHandler("Order Prepared")
                }
                else{
                    headerLabel.text = "Your order is being prepared"
                    orderStatusHandler("Order Placed")
                    
                }
            }
            else{
                setTimer(remainingTime)
                
                //                if(window.served_item_count > 0){
                //                    order_status_title_div.innerHTML= 'Order Delivered';
                //                    order_status_div.innerHTML = 'Your order has been delivered';
                //                }
                //                else
                if(item?.item_status == ItemStatus.prepared.rawValue){
                    headerLabel.text = "Your order is on the way"
                    orderStatusHandler("Order Dispatched")
                }
                else{
                    headerLabel.text = "Your order is being prepared"
                    orderStatusHandler("Order Placed")
                }
            }
        }
            
        else if orderDict.order_status == OrderStatus.completed.rawValue{
            headerLabel.text = "Your order is completed"
            orderStatusHandler("Order Completed")
        }
        else if orderDict.order_status == OrderStatus.moreItemAdded.rawValue{
            
            /* if(cancelled_item_count === item_length){
             order_status_title_div.innerHTML= 'Order Declined';
             order_status_div.innerHTML = 'Order were declined by manager';
             }
             else if(cancelled_item_count > 0){
             order_status_title_div.innerHTML= 'Order pending Confirmation';
             order_status_div.innerHTML = 'Order will be confirmed by the manager. Some Items were declined';
             }
             else{*/
            headerLabel.text = "Order Pending Confirmation"
            orderStatusHandler("Waiting to confirmation for new items")
            //  }
            
        }
     
    }
    
    func diningorderStatus(_ item_status : Int, remainingTime : Int,_ orderDict :OrderData){
        
        if orderDict.order_status == OrderStatus.pending.rawValue{
            
            let items  = orderDict.orderItems
            
            let cancelItem = items?.filter{$0.item_status == 5}
            let declinedItems = items?.filter{$0.item_status == 0}

            if cancelItem?.count ?? 0 > 0 {
                headerLabel.text =  "Order were declined by manager"
                orderStatusHandler("Order Declined")
            }
            else   if declinedItems?.count ?? 0 > 0 {
                headerLabel.text =  "Order will be confirmed by the manager. Some items were declined"
                orderStatusHandler("Order Pending Confirmation")
            }
            else{
                headerLabel.text =  "Order will be confirmed by the manager"
                orderStatusHandler("Order Pending Confirmation")
            }
            
            
        }
        else  if orderDict.order_status == OrderStatus.confirmed.rawValue{
            headerLabel.text =  "Your order is confirmed and will be start preparing soon"
            orderStatusHandler("Order Placed")
        }
            
        else  if orderDict.order_status == OrderStatus.inprogress.rawValue{
            setTimer(remainingTime)
            
            headerLabel.text =  "Your order is being prepared"
            orderStatusHandler("Order Placed")
        }
            
        else  if orderDict.order_status == OrderStatus.completed.rawValue{
            headerLabel.text =  "Your order is completed"
            orderStatusHandler("Order Completed")
        }
            
        else  if orderDict.order_status == OrderStatus.moreItemAdded.rawValue{
            headerLabel.text =  "'Waiting to confirmation for new items"
            orderStatusHandler("Order Pending Confirmation")
        }
        
        
    }
    
}

extension Date {
    func toMillis() -> Int! {
        return Int(self.timeIntervalSince1970 )
    }
}
