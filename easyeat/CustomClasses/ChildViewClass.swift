//
//  ChildViewClass.swift
//  easyeat
//
//  Created by Ramniwas on 25/03/20.
//  Copyright © 2020 EasyEat. All rights reserved.
//

import Foundation
import UIKit

enum AllergiesType {
    case add
    case no
}

internal func noAllergies(_ selfView : UIViewController,_ allergiesType : AllergiesType,_ dict :  UserData,_ isEditProfile : Bool,_ userAllergiesDict : UserAllergicItems){
    
    removePreviousView(selfView)
       let controller = NoAllergiesViewController.instantiate()
    controller.viewModel.delegate = selfView as? AllergiesDelegate
    controller.viewModel.allgergieType = allergiesType
    controller.viewModel.userDictData = dict
    controller.viewModel.isEditProfile = isEditProfile
    controller.viewModel.userAllergiesDict = userAllergiesDict

       controller.view.frame = selfView.view.bounds;
       controller.willMove(toParent: selfView)
       selfView.view.addSubview(controller.view)
       selfView.addChild(controller)
       controller.didMove(toParent: selfView)
   }

internal func addCategory(_ controller : UIViewController,_ allergiesType : AllergiesType ){
    
    controller.view.frame = (RootViewController.controller?.view.bounds)!;
    controller.willMove(toParent: RootViewController.controller)
    RootViewController.controller?.view.addSubview(controller.view)
    RootViewController.controller?.addChild(controller)
    controller.didMove(toParent: RootViewController.controller)
}

internal func addRootView(_ controller : UIViewController){
    controller.view.frame = (RootViewController.controller?.view.bounds)!;
    controller.willMove(toParent: RootViewController.controller)
    RootViewController.controller?.view.addSubview(controller.view)
    RootViewController.controller?.addChild(controller)
    controller.didMove(toParent: RootViewController.controller)
}

func removeFromSuperView(_ controller : UIViewController){
    controller.willMove(toParent: nil)
    controller.view.removeFromSuperview()
    controller.removeFromParent()
}

func removePreviousView(_ controller : UIViewController){
    
    let appDlegate = UIApplication.shared.delegate as? AppDelegate
    

    if (appDlegate?.window?.rootViewController?.presentedViewController) != nil
    {
        
        guard let views = appDlegate?.window?.rootViewController?.presentedViewController else {return}
        for v in controller.navigationController?.viewControllers ?? [views] {
               if v is AddAllergiesViewController || v is NoAllergiesViewController {
                 removeFromSuperView(v)
               }
           }
        
    }
    else if (appDlegate?.window?.rootViewController?.children) != nil
    {
        
         guard let views = appDlegate?.window?.rootViewController?.children else {return}
        for v in views{
               if v is AddAllergiesViewController || v is NoAllergiesViewController {
                   removeFromSuperView(v)
               }
           }
    }
    
   
   

}
