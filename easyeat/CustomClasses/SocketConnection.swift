//
//  SocketConnection.swift
//  easyeat
//
//  Created by Ramniwas on 14/04/20.
//  Copyright © 2020 EasyEat. All rights reserved.
//

import Foundation
import CocoaMQTT



enum OrderCancelBy : Int{
    case none = 0
    case user
    case MM
}

enum MType : Int {
    case OID = 1
    case OCM
    case OCL
    case RELOAD
    
}

protocol MQTTDelegates {
    func updateQMTTResponce(_ dict : [String : Any])
}

final class  SocketConnection : CocoaMQTTDelegate{
    
    var mqtt : CocoaMQTT?
    var orderId : String?
    var delegates : MQTTDelegates?
    var websocket : CocoaMQTTWebSocket = {
        let websocket = CocoaMQTTWebSocket(uri: "/mqtt")
        return websocket
    }()
    
    
    
    func callMQTTSocket(_ order_id : String){
        
        let clientID = "easyeat-" + String(ProcessInfo().processIdentifier)
        orderId = order_id
        mqtt = CocoaMQTT(clientID: clientID, host: kMQTTHost, port: UInt16(kMQTTPort), socket: websocket)
        mqtt?.username = ""
        mqtt?.password = ""
        mqtt?.autoReconnect = true
//        mqtt?.keepAlive = 60
        mqtt?.delegate = self
        mqtt?.allowUntrustCACertificate = true
        mqtt?.enableSSL = true
        
        _ = mqtt?.connect()
        
     }
    
  
    
    func mqtt(_ mqtt: CocoaMQTT, didUnsubscribeTopic topic: String) {
        
    }
    
    // Optional ssl CocoaMQTTDelegate
    func mqtt(_ mqtt: CocoaMQTT, didReceive trust: SecTrust, completionHandler: @escaping (Bool) -> Void) {
        completionHandler(true)
    }
    
    func mqtt(_ mqtt: CocoaMQTT, didConnectAck ack: CocoaMQTTConnAck) {
        
        print(orderId as Any)
        if ack == .accept {
            mqtt.subscribe(orderId ?? "")

            print("accept")
        }
    }
    
    func mqtt(_ mqtt: CocoaMQTT, didStateChangeTo state: CocoaMQTTConnState) {
        print("new state: \(state)")
    }
    
    func mqtt(_ mqtt: CocoaMQTT, didPublishMessage message: CocoaMQTTMessage, id: UInt16) {
        print("message: \(String(describing: message.string?.description)), id: \(id)")
    }
    
    func mqtt(_ mqtt: CocoaMQTT, didPublishAck id: UInt16) {
        print("id: \(id)")
    }
    
    func mqtt(_ mqtt: CocoaMQTT, didReceiveMessage message: CocoaMQTTMessage, id: UInt16 ) {
        print("message: \(message.string?.description ?? ""), id: \(id)")
        
        guard let data = (message.string?.description.data(using: .utf8)) else {return}
        let json = try? JSONSerialization.jsonObject(with: data)
        delegates?.updateQMTTResponce((json as? [String : Any])!)
        
//        NotificationCenter.default.post(name: Notification.Name(kMQTTNotificationCenter), object: nil, userInfo: json as? [AnyHashable : Any])
    }
    
    func mqtt(_ mqtt: CocoaMQTT, didSubscribeTopics success: NSDictionary, failed: [String]) {
        print("subscribed: \(success), failed: \(failed)")
    }
    
    func mqtt(_ mqtt: CocoaMQTT, didUnsubscribeTopics topics: [String]) {
        print("topic: \(topics)")
    }
    
    func mqttDidPing(_ mqtt: CocoaMQTT) {
        
        print()
    }
    
    func mqttDidReceivePong(_ mqtt: CocoaMQTT) {
        print()
    }
    
    func mqttDidDisconnect(_ mqtt: CocoaMQTT, withError err: Error?) {
        print("\(err.debugDescription)")
    }
        
    
}

