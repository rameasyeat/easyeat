//
//  Userlocation.swift
//  easyeat
//
//  Created by Ramniwas Patidar on 30/04/20.
//  Copyright © 2020 Demo. All rights reserved.
//

import Foundation
import CoreLocation

class UserLocation : NSObject{
    static let sharedManager = UserLocation()
    
    let locationManager = CLLocationManager()
    var location: CLLocation?
    
    let geocoder = CLGeocoder()
    var placemark: CLPlacemark?
    
    var city: String?
    var country: String?
    var countryShortName: String?
    var lat : String?
    var lng : String?
    
    var locationHandler : ((String,String,CLLocationCoordinate2D,Bool) -> Void)!
    private override init() {
        
    }
    func startLocationManager(handler:@escaping (String,String,CLLocationCoordinate2D,Bool) -> Void) {
        
        locationHandler = handler
        let authStatus = CLLocationManager.authorizationStatus()
        if authStatus == .notDetermined {
            locationManager.requestWhenInUseAuthorization()
        }
        
        if authStatus == .denied || authStatus == .restricted {
            print("Please enable location permissions in settings")
            
            var cord: CLLocationCoordinate2D = CLLocationCoordinate2D()
            cord.latitude = 0
            cord.longitude = 0
            self.locationHandler("","",cord,false)

            return
        }
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
            locationManager.requestLocation()
            locationManager.startUpdatingLocation()
        }
    }
    
    func stopLocationManager() {
        locationManager.stopUpdatingLocation()
        locationManager.delegate = nil
    }
}

extension UserLocation : CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("didFailwithError\(error)")
        print(error.localizedDescription)
        
        var cord: CLLocationCoordinate2D = CLLocationCoordinate2D()
        cord.latitude = 0
        cord.longitude = 0
        self.locationHandler("","",cord,false)
        
        stopLocationManager()
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let latestLocation = locations.last!
        
    
//        if location == nil || location!.horizontalAccuracy > latestLocation.horizontalAccuracy {
            
            location = latestLocation
            stopLocationManager()
            
            geocoder.reverseGeocodeLocation(location!) { (placemarks, error) in
                if let placemarks = placemarks{
                    let pm = placemarks as [CLPlacemark]
                    
                    if pm.count > 0 {
                        let pm = placemarks[0]
                        
                        var addressString : String = ""
                        var locality : String = ""
                        
                        if pm.thoroughfare != nil {
                            addressString = addressString + pm.thoroughfare! + ", "
                        }
                        if pm.subLocality != nil {
                            addressString = addressString + pm.subLocality! + ", "
                            locality = locality + pm.subLocality! + ", "
                        }
                        if pm.locality != nil {
                            addressString = addressString + pm.locality! + ", "
                            locality = locality  + pm.locality! 
                        }
                        
                        if let subAdministrativeArea = pm.subAdministrativeArea {
                            addressString = addressString + subAdministrativeArea + ", "
                        }
                        
                        if let administrativeArea = pm.administrativeArea {
                            addressString = addressString + administrativeArea + ", "
                        }
                        if pm.country != nil {
                            addressString = addressString + pm.country! + ", "
                        }
                        if pm.postalCode != nil {
                            addressString = addressString + pm.postalCode! + " "
                        }
                        self.locationHandler(addressString,locality,self.location!.coordinate, true)
                        
                    }
                }
          //  }
            
        }
    }
}

